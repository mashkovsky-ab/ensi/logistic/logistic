<?php

namespace App\Domain\External;

use App\Domain\External\Dto\Request\CalculatorRequestDto;
use App\Domain\External\Dto\Response\CalculatorResponseDto;

interface CalculatorInterface
{
    public function calculate(CalculatorRequestDto $requestDto): CalculatorResponseDto;
}
