<?php

namespace App\Domain\External\Actions\DeliveryOrder;

use App\Domain\External\Dto\Response\DeliveryOrdersStatusResponseDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Получить статус заказа(ов) на доставку у службы доставки
 * Class GetDeliveryOrderStatusAction
 * @package App\Domain\External\Actions\DeliveryOrder
 */
class GetDeliveryOrderStatusAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  array  $externalIds
     * @return DeliveryOrdersStatusResponseDto
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(int $deliveryServiceId, array $externalIds): DeliveryOrdersStatusResponseDto
    {
        $api = $this->getApi($deliveryServiceId);

        return $api->status($externalIds);
    }
}
