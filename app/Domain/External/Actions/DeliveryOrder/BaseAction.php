<?php

namespace App\Domain\External\Actions\DeliveryOrder;

use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\B2Cpl;
use App\Domain\External\Cdek;
use App\Domain\External\DeliveryOrderInterface;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Class BaseAction
 * @package App\Domain\External\Actions\DeliveryOrder
 */
abstract class BaseAction
{
    protected const API = [
        DeliveryService::SERVICE_B2CPL => B2Cpl\Api\DeliveryOrderApi::class,
        DeliveryService::SERVICE_CDEK => Cdek\Api\DeliveryOrderApi::class,
    ];

    /**
     * @param  int  $deliveryServiceId
     * @return DeliveryOrderInterface
     * @throws DeliveryServiceApiConnectorNotFound
     */
    protected function getApi(int $deliveryServiceId): DeliveryOrderInterface
    {
        if (isset(static::API[$deliveryServiceId])) {
            $apiClass = static::API[$deliveryServiceId];
            /** @var DeliveryOrderInterface $api */
            return resolve($apiClass);
        } else {
            throw new DeliveryServiceApiConnectorNotFound($deliveryServiceId);
        }
    }
}
