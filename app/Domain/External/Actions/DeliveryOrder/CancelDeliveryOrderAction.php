<?php

namespace App\Domain\External\Actions\DeliveryOrder;

use App\Domain\External\Dto\Response\DeliveryOrderCancelResponseDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Отменить заказ на доставку у службы доставки
 * Class CancelDeliveryOrderAction
 * @package App\Domain\External\Actions\DeliveryOrder
 */
class CancelDeliveryOrderAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  string  $externalId  - id заказа на доставку во внешней системе
     * @return DeliveryOrderCancelResponseDto
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(int $deliveryServiceId, string $externalId): DeliveryOrderCancelResponseDto
    {
        $api = $this->getApi($deliveryServiceId);

        return $api->cancel($externalId);
    }
}
