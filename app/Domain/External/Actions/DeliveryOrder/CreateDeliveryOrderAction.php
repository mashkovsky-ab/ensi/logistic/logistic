<?php

namespace App\Domain\External\Actions\DeliveryOrder;

use App\Domain\External\Dto\Request\DeliveryOrderRequestDto;
use App\Domain\External\Dto\Response\DeliveryOrderResponseDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Создать заказ на доставку у службы доставки
 * Class CreateDeliveryOrderAction
 * @package App\Domain\External\Actions\DeliveryOrder
 */
class CreateDeliveryOrderAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  DeliveryOrderRequestDto  $orderRequestDto
     * @return DeliveryOrderResponseDto
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(int $deliveryServiceId, DeliveryOrderRequestDto $orderRequestDto): DeliveryOrderResponseDto
    {
        $api = $this->getApi($deliveryServiceId);

        return $api->create($orderRequestDto);
    }
}
