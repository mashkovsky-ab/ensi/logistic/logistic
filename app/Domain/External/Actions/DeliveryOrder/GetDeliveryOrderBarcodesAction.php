<?php

namespace App\Domain\External\Actions\DeliveryOrder;

use App\Domain\External\Dto\Response\DeliveryOrderBarcodesResponseDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Получить штрихкоды для мест заказа на доставку у службы доставки
 * Class GetDeliveryOrderBarcodesAction
 * @package App\Domain\External\Actions\DeliveryOrder
 */
class GetDeliveryOrderBarcodesAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  string  $externalId
     * @param  array  $placesExternalId
     * @return DeliveryOrderBarcodesResponseDto
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(
        int $deliveryServiceId,
        string $externalId,
        array $placesExternalId = []
    ): DeliveryOrderBarcodesResponseDto {
        $api = $this->getApi($deliveryServiceId);

        return $api->barcodes($externalId, $placesExternalId);
    }
}
