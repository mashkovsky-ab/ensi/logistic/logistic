<?php

namespace App\Domain\External\Dto;

use Illuminate\Support\Carbon;

/**
 * Class AbstractAvailableTimeDto
 * @package App\Domain\External\Dto
 */
abstract class AbstractAvailableTimeDto
{
    /** @var string */
    public string $code;
    /** @var int */
    public int $from;
    /** @var int */
    public int $to;
    /** @var callable */
    public $callable;

    /**
     * AbstractAvailableTimeDto constructor.
     * @param  string  $code
     * @param  int  $from
     * @param  int  $to
     * @param  callable  $callable - функция проверки применимости интервала, возвращающая true или false
     */
    public function __construct(
        string $code,
        int $from,
        int $to,
        callable $callable
    ) {
        $this->code = $code;
        $this->from = $from;
        $this->to = $to;
        $this->callable = $callable;
    }

    /**
     * @param  Carbon  $date - день доставки
     * @param  string  $regionFiasId - ФИАС ID региона доставки
     * @param  array  $params - дополнительные параметры
     * @return self[]
     */
    abstract protected static function all(Carbon $date, string $regionFiasId, array $params = []): array;

    /**
     * @param  Carbon  $date - день доставки
     * @param  string  $regionFiasId - ФИАС ID региона доставки
     * @param  array  $params - дополнительные параметры
     * @return self[]
     */
    public static function available(Carbon $date, string $regionFiasId, array $params = []): array
    {
        return array_values(array_filter(static::all($date, $regionFiasId, $params), function (self $availableTimeDto) {
            $callable = $availableTimeDto->callable;

            return $callable();
        }));
    }
}
