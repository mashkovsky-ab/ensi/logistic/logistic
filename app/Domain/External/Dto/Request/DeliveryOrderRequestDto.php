<?php

namespace App\Domain\External\Dto\Request;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\External\Dto\Request\Part\DeliveryOrder\DeliveryOrderCostDto;
use App\Domain\External\Dto\Request\Part\DeliveryOrder\DeliveryOrderDto;
use App\Domain\External\Dto\Request\Part\DeliveryOrder\DeliveryOrderPlaceDto;
use App\Domain\External\Dto\Request\Part\DeliveryOrder\DeliveryOrderPlaceItemDto;
use App\Domain\External\Dto\Request\Part\DeliveryOrder\RecipientDto;
use App\Domain\External\Dto\Request\Part\DeliveryOrder\SenderDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Создание/обновление заказа на доставку
 * Class DeliveryOrderRequestDto
 * @package App\Domain\External\Dto\Request
 * @property int $delivery_service_id - id службы доставки
 * @property DeliveryOrderDto $order -  информация о заказе*
 * @property DeliveryOrderCostDto $cost - информация о стоимости заказа*
 * @property SenderDto $sender - информация об отправителе заказа*
 * @property RecipientDto $recipient - информация о получателе заказа
 * @property Collection|DeliveryOrderPlaceDto[] $places - информация о местах (коробках) заказа*
 */
class DeliveryOrderRequestDto extends Fluent
{
    /**
     * OrderRequestDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->order = new DeliveryOrderDto($this->order ?? []);
        $this->cost = new DeliveryOrderCostDto($this->cost ?? []);
        $this->sender = new SenderDto($this->sender ?? []);
        $this->recipient = new RecipientDto($this->recipient ?? []);

        $this->places = DeliveryOrderPlaceDto::collect($this->places ?? []);
    }

    public static function makeFromDeliveryOrder(DeliveryOrder $deliveryOrder): self
    {
        $deliveryOrderRequestDto = new self();

        $deliveryOrderDto = new DeliveryOrderDto();
        $deliveryOrderRequestDto->order = $deliveryOrderDto;
        $deliveryOrderDto->number = $deliveryOrder->number;
        $deliveryOrderDto->height = mm2cm($deliveryOrder->height);
        $deliveryOrderDto->length = mm2cm($deliveryOrder->length);
        $deliveryOrderDto->width = mm2cm($deliveryOrder->width);
        $deliveryOrderDto->weight = $deliveryOrder->weight;
        $deliveryOrderDto->shipment_method = $deliveryOrder->shipment_method;
        $deliveryOrderDto->delivery_method = $deliveryOrder->delivery_method;
        $deliveryOrderDto->tariff_id = $deliveryOrder->tariff_id;
        $deliveryOrderDto->delivery_date = $deliveryOrder->delivery_date?->format('d.m.Y');
        $deliveryOrderDto->point_in_id = $deliveryOrder->pointIn?->external_id;
        $deliveryOrderDto->point_out_id = $deliveryOrder->pointOut?->external_id;
        $deliveryOrderDto->shipment_time_start = $deliveryOrder->shipment_time_start?->format('H:i');
        $deliveryOrderDto->shipment_time_end = $deliveryOrder->shipment_time_end?->format('H:i');
        $deliveryOrderDto->delivery_time_start = $deliveryOrder->delivery_time_start?->format('H:i');
        $deliveryOrderDto->delivery_time_end = $deliveryOrder->delivery_time_end?->format('H:i');
        $deliveryOrderDto->delivery_time_code = $deliveryOrder->delivery_time_code;
        $deliveryOrderDto->description = $deliveryOrder->description;

        $deliveryOrderCostDto = new DeliveryOrderCostDto();
        $deliveryOrderRequestDto->cost = $deliveryOrderCostDto;
        $deliveryOrderCostDto->assessed_cost = penny2rub($deliveryOrder->assessed_cost);
        $deliveryOrderCostDto->delivery_cost = penny2rub($deliveryOrder->delivery_cost);
        $deliveryOrderCostDto->delivery_cost_vat = $deliveryOrder->delivery_cost_vat;
        $deliveryOrderCostDto->delivery_cost_pay = penny2rub($deliveryOrder->delivery_cost_pay);
        $deliveryOrderCostDto->cod_cost = $deliveryOrder->cod_cost;
        $deliveryOrderCostDto->is_delivery_payed_by_recipient = $deliveryOrder->is_delivery_payed_by_recipient;

        $senderDto = new SenderDto();
        $deliveryOrderRequestDto->sender = $senderDto;
        $senderDto->is_seller = $deliveryOrder->sender_is_seller;
        $senderDto->inn = $deliveryOrder->sender_inn;
        $senderAddress = $deliveryOrder->sender_address;
        $senderDto->address_string = $senderAddress?->address_string;
        $senderDto->post_index = $senderAddress?->post_index;
        $senderDto->country_code = $senderAddress?->country_code;
        $senderDto->region = $senderAddress?->region;
        $senderDto->area = $senderAddress?->area;
        $senderDto->city = $senderAddress?->city;
        $senderDto->city_guid = $senderAddress?->city_guid;
        $senderDto->street = $senderAddress?->street;
        $senderDto->house = $senderAddress?->house;
        $senderDto->block = $senderAddress?->block;
        $senderDto->flat = $senderAddress?->flat;
        $senderDto->company_name = $deliveryOrder->sender_company_name;
        $senderDto->contact_name = $deliveryOrder->sender_contact_name;
        $senderDto->email = $deliveryOrder->sender_email;
        $senderDto->phone = $deliveryOrder->sender_phone;
        $senderDto->comment = $deliveryOrder->sender_comment;

        $recipientDto = new RecipientDto();
        $deliveryOrderRequestDto->recipient = $recipientDto;
        $recipientAddress = $deliveryOrder->recipient_address;
        $recipientDto->address_string = $recipientAddress?->address_string;
        $recipientDto->post_index = $recipientAddress?->post_index;
        $recipientDto->country_code = $recipientAddress?->country_code;
        $recipientDto->region = $recipientAddress?->region;
        $recipientDto->area = $recipientAddress?->area;
        $recipientDto->city = $recipientAddress?->city;
        $recipientDto->city_guid = $recipientAddress?->city_guid;
        $recipientDto->street = $recipientAddress?->street;
        $recipientDto->house = $recipientAddress?->house;
        $recipientDto->block = $recipientAddress?->block;
        $recipientDto->flat = $recipientAddress?->flat;
        $recipientDto->company_name = $deliveryOrder->recipient_company_name;
        $recipientDto->contact_name = $deliveryOrder->recipient_contact_name;
        $recipientDto->email = $deliveryOrder->recipient_email;
        $recipientDto->phone = $deliveryOrder->recipient_phone;
        $recipientDto->comment = $deliveryOrder->recipient_comment;

        foreach ($deliveryOrder->places as $place) {
            $deliveryOrderPlaceDto = new DeliveryOrderPlaceDto();
            $deliveryOrderRequestDto->places->push($deliveryOrderPlaceDto);
            $deliveryOrderPlaceDto->number = $place->number;
            $deliveryOrderPlaceDto->code = $place->number;
            $deliveryOrderPlaceDto->height = $place->height;
            $deliveryOrderPlaceDto->length = $place->length;
            $deliveryOrderPlaceDto->width = $place->width;
            $deliveryOrderPlaceDto->weight = $place->weight;

            foreach ($place->items as $item) {
                $deliveryOrderPlaceItemDto = new DeliveryOrderPlaceItemDto();
                $deliveryOrderPlaceDto->items->push($deliveryOrderPlaceItemDto);
                $deliveryOrderPlaceItemDto->articul = $item->vendor_code;
                $deliveryOrderPlaceItemDto->name = $item->name;
                $deliveryOrderPlaceItemDto->quantity = $item->qty;
                $deliveryOrderPlaceItemDto->quantity_delivered = $item->qty_delivered;
                $deliveryOrderPlaceItemDto->height = mm2cm($item->height);
                $deliveryOrderPlaceItemDto->length = mm2cm($item->length);
                $deliveryOrderPlaceItemDto->width = mm2cm($item->width);
                $deliveryOrderPlaceItemDto->weight = $item->weight;
                $deliveryOrderPlaceItemDto->assessed_cost = penny2rub($item->assessed_cost);
                $deliveryOrderPlaceItemDto->cost = penny2rub($item->cost);
                $deliveryOrderPlaceItemDto->cost_vat = $item->cost_vat;
                $deliveryOrderPlaceItemDto->price = penny2rub($item->price);
                $deliveryOrderPlaceItemDto->barcode = $item->barcode;
            }
        }

        return $deliveryOrderRequestDto;
    }
}
