<?php

namespace App\Domain\External\Dto\Request\Part\CourierCall;

use App\Domain\External\Dto\Request\Part\AbstractAddressDto;

/**
 * Class SenderDto
 * @package App\Domain\External\Dto\Request\Part\CourierCall
 */
class SenderDto extends AbstractAddressDto
{
}
