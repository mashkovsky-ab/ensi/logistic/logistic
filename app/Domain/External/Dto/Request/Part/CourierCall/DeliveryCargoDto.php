<?php

namespace App\Domain\External\Dto\Request\Part\CourierCall;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryOrderDto
 * @package App\Domain\External\Dto\Request\Part\CourierCall
 *
 * @property string $date - дата забора*
 * @property string $time_code - код времени забора (должен быть заполнен, либо $timeStart и $timeEnd)
 * @property string $time_start - начальное время забора
 * @property string $time_end - конечное время забора
 * @property float $weight - вес всего заказа (в граммах)*
 * @property float $width - ширина всего заказа (в сантиметрах)*
 * @property float $height - высота всего заказа (в сантиметрах)*
 * @property float $length - длина заказа (в сантиметрах)*
 * @property array|string[] $order_ids - номера заказов из системы службы доставки,
 * которые планируются передать с этим курьером*
 */
class DeliveryCargoDto extends Fluent
{
}
