<?php

namespace App\Domain\External\Dto\Request\Part;

use Illuminate\Support\Fluent;

/**
 * Class AbstractAddressDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 *
 * @property string $address_string - полный адрес одной строкой*
 * @property string $post_index - почтовый индекс*
 * @property string $country_code - код страны в соответствии с ISO 3166-1 alpha-2
 * @property string $region - область или республика или край*
 * @property string $area - район
 * @property string $city - город или населенный пункт*
 * @property string $city_guid - ID города в базе ФИАС*
 * @property string $street - улица*
 * @property string $house - дом
 * @property string $block - строение/корпус
 * @property string $flat - офис/квартира
 * @property string $company_name - название компании
 * @property string $contact_name - ФИО контактного лица
 * @property string $email - контактный email
 * @property string $phone - контактный телефон
 * @property string $comment - комментарий
 */
class AbstractAddressDto extends Fluent
{
}
