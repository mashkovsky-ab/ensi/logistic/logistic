<?php

namespace App\Domain\External\Dto\Request\Part\DeliveryOrder;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Fluent;

/**
 * Class DeliveryOrderPlaceItemDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 *
 * @property string $articul - артикул товара
 * @property string $name - наименование товара*
 * @property int $quantity - кол-во товара*
 * @property int $quantity_delivered - заполняется только при частичной доставке и показывает сколько вложимых выкуплено
 * @property float $height - высота единицы товара в сантиметрах*
 * @property float $length - длина единицы товара в сантиметрах*
 * @property float $width - ширина единицы товара в сантиметрах*
 * @property float $weight - вес единицы товара в граммах*
 * @property int $assessed_cost - оценочная стоимость единицы товара в рублях
 * @property int $cost - стоимость единицы товара с учетом НДС в рублях*
 * @property float $cost_vat - процентная ставка НДС
 * @property int $price - цена единицы товара к оплате с учетом НДС в рублях*
 * @property string $barcode - штрихкод на товаре
 */
class DeliveryOrderPlaceItemDto extends Fluent
{
    use WithCollect;
}
