<?php

namespace App\Domain\External\Dto\Request\Part\DeliveryOrder;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryOrderDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 *
 * @property string $number - номер доставки*
 * @property string $external_id - номер заказа в службе доставки
 * @property float $height - высота заказа (в сантиметрaх)*
 * @property float $length - длина заказа (в сантиметрaх)*
 * @property float $width - ширина заказа (в сантиметрaх)*
 * @property float $weight - вес всего заказа (в граммах)*
 * @property int $shipment_method - способ доставки на нулевой миле (доставка от продавца до распределительного центра)*
 * @property int $delivery_method - способ доставки на последней миле (доставка до места получения заказа)*
 * @property string $tariff_id - тариф службы доставки по которому осуществляется доставка*
 * @property string $delivery_date - желаемая дата доставки
 * @property int $point_in_id - ID пункта приема заказа
 * @property int $point_out_id - ID пункта выдачи заказа
 * @property string $shipment_time_start - начальное время забора груза
 * @property string $shipment_time_end - конечное время забора груза
 * @property string $delivery_time_start - начальное время доставки
 * @property string $delivery_time_end - конечное время доставки
 * @property string $delivery_time_code - код времени доставки
 * @property string $description - комментарий
 */
class DeliveryOrderDto extends Fluent
{
}
