<?php

namespace App\Domain\External\Dto\Request\Part\DeliveryOrder;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class DeliveryOrderPlaceDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 *
 * @property string $number - номер места в информационной системе клиента
 * @property string $code - код места (уникальный код места)*
 * @property string $barcode - штрихкод места
 * @property float $height - высота места в мм*
 * @property float $length - длина места в мм*
 * @property float $width - ширина места в мм*
 * @property float $weight - вес места в граммах*
 * @property Collection|DeliveryOrderPlaceItemDto[] $items - состав места*
 */
class DeliveryOrderPlaceDto extends Fluent
{
    use WithCollect;

    /**
     * DeliveryOrderPlaceDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->items = DeliveryOrderPlaceItemDto::collect($this->items);
    }
}
