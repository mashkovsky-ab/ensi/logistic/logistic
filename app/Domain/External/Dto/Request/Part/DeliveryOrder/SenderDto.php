<?php

namespace App\Domain\External\Dto\Request\Part\DeliveryOrder;

use App\Domain\External\Dto\Request\Part\AbstractAddressDto;

/**
 * Class SenderDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 *
 * @property bool $is_seller - отправитель является истинным продавцом?
 * @property string $inn - инн истинного продавца
 * @property int $store_id - id склада продавца
 */
class SenderDto extends AbstractAddressDto
{
}
