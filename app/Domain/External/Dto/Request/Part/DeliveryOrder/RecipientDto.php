<?php

namespace App\Domain\External\Dto\Request\Part\DeliveryOrder;

use App\Domain\External\Dto\Request\Part\AbstractAddressDto;

/**
 * Class RecipientDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 */
class RecipientDto extends AbstractAddressDto
{
}
