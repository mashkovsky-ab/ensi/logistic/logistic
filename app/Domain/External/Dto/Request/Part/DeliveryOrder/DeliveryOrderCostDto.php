<?php

namespace App\Domain\External\Dto\Request\Part\DeliveryOrder;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryOrderCostDto
 * @package App\Domain\External\Dto\Request\Part\DeliveryOrder
 *
 * @property int $assessed_cost - оценочная стоимость / сумма страховки (в рублях)
 * @property int $delivery_cost - стоимость доставки с учетом НДС (в рублях)*
 * @property float $delivery_cost_vat - процентная ставка НДС
 * @property int $delivery_cost_pay - стоимость доставки к оплате с учетом НДС (в рублях)*
 * @property int $cod_cost - сумма наложенного платежа с учетом НДС (в рублях)
 * @property bool $is_delivery_payed_by_recipient - флаг для указания стороны, которая платит за услуги доставки
 * (0-отправитель, 1-получатель)
 */
class DeliveryOrderCostDto extends Fluent
{
}
