<?php

namespace App\Domain\External\Dto\Request;

use App\Domain\CargoOrders\Models\CargoOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\External\Dto\Request\Part\CourierCall\DeliveryCargoDto;
use App\Domain\External\Dto\Request\Part\CourierCall\SenderDto;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\OmsClient\Api\ShipmentsApi;
use Ensi\OmsClient\Dto\SearchShipmentsRequest;
use Illuminate\Support\Fluent;

/**
 * Создание заявки на вызов курьера
 * Class CourierCallRequestDto
 * @package App\Domain\External\Dto\Request
 * @property int $delivery_service_id - id службы доставки*
 * @property DeliveryCargoDto $cargo - информация о грузе*
 * @property SenderDto $sender - информация об отправителе груза*
 */
class CourierCallRequestDto extends Fluent
{
    /**
     * CourierCallRequestDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->cargo = new DeliveryCargoDto($this->cargo ?? []);
        $this->sender = new SenderDto($this->sender ?? []);
    }

    public static function makeFromCargoOrder(CargoOrder $model): self
    {
        /** @var StoresApi $storesApi */
        $storesApi = resolve(StoresApi::class);
        /** @var SellersApi $sellersApi */
        $sellersApi = resolve(SellersApi::class);
        $store = $storesApi->getStore($model->cargo->store_id, 'contacts')->getData();
        $seller = $sellersApi->getSeller($model->cargo->seller_id)->getData();

        /** @var StoreContact $contact */
        $contact = collect($store->getContacts())->first();
        $storeAddress = $store->getAddress();
        $cargo = $model->cargo;

        $courierCallRequestDto = new CourierCallRequestDto();

        $deliveryCargoDto = new DeliveryCargoDto();
        $courierCallRequestDto->cargo = $deliveryCargoDto;

        $deliveryCargoDto->date = $model->date;
        $deliveryCargoDto->time_code = $model->timeslot_id ?? null;
        $deliveryCargoDto->time_start = $model->timeslot_from ?? null;
        $deliveryCargoDto->time_end = $model->timeslot_to ?? null;
        $deliveryCargoDto->height = $cargo->height;
        $deliveryCargoDto->length = $cargo->length;
        $deliveryCargoDto->width = $cargo->width;
        $deliveryCargoDto->weight = $cargo->weight;
        $deliveryCargoDto->order_ids = self::getDeliveryOrderExternalIds($model);

        $senderDto = new SenderDto();
        $courierCallRequestDto->sender = $senderDto;

        $senderDto->address_string = $storeAddress->getAddressString();
        $senderDto->post_index = $storeAddress->getPostIndex();
        $senderDto->country_code = $storeAddress->getCountryCode() ?? null;
        $senderDto->region = $storeAddress->getRegion();
        $senderDto->area = $storeAddress->getArea();
        $senderDto->city = $storeAddress->getCity();
        $senderDto->city_guid = $storeAddress->getCityGuid();
        $senderDto->street = $storeAddress->getStreet() ?? null;
        $senderDto->house = $storeAddress->getHouse() ?? null;
        $senderDto->block = $storeAddress->getBlock() ?? null;
        $senderDto->flat = $storeAddress->getFlat() ?? null;
        $senderDto->company_name = $seller->getLegalName() ?? null;
        $senderDto->contact_name = $contact->getName() ?? null;
        $senderDto->email = $contact->getEmail() ?? null;
        $senderDto->phone = $contact->getPhone() ?? null;
        $senderDto->comment = null;

        return $courierCallRequestDto;
    }

    protected static function getDeliveryOrderExternalIds(CargoOrder $cargoOrder): array
    {
        $shipmentsApi = resolve(ShipmentsApi::class);
        $shipmentIds = $cargoOrder->cargo->shipmentLinks->pluck('shipment_id')->all();
        $request = new SearchShipmentsRequest();
        $request->setFilter((object)[
            'id' => $shipmentIds,
        ]);
        $shipments = $shipmentsApi->searchShipments($request)->getData();

        $deliveryIds = [];
        foreach ($shipments as $shipment) {
            $deliveryIds[] = $shipment->getDeliveryId();
        }

        return DeliveryOrder::query()->whereIn('delivery_id', $deliveryIds)->pluck('external_id')->all();
    }
}
