<?php

namespace App\Domain\External\Dto\Request;

use Illuminate\Support\Fluent;

/**
 * Запрос на расчет тарифов доставки
 * Class CalculatorRequestDto
 * @package App\Domain\External\Dto\Request
 *
 * @property string $city_guid_from - откуда
 * @property string $city_guid_to - куда
 * @property int $weight - вес всего заказа (в граммах)
 * @property int $volume - объем всего заказа (в мм3)
 * @property int $width - ширина заказа (в мм)
 * @property int $height - высота заказа (в мм)
 * @property int $length - длина заказа (в мм)
 * @property int $cost - сумма отправления
 * @property array|int[] $delivery_services_ids - массив ключей служб доставки (не обязательно)
 */
class CalculatorRequestDto extends Fluent
{
}
