<?php

namespace App\Domain\External\Dto\Response;

use App\Domain\External\Dto\Response\Part\DeliveryOrderStatus\DeliveryOrderStatusOutputDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Ответ на запрос статусов заказов на доставку
 * Class OrdersStatusResponseDto
 * @package App\Dto\Response
 *
 * @property Collection|DeliveryOrderStatusOutputDto[] $items - массив заказов с результатом загрузки
 */
class DeliveryOrdersStatusResponseDto extends Fluent
{
}
