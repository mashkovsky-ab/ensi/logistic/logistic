<?php

namespace App\Domain\External\Dto\Response;

use Illuminate\Support\Fluent;

/**
 * Ответ на отмену заявки на вызов курьера
 * Class CourierCallCancelResponseDto
 * @package App\Dto\Response
 *
 * @property bool $success
 * @property string $message
 */
class CourierCallCancelResponseDto extends Fluent
{
}
