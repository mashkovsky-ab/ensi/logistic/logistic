<?php

namespace App\Domain\External\Dto\Response;

use Illuminate\Support\Fluent;

/**
 * Ответ на создание заявки на вызов курьера
 * Class CourierCallResponseDto
 * @package App\Dto\Response
 *
 * @property string $external_id - номер заявки в службе доставке (интеграторе) ИЛИ uuid в случае со CDEK
 * @property string $delivery_service_external_id - номер заказа в службе доставке
 * @property bool $success - результат создания заявки на вызов курьера
 * (true – успех, false – ошибка)
 * @property string $message - сообщение об ошибке
 * @property string|null $special_courier_call_status - особый статус заявки, присвоенный внешним сервисом
 * @property string|null $cdek_intake_number - Номер заявки СДЭК на вызов курьера
 *** @example https://confluence.cdek.ru/pages/viewpage.action?pageId=29948360
 */
class CourierCallResponseDto extends Fluent
{
}
