<?php

namespace App\Domain\External\Dto\Response;

use Illuminate\Support\Fluent;

/**
 * Ответ на получение штрихкодов мест (коробок) заказа на доставку
 * Class DeliveryOrderBarcodesResponseDto
 * @package App\Dto\Response
 *
 * @property string $file_url
 * @property string $file_name
 * @property int $file_size
 * @property bool $success
 * @property string $message
 */
class DeliveryOrderBarcodesResponseDto extends Fluent
{
}
