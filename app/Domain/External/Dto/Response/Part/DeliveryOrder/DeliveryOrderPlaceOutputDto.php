<?php

namespace App\Domain\External\Dto\Response\Part\DeliveryOrder;

use Illuminate\Support\Fluent;

/**
 * Место в созданном/обновленном заказе на доставку
 * Class DeliveryOrderPlaceOutputDto
 * @package App\Domain\External\Dto\Response\Part\DeliveryOrder
 *
 * @property string $number - порядковый номер места
 * @property string $code - код места (уникальный код места)
 * @property string $code_external_id - код места в службе доставки (уникальный код места)
 */
class DeliveryOrderPlaceOutputDto extends Fluent
{
}
