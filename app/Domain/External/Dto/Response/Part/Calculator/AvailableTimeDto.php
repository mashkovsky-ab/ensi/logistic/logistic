<?php

namespace App\Domain\External\Dto\Response\Part\Calculator;

use Illuminate\Support\Fluent;

/**
 * Class AvailableTimeDto
 * @package App\Dto\Response\Part\Calculator
 *
 * @property string $code - код интервала доставки
 * @property int $from - начальный час доставки «С»
 * @property int $to - конечный час доставки «ДО»
 */
class AvailableTimeDto extends Fluent
{
}
