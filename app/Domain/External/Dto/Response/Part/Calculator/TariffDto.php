<?php

namespace App\Domain\External\Dto\Response\Part\Calculator;

use App\Domain\Support\Traits\WithCollect;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class TariffDto
 * @package App\Dto\Response\Part\Calculator
 *
 * @property string $external_id - ID тарифа в службе доставки
 * @property string $name - наименование тарифа
 * @property int $delivery_service_cost - стоимость доставки от службы доставки @todo - использовать копейки
 * @property int $days_min - минимальное количество дней на доставку
 * @property int $days_max - максимальное количество дней на доставку
 * @property Collection|AvailableDateDto[] $available_dates - доступные даты для доставки
 * @property array|int[] $point_ids - список ID точек выдачи заказа
 */
class TariffDto extends Fluent
{
    use WithCollect;
    /** @var int - максимальное кол-во доступных дней для доставки от первой даты доставки */
    const MAX_AVAILABLE_DAYS = 7;

    /**
     * TariffDto constructor.
     * @param  array  $attributes
     * @throws \Exception
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        if (is_null($this->available_dates) && $this->days_min && $this->days_max) {
            $today = (new DateTime())
                ->setTime(0, 0);
            $maxDeliveryDays = max($this->days_min, $this->days_max);
            $begin = clone $today;
            $begin = $begin->modify('+' . $maxDeliveryDays . ($maxDeliveryDays > 1 ? ' days' : ' day'));
            $end = clone $begin;
            $end = $end->modify('+' . self::MAX_AVAILABLE_DAYS . (self::MAX_AVAILABLE_DAYS > 1 ? ' days' : ' day'));

            $interval = new DateInterval('P1D');
            $datePeriod = new DatePeriod($begin, $interval, $end);

            $this->available_dates = collect();
            foreach ($datePeriod as $date) {
                $availableDateDto = new AvailableDateDto();
                $availableDateDto->date = Carbon::instance($date);
                $availableDateDto->available_times = collect();
                $this->available_dates->push($availableDateDto);
            }
        }
    }

    /**
     * По коллекции доступных дат для доставки определить минимальное=максимальное количество дней на доставку
     */
    public function calculateDaysMinMax(): void
    {
        $minAvailableDate = $this->available_dates->min('date');
        $this->days_min = $this->days_max = (int) now()->setTime(0, 0)->diff($minAvailableDate)->days;
    }
}
