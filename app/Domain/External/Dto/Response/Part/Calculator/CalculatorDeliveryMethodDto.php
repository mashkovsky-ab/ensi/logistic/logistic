<?php

namespace App\Domain\External\Dto\Response\Part\Calculator;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculatorDeliveryMethodDto
 * @package App\Dto\Response\Part\Calculator
 *
 * @property int $delivery_service - служба доставки
 * @property int $delivery_method - способ доставки
 * @property Carbon $psd - planned shipment date - плановая дата и время, когда отправление должно быть собрано
 * (получить статус "Готово к отгрузке")
 * @property Carbon $pdd - planned delivery date - плановая дата,
 * начиная с которой отправление может быть доставлено клиенту
 * @property int $averageCost - средняя стоимость всех тарифов на доставку @todo - использовать копейки
 * @property Collection|TariffDto[] $tariffs - тарифы на доставку
 */
class CalculatorDeliveryMethodDto extends Fluent
{
    use WithCollect;

    /**
     * CalculationOutDto constructor.
     * @param  array  $attributes
     * @throws \Exception
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->tariffs = TariffDto::collect($this->tariffs);
    }
}
