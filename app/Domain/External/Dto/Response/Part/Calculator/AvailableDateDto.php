<?php

namespace App\Domain\External\Dto\Response\Part\Calculator;

use App\Domain\External\Dto\AbstractAvailableTimeDto;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class AvailableDateDto
 * @package App\Dto\Response\Part\Calculator
 *
 * @property Carbon $date
 * @property Collection|AvailableTimeDto[] $available_times - доступные интервалы времени доставки
 */
class AvailableDateDto extends Fluent
{
    /**
     * @param  AbstractAvailableTimeDto[]  $availableTimeDtos
     */
    public function addAvailableTimes(array $availableTimeDtos): void
    {
        $this->available_times = collect();
        foreach ($availableTimeDtos as $courierAvailableTimeDto) {
            $availableTimeDto = new AvailableTimeDto();
            $availableTimeDto->code = $courierAvailableTimeDto->code;
            // так как СДЭК присылает часы доставки в виде числа (integer)
            // то преобразуем их в строки вида формата "H" ("01", а не "1")
            $availableTimeDto->from = $courierAvailableTimeDto->from > 9 ?
                strval($courierAvailableTimeDto->from) :
                '0' . strval($courierAvailableTimeDto->from);
            $availableTimeDto->to = $courierAvailableTimeDto->to > 9 ?
                strval($courierAvailableTimeDto->to) :
                '0' . strval($courierAvailableTimeDto->to);

            $this->available_times->push($availableTimeDto);
        }
    }
}
