<?php

namespace App\Domain\External\Dto\Response\Part\DeliveryOrderStatus;

use Illuminate\Support\Fluent;

/**
 * Статус заказа на доставку
 * Class DeliveryOrderStatusOutputDto
 * @package App\Dto\Response\Part\OrderStatus
 *
 * @property bool $success - указывает успешно ли получен статус по заказу (если false, то
* продолжать разбор ответа нет смысла, поля без данных могут отсутствовать)
 * @property string $message - сообщение от сервиса/описание ошибок загрузки
 * @property string $number - номер заказа на доставку в платформе
 * @property string $external_id - id заказа на доставку в службе доставки
 * @property string $status - статус в платформе
 * @property string $status_external_id - статус в службе доставки
 * @property string $status_date - дата статуса
 * @property bool $is_canceled - заказ отменен?
 * @property bool $is_problem - заказ проблемный?
 */
class DeliveryOrderStatusOutputDto extends Fluent
{
}
