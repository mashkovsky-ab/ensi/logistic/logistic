<?php

namespace App\Domain\External\Dto\Response;

use App\Domain\External\Dto\Response\Part\DeliveryOrder\DeliveryOrderPlaceOutputDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Ответ на создание/обновление заказа на доставку
 * Class OrderResponseDto
 * @package App\Dto\Response
 *
 * @property string $external_id - номер заказа в службе доставке (интеграторе)
 * @property bool $success - результат загрузки/обновления заказа
 * (true – успех, false – ошибка)
 * @property string $message - сообщение об ошибке
 * @property Collection|DeliveryOrderPlaceOutputDto[] $places - массив созданных/обновленных мест
 */
class DeliveryOrderResponseDto extends Fluent
{
}
