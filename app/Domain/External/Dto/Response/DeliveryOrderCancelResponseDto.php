<?php

namespace App\Domain\External\Dto\Response;

use Illuminate\Support\Fluent;

/**
 * Ответ на возврат/отмену заказа на доставку
 * Class DeliveryOrderCancelResponseDto
 * @package App\Dto\Response
 *
 * @property bool $success
 * @property string $message
 */
class DeliveryOrderCancelResponseDto extends Fluent
{
}
