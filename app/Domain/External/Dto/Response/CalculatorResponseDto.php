<?php

namespace App\Domain\External\Dto\Response;

use App\Domain\External\Dto\Response\Part\Calculator\CalculatorDeliveryMethodDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Ответ на расчет тарифов доставки
 * Class CalculatorResponseDto
 * @package App\Dto\Response
 *
 * @property Collection|CalculatorDeliveryMethodDto[] $methods - способы получения товара
 */
class CalculatorResponseDto extends Fluent
{
    /**
     * CalculatorResponseDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->methods = CalculatorDeliveryMethodDto::collect($this->methods);
    }

    /**
     * @param  CalculatorResponseDto  $calculatorResponseDto
     * @return $this
     */
    public function merge(self $calculatorResponseDto): self
    {
        $this->methods = $this->methods->merge($calculatorResponseDto->methods);

        return $this;
    }
}
