<?php

namespace App\Domain\External\B2Cpl\Api;

use Dadata\DadataClient;
use Ensi\B2Cpl\B2Cpl;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class AbstractApi
{
    protected LoggerInterface $logger;

    protected const KAZAN_NUMBER = 16;
    protected const MOSCOW_NUMBER = 77;
    protected const SPB_NUMBER = 78;

    /** @var int[] - коды регионов, из которых B2CPL забирает заказы (есть сортировочный центр) */
    protected const ALLOWED_REGIONS = [self::KAZAN_NUMBER, self::MOSCOW_NUMBER, self::SPB_NUMBER];

    public function __construct(protected B2Cpl $service)
    {
        $this->logger = Log::channel('b2cpl');
    }

    protected function getRegionNumberByCityGuid(string $cityGuid): int
    {
        $daData = resolve(DadataClient::class);
        $address = $daData->findById('address', $cityGuid);
        $kladrId = $address['data']['region_kladr_id'] ?? null;
        $regionNumber = $kladrId ? (int)substr($kladrId, 0, 2) : 0;

        return in_array($regionNumber, self::ALLOWED_REGIONS) ? $regionNumber : self::MOSCOW_NUMBER;
    }
}
