<?php

namespace App\Domain\External\B2Cpl\Api;

use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\External\CalculatorInterface;
use App\Domain\External\Dto\Request\CalculatorRequestDto;
use App\Domain\External\Dto\Response\CalculatorResponseDto;
use App\Domain\External\Dto\Response\Part\Calculator\AvailableDateDto;
use App\Domain\External\Dto\Response\Part\Calculator\AvailableTimeDto;
use App\Domain\External\Dto\Response\Part\Calculator\CalculatorDeliveryMethodDto;
use App\Domain\External\Dto\Response\Part\Calculator\TariffDto;
use Ensi\B2Cpl\Dto\Request\Part\Tariff\TariffParcelDto;
use Ensi\B2Cpl\Dto\Request\TariffRequestDto;
use Ensi\B2Cpl\Dto\Response\TariffResponseDto;
use Exception;
use Illuminate\Support\Carbon;

/**
 * Class Calculator
 * @package App\Domain\External\B2Cpl\Api
 */
class CalculatorApi extends AbstractApi implements CalculatorInterface
{
    /** @var array */
    protected array $tariffsCache = [];

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function calculate(CalculatorRequestDto $requestDto): CalculatorResponseDto
    {
        $this->loadTariffs();
        $tariffRequestDto = new TariffRequestDto();

        $tariffRequestDto->region = $this->getRegionNumberByCityGuid($requestDto->city_guid_from);
        $tariffRequestDto->fias_id = $requestDto->city_guid_to;

        $parcel = new TariffParcelDto();
        $tariffRequestDto->parcels = [$parcel];
        $parcel->weight = $requestDto->weight;
        $parcel->x = (int)ceil(mm2cm($requestDto->length));
        $parcel->y = (int)ceil(mm2cm($requestDto->width));
        $parcel->z = (int)ceil(mm2cm($requestDto->height));

        if (!app()->isProduction() || config('app.debug')) {
            $this->logger->debug('Calculator request', $tariffRequestDto->jsonSerialize());
        }
        $tariffResponseDto = $this->service->calculator()->calculate($tariffRequestDto);
        if (!app()->isProduction() || config('app.debug')) {
            //Здесь логируем именно json из-за ошибки monolog/monolog "Over 9 levels deep, aborting normalization"
            $this->logger->debug('Calculator response', $tariffResponseDto->toArray());
        }

        $responseDto = new CalculatorResponseDto();
        if ($tariffResponseDto->flag_delivery) {
            $this->addDeliveryToDoor($responseDto, $tariffResponseDto);
            $this->addDeliveryToPoint($responseDto, $tariffResponseDto);
        }

        return $responseDto;
    }

    /**
     * Добавить данные по доставке курьером
     * @param  CalculatorResponseDto  $responseDto
     * @param  TariffResponseDto  $tariffResponseDto
     * @throws Exception
     */
    protected function addDeliveryToDoor(
        CalculatorResponseDto $responseDto,
        TariffResponseDto $tariffResponseDto
    ): void {
        if ($tariffResponseDto->delivery_courier) {
            $calculatorItemDto = new CalculatorDeliveryMethodDto();
            $calculatorItemDto->delivery_service = DeliveryService::SERVICE_B2CPL;
            $calculatorItemDto->delivery_method = DeliveryMethod::METHOD_DELIVERY;

            $hasTariffs = false;
            foreach ($tariffResponseDto->delivery_courier as $deliveryCourierDto) {
                $tariffDto = new TariffDto();
                $calculatorItemDto->tariffs->push($tariffDto);
                $tariffDto->external_id = $this->tariffsCache[mb_strtolower($deliveryCourierDto->code)] ?? '';
                $tariffDto->name = $deliveryCourierDto->name;
                $tariffDto->delivery_service_cost = $deliveryCourierDto->price;

                $tariffDto->available_dates = collect();
                foreach ($deliveryCourierDto->available_dates as $courierAvailableDateDto) {
                    $availableDateDto = new AvailableDateDto();
                    $tariffDto->available_dates->push($availableDateDto);
                    $availableDateDto->date = Carbon::createFromFormat('Y-m-d\TH:i:s', $courierAvailableDateDto->date)
                        ->setTime(0, 0);

                    $availableDateDto->available_times = collect();

                    foreach ($courierAvailableDateDto->times as $courierAvailableTimeDto) {
                        $availableTimeDto = new AvailableTimeDto();
                        $availableTimeDto->code = $courierAvailableTimeDto->time_code;
                        $availableTimeDto->from = $courierAvailableTimeDto->time_begin;
                        $availableTimeDto->to = $courierAvailableTimeDto->time_end;

                        $availableDateDto->available_times->push($availableTimeDto);
                    }
                    $hasTariffs = true;
                }
                $tariffDto->calculateDaysMinMax();
            }

            if ($hasTariffs) {
                $responseDto->methods->push($calculatorItemDto);
            }
        }
    }

    /**
     * Добавить данные по самовывозу
     * @param  CalculatorResponseDto  $responseDto
     * @param  TariffResponseDto  $tariffResponseDto
     * @throws Exception
     */
    protected function addDeliveryToPoint(
        CalculatorResponseDto $responseDto,
        TariffResponseDto $tariffResponseDto
    ): void {
        if ($tariffResponseDto->delivery_pvz) {
            $calculatorItemDto = new CalculatorDeliveryMethodDto();
            $calculatorItemDto->delivery_service = DeliveryService::SERVICE_B2CPL;
            $calculatorItemDto->delivery_method = DeliveryMethod::METHOD_PICKUP;

            $hasTariffs = false;
            foreach ($tariffResponseDto->delivery_pvz as $deliveryPvzDto) {
                $tariffDto = new TariffDto([
                    'days_min' => $deliveryPvzDto->transport_days,
                    'days_max' => $deliveryPvzDto->transport_days,
                ]);
                $tariffDto->external_id = $this->tariffsCache[mb_strtolower($deliveryPvzDto->name)] ?? '';
                $tariffDto->name = $deliveryPvzDto->name;
                $tariffDto->days_min = $deliveryPvzDto->transport_days;
                $tariffDto->days_max = $deliveryPvzDto->transport_days;
                $tariffDto->delivery_service_cost = $deliveryPvzDto->price;

                $pvzCodes = [];
                foreach ($deliveryPvzDto->available_pvz as $deliveryPvzInfoDto) {
                    if (!in_array($deliveryPvzInfoDto->code, $pvzCodes)) {
                        $pvzCodes[] = $deliveryPvzInfoDto->code;
                    }
                }
                if ($pvzCodes) {
                    $points = Point::query()
                        ->where('active', true)
                        ->where('delivery_service', DeliveryService::SERVICE_B2CPL)
                        ->whereIn('external_id', $pvzCodes)
                        ->select('id')
                        ->get();
                    if ($points->isNotEmpty()) {
                        $tariffDto->point_ids = $points->pluck('id')->toArray();

                        $calculatorItemDto->tariffs->push($tariffDto);
                    }
                }
                $hasTariffs = true;
            }

            if ($hasTariffs) {
                $responseDto->methods->push($calculatorItemDto);
            }
        }
    }

    protected function loadTariffs(): void
    {
        $tariffs = Tariff::query()
            ->where('delivery_service', DeliveryService::SERVICE_B2CPL)
            ->select(['id', 'external_id'])
            ->get();

        $this->tariffsCache = array_combine($tariffs->pluck('external_id')->toArray(), $tariffs->pluck('id')->toArray());
    }
}
