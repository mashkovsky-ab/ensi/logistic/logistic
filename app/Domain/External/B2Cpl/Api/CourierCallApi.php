<?php

namespace App\Domain\External\B2Cpl\Api;

use App\Domain\External\CourierCallInterface;
use App\Domain\External\Dto\Request\CourierCallRequestDto;
use App\Domain\External\Dto\Response\CourierCallCancelResponseDto;
use App\Domain\External\Dto\Response\CourierCallResponseDto;
use DateTime;
use Ensi\B2Cpl;
use Exception;

class CourierCallApi extends AbstractApi implements CourierCallInterface
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function create(CourierCallRequestDto $requestDto): CourierCallResponseDto
    {
        $cargo = $requestDto->cargo;
        $sender = $requestDto->sender;

        $courierCallRequestDto = new B2Cpl\Dto\Request\CourierCallRequestDto();
        $courierCallRequestDto->address = urlencode(iconv("UTF-8", "Windows-1251", $sender->address_string));
        $courierCallRequestDto->person = urlencode(iconv("UTF-8", "Windows-1251", $sender->contact_name));
        $courierCallRequestDto->phone = urlencode($sender->phone);
        $courierCallRequestDto->date = (new DateTime($cargo->date))->format('d.m.Y');
        $courierCallRequestDto->time = $cargo->time_code;
        $courierCallRequestDto->quantity = $cargo->order_ids && is_array($cargo->order_ids)
            ? count($cargo->order_ids) : 0;
        $courierCallRequestDto->volume = $cargo->width * $cargo->height * $cargo->length / pow(100, 3);
        $courierCallRequestDto->weight = $cargo->weight;

        $this->logger->debug('Create CourierCall request', $courierCallRequestDto->toArray());
        $courierCallResponseDto = $this->service->courierCall()->create($courierCallRequestDto);
        $this->logger->debug('Create CourierCall response', $courierCallResponseDto->toArray());

        $responseDto = new CourierCallResponseDto();
        $responseDto->success = $courierCallResponseDto->result != -1;
        $responseDto->message = $courierCallResponseDto->message;
        if ($responseDto->success) {
            $responseDto->external_id = $courierCallResponseDto->result;
        }

        return $responseDto;
    }

    public function cancel(string $externalId): CourierCallCancelResponseDto
    {
        $courierCallCancelRequestDto = new B2Cpl\Dto\Request\CourierCallCancelRequestDto();
        $courierCallCancelRequestDto->id = $externalId;

        $this->logger->debug('Cancel CourierCall request', $courierCallCancelRequestDto->toArray());
        $courierCallCancelResponseDto = $this->service->courierCall()->cancel($courierCallCancelRequestDto);
        $this->logger->debug('Cancel CourierCall response', $courierCallCancelResponseDto->toArray());
        $responseDto = new CourierCallCancelResponseDto();
        $responseDto->success = $courierCallCancelResponseDto->result == 1;
        $responseDto->message = $courierCallCancelResponseDto->message;

        return $responseDto;
    }

    /**
     * Проверить заявку на вызов курьера
     * @param string $externalId
     * @throws Exception
     */
    public function check(string $externalId)
    {
        throw new Exception('Method is not supported for this Service');
    }
}
