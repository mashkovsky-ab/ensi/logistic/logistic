<?php

namespace App\Domain\External\B2Cpl\Api;

use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\External\B2Cpl\Lists\B2CplDeliveryOrderStatus;
use App\Domain\External\DeliveryOrderInterface;
use App\Domain\External\Dto\Request\DeliveryOrderRequestDto;
use App\Domain\External\Dto\Response\DeliveryOrderBarcodesResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrderCancelResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrderResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrdersStatusResponseDto;
use App\Domain\External\Dto\Response\Part\DeliveryOrder\DeliveryOrderPlaceOutputDto;
use App\Domain\External\Dto\Response\Part\DeliveryOrderStatus\DeliveryOrderStatusOutputDto;
use Ensi\B2Cpl\B2Cpl;
use Ensi\B2Cpl\Dto\Request\OrderCancelRequestDto;
use Ensi\B2Cpl\Dto\Request\OrdersRequestDto;
use Ensi\B2Cpl\Dto\Request\OrdersStatusRequestDto;
use Ensi\B2Cpl\Dto\Request\Part\Order\OrderInputDto;
use Ensi\B2Cpl\Dto\Request\Part\Order\OrderInputParcelDto;
use Ensi\B2Cpl\Dto\Request\Part\Order\OrderInputParcelItemDto;
use Ensi\B2Cpl\Dto\Request\Part\Sticker\StickerCodeDto;
use Ensi\B2Cpl\Dto\Request\Part\Sticker\StickerCodeParcelDto;
use Ensi\B2Cpl\Dto\Request\Part\Sticker\StickerFormatDto;
use Ensi\B2Cpl\Dto\Request\StickerRequestDto;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Exception;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

class DeliveryOrderApi extends AbstractApi implements DeliveryOrderInterface
{
    public function __construct(B2Cpl $service, protected EnsiFilesystemManager $fileManager)
    {
        parent::__construct($service);
    }

    /** @var int - максимальное кол-во заказов в одном запросе для получения статуса от СД */
    protected const MAX_ORDER_STATUS_QTY = 125;

    public function create(DeliveryOrderRequestDto $requestDto): DeliveryOrderResponseDto
    {
        return $this->upsert($requestDto);
    }

    public function update(DeliveryOrderRequestDto $requestDto): DeliveryOrderResponseDto
    {
        return $this->upsert($requestDto, true);
    }

    public function cancel(string $externalId): DeliveryOrderCancelResponseDto
    {
        $orderCancelRequestDto = new OrderCancelRequestDto();
        $orderCancelRequestDto->code_b2cpl = $externalId;

        $this->logger->debug('Cancel DeliveryOrder request', $orderCancelRequestDto->toArray());
        $orderCancelResponseDto = $this->service->orders()->cancel($orderCancelRequestDto);
        $this->logger->debug('Cancel DeliveryOrder response', $orderCancelResponseDto->toArray());

        return new DeliveryOrderCancelResponseDto($orderCancelResponseDto->toArray());
    }

    public function status(array $externalIds): DeliveryOrdersStatusResponseDto
    {
        $ordersStatusResponseDto = new DeliveryOrdersStatusResponseDto();
        $ordersStatusResponseDto->items = collect();

        foreach (array_chunk($externalIds, self::MAX_ORDER_STATUS_QTY) as $chunkedExternalIds) {
            $ordersStatusRequestDto = new OrdersStatusRequestDto();
            $ordersStatusRequestDto->code_type = OrdersStatusRequestDto::CODE_TYPE_B2CPL;
            $ordersStatusRequestDto->codes = $chunkedExternalIds;

            $this->logger->debug('Status DeliveryOrder request', $ordersStatusRequestDto->toArray());
            $b2cplOrdersStatusResponseDto = $this->service->orders()->status($ordersStatusRequestDto);
            $this->logger->debug('Status DeliveryOrder response', $ordersStatusRequestDto->toArray());
            foreach ($b2cplOrdersStatusResponseDto->orders as $b2cplOrderStatusOutputDto) {
                $orderStatusOutputDto = new DeliveryOrderStatusOutputDto();
                $ordersStatusResponseDto->items->push($orderStatusOutputDto);
                $orderStatusOutputDto->success = $b2cplOrderStatusOutputDto->success;
                $orderStatusOutputDto->message = $b2cplOrderStatusOutputDto->message;
                $orderStatusOutputDto->number = $b2cplOrderStatusOutputDto->code_client;
                $orderStatusOutputDto->external_id = $b2cplOrderStatusOutputDto->code_b2cpl;
                if (isset(B2CplDeliveryOrderStatus::all()[$b2cplOrderStatusOutputDto->status_b2cpl])) {
                    $orderStatusOutputDto->status =
                        B2CplDeliveryOrderStatus::all()[$b2cplOrderStatusOutputDto->status_b2cpl]->delivery_status_id;
                }
                $orderStatusOutputDto->status_external_id = $b2cplOrderStatusOutputDto->status_b2cpl;
                $orderStatusOutputDto->status_date = $b2cplOrderStatusOutputDto->status_b2cpl_date;
                $orderStatusOutputDto->is_canceled =
                    B2CplDeliveryOrderStatus::isCanceled($b2cplOrderStatusOutputDto->status_b2cpl_date);
                $orderStatusOutputDto->is_problem =
                    B2CplDeliveryOrderStatus::isProblem($b2cplOrderStatusOutputDto->status_b2cpl_date);
            }
        }

        return $ordersStatusResponseDto;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function barcodes(string $externalId, array $placesExternalId = []): DeliveryOrderBarcodesResponseDto
    {
        $orderBarcodesResponseDto = new DeliveryOrderBarcodesResponseDto();

        $stickerRequestDto = new StickerRequestDto();
        $stickerFormatDto = new StickerFormatDto();
        $stickerRequestDto->format = $stickerFormatDto;
        $stickerFormatDto->type = StickerFormatDto::TYPE_58_60;
        $stickerCodeDto = new StickerCodeDto();
        $stickerRequestDto->codes = [$stickerCodeDto];
        $stickerCodeDto->code = $externalId;
        if ($placesExternalId) {
            $parcels = [];

            foreach ($placesExternalId as $placeExternalId) {
                $stickerCodeParcelDto = new StickerCodeParcelDto();
                $parcels[] = $stickerCodeParcelDto;
                $stickerCodeParcelDto->code = $placeExternalId;
            }

            $stickerCodeDto->parcels = $parcels;
        }

        $this->logger->debug('Barcodes DeliveryOrder request', $stickerRequestDto->toArray());
        $stickerResponseDto = $this->service->orders()->barcodes($stickerRequestDto);
        $this->logger->debug('Barcodes DeliveryOrder response', $stickerResponseDto->toArray());
        $orderBarcodesResponseDto->success = $stickerResponseDto->success;
        if ($orderBarcodesResponseDto->success) {
             try {
                 $fileName = $externalId . '.pdf';
                 $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);
                 /** @var FilesystemAdapter $disk */
                 $disk = Storage::disk($this->fileManager->protectedDiskName());
                 $path = $disk->putFileAs(DeliveryOrderInterface::BARCODES_FOLDER . "/{$hashedSubDirs}", $stickerResponseDto->link, $fileName);
                 if (!$path) {
                     $orderBarcodesResponseDto->success = false;
                     $orderBarcodesResponseDto->message = 'Ошибка при сохранении файла со штрихкодами';
                 }
                 $orderBarcodesResponseDto->file_url = $path;
             } catch (Exception $e) {
                 $orderBarcodesResponseDto->success = false;
                 $orderBarcodesResponseDto->message = $e->getMessage();
             }
        } else {
            $orderBarcodesResponseDto->message = $stickerResponseDto->message;
        }

        return $orderBarcodesResponseDto;
    }

    /**
     * @param DeliveryOrderRequestDto $requestDto
     * @param bool $isUpdate
     * @return DeliveryOrderResponseDto
     */
    protected function upsert(DeliveryOrderRequestDto $requestDto, bool $isUpdate = false): DeliveryOrderResponseDto
    {
        $ordersRequestDto = new OrdersRequestDto();

        $ordersRequestDto->region = $this->getRegionNumberByCityGuid($requestDto->sender->city_guid);
        $ordersRequestDto->flag_update = (int)$isUpdate;

        $order = new OrderInputDto();
        $ordersRequestDto->orders = [$order];
        $order->sender = $requestDto->sender->company_name ?? $requestDto->sender->contact_name;
        $order->code = $requestDto->order->number;
        $order->code_b2cpl = $requestDto->order->external_id;
        $order->fio = $requestDto->recipient->contact_name ?? $requestDto->recipient->company_name;
        $order->zip = $requestDto->recipient->post_index;
        $order->region = $requestDto->recipient->region;
        $order->city = $requestDto->recipient->city;
        $order->address = $requestDto->recipient->address_string;
        $order->phone1 = $requestDto->recipient->phone;
        $order->email = $requestDto->recipient->email;
        $order->price_assess = $requestDto->cost->assessed_cost;
        $order->price_delivery = $requestDto->cost->delivery_cost;
        $order->price_delivery_pay = $requestDto->cost->is_delivery_payed_by_recipient ?
            $requestDto->cost->cod_cost : 0;
        $order->amount_prepaid = 0;
        $order->delivery_type = $requestDto->order->delivery_method == DeliveryMethod::METHOD_PICKUP ?
            $this->getPointExternalId($requestDto->order->point_out_id) :
            $this->getTariffExternalId($requestDto->order->tariff_id);
        $order->delivery_date = $requestDto->order->delivery_date;
        $order->delivery_time = $requestDto->order->delivery_time_code;
        $order->flag_open = 0;
        $order->comment = $requestDto->order->description;

        if (!is_null($requestDto->places)) {
            $parcels = [];
            $number = 1;

            foreach ($requestDto->places as $deliveryOrderPlaceDto) {
                $parcel = new OrderInputParcelDto();
                $parcels[] = $parcel;
                $parcel->number = $number++;
                $parcel->code = $deliveryOrderPlaceDto->code;
                $parcel->weight = $deliveryOrderPlaceDto->weight;
                $parcel->dim_x = $deliveryOrderPlaceDto->length;
                $parcel->dim_y = $deliveryOrderPlaceDto->width;
                $parcel->dim_z = $deliveryOrderPlaceDto->height;

                if (!is_null($deliveryOrderPlaceDto->items)) {
                    $items = [];

                    foreach ($deliveryOrderPlaceDto->items as $deliveryOrderItemDto) {
                        $item = new OrderInputParcelItemDto();
                        $items[] = $item;
                        $item->prodcode = $deliveryOrderItemDto->articul;
                        $item->prodname = $deliveryOrderItemDto->name;
                        $item->quantity = $deliveryOrderItemDto->quantity;
                        $item->weight = $deliveryOrderItemDto->weight;
                        $item->price = $deliveryOrderItemDto->cost;
                        $item->price_pay = $deliveryOrderItemDto->price;
                        $item->price_assess = $deliveryOrderItemDto->assessed_cost;
                        $item->vat = $deliveryOrderItemDto->cost_vat;
                    }

                    $parcel->items = $items;
                }
            }

            $order->parcels = $parcels;
        }

        $this->logger->debug('Upsert DeliveryOrder request', $ordersRequestDto->toArray());
        $ordersResponseDto = $isUpdate ?
            $this->service->orders()->update($ordersRequestDto) : $this->service->orders()->create($ordersRequestDto);
        $this->logger->debug('Upsert DeliveryOrder response', $ordersResponseDto->toArray());
        $orderOutputDto = $ordersResponseDto->orders[0];

        $responseDto = new DeliveryOrderResponseDto();
        $responseDto->success = $orderOutputDto->success;
        $responseDto->message = $orderOutputDto->message;
        if ($orderOutputDto->success) {
            $responseDto->external_id = $orderOutputDto->code_b2cpl;
            if ($orderOutputDto->parcels) {
                $responseDto->places = collect();

                foreach ($orderOutputDto->parcels as $parcel) {
                    $deliveryOrderPlaceOutputDto = new DeliveryOrderPlaceOutputDto();
                    $responseDto->places->push($deliveryOrderPlaceOutputDto);
                    $deliveryOrderPlaceOutputDto->number = $parcel->number;
                    $deliveryOrderPlaceOutputDto->code = $parcel->code;
                    $deliveryOrderPlaceOutputDto->code_external_id = $parcel->code_b2cpl;
                }
            }
        }

        return $responseDto;
    }

    /**
     * @param  int  $id
     * @return string
     */
    protected function getTariffExternalId(int $id): string
    {
        /** @var Tariff $tariff */
        $tariff = Tariff::query()
            ->where('id', $id)
            ->where('delivery_service', DeliveryService::SERVICE_B2CPL)
            ->select(['external_id'])
            ->first();

        return !is_null($tariff) ? $tariff->external_id : '';
    }

    /**
     * @param  int  $id
     * @return string
     */
    protected function getPointExternalId(int $id): string
    {
        /** @var Point $point */
        $point = Point::query()
            ->where('id', $id)
            ->where('delivery_service', DeliveryService::SERVICE_B2CPL)
            ->select(['external_id'])
            ->first();

        return !is_null($point) ? $point->external_id : '';
    }
}
