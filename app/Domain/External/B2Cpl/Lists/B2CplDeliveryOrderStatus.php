<?php

namespace App\Domain\External\B2Cpl\Lists;

/**
 * Статус доставки B2Cpl
 * Class B2CplDeliveryOrderStatus
 * @package App\Models\DeliveryOrderStatus
 */
class B2CplDeliveryOrderStatus
{
    //статусы доставки в случае "нормального" процесса доставки
    /** @var string */
    public const CREATED = 'Ожидание посылки от ИМ';
    /** @var string */
    public const ASSEMBLING = 'Комплектация и упаковка';
    /** @var string */
    public const AWAITING_SHIPMENT = 'Ожидание отгрузки в ТК';
    /** @var string */
    public const ON_WAY = 'Транспортировка в город';
    /** @var string */
    public const RING_UP = 'Обзвон';
    /** @var string */
    public const DELIVERING = 'В доставке';
    /** @var string */
    public const READY_FOR_RECIPIENT = 'Ожидает в ПВЗ';
    /** @var string */
    public const DONE = 'Доставлено';
    /** @var string */
    public const PARTIALLY_DONE = 'Доставлено (частично)';
    //статусы по возвратам
    /** @var string */
    public const RETURNED_FROM_DELIVERY = 'Возврат';
    /** @var string */
    public const RETURNING = 'Возврат в пути';
    /** @var string */
    public const RETURNED_TO_B2CPL = 'Возврат на СЦ B2CPL';
    /** @var string */
    public const RETURNED = 'Возврат передан в ИМ';
    //проблемные и отмененные статусы
    /** @var string */
    public const PROBLEM = 'Приостановлено';
    /** @var string */
    public const CANCEL = 'Аннулировано';
    /** @var string */
    public const LOST = 'Утеряно';

    /** @var string */
    public string $id;
    /** @var string */
    public string $name;
    /** @var string */
    public string $description;

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            //статусы доставки в случае "нормального" процесса доставки
            self::CREATED => new self(
                self::CREATED,
                'Создан',
                'Ожидание посылки от заказчика'
            ),
            self::ASSEMBLING => new self(
                self::ASSEMBLING,
                'Комплектация и упаковка',
                'Заказ комплектуется (для клиентов с ФФ)'
            ),
            self::AWAITING_SHIPMENT => new self(
                self::AWAITING_SHIPMENT,
                'Ожидание отгрузки в ТК',
                'Заказ ожидает отгрузки с СЦ B2CPL'
            ),
            self::ON_WAY => new self(
                self::ON_WAY,
                'Транспортировка в город',
                'Заказ на пути в курьерскую компанию'
            ),
            self::RING_UP => new self(
                self::RING_UP,
                'Обзвон',
                'Заказ в обзвоне'
            ),
            self::DELIVERING => new self(
                self::DELIVERING,
                'В доставке',
                'Заказ ожидает доставки'
            ),
            self::READY_FOR_RECIPIENT => new self(
                self::READY_FOR_RECIPIENT,
                'Ожидает в ПВЗ',
                'Заказ ожидает в ПВЗ (только для ПВЗ)'
            ),
            self::DONE => new self(
                self::DONE,
                'Доставлено',
                'Заказ доставлен'
            ),
            self::PARTIALLY_DONE => new self(
                self::PARTIALLY_DONE,
                'Доставлено (частично)',
                'Заказ доставлен частично'
            ),

            //статусы по возвратам
            self::RETURNED_FROM_DELIVERY => new self(
                self::RETURNED_FROM_DELIVERY,
                'Возврат',
                'Заказ получил возвратный статус'
            ),
            self::RETURNING => new self(
                self::RETURNING,
                'Возврат в пути',
                'Возврат на пути к СЦ B2CPL'
            ),
            self::RETURNED_TO_B2CPL => new self(
                self::RETURNED_TO_B2CPL,
                'Возврат на СЦ B2CPL',
                'Возврат находится на СЦ B2CPL'
            ),
            self::RETURNED => new self(
                self::RETURNED,
                'Возврат передан в ИМ',
                'Возврат возвращен заказчику'
            ),

            //проблемные и отмененные статусы
            self::PROBLEM => new self(
                self::PROBLEM,
                'Приостановлено',
                'Обработка заказа приостановлена'
            ),
            self::CANCEL => new self(
                self::CANCEL,
                'Аннулировано',
                'Заказ аннулирован'
            ),
            self::LOST => new self(
                self::LOST,
                'Утеряно',
                'Заказ утерян'
            ),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return array_keys(static::all());
    }

    /**
     * B2CplDeliveryOrderStatus constructor.
     * @param  string  $id
     * @param  string  $name
     * @param  string  $description
     */
    public function __construct(string $id, string $name, string $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param  string  $statusId
     * @return bool
     */
    public static function isCanceled(string $statusId): bool
    {
        return $statusId == static::CANCEL;
    }

    /**
     * @param  string  $statusId
     * @return bool
     */
    public static function isProblem(string $statusId): bool
    {
        return in_array($statusId, [static::PROBLEM, static::LOST]);
    }
}
