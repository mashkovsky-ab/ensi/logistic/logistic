<?php

namespace App\Domain\External;

use App\Domain\External\Dto\Request\CourierCallRequestDto;
use App\Domain\External\Dto\Response\CourierCallCancelResponseDto;
use App\Domain\External\Dto\Response\CourierCallResponseDto;

interface CourierCallInterface
{
    public function create(CourierCallRequestDto $requestDto): CourierCallResponseDto;

    /**
     * @param  string $externalId - id заявки на вызов курьера во внешней системе
     * @return CourierCallCancelResponseDto
     */
    public function cancel(string $externalId): CourierCallCancelResponseDto;

    /**
     * Проверить ошибки в заявке на вызов курьера во внешнем сервисе;
     * Если ошибок нет - возвращается также актуальный номер заявки на вызов курьера
     * @note Метод актуален только для CDEK
     * @param  string  $externalId  - id заявки на вызов курьера во внешней системе
     * @return mixed
     */
    public function check(string $externalId);
}
