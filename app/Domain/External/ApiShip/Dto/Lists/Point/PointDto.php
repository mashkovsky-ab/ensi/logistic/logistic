<?php

namespace App\Domain\External\ApiShip\Dto\Lists\Point;

use App\Domain\External\ApiShip\Dto\AbstractDto;
use Illuminate\Support\Collection;

/**
 * Class PointDto
 * @package App\Domain\External\ApiShip\Dto\Lists\Point
 *
 * @property int $id
 * @property string $providerKey
 * @property int $type
 * @property int $availableOperation
 * @property int $cod
 * @property int $paymentCard
 * @property string $name
 * @property float $lat
 * @property float $lng
 * @property string $code
 * @property string $postIndex
 * @property string $countryCode
 * @property string $region
 * @property string $regionType
 * @property string $area
 * @property string $city
 * @property string $cityType
 * @property string $cityGuid
 * @property string $street
 * @property string $streetType
 * @property string $house
 * @property string $address
 * @property string $block
 * @property string $flat
 * @property string $url
 * @property string $email
 * @property string $phone
 * @property string $timetable
 * @property array $worktime
 * @property array $photos
 * @property int $fittingRoom
 * @property string $description
 * @property Collection|PointMetroDto[] $metro
 * @property array $limits
 */
class PointDto extends AbstractDto
{
    /**
     * PointDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
    
        $this->metro = PointMetroDto::collect($this->metro);
    }
}
