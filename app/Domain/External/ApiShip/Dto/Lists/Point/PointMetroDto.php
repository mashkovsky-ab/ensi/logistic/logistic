<?php

namespace App\Domain\External\ApiShip\Dto\Lists\Point;

use App\Domain\External\ApiShip\Dto\AbstractDto;
use App\Domain\Support\Traits\WithCollect;

/**
 * Class PointMetroDto
 * @package App\Domain\External\ApiShip\Dto\Lists\Point
 *
 * @property string $name
 * @property string $line
 * @property float $distance
 */
class PointMetroDto extends AbstractDto
{
    use WithCollect;
}
