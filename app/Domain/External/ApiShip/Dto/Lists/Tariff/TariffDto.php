<?php

namespace App\Domain\External\ApiShip\Dto\Lists\Tariff;

use App\Domain\External\ApiShip\Dto\AbstractDto;

/**
 * Class TariffDto
 * @package App\Domain\External\ApiShip\Dto\Lists\Tariff
 *
 * @property int $id
 * @property string $providerKey
 * @property string $name
 * @property string $description
 * @property string $aliasName
 * @property int $weightMin
 * @property int $weightMax
 * @property int $pickupType
 * @property int $deliveryType
 */
class TariffDto extends AbstractDto
{
}
