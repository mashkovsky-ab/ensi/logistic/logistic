<?php

namespace App\Domain\External\ApiShip\Dto\Lists\ProviderCity;

/**
 * Class CdekProviderCityDto
 * @package App\Domain\External\ApiShip\Dto\Lists\ProviderCity
 * @property string $full_name
 * @property string $country_code
 * @property string $cdek_id
 * @property string $cod_cost_limit
 * @property string $payment_limit
 */
class CdekProviderCityDto extends AbstractProviderCityDto
{
    /** @var array */
    protected const RENAME = [
        'fullName' => 'full_name',
        'cdekId' => 'cdek_id',
        'cityName' => 'city',
        'oblName' => 'region',
        'countryCode' => 'country_code',
        'codCostLimit' => 'cod_cost_limit',
        'paymentLimit' => 'payment_limit',
    ];
}
