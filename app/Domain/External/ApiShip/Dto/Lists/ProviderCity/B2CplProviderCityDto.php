<?php

namespace App\Domain\External\ApiShip\Dto\Lists\ProviderCity;

/**
 * Class B2cplProviderCityDto
 * @package App\Domain\External\ApiShip\Dto\Lists\ProviderCity
 * @property string $zip_first
 * @property string $zip_last
 * @property int $transport_days
 * @property int $flag_courier
 * @property int $flag_pvz
 * @property int $flag_avia
 */
class B2CplProviderCityDto extends AbstractProviderCityDto
{
    /** @var array */
    protected const RENAME = [
        'residence' => 'city',
        'zipFirst' => 'zip_first',
        'zipLast' => 'zip_last',
        'transportDays' => 'transport_days',
        'flagCourier' => 'flag_courier',
        'flagPvz' => 'flag_pvz',
        'flagAvia' => 'flag_avia',
    ];
}
