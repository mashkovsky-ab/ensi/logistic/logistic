<?php

namespace App\Domain\External\ApiShip\Dto\Lists\ProviderCity;

/**
 * Class B2cplProviderCityDto
 * @package App\Domain\External\ApiShip\Dto\Lists\MaxiPostProviderCityDto
 * @property string $city_code
 * @property string $kladr_code
 * @property int $city_comment
 * @property int $zone
 * @property int $delivery_days
 */
class MaxiPostProviderCityDto extends AbstractProviderCityDto
{
    /** @var array */
    protected const RENAME = [
        'cityCode' => 'city_code',
        'cityName' => 'city',
        'regionName' => 'region',
        'kladrCode' => 'kladr_code',
        'cityComment' => 'city_comment',
        'deliveryDays' => 'delivery_days',
    ];
}
