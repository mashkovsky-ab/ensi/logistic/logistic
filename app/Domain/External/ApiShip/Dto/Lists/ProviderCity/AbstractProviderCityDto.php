<?php

namespace App\Domain\External\ApiShip\Dto\Lists\ProviderCity;

use App\Domain\External\ApiShip\Dto\AbstractDto;

/**
 * Class AbstractProviderCityDto
 * @package App\Domain\External\ApiShip\Dto\Lists\ProviderCity
 *
 * @property int $id
 * @property string $cityGuid
 * @property string $region
 * @property string $city
 */
class AbstractProviderCityDto extends AbstractDto
{
    /** @var array */
    protected const RENAME = [];
    
    /**
     * AbstractProviderCityDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        if (static::RENAME) {
            foreach (static::RENAME as $old => $new) {
                if (isset($attributes[$old])) {
                    $attributes[$new] = $attributes[$old];
                    unset($attributes[$old]);
                }
            }
        }
        
        parent::__construct($attributes);
    }
}
