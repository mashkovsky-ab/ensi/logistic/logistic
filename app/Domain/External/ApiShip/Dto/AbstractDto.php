<?php

namespace App\Domain\External\ApiShip\Dto;

use Illuminate\Support\Fluent;

/**
 * Class AbstractDto
 * @package App\Domain\External\ApiShip\Dto
 */
class AbstractDto extends Fluent
{
}
