<?php

namespace App\Domain\External\ApiShip\Lists;

/**
 * Статус доставки ApiShip
 * Class ApiShipDeliveryOrderStatus
 * @package App\Domain\External\ApiShip\Lists
 */
class ApiShipDeliveryOrderStatus
{
    //статусы, связанные с передачей информации по сущности во внешнюю систему
    /** @var string - загрузка информации в систему перевозчика */
    public const STATUS_UPLOADING = 'uploading';
    /** @var string - информация успешно загружена в систему перевозчика */
    public const STATUS_UPLOADED = 'uploaded';
    /** @var string - ошибка передачи информации в систему перевозчика */
    public const STATUS_UPLOADING_ERROR = 'uploadingError';

    //статусы доставки в случае "нормального" процесса доставки
    /** @var string - принят на склад в пункте отправления */
    public const STATUS_ON_POINT_IN = 'onPointIn';
    /** @var string - в пути */
    public const STATUS_ON_WAY = 'onWay';
    /** @var string - прибыл на склад в пункте назначения */
    public const STATUS_ON_POINT_OUT = 'onPointOut';
    /** @var string - передана на доставку в пункте назначения */
    public const STATUS_DELIVERING = 'delivering';
    /** @var string - готов к выдаче в пункте назначения */
    public const STATUS_READY_FOR_RECIPIENT = 'readyForRecipient';
    /** @var string - доставлен получателю */
    public const STATUS_DONE = 'delivered';

    //статусы по возвратам
    /** @var string - возвращен с доставки */
    public const STATUS_RETURNED_FROM_DELIVERY = 'returnedFromDelivery';
    /** @var string - частичный возврат */
    public const STATUS_PARTIAL_RETURN = 'partialReturn';
    /** @var string - подготовлен возврат */
    public const STATUS_RETURN_READY = 'returnReady';
    /** @var string - возвращается отправителю */
    public const STATUS_RETURNING = 'returning';
    /** @var string - возвращен отправителю */
    public const STATUS_RETURNED = 'returned';

    //проблемные и отмененные статусы
    /** @var string - утеряна */
    public const STATUS_LOST = 'lost';
    /** @var string - отменена */
    public const STATUS_PROBLEM = 'problem';
    /** @var string - возникла проблема */
    public const STATUS_CANCEL = 'deliveryCanceled';

    //нестандартные статусы
    /** @var string - неизвестный статус */
    public const STATUS_UNKNOWN = 'unknown';
    /** @var string - n/a */
    public const STATUS_NA = 'notApplicable';

    /** @var string */
    public string $id;
    /** @var string */
    public string $name;

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            //статусы, связанные с передачей информации по сущности во внешнюю систему
            self::STATUS_UPLOADING => new self(
                self::STATUS_UPLOADING,
                'Загрузка информации в систему перевозчика',
            ),
            self::STATUS_UPLOADED => new self(
                self::STATUS_UPLOADED,
                'Информация успешно загружена в систему перевозчика',
            ),
            self::STATUS_UPLOADING_ERROR => new self(
                self::STATUS_UPLOADING_ERROR,
                'Ошибка передачи информации в систему перевозчика',
            ),

            //статусы доставки в случае "нормального" процесса доставки
            self::STATUS_ON_POINT_IN => new self(
                self::STATUS_ON_POINT_IN,
                'Принят на склад в пункте отправления'
            ),
            self::STATUS_ON_WAY => new self(
                self::STATUS_ON_WAY,
                'В пути'
            ),
            self::STATUS_ON_POINT_OUT => new self(
                self::STATUS_ON_POINT_OUT,
                'Прибыл на склад в пункте назначения'
            ),
            self::STATUS_DELIVERING => new self(
                self::STATUS_DELIVERING,
                'Передан на доставку в пункте назначения'
            ),
            self::STATUS_READY_FOR_RECIPIENT => new self(
                self::STATUS_READY_FOR_RECIPIENT,
                'Готов к выдаче в пункте назначения'
            ),
            self::STATUS_DONE => new self(
                self::STATUS_DONE,
                'Доставлен получателю'
            ),

            //статусы по возвратам
            self::STATUS_RETURNED_FROM_DELIVERY => new self(
                self::STATUS_RETURNED_FROM_DELIVERY,
                'Возвращен с доставки'
            ),
            self::STATUS_PARTIAL_RETURN => new self(
                self::STATUS_PARTIAL_RETURN,
                'Частичный возврат'
            ),
            self::STATUS_RETURN_READY => new self(
                self::STATUS_RETURN_READY,
                'Подготовлен возврат'
            ),
            self::STATUS_RETURNING => new self(
                self::STATUS_RETURNING,
                'Возвращается отправителю'
            ),
            self::STATUS_RETURNED => new self(
                self::STATUS_RETURNED,
                'Возвращен отправителю'
            ),

            //проблемные и отмененные статусы
            self::STATUS_LOST => new self(
                self::STATUS_LOST,
                'Утеряна'
            ),
            self::STATUS_PROBLEM => new self(
                self::STATUS_PROBLEM,
                'Возникла проблема'
            ),
            self::STATUS_CANCEL => new self(
                self::STATUS_CANCEL,
                'Отменена'
            ),

            //нестандартные статусы
            self::STATUS_UNKNOWN => new self(
                self::STATUS_UNKNOWN,
                'Неизвестный статус'
            ),
            self::STATUS_NA => new self(
                self::STATUS_NA,
                'N/A'
            ),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return array_keys(static::all());
    }

    /**
     * ApiShipDeliveryOrderStatus constructor.
     * @param  string  $id
     * @param  string  $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @param  string  $statusId
     * @return bool
     */
    public static function isCanceled(string $statusId): bool
    {
        return $statusId == static::STATUS_CANCEL;
    }

    /**
     * @param  string  $statusId
     * @return bool
     */
    public static function isProblem(string $statusId): bool
    {
        return in_array(
            $statusId,
            [static::STATUS_LOST, static::STATUS_PROBLEM, static::STATUS_PROBLEM, static::STATUS_NA]
        );
    }
}
