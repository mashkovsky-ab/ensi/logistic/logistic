<?php

namespace App\Domain\External\ApiShip;

use Apiship\Apiship;
use App\Domain\External\ApiShip\Api\ListsApi;

/**
 * Class ApiShipService
 * @package App\Domain\External\ApiShip
 */
class ApiShipService extends Apiship
{
    /**
     * @return ListsApi
     */
    public function lists(): ListsApi
    {
        return new ListsApi($this->adapter);
    }
}
