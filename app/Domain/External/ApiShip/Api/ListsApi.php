<?php

namespace App\Domain\External\ApiShip\Api;

use Apiship\Api\Lists;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\ApiShip\Responses\Lists\ProviderCity\B2CplProviderCityListsResponse;
use App\Domain\External\ApiShip\Responses\Lists\ProviderCity\CdekProviderCityListsResponse;
use App\Domain\External\ApiShip\Responses\Lists\ProviderCity\MaxiPostProviderCityListsResponse;
use App\Domain\External\ApiShip\Responses\Lists\ProviderCity\ProviderCityListsResponse;
use App\Domain\External\ApiShip\Responses\Lists\TariffListsResponse;

/**
 * Class ListsApi
 * @package App\Domain\External\ApiShip\Api
 */
class ListsApi extends Lists
{
    /**
     * Получение тарифов
     * @param int $limit
     * @param int $offset
     * @param string $filter - возможна фильтрация по полям id, providerKey, name
     * @return \App\Domain\External\ApiShip\Responses\Lists\TariffListsResponse
     */
    public function getTariffs($limit = 20, $offset = 0, $filter = ''): TariffListsResponse
    {
        if (!is_int($limit) || $limit < 0) {
            $limit = 20;
        }

        if (!is_int($offset) || $offset < 0) {
            $offset = 0;
        }

        $resultJson = $this->adapter->get(
            'lists/tariffs/',
            [],
            [
                'limit' => $limit,
                'offset' => $offset,
                'filter' => $filter,
            ]
        );

        return new TariffListsResponse($resultJson);
    }

    /**
     * Получение населенных пунктов, в который возможна доставка
     * @param int $deliveryService
     * @param int $limit
     * @param int $offset
     * @param string $filter - возможна фильтрация по полям id, providerKey, name
     * @return \App\Domain\External\ApiShip\Responses\Lists\ProviderCity\ProviderCityListsResponse
     */
    public function getProviderCities(
        int $deliveryService,
        $limit = 20,
        $offset = 0,
        $filter = ''
    ): ProviderCityListsResponse {
        if (!is_int($limit) || $limit < 0) {
            $limit = 20;
        }

        if (!is_int($offset) || $offset < 0) {
            $offset = 0;
        }

        $providerKey = DeliveryService::all()[$deliveryService]->apiship_key;
        $resultJson = $this->adapter->get(
            "lists/providerCities/{$providerKey}/",
            [],
            [
                'limit' => $limit,
                'offset' => $offset,
                'filter' => $filter,
            ]
        );

        switch ($deliveryService) {
            case DeliveryService::SERVICE_B2CPL:
                return new B2CplProviderCityListsResponse($resultJson);

                break;
            case DeliveryService::SERVICE_MAXIPOST:
                return new MaxiPostProviderCityListsResponse($resultJson);

                break;
            case DeliveryService::SERVICE_CDEK:
                return new CdekProviderCityListsResponse($resultJson);

                break;
            default:
                return new ProviderCityListsResponse($resultJson);
        }
    }
}
