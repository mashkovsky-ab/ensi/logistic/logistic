<?php

namespace App\Domain\External\ApiShip\Responses;

/**
 * Class AbstractResponse
 * @package App\Domain\External\ApiShip\Responses
 */
class AbstractResponse
{
    /** @var array */
    protected mixed $json;
    
    /**
     * AbstractDto constructor.
     * @param  string  $originJson
     */
    public function __construct(string $originJson)
    {
        $this->json = json_decode($originJson, true);
    }
}
