<?php

namespace App\Domain\External\ApiShip\Responses\Lists\ProviderCity;

use App\Domain\External\ApiShip\Dto\Lists\ProviderCity\AbstractProviderCityDto;
use App\Domain\External\ApiShip\Responses\Lists\ListsResponse;
use Illuminate\Support\Collection;

/**
 * Class ProviderCityListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists\ProviderCity
 */
class ProviderCityListsResponse extends ListsResponse
{
    /** @var string */
    protected string $wrapper = AbstractProviderCityDto::class;

    /**
     * @return Collection|AbstractProviderCityDto[]
     */
    public function getResults(): Collection
    {
        return parent::getResults();
    }
}
