<?php

namespace App\Domain\External\ApiShip\Responses\Lists\ProviderCity;

use App\Domain\External\ApiShip\Dto\Lists\ProviderCity\B2CplProviderCityDto;
use Illuminate\Support\Collection;

/**
 * Class B2CplProviderCityListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists\ProviderCity
 */
class B2CplProviderCityListsResponse extends ProviderCityListsResponse
{
    /** @var string */
    protected string $wrapper = B2CplProviderCityDto::class;
    
    /**
     * @return Collection|B2CplProviderCityDto[]
     */
    public function getResults(): Collection
    {
        return parent::getResults();
    }
}
