<?php

namespace App\Domain\External\ApiShip\Responses\Lists\ProviderCity;

use App\Domain\External\ApiShip\Dto\Lists\ProviderCity\CdekProviderCityDto;
use Illuminate\Support\Collection;

/**
 * Class CdekProviderCityListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists\ProviderCity
 */
class CdekProviderCityListsResponse extends ProviderCityListsResponse
{
    /** @var string */
    protected string $wrapper = CdekProviderCityDto::class;

    /**
     * @return Collection|CdekProviderCityDto[]
     */
    public function getResults(): Collection
    {
        return parent::getResults();
    }
}
