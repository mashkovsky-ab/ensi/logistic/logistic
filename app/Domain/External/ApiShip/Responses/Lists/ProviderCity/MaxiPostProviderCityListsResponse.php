<?php

namespace App\Domain\External\ApiShip\Responses\Lists\ProviderCity;

use App\Domain\External\ApiShip\Dto\Lists\ProviderCity\MaxiPostProviderCityDto;
use Illuminate\Support\Collection;

/**
 * Class MaxiPostProviderCityListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists\ProviderCity
 */
class MaxiPostProviderCityListsResponse extends ProviderCityListsResponse
{
    /** @var string */
    protected string $wrapper = MaxiPostProviderCityDto::class;
    
    /**
     * @return Collection|MaxiPostProviderCityDto[]
     */
    public function getResults(): Collection
    {
        return parent::getResults();
    }
}
