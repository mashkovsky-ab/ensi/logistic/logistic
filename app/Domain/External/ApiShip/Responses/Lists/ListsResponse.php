<?php

namespace App\Domain\External\ApiShip\Responses\Lists;

use App\Domain\External\ApiShip\Responses\AbstractResponse;
use Illuminate\Support\Collection;

/**
 * Class ListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists
 */
abstract class ListsResponse extends AbstractResponse
{
    /** @var string */
    protected string $wrapper;

    /**
     * @return array
     */
    public function getRows(): array
    {
        return isset($this->json['rows']) ? $this->json['rows'] : [];
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return isset($this->json['meta']) ? $this->json['meta'] : [];
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        $meta = $this->getMeta();

        return isset($meta['total']) ? (int)$meta['total'] : 0;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        $meta = $this->getMeta();

        return isset($meta['offset']) ? (int)$meta['offset'] : 0;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        $meta = $this->getMeta();

        return isset($meta['limit']) ? (int)$meta['limit'] : 0;
    }

    /**
     * @return Collection|\App\Domain\External\ApiShip\Dto\AbstractDto[]
     */
    public function getResults(): Collection
    {
        $result = collect();

        foreach ($this->getRows() as $row) {
            $result[] = new $this->wrapper($row);
        }

        return $result;
    }
}
