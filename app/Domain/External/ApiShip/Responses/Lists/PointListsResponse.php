<?php

namespace App\Domain\External\ApiShip\Responses\Lists;

use App\Domain\External\ApiShip\Dto\Lists\Point\PointDto;
use Illuminate\Support\Collection;

/**
 * Class PointListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists
 */
class PointListsResponse extends ListsResponse
{
    /** @var string */
    protected string $wrapper = PointDto::class;

    /**
     * @return Collection|\App\Domain\External\ApiShip\Dto\Lists\Point\PointDto[]
     */
    public function getResults(): Collection
    {
        return parent::getResults();
    }
}
