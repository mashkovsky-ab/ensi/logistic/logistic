<?php

namespace App\Domain\External\ApiShip\Responses\Lists;

use App\Domain\External\ApiShip\Dto\Lists\Tariff\TariffDto;
use Illuminate\Support\Collection;

/**
 * Class TariffListsResponse
 * @package App\Domain\External\ApiShip\Responses\Lists
 */
class TariffListsResponse extends ListsResponse
{
    /** @var string */
    protected string $wrapper = TariffDto::class;

    /**
     * @return Collection|TariffDto[]
     */
    public function getResults(): Collection
    {
        return parent::getResults();
    }
}
