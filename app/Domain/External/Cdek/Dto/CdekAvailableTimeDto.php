<?php

namespace App\Domain\External\Cdek\Dto;

use App\Domain\External\Dto\AbstractAvailableTimeDto;
use App\Domain\DeliveryServices\Models\City;
use Illuminate\Support\Carbon;

/**
 * Class CdekAvailableTimeDto
 * @package App\Domain\External\Cdek\Dto
 */
class CdekAvailableTimeDto extends AbstractAvailableTimeDto
{
    /**
     * @inheritDoc
     */
    protected static function all(Carbon $date, string $regionFiasId, array $params = []): array
    {
        $isMoscow = function () use ($date, $regionFiasId) {
            return $regionFiasId == City::MOSCOW_GUID;
        };

        return [
            new self(
                '09-18',
                9,
                18,
                function () use ($date, $regionFiasId) {
                    return $regionFiasId == City::MOSCOW_GUID ||
                        ($regionFiasId == City::MOSCOW_DISTRICT_GUID && $date->format('N') < 7) ||
                        (
                            !in_array($regionFiasId, [City::MOSCOW_GUID, City::MOSCOW_DISTRICT_GUID]) &&
                            $date->format('N') < 6
                        );
                }
            ),
            new self('09-14', 9, 14, $isMoscow),
            new self('14-18', 14, 18, $isMoscow),
        ];
    }
}
