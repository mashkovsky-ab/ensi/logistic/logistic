<?php

namespace App\Domain\External\Cdek\Dto;

/**
 * Class ExternalStatusCheck
 * @package App\Domain\External\Cdek\Dto
 * @example https://confluence.cdek.ru/pages/viewpage.action?pageId=29948360
 */
class ExternalStatusCheck
{
    /** @var string - ошибка от сдек при проверке задания на забор курьером */
    public string $error;
    /** @var string - номер заявки СДЭК на вызов курьера */
    public string $intake_number;
}
