<?php

namespace App\Domain\External\Cdek\Dto\Response;

use CdekSDK2\Constants;

/**
 * Статусы и их расшифровки, приходящие от CDEK
 * Class ResponseStatusesDto
 * @package App\Domain\External\Cdek\Dto\Response
 * @uses \CdekSDK2\Constants
 */
class ResponseStatusesDto
{
    /**
     * Расшифровка статусов проверки заявки по uuid
     * @see https://confluence.cdek.ru/pages/viewpage.action?pageId=29948360
     * @param array $status - информация о присвоенном статусе
     * @return string
     */
    public static function uuidCheckStatus(array $status): string
    {
        switch ($status['code']) {
            case Constants::STATUS_ACCEPTED:
                return 'Заявка создана в информационной системе СДЭК, но требуются дополнительные валидации';
            case Constants::STATUS_CREATED:
                return 'Заявка создана в информационной системе СДЭК и прошла необходимые валидации';
            case Constants::STATUS_DELETED:
                return 'Заявка отменена ИМ после регистрации в системе';
            case 'INVALID':
                return 'Заявка содержит некорректные данные';
            default:
                return 'Статус от CDEK: ' . $status['name'];
        }
    }
}
