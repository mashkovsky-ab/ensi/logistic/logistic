<?php

namespace App\Domain\External\Cdek\Dto\Response;

use Illuminate\Support\Fluent;

/**
 * Ответ на получение квитанции к заказу на доставку
 * Class DeliveryOrderReceiptResponseDto
 * @package App\Domain\External\Cdek\Dto\Response
 *
 * @property string $file_url
 * @property string $file_name
 * @property int $file_size
 * @property bool $success
 * @property string $message
 */
class DeliveryOrderReceiptResponseDto extends Fluent
{
}
