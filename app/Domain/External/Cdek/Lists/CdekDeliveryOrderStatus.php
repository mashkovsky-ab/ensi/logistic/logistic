<?php

namespace App\Domain\External\Cdek\Lists;

/**
 * Статус доставки Cdek
 * Class CdekDeliveryOrderStatus
 * @package App\Models\DeliveryOrderStatus
 */
class CdekDeliveryOrderStatus
{
    /** @var string - создан */
    public const CREATED = 1;
    /** @var string - удален */
    public const CANCEL = 2;
    /** @var string - принят на склад отправителя */
    public const ON_POINT_IN = 3;
    /** @var string - выдан на отправку в г. отправителе */
    public const READY_FOR_DELIVERY = 6;
    /** @var string - возвращен на склад отправителя */
    public const RETURN_FOR_DELIVERY = 16;
    /** @var string - сдан перевозчику в г. отправителе */
    public const ON_CARRIER_AT_DEPARTURE_CITY = 7;
    /** @var string - отправлен в г. транзит */
    public const DELIVERING_TO_TRANSIT_CITY = 21;
    /** @var string - встречен в г. транзите */
    public const MET_IN_TRANSIT_CITY = 22;
    /** @var string - принят на склад транзита */
    public const TAKE_ON_TRANSIT_STORE = 13;
    /** @var string - возвращен на склад транзита */
    public const RETURNED_TO_TRANSIT_STORE = 17;
    /** @var string - выдан на отправку в г. транзите */
    public const TO_CARRIER_AT_TRANSIT_CITY = 19;
    /** @var string - сдан перевозчику в г. транзите */
    public const ON_CARRIER_AT_TRANSIT_CITY = 20;
    /** @var string - отправлен в г. получатель */
    public const DELIVERING_TO_DESTINATION_CITY = 8;
    /** @var string - встречен в г. получателе */
    public const MET_IN_DESTINATION_CITY = 9;
    /** @var string - принят на склад доставки */
    public const ON_POINT_OUT = 10;
    /** @var string - принят на склад до востребования */
    public const READY_FOR_RECIPIENT = 12;
    /** @var string - выдан на доставку */
    public const DELIVERING = 11;
    /** @var string - возвращен на склад доставки */
    public const RETURNED_FROM_DELIVERY = 18;
    /** @var string - вручен */
    public const DONE = 4;
    /** @var string - не вручен */
    public const UNDONE = 5;

    /** @var string */
    public string $id;
    /** @var string */
    public string $name;
    /** @var string */
    public string $description;

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            //статусы доставки в случае "нормального" процесса доставки
            self::CREATED => new self(
                self::CREATED,
                'Создан',
                'Заказ зарегистрирован в базе данных СДЭК'
            ),
            self::CANCEL => new self(
                self::CANCEL,
                'Удален',
                'Заказ отменен ИМ после регистрации в системе до прихода груза на склад СДЭК в городе-отправителе'
            ),
            self::ON_POINT_IN => new self(
                self::ON_POINT_IN,
                'Принят на склад отправителя',
                'Оформлен приход на склад СДЭК в городе-отправителе'
            ),
            self::READY_FOR_DELIVERY => new self(
                self::READY_FOR_DELIVERY,
                'Выдан на отправку в г. отправителе',
                'Оформлен расход со склада СДЭК в городе-отправителе. 
                Груз подготовлен к отправке (консолидирован с другими посылками)'
            ),
            self::RETURN_FOR_DELIVERY => new self(
                self::RETURN_FOR_DELIVERY,
                'Возвращен на склад отправителя',
                'Повторно оформлен приход в городе-отправителе 
                (не удалось передать перевозчику по какой-либо причине). 
                 Примечание: этот статус не означает возврат груза отправителю'
            ),
            self::ON_CARRIER_AT_DEPARTURE_CITY => new self(
                self::ON_CARRIER_AT_DEPARTURE_CITY,
                'Сдан перевозчику в г. отправителе',
                'Зарегистрирована отправка в городе-отправителе. 
                Консолидированный груз передан на доставку (в аэропорт/загружен машину)'
            ),
            self::DELIVERING_TO_TRANSIT_CITY => new self(
                self::DELIVERING_TO_TRANSIT_CITY,
                'Отправлен в г. транзит',
                'Зарегистрирована отправка в город-транзит. 
                Проставлены дата и время отправления у перевозчика'
            ),
            self::MET_IN_TRANSIT_CITY => new self(
                self::MET_IN_TRANSIT_CITY,
                'Встречен в г. транзите',
                'Зарегистрирована встреча в городе-транзите'
            ),
            self::TAKE_ON_TRANSIT_STORE => new self(
                self::TAKE_ON_TRANSIT_STORE,
                'Принят на склад транзита',
                'Оформлен приход в городе-транзите'
            ),
            self::RETURNED_TO_TRANSIT_STORE => new self(
                self::RETURNED_TO_TRANSIT_STORE,
                'Возвращен на склад транзита',
                'Повторно оформлен приход в городе-транзите (груз возвращен на склад). 
                Примечание: этот статус не означает возврат груза отправителю'
            ),
            self::TO_CARRIER_AT_TRANSIT_CITY => new self(
                self::TO_CARRIER_AT_TRANSIT_CITY,
                'Выдан на отправку в г. транзите',
                'Оформлен расход в городе-транзите'
            ),
            self::ON_CARRIER_AT_TRANSIT_CITY => new self(
                self::ON_CARRIER_AT_TRANSIT_CITY,
                'Сдан перевозчику в г. транзите',
                'Зарегистрирована отправка у перевозчика в городе-транзите'
            ),
            self::DELIVERING_TO_DESTINATION_CITY => new self(
                self::DELIVERING_TO_DESTINATION_CITY,
                'Отправлен в г. получатель',
                'Зарегистрирована отправка в город-получатель, груз в пути.'
            ),
            self::MET_IN_DESTINATION_CITY => new self(
                self::MET_IN_DESTINATION_CITY,
                'Встречен в г. получателе',
                'Зарегистрирована встреча груза в городе-получателе'
            ),
            self::ON_POINT_OUT => new self(
                self::ON_POINT_OUT,
                'Принят на склад доставки',
                'Оформлен приход на склад города-получателя, ожидает доставки до двери'
            ),
            self::READY_FOR_RECIPIENT => new self(
                self::READY_FOR_RECIPIENT,
                'Принят на склад до востребования',
                'Оформлен приход на склад города-получателя.
                 Доставка до склада, посылка ожидает забора клиентом - покупателем ИМ'
            ),
            self::DELIVERING => new self(
                self::DELIVERING,
                'Выдан на доставку',
                'Добавлен в курьерскую карту, выдан курьеру на доставку'
            ),
            self::RETURNED_FROM_DELIVERY => new self(
                self::RETURNED_FROM_DELIVERY,
                'Возвращен на склад доставки',
                'Оформлен повторный приход на склад в городе-получателе.
                 Доставка не удалась по какой-либо причине, ожидается очередная попытка доставки. 
                 Примечание: этот статус не означает возврат груза отправителю'
            ),
            self::DONE => new self(
                self::DONE,
                'Вручен',
                'Успешно доставлен и вручен адресату (конечный статус)'
            ),
            self::UNDONE => new self(
                self::UNDONE,
                'Не вручен',
                'Покупатель отказался от покупки, возврат в ИМ (конечный статус)'
            ),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return array_keys(static::all());
    }

    /**
     * CdekDeliveryOrderStatus constructor.
     * @param  string  $id
     * @param  string  $name
     * @param  string  $description
     */
    public function __construct(string $id, string $name, string $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param  string  $statusId
     * @return bool
     */
    public static function isCanceled(string $statusId): bool
    {
        return $statusId == static::CANCEL;
    }

    /**
     * @param  string  $statusId
     * @return bool
     */
    public static function isProblem(string $statusId): bool
    {
        return false;
    }
}
