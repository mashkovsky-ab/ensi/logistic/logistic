<?php

namespace App\Domain\External\Cdek\Api;

use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\External\Cdek\Dto\Response\DeliveryOrderReceiptResponseDto;
use App\Domain\External\Cdek\Lists\CdekDeliveryOrderStatus;
use App\Domain\External\DeliveryOrderInterface;
use App\Domain\External\Dto\Request\DeliveryOrderRequestDto;
use App\Domain\External\Dto\Response\DeliveryOrderBarcodesResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrderCancelResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrderResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrdersStatusResponseDto;
use App\Domain\External\Dto\Response\Part\DeliveryOrderStatus\DeliveryOrderStatusOutputDto;
use CdekSDK\CdekClient;
use CdekSDK\Common;
use CdekSDK\Requests;
use DateTimeImmutable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class DeliveryOrderApi
 * @package App\Domain\External\Cdek\Api
 */
class DeliveryOrderApi extends AbstractApi implements DeliveryOrderInterface
{
    /**
     * DeliveryOrderApi constructor.
     */
    public function __construct(CdekClient $service)
    {
        parent::__construct($service);

        $this->service->setLogger($this->logger);
    }

    /** @var int - максимальное кол-во заказов в одном запросе для получения статуса от СД */
    protected const MAX_ORDER_STATUS_QTY = 125;
    /** @var string - папка в сервисе file для хранения файлов с квитанциями */
    public const RECEIPTS_FOLDER = 'cdek_delivery_order_receipts';

    /**
     * @inheritDoc
     */
    public function create(DeliveryOrderRequestDto $requestDto): DeliveryOrderResponseDto
    {
        return $this->upsert($requestDto);
    }

    /**
     * @inheritDoc
     */
    public function update(DeliveryOrderRequestDto $requestDto): DeliveryOrderResponseDto
    {
        return $this->upsert($requestDto, true);
    }

    /**
     * @inheritDoc
     */
    public function cancel(string $externalId): DeliveryOrderCancelResponseDto
    {
        $cdekRequest = Requests\DeleteRequest::create([
            'Number' => $externalId,
        ])->addOrder(new Common\Order([
            'DispatchNumber' => $externalId,
        ]));

        $cdekResponse = $this->service->sendDeleteRequest($cdekRequest);
        $responseDto = new DeliveryOrderCancelResponseDto();
        $responseDto->success = !$cdekResponse->hasErrors();
        if (!$responseDto->success) {
            $responseDto->message = $this->getErrors($cdekResponse);
        }

        return $responseDto;
    }

    /**
     * @inheritDoc
     */
    public function status(array $externalIds): DeliveryOrdersStatusResponseDto
    {
        $ordersStatusResponseDto = new DeliveryOrdersStatusResponseDto();
        $ordersStatusResponseDto->items = collect();

        foreach (array_chunk($externalIds, self::MAX_ORDER_STATUS_QTY) as $chunkedExternalIds) {
            $cdekRequest = new Requests\StatusReportRequest();
            foreach ($chunkedExternalIds as $externalId) {
                $cdekRequest->addOrder(Common\Order::withDispatchNumber($externalId));
            }

            $cdekResponse = $this->service->sendStatusReportRequest($cdekRequest);
            /** @var \CdekSDK\Common\Order $order */
            foreach ($cdekResponse as $order) {
                $orderStatusOutputDto = new DeliveryOrderStatusOutputDto();
                $ordersStatusResponseDto->items->push($orderStatusOutputDto);
                $status = $order->getStatus();
                $orderStatusOutputDto->success = is_null($status) ? false : true;
                $orderStatusOutputDto->message = is_null($status) ? 'Статус заказа не найден' : '';
                if ($status) {
                    $orderStatusOutputDto->number = $order->getNumber();
                    $orderStatusOutputDto->external_id = $order->getDispatchNumber();
                    if (isset(CdekDeliveryOrderStatus::all()[$status->getCode()])) {
                        $orderStatusOutputDto->status =
                            CdekDeliveryOrderStatus::all()[$status->getCode()]->delivery_status_id;
                    }
                    $orderStatusOutputDto->status_external_id = $status->getCode();
                    $orderStatusOutputDto->status_date = Carbon::createFromImmutable($status->getDate())
                        ->modify('+3 hours')->format('Y-m-d H:i:s'); //приводим время к МСК
                    $orderStatusOutputDto->is_canceled =
                        CdekDeliveryOrderStatus::isCanceled($status->getCode());
                    $orderStatusOutputDto->is_problem =
                        CdekDeliveryOrderStatus::isProblem($status->getCode());
                }
            }
        }

        return $ordersStatusResponseDto;
    }

    /**
     * @inheritDoc
     */
    public function barcodes(string $externalId, array $placesExternalId = []): DeliveryOrderBarcodesResponseDto
    {
        $orderBarcodesResponseDto = new DeliveryOrderBarcodesResponseDto();

        $cdekRequest = new Requests\PrintLabelsRequest([
            'PrintFormat' => Requests\PrintLabelsRequest::PRINT_FORMAT_A4,
        ]);
        /**
         * Cdek не умеет формировать штрихкоды для определенных мест заказа,
         * поэтому передаем только id заказа на доставку у cdek
         */
        $cdekRequest->addOrder(Common\Order::withDispatchNumber($externalId));

        $cdekResponse = $this->service->sendPrintLabelsRequest($cdekRequest);
        $orderBarcodesResponseDto->success = !$cdekResponse->hasErrors();
        if ($orderBarcodesResponseDto->success) {
            $orderBarcodesResponseDto->success = false;
            $orderBarcodesResponseDto->message = "Not implemented"; // TODO delete or fix https://redmine.greensight.ru/issues/82088
//            /** @var FileService $fileService */
//            $fileService = resolve(FileService::class);
//            $filePath = '/tmp/' . $externalId . '.pdf';
//            if (File::put($filePath, $cdekResponse->getBody())) {
//                try {
//                    $fileId = $fileService->uploadFile(
//                        DeliveryOrderInterface::BARCODES_FOLDER,
//                        File::basename($filePath),
//                        $filePath
//                    );
//                    if ($fileId) {
//                        $orderBarcodesResponseDto->file_id = $fileId;
//                    } else {
//                        $orderBarcodesResponseDto->success = false;
//                        $orderBarcodesResponseDto->message = 'Ошибка при сохранении файла со штрихкодами';
//                    }
//                } catch (Exception $e) {
//                    $orderBarcodesResponseDto->success = false;
//                    $orderBarcodesResponseDto->message = $e->getMessage();
//                }
//
//                File::delete($filePath);
//            }
        } else {
            $orderBarcodesResponseDto->message = $this->getErrors($cdekResponse);
        }

        return $orderBarcodesResponseDto;
    }

    /**
     * Получить квитанцию к заказу на доставку
     * @param  string  $externalId
     * @return DeliveryOrderReceiptResponseDto
     */
    public function receipt(string $externalId): DeliveryOrderReceiptResponseDto
    {
        $orderReceiptResponseDto = new DeliveryOrderReceiptResponseDto();

        $cdekRequest = new Requests\PrintReceiptsRequest([
            'CopyCount' => 1,
        ]);
        $cdekRequest->addOrder(Common\Order::withDispatchNumber($externalId));

        $cdekResponse = $this->service->sendPrintReceiptsRequest($cdekRequest);
        $orderReceiptResponseDto->success = !$cdekResponse->hasErrors();
        if ($orderReceiptResponseDto->success) {
            $orderReceiptResponseDto->success = false;
            $orderReceiptResponseDto->message = "Not implemented"; // TODO delete or fix
//            /** @var FileService $fileService */
//            $fileService = resolve(FileService::class);
//            $filePath = '/tmp/' . $externalId . '_receipt.pdf';
//            if (File::put($filePath, $cdekResponse->getBody())) {
//                try {
//                    $fileId = $fileService->uploadFile(
//                        self::RECEIPTS_FOLDER,
//                        File::basename($filePath),
//                        $filePath
//                    );
//                    if ($fileId) {
//                        $orderReceiptResponseDto->file_id = $fileId;
//                    } else {
//                        $orderReceiptResponseDto->success = false;
//                        $orderReceiptResponseDto->message = 'Ошибка при сохранении файла с квитанцией';
//                    }
//                } catch (Exception $e) {
//                    $orderReceiptResponseDto->success = false;
//                    $orderReceiptResponseDto->message = $e->getMessage();
//                }
//
//                File::delete($filePath);
//            }
        } else {
            $orderReceiptResponseDto->message = $this->getErrors($cdekResponse);
        }

        return $orderReceiptResponseDto;
    }

    /**
     * @param  DeliveryOrderRequestDto  $requestDto
     * @param  bool  $isUpdate
     * @return DeliveryOrderResponseDto
     */
    protected function upsert(DeliveryOrderRequestDto $requestDto, bool $isUpdate = false): DeliveryOrderResponseDto
    {
        $responseDto = new DeliveryOrderResponseDto();

        //Получаем коды городов у СДЭК
        /** @var Collection|CityDeliveryServiceLink[] $deliveryCities */
        $deliveryCities = CityDeliveryServiceLink::query()
            ->whereIn('city_guid', [$requestDto->sender->city_guid, $requestDto->recipient->city_guid])
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->get()
            ->keyBy('city_guid');
        if (!$deliveryCities->has($requestDto->sender->city_guid) ||
            !$deliveryCities->has($requestDto->recipient->city_guid)) {
            $responseDto->success = false;
            $responseDto->message = 'Населенный пункт отправления/доставки не найден';

            return $responseDto;
        }

        $order = new Common\Order([
            'Number' => $requestDto->order->number,
            'DispatchNumber' => $requestDto->order->external_id,
            'SendCityCode' => $deliveryCities->has($requestDto->sender->city_guid) ?
                $deliveryCities[$requestDto->sender->city_guid]->getCdekId() : null,
            'RecCityCode' => $deliveryCities->has($requestDto->recipient->city_guid) ?
                $deliveryCities[$requestDto->recipient->city_guid]->getCdekId() : null,
            'SendCityPostCode' => $requestDto->sender->post_index,
            'RecCityPostCode' => $requestDto->recipient->post_index,
            'SendCountryCode' => $requestDto->sender->country_code,
            'RecCountryCode' => $requestDto->recipient->country_code,
            'SendCityName' => $requestDto->sender->city,
            'RecCityName' => $requestDto->recipient->city,
            'RecipientName' => $requestDto->recipient->contact_name,
            'RecipientEmail' => $requestDto->recipient->email,
            'Phone' => $requestDto->recipient->phone,
            'TariffTypeCode' => $this->getTariffExternalId($requestDto->order->tariff_id),
            'Comment' => $requestDto->order->description,
        ]);

        $order->setSender(
            Common\Sender::create([
                'Company' => $requestDto->sender->company_name,
                'Name' => $requestDto->sender->contact_name,
                'Phone' => $requestDto->sender->phone,
            ])->setAddress(Common\Address::create([
                'Street' => $requestDto->sender->street,
                'House' => $requestDto->sender->house ?: $requestDto->sender->block,
                'Flat' => $requestDto->sender->flat,
            ]))
        );

        if ($requestDto->sender->is_seller) {
            $order->setSeller(
                Common\Seller::create([
                    'Name' => $requestDto->sender->store_id,
                    'Phone' => $requestDto->sender->phone,
                    'Address' => $requestDto->sender->address_string,
                    'INN' => $requestDto->sender->inn,
                    'OwnershipForm' => 63, //todo Указывать реальную
                ])
            );
        }

        $order->setAddress(Common\Address::create([
            'Street'  => $requestDto->recipient->street,
            'House' => $requestDto->recipient->house ? : $requestDto->recipient->block,
            'Flat' => $requestDto->recipient->flat,
            'PvzCode' => $requestDto->order->delivery_method == DeliveryMethod::METHOD_PICKUP ?
                $this->getPointExternalId($requestDto->order->point_out_id) : null,
        ]));

        $order->addScheduleAttempt(Common\Attempt::create([
            /*
             * Идентификационный номер расписания по базе ИМ. По умолчанию можно использовать 1
             */
            'ID' => 1,
            'Date' => DateTimeImmutable::createFromFormat('Y-m-d', $requestDto->order->delivery_date),
            'TimeBeg' => $requestDto->order->delivery_time_start ?
                DateTimeImmutable::createFromFormat('H:i:s', $requestDto->order->delivery_time_start) : null,
            'TimeEnd' => $requestDto->order->delivery_time_end ?
                DateTimeImmutable::createFromFormat('H:i:s', $requestDto->order->delivery_time_end) : null,
        ]));

        $number = 1;
        foreach ($requestDto->places as $deliveryOrderPlaceDto) {
            $package = Common\Package::create([
                'Number' => $number++,
                'BarCode' => $deliveryOrderPlaceDto->code,
                'Weight' => $deliveryOrderPlaceDto->weight,
                'SizeA' => (int)ceil(mm2cm($deliveryOrderPlaceDto->length)),
                'SizeB' => (int)ceil(mm2cm($deliveryOrderPlaceDto->width)),
                'SizeC' => (int)ceil(mm2cm($deliveryOrderPlaceDto->height)),
            ]);
            $order->addPackage($package);

            if (!is_null($deliveryOrderPlaceDto->items)) {
                foreach ($deliveryOrderPlaceDto->items as $deliveryOrderItemDto) {
                    $package->addItem(new Common\Item([
                        'WareKey' => $deliveryOrderItemDto->articul,
                        'Cost' => $deliveryOrderItemDto->cost,
                        'Payment' => $deliveryOrderItemDto->price,
                        'PaymentVATSum' => $deliveryOrderItemDto->cost_vat,
                        'Weight' => $deliveryOrderItemDto->weight,
                        'Amount' => $deliveryOrderItemDto->quantity,
                        'Comment' => $deliveryOrderItemDto->name,
                    ]));
                }
            }
        }

        if ($isUpdate) {
            $cdekRequest = Requests\UpdateRequest::create([
                /*
                 * Номер акта приема-передачи. Идентификатор заказа в ИС клиента СДЭК.
                 */
                'Number' => 1,
            ])->addOrder($order);

            $cdekResponse = $this->service->sendUpdateRequest($cdekRequest);
        } else {
            $cdekRequest = Requests\DeliveryRequest::create([
                /*
                 * Номер акта приема-передачи/ТТН
                 * (сопроводительного документа при передаче груза СДЭК, формируется в системе ИМ).
                 *  Идентификатор реестра грузов в ИС клиента СДЭК. По умолчанию можно использовать 1.
                 */
                'Number' => 1,
            ])->addOrder($order);

            $cdekResponse = $this->service->sendDeliveryRequest($cdekRequest);
        }

        $responseDto->success = !$cdekResponse->hasErrors();
        if ($responseDto->success) {
            foreach ($cdekResponse->getOrders() as $order) {
                if ($order->getNumber() == $requestDto->order->number) {
                    $responseDto->external_id = $order->getDispatchNumber();

                    break;
                }
            }
        } else {
            $responseDto->message = $this->getErrors($cdekResponse);
        }

        return $responseDto;
    }

    /**
     * @param  int  $id
     * @return string
     */
    protected function getTariffExternalId(int $id): string
    {
        /** @var Tariff $tariff */
        $tariff = Tariff::query()
            ->where('id', $id)
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->select(['external_id'])
            ->first();

        return !is_null($tariff) ? $tariff->external_id : '';
    }

    /**
     * @param  int  $id
     * @return string
     */
    protected function getPointExternalId(int $id): string
    {
        /** @var Point $point */
        $point = Point::query()
            ->where('id', $id)
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->select(['external_id'])
            ->first();

        return !is_null($point) ? $point->external_id : '';
    }

    /**
     * @param  \CdekSDK\Contracts\Response  $response
     * @return string
     */
    protected function getErrors(\CdekSDK\Contracts\Response $response): string
    {
        $errors = [];
        foreach ($response->getMessages() as $message) {
            $errors[] = join(' ', array_filter([$message->getErrorCode(), $message->getMessage()]));
        }

        return join('; ', $errors);
    }
}
