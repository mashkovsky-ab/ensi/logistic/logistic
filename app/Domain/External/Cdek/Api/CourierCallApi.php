<?php

namespace App\Domain\External\Cdek\Api;

use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\Cdek\Dto\ExternalStatusCheck;
use App\Domain\External\Cdek\Dto\Response\ResponseStatusesDto;
use App\Domain\External\CourierCallInterface;
use App\Domain\External\Dto\Request\CourierCallRequestDto;
use App\Domain\External\Dto\Response\CourierCallCancelResponseDto;
use App\Domain\External\Dto\Response\CourierCallResponseDto;
use CdekSDK\CdekClient;
use CdekSDK2\BaseTypes;
use CdekSDK2\Client;
use CdekSDK2\Constants;
use CdekSDK2\Exceptions\RequestException;
use CdekSDK2\Http\ApiResponse;
use DateTime;
use Exception;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;

class CourierCallApi extends AbstractApi implements CourierCallInterface
{
    /** @var Client */
    protected $service;

    /** @var Serializer */
    protected $serializer;

    /**
     * CourierCallApi constructor.
     */
    public function __construct(CdekClient $service, Client $service2)
    {
        parent::__construct($service);

        $this->service = $service2;
        $this->serializer = SerializerBuilder::create()->setPropertyNamingStrategy(
            new SerializedNameAnnotationStrategy(
                new IdenticalPropertyNamingStrategy()
            )
        )->build();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function create(CourierCallRequestDto $requestDto): CourierCallResponseDto
    {
        $responseDto = new CourierCallResponseDto();
        $cargo = $requestDto->cargo;
        $sender = $requestDto->sender;

        //Получаем код города для склада продавца у СДЭК
        /** @var CityDeliveryServiceLink $deliveryCity */
        $deliveryCity = CityDeliveryServiceLink::query()
            ->where('city_guid', $sender->city_guid)
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->first();

        /** @var BaseTypes\Intake $intake */
        $intake = BaseTypes\Intake::create();
        $intake->intake_date = (new DateTime($cargo->date))->format('Y-m-d');
        $intake->intake_time_from = (new DateTime($cargo->time_start))->format('H:i');
        $intake->intake_time_to = (new DateTime($cargo->time_end))->format('H:i');
        $intake->name = "Груз от " . $cargo->date . " из " . $sender->address_string;
        $intake->weight = $cargo->weight;
        $intake->length = (int)ceil(mm2cm($cargo->length));
        $intake->width = (int)ceil(mm2cm($cargo->width));
        $intake->height = (int)ceil(mm2cm($cargo->height));

        $intake->sender = BaseTypes\Contact::create([
            'name' => $sender->contact_name,
            'company' => $sender->company_name,
            'phones' => [
                BaseTypes\Phone::create(['number' => $sender->phone]),
            ],
        ]);

        /** @var BaseTypes\Location $location */
        $location = BaseTypes\Location::create();
        $location->code = $deliveryCity? $deliveryCity->getCdekId() : '';
        $location->fias_guid = $sender->city_guid;
        $location->postal_code = $sender->post_index;
        $location->country_code = $sender->country_code;
        $location->region = $sender->region;
        $location->sub_region = $sender->area;
        $location->city = $sender->city;
        $location->address = join(', ', array_filter([$sender->street, $sender->house, $sender->flat])) ?
            : $sender->address_string;
        $intake->from_location = $location;

        $this->logger->debug('Create CourierCall request', $this->toArray($intake));
        $cdekResponse = $this->service->intakes()->add($intake);

        $responseDto->success = $cdekResponse->getStatus() == 202;
        if ($responseDto->success) {
            $responseIntake = new BaseTypes\Intake(json_decode($cdekResponse->getBody(), true)['entity']);
            $this->logger->debug('Create CourierCall response', $this->toArray($responseIntake));
            $responseDto->external_id = $responseIntake->uuid;
            $responseDto->special_courier_call_status = $this->check($responseIntake->uuid)->error;
            $responseDto->cdek_intake_number = $this->check($responseIntake->uuid)->intake_number;
        } else {
            $responseDto->message = $this->getErrors($cdekResponse);
            $this->logger->debug('Create CourierCall error response', ['error' => $responseDto->message]);
        }

        return $responseDto;
    }

    /**
     * @inheritDoc
     * @throws RequestException
     */
    public function cancel(string $externalId): CourierCallCancelResponseDto
    {
        $this->logger->debug('Cancel CourierCall request', ['external_id' => $externalId]);
        $cdekResponse = $this->service->intakes()->delete($externalId);

        $responseDto = new CourierCallCancelResponseDto();
        $responseDto->success = $cdekResponse->getStatus() == 200;
        $responseDto->message = $this->getErrors($cdekResponse);
        if ($responseDto->message) {
            $this->logger->debug('Cancel CourierCall error response', ['error' => $responseDto->message]);
        } else {
            $this->logger->debug('Cancel CourierCall response', ['Ok']);
        }

        return $responseDto;
    }

    /**
     * Проверить ошибки в заявке на вызов курьера
     * @example https://confluence.cdek.ru/pages/viewpage.action?pageId=29948360
     * @param string $uuid
     * @return ExternalStatusCheck
     * @throws RequestException
     */
    public function check(string $uuid): ExternalStatusCheck
    {
        $cdekResponse = $this->service->intakes()->get($uuid);
        $responseIntake = new BaseTypes\Intake(
            json_decode($cdekResponse->getBody(), true)['entity']
        );

        $statusCheckDto = new ExternalStatusCheck();
        if ($responseIntake) {
            /** @var array $status */
            $status = array_pop($responseIntake->statuses);
            if ($status['code'] == Constants::STATUS_CREATED) {
                $statusCheckDto->intake_number = $responseIntake->intake_number ?? 'Intake number is not provided';
            } else {
                $statusCheckDto->error = ResponseStatusesDto::uuidCheckStatus($status);
            }
        } else {
            $statusCheckDto->error = 'Не удалось связаться со CDEK';
        }

        return $statusCheckDto;
    }

    /**
     * @param  ApiResponse  $apiResponse
     * @return string
     */
    protected function getErrors(ApiResponse $apiResponse): string
    {
        $errors = $apiResponse->getErrors();
        if (!$errors) {
            $body = json_decode($apiResponse->getBody(), true);
            if (isset($body['requests'][0]['errors'])) {
                $errors = $body['requests'][0]['errors'];
            } else {
                return '';
            }
        }

        return join('; ', array_map(function ($value) {
            return $value['code'] . ': ' . $value['message'];
        }, $errors));
    }

    /**
     * @param $data
     * @return array
     */
    public function toArray($data): array
    {
        return $this->serializer->toArray($data);
    }
}
