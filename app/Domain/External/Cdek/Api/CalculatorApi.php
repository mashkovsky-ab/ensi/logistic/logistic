<?php

namespace App\Domain\External\Cdek\Api;

use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\External\CalculatorInterface;
use App\Domain\External\Cdek\Dto\CdekAvailableTimeDto;
use App\Domain\External\Dto\Request\CalculatorRequestDto;
use App\Domain\External\Dto\Response\CalculatorResponseDto;
use App\Domain\External\Dto\Response\Part\Calculator\CalculatorDeliveryMethodDto;
use App\Domain\External\Dto\Response\Part\Calculator\TariffDto;
use CdekSDK\CdekClient;
use CdekSDK\Requests\CalculationWithTariffListAuthorizedRequest;
use CdekSDK\Requests\CalculationWithTariffListRequest;
use CdekSDK\Responses\CalculationWithTariffListResponse;
use Illuminate\Support\Collection;

/**
 * Class Calculator
 * @package App\Domain\External\Cdek\Api
 */
class CalculatorApi extends AbstractApi implements CalculatorInterface
{
    /** @var Collection|Tariff[] - тарифы СДЭК */
    protected $tariffsCache;

    /**
     * CalculatorApi constructor.
     */
    public function __construct(CdekClient $service)
    {
        parent::__construct($service);

        if (!app()->isProduction() || config('app.debug')) {
            $this->service->setLogger($this->logger);
        }
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function calculate(CalculatorRequestDto $requestDto): CalculatorResponseDto
    {
        $responseDto = new CalculatorResponseDto();

        //Получаем коды городов у СДЭК
        /** @var Collection|CityDeliveryServiceLink[] $deliveryCities */
        $deliveryCities = CityDeliveryServiceLink::query()
            ->whereIn('city_guid', [$requestDto->city_guid_from, $requestDto->city_guid_to])
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->get()
            ->keyBy('city_guid');
        if (!$deliveryCities->has($requestDto->city_guid_from) || !$deliveryCities->has($requestDto->city_guid_to)) {
            return $responseDto;
        }

        $cdekRequest = app()->isProduction() ?
            new CalculationWithTariffListAuthorizedRequest() : new CalculationWithTariffListRequest();
        $cdekRequest->setSenderCityId($deliveryCities[$requestDto->city_guid_from]->getCdekId())
            ->setReceiverCityId($deliveryCities[$requestDto->city_guid_to]->getCdekId())
            ->addPackage([
                'weight' => g2kg($requestDto->weight),
                'length' => (int)ceil(mm2cm($requestDto->length)),
                'width'  => (int)ceil(mm2cm($requestDto->width)),
                'height' => (int)ceil(mm2cm($requestDto->height)),
            ]);

        $this->loadTariffs();
        foreach ($this->tariffsCache->keys() as $tariffXmlId) {
            $cdekRequest->addTariffToList($tariffXmlId);
        }

        $cdekResponse = $this->service->sendCalculationWithTariffListRequest($cdekRequest);
        if (!$cdekResponse->hasErrors()) {
            $deliveryMethods = DeliveryMethod::validValues();
            foreach ($deliveryMethods as $deliveryMethodId) {
                $this->addMethod($deliveryMethodId, $requestDto, $responseDto, $cdekResponse);
            }
        }

        return $responseDto;
    }

    /**
     * Добавить способ доставки
     * @param  int $deliveryMethod
     * @param  CalculatorRequestDto $requestDto
     * @param  \App\Domain\External\Dto\Response\CalculatorResponseDto  $responseDto
     * @param  CalculationWithTariffListResponse  $cdekResponse
     * @throws \Exception
     */
    protected function addMethod(
        int $deliveryMethod,
        CalculatorRequestDto $requestDto,
        CalculatorResponseDto $responseDto,
        CalculationWithTariffListResponse $cdekResponse
    ): void {
        /*
         * СДЭК не возвращает список точек самовывоза,
         * поэтому возвращаем все точки самовывоза из пункта назначения
         */
        $points = Point::query()
            ->where('active', true)
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->where('city_guid', $requestDto->city_guid_to)
            ->select('id')
            ->get();
        $region = get_region_by_city_guid($requestDto->city_guid_to);

        $calculatorItemDto = new CalculatorDeliveryMethodDto();
        $calculatorItemDto->delivery_service = DeliveryService::SERVICE_CDEK;
        $calculatorItemDto->delivery_method = $deliveryMethod;

        $hasTariffs = false;
        foreach ($cdekResponse->getResults() as $result) {
            if (!$result->hasErrors()) {
                $tariff = $this->tariffsCache->has($result->getTariffId()) ?
                    $this->tariffsCache[$result->getTariffId()] : null;
                if (!is_null($tariff) && $tariff->delivery_method == $deliveryMethod) {
                    $tariffDto = new TariffDto([
                        'days_min' => $result->getDeliveryPeriodMin(),
                        'days_max' => $result->getDeliveryPeriodMax(),
                    ]);
                    foreach ($tariffDto->available_dates as $key => $availableDateDto) {
                        $availableTimeDtos = CdekAvailableTimeDto::available(
                            $availableDateDto->date,
                            $region ? $region->guid : null
                        );
                        if ($availableTimeDtos) {
                            $availableDateDto->addAvailableTimes($availableTimeDtos);
                        } else {
                            $tariffDto->available_dates->forget($key);
                        }
                    }
                    $tariffDto->external_id = $tariff->id;
                    $tariffDto->name = $tariff->name;
                    $tariffDto->delivery_service_cost = $result->getPrice();

                    if ($deliveryMethod == DeliveryMethod::METHOD_PICKUP) {
                        $tariffDto->days_min = $result->getDeliveryPeriodMin();
                        $tariffDto->days_max = $result->getDeliveryPeriodMax();

                        if ($points->isNotEmpty()) {
                            $tariffDto->point_ids = $points->pluck('id')->toArray();

                            $calculatorItemDto->tariffs->push($tariffDto);
                        }
                    } else {
                        $calculatorItemDto->tariffs->push($tariffDto);
                    }
                    $hasTariffs = true;
                }
            }
        }

        if ($hasTariffs) {
            $responseDto->methods->push($calculatorItemDto);
        }
    }

    protected function loadTariffs(): void
    {
        $this->tariffsCache = Tariff::query()
            ->where('delivery_service', DeliveryService::SERVICE_CDEK)
            ->where('name', '=', 'Посылка склад-дверь')
            ->orWhere('name', '=', 'Посылка склад-склад')
            ->get()
            ->keyBy('external_id');
    }
}
