<?php

namespace App\Domain\External\Cdek\Api;

use CdekSDK\CdekClient;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractApi
 * @package App\Domain\External\Cdek\Api
 */
class AbstractApi
{
    /** @var CdekClient */
    protected $service;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(CdekClient $service)
    {
        $this->service = $service;
        $this->logger = Log::channel('cdek');
    }
}
