<?php

namespace App\Domain\External;

use App\Domain\External\Dto\Request\DeliveryOrderRequestDto;
use App\Domain\External\Dto\Response\DeliveryOrderBarcodesResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrderCancelResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrderResponseDto;
use App\Domain\External\Dto\Response\DeliveryOrdersStatusResponseDto;

interface DeliveryOrderInterface
{
    /** @var string - папка в сервисе file для хранения файлов со штрихкодами */
    public const BARCODES_FOLDER = 'delivery-order-barcodes';

    public function create(DeliveryOrderRequestDto $requestDto): DeliveryOrderResponseDto;

    public function update(DeliveryOrderRequestDto $requestDto): DeliveryOrderResponseDto;

    /**
     * @param  string $externalId - id заказа на доставку во внешней системе
     * @return DeliveryOrderCancelResponseDto
     */
    public function cancel(string $externalId): DeliveryOrderCancelResponseDto;

    /**
     * @param  array $externalIds - id заказов на доставку во внешней системе
     * @return DeliveryOrdersStatusResponseDto
     */
    public function status(array $externalIds): DeliveryOrdersStatusResponseDto;

    /**
     * @param  string  $externalId - id заказа на доставку во внешней системе
     * @param  array|string[] $placesExternalId - массив id мест заказа на доставку во внешней системе
     * @return DeliveryOrderBarcodesResponseDto
     */
    public function barcodes(string $externalId, array $placesExternalId = []): DeliveryOrderBarcodesResponseDto;
}
