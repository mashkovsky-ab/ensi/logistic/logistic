<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos;

use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\ShipmentMethod;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\Kafka\Messages\Listen\Dtos\Factories\DeliveryDtoFactory;
use App\Domain\Support\Data\Address;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\Shipment;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

/**
 * Класс-dto для сущности "Доставка"
 * Class DeliveryDto
 *
 * @property int $id - id отправления
 * @property int $order_id - id заказа
 * @property string $number - номер отправления
 * @property int $cost - стоимость отправления, полученная от службы доставки (не влияет на общую стоимость доставки по заказу!) (коп.)
 *
 * @property int $status - статус
 * @property Carbon $status_at
 *
 * @property string $date
 * @property TimeslotDto|null $timeslot
 *
 * @property float $width - ширина
 * @property float $height - высота
 * @property float $length - длина
 * @property float $weight - вес
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class DeliveryDto extends AbstractDto
{
    public function __construct($attributes = [])
    {
        if (is_array($attributes['timeslot'])) {
            $attributes['timeslot'] = new TimeslotDto($attributes['timeslot']);
        }

        parent::__construct($attributes);

        $this->casts = array_merge($this->casts, [
            'status_at' => 'datetime',
        ]);
    }

    public function toDeliveryOrderFields(): array
    {
        $ordersApi = resolve(OrdersApi::class);
        $sellersApi = resolve(SellersApi::class);
        $storesApi = resolve(StoresApi::class);

        $order = $ordersApi->getOrder($this->order_id, 'deliveries.shipments.orderItems')->getData();
        /** @var Delivery $delivery */
        $delivery = Arr::first(array_filter($order->getDeliveries(), fn(Delivery $delivery) => $delivery->getId() == $this->id));
        /** @var Shipment $firstShipment */
        $firstShipment = Arr::first($delivery->getShipments());
        $seller = $sellersApi->getSeller($firstShipment->getSellerId())->getData();
        $store = $storesApi->getStore($firstShipment->getStoreId(), 'contacts,pickup_times')->getData();
        /** @var StoreContact $contact */
        $contact = Arr::first($store->getContacts());
        /** @var Point $point */
        $point = null;
        if ($pointId = $order->getDeliveryPointId()) {
            $point = Point::query()->find($pointId);
        }

        $codCost = $order->getPrice() - $order->getDeliveryPrice();
        $fields = [
            'delivery_id' => $delivery->getId(),
            'delivery_service_id' => $order->getDeliveryService(),

            'number' => $delivery->getNumber(),
            'height' => $delivery->getHeight(),
            'length' => $delivery->getLength(),
            'width' => $delivery->getWidth(),
            'weight' => $delivery->getWeight(),
            'shipment_method' => ShipmentMethod::METHOD_DS_COURIER, //todo Доработать в будущем настройку своего способа отгрузки для каждого продавца #84009
            'delivery_method' => $order->getDeliveryMethod(),
            'tariff_id' => $order->getDeliveryTariffId(),
            'delivery_date' => $delivery->getDate(),
            'point_out_id' => $order->getDeliveryPointId(),
            'delivery_time_start' => $delivery->getTimeslot()?->getFrom(),
            'delivery_time_end' => $delivery->getTimeslot()?->getTo(),
            'delivery_time_code' => $delivery->getTimeslot()?->getId(),

            'assessed_cost' => $codCost,
            'delivery_cost' => $order->getDeliveryPrice(),
            'delivery_cost_pay' => in_array($order->getPaymentStatus(), [PaymentStatusEnum::HOLD, PaymentStatusEnum::PAID]) ? 0 : $order->getDeliveryPrice(),
            'cod_cost' => $codCost,
            'is_delivery_payed_by_recipient' => false, //todo Когда будет постоплата, указать true в случае постоплаты

            'sender_is_seller' => true, //todo Если в отправлении будут отгрузки разных продавцов, то указывать данные маркетплейса
            'sender_inn' => $seller->getInn(),
            'sender_address' => Address::makeFromBuStoreAddress($store->getAddress()),
            'sender_company_name' => $seller->getLegalName(),
            'sender_contact_name' => $contact->getName(),
            'sender_email' => $contact->getEmail(),
            'sender_phone' => $contact->getPhone(),

            'recipient_address' => $order->getDeliveryMethod() == DeliveryMethod::METHOD_DELIVERY ? Address::makeFromOmsDeliveryAddress($order->getDeliveryAddress()) : $point?->address,
            'recipient_contact_name' => $order->getReceiverName(),
            'recipient_email' => $order->getReceiverEmail(),
            'recipient_phone' => $order->getReceiverPhone(),
            'recipient_comment' => $order->getClientComment(),
        ];
        $placeItems = [];
        foreach ($firstShipment->getOrderItems() as $orderItem) {
            $placeItems[] = [
                'vendor_code' => $orderItem->getProductBarcode(),
                'barcode' => $orderItem->getProductBarcode(),
                'name' => $orderItem->getName(),
                'qty' => $orderItem->getQty(),
                'height' => $orderItem->getProductHeight(),
                'length' => $orderItem->getProductLength(),
                'width' => $orderItem->getProductWidth(),
                'weight' => $orderItem->getProductWeight(),
                'assessed_cost' => $orderItem->getPrice(),
                'cost' => $orderItem->getCost(),
                'cost_vat' => 0, //todo Доделать, если появится НДС в Ensi
                'price' => 0, //todo Когда будет постоплата, указать реальную стоимость к оплате за товар
            ];
        }
        $place = [
            'number' => $firstShipment->getNumber(),
            'items' => $placeItems,
        ];
        $fields['places'] = [$place];

        return $fields;
    }

    public static function factory(): DeliveryDtoFactory
    {
        return DeliveryDtoFactory::new();
    }
}
