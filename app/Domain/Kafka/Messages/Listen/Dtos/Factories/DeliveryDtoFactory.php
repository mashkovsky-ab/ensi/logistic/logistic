<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos\Factories;

use App\Domain\Kafka\Messages\Listen\Dtos\DeliveryDto;
use App\Domain\Kafka\Messages\Listen\Dtos\TimeslotDto;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\TestFactories\Factory;

class DeliveryDtoFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(DeliveryStatusEnum::getAllowableEnumValues()),
            'number' => $this->faker->unique()->numerify('######-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'date' => $this->faker->date(),
            'timeslot' => $this->faker->boolean() ? TimeslotDto::factory()->make() : null,
        ];
    }

    public function make(array $extra = []): DeliveryDto
    {
        return new DeliveryDto($this->makeArray($extra));
    }
}
