<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos\Factories;

use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use App\Domain\Kafka\Messages\Listen\Dtos\StorePickupTimeDto;
use Ensi\TestFactories\Factory;

class StorePickupTimeDtoFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'store_id' => $this->faker->randomNumber(),
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}",
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'cargo_export_time' => $this->faker->time('H:i'),
            'delivery_service_id' => $this->faker->randomElement(DeliveryServiceEnum::toValues()),
        ];
    }

    public function make(array $extra = []): StorePickupTimeDto
    {
        return new StorePickupTimeDto($this->makeArray($extra));
    }
}
