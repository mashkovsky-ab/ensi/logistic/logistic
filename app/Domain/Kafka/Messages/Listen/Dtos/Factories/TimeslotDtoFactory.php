<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos\Factories;

use App\Domain\Kafka\Messages\Listen\Dtos\TimeslotDto;
use Ensi\TestFactories\Factory;

class TimeslotDtoFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'from' => $this->faker->numerify('##:##'),
            'to' => $this->faker->numerify('##:##'),
        ];
    }

    public function make(array $extra = []): TimeslotDto
    {
        return new TimeslotDto($this->makeArray($extra));
    }
}
