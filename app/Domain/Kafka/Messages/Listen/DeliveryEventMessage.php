<?php

namespace App\Domain\Kafka\Messages\Listen;

use App\Domain\Kafka\Messages\Listen\Dtos\DeliveryDto;

/**
 * @property DeliveryDto $attributes
 */
class DeliveryEventMessage extends AbstractEventMessage
{
    protected function getDtoClass(): string
    {
        return DeliveryDto::class;
    }
}
