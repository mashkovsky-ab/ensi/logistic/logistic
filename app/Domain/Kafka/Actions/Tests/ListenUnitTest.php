<?php

use App\Domain\CargoOrders\Models\StorePickupTime;
use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\External\Actions\DeliveryOrder\CancelDeliveryOrderAction as CancelExternalDeliveryOrderAction;
use App\Domain\External\Actions\DeliveryOrder\CreateDeliveryOrderAction as CreateExternalDeliveryOrderAction;
use App\Domain\External\Actions\DeliveryOrder\UpdateDeliveryOrderAction as UpdateExternalDeliveryOrderAction;
use App\Domain\External\Dto\Response\DeliveryOrderResponseDto;
use App\Domain\Kafka\Actions\Listen\DeliveryListenAction;
use App\Domain\Kafka\Actions\Listen\StorePickupTimeListenAction;
use App\Domain\Kafka\Messages\Listen\DeliveryEventMessage;
use App\Domain\Kafka\Messages\Listen\StorePickupTimeEventMessage;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\DeliveryEventMessageFactory;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\StorePickupTimeEventMessageFactory;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Illuminate\Support\Arr;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use Tests\IntegrationTestCase;
use Tests\UnitTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');


// region StorePickupTimeListenAction
test("Action StorePickupTimeListenAction create success", function () {
    /** @var IntegrationTestCase $this */
    $storePickupTimeExternalId = 1;
    $message = StorePickupTimeEventMessageFactory::new()->make([
        'event' => StorePickupTimeEventMessage::CREATE,
        'attributes' => [
            'id' => $storePickupTimeExternalId,
        ],
    ]);

    resolve(StorePickupTimeListenAction::class)->execute($message);

    assertDatabaseHas((new StorePickupTime())->getTable(), [
        'external_id' => $storePickupTimeExternalId,
    ]);
});

test("Action StorePickupTimeListenAction patch success", function () {
    /** @var IntegrationTestCase $this */
    $storePickupTimeExternalId = 1;
    /** @var StorePickupTime $storePickupTime */
    $storePickupTime = StorePickupTime::factory()->create(['external_id' => $storePickupTimeExternalId]);

    $message = StorePickupTimeEventMessageFactory::new()->make([
        'event' => StorePickupTimeEventMessage::UPDATE,
        'attributes' => [
            'id' => $storePickupTimeExternalId,
        ],
    ]);
    $messageData = StorePickupTimeEventMessage::makeFromRdKafka($message);

    $messageUpdateData = Arr::only($messageData->attributes->toArray(), $messageData->dirty);
    resolve(StorePickupTimeListenAction::class)->execute($message);

    assertDatabaseHas(
        (new StorePickupTime())->getTable(),
        array_merge($storePickupTime->toArray(), $messageUpdateData)
    );
});

test("Action StorePickupTimeListenAction delete success", function () {
    /** @var IntegrationTestCase $this */
    $storePickupTimeExternalId = 1;
    /** @var StorePickupTime $storePickupTime */
    $storePickupTime = StorePickupTime::factory()->create(['external_id' => $storePickupTimeExternalId]);

    $message = StorePickupTimeEventMessageFactory::new()->make([
        'event' => StorePickupTimeEventMessage::DELETE,
        'attributes' => [
            'id' => $storePickupTimeExternalId,
        ],
    ]);

    resolve(StorePickupTimeListenAction::class)->execute($message);

    assertModelMissing($storePickupTime);
});
// endregion

// region DeliveryListenAction
test("Action DeliveryListenAction not create success", function () {
    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::CREATE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => DeliveryStatusEnum::NEW,
        ],
    ]);

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseMissing((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryId,
    ]);
});
test("Action DeliveryListenAction create success", function () {
    /** @var UnitTestCase $this */
    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::CREATE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => DeliveryStatusEnum::ASSEMBLED,
        ],
    ]);

    if (!Tariff::query()->find(1)) {
        Tariff::factory()->create(['id' => 1]);
    }
    if (!Point::query()->find(1)) {
        Point::factory()->create(['id' => 1]);
    }

    $this->mockOrdersOrdersApi();
    $this->mockBuSellersApi();
    $this->mockBuStoresApi();
    $this->mock(CreateExternalDeliveryOrderAction::class)
        ->allows([
            'execute' => new DeliveryOrderResponseDto(['external_id' => 1]),
        ]);
    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'delivery_id' => $deliveryId,
    ]);
});

test("Action DeliveryListenAction patch success", function () {
    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create([
        'delivery_id' => $deliveryId,
        'delivery_service_id' => DeliveryServiceEnum::B2CPL()->value,
    ]);

    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::UPDATE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => DeliveryStatusEnum::ASSEMBLED,
        ],
    ]);

    if (!Tariff::query()->find(1)) {
        Tariff::factory()->create(['id' => 1]);
    }
    if (!Point::query()->find(1)) {
        Point::factory()->create(['id' => 1]);
    }

    $this->mockOrdersOrdersApi();
    $this->mockBuSellersApi();
    $this->mockBuStoresApi();
    $this->mock(UpdateExternalDeliveryOrderAction::class)
        ->allows([
            'execute' => new DeliveryOrderResponseDto(),
        ]);
    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'delivery_id' => $deliveryId,
        'delivery_cost' => 40,
    ]);
});

test("Action DeliveryListenAction cancel success", function () {
    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create([
        'delivery_id' => $deliveryId,
        'delivery_service_id' => DeliveryServiceEnum::B2CPL()->value,
    ]);

    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::UPDATE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => DeliveryStatusEnum::CANCELED,
        ],
    ]);

    $this->mock(CancelExternalDeliveryOrderAction::class)->shouldReceive('execute');

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => DeliveryOrderStatus::CANCELED,
    ]);
});

test("Action DeliveryListenAction delete success", function () {
    /** @var IntegrationTestCase $this */
    $deliveryId = 1;
    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create([
        'delivery_id' => $deliveryId,
        'delivery_service_id' => DeliveryServiceEnum::B2CPL()->value,
    ]);

    $message = DeliveryEventMessageFactory::new()->make([
        'event' => DeliveryEventMessage::DELETE,
        'attributes' => [
            'id' => $deliveryId,
            'status' => DeliveryStatusEnum::CANCELED,
        ],
    ]);

    $this->mock(CancelExternalDeliveryOrderAction::class)->shouldReceive('execute');

    resolve(DeliveryListenAction::class)->execute($message);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => DeliveryOrderStatus::CANCELED,
    ]);
});
// endregion
