<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\DeliveryOrders\Actions\DeliveryOrder\CancelDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\CreateDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\PatchDeliveryOrderAction;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Messages\Listen\DeliveryEventMessage;
use App\Domain\Kafka\Messages\Listen\Dtos\DeliveryDto;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use RdKafka\Message;

class DeliveryListenAction
{
    public function __construct(
        protected CreateDeliveryOrderAction $createDeliveryOrderAction,
        protected PatchDeliveryOrderAction $patchDeliveryOrderAction,
        protected CancelDeliveryOrderAction $cancelDeliveryOrderAction,
    ) {
    }

    public function execute(Message $message)
    {
        $eventMessage = DeliveryEventMessage::makeFromRdKafka($message);
        /** @var DeliveryDto $deliveryDto */
        $deliveryDto = $eventMessage->attributes;
        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = DeliveryOrder::query()->where('delivery_id', $deliveryDto->id)->first();

        switch ($eventMessage->event) {
            case DeliveryEventMessage::CREATE:
            case DeliveryEventMessage::UPDATE:
                if ($deliveryOrder?->external_id) {
                    if ($deliveryDto->status == DeliveryStatusEnum::CANCELED) {
                        $this->cancelDeliveryOrderAction->execute($deliveryOrder->id);
                    } else {
                        $this->patchDeliveryOrderAction->patchFromDeliveryDto($deliveryOrder->id, $deliveryDto);
                    }
                } else {
                    $this->createDeliveryOrderAction->createFromDeliveryDto($deliveryDto);
                }

                break;
            case DeliveryEventMessage::DELETE:
                if ($deliveryOrder?->external_id && $deliveryDto->status == DeliveryStatusEnum::CANCELED) {
                    $this->cancelDeliveryOrderAction->execute($deliveryOrder->id);
                }

                break;
        }
    }
}
