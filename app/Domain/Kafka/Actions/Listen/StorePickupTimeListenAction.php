<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\CargoOrders\Actions\CreateStorePickupTimeAction;
use App\Domain\CargoOrders\Actions\DeleteStorePickupTimeAction;
use App\Domain\CargoOrders\Actions\PatchStorePickupTimeAction;
use App\Domain\Kafka\Messages\Listen\StorePickupTimeEventMessage;
use Illuminate\Support\Arr;
use RdKafka\Message;

class StorePickupTimeListenAction
{
    public function __construct(
        protected CreateStorePickupTimeAction $createStorePickupTimeAction,
        protected PatchStorePickupTimeAction $patchStorePickupTimeAction,
        protected DeleteStorePickupTimeAction $deleteStorePickupTimeAction,
    ) {
    }

    public function execute(Message $message)
    {
        $eventMessage = StorePickupTimeEventMessage::makeFromRdKafka($message);

        switch ($eventMessage->event) {
            case StorePickupTimeEventMessage::CREATE:
                $this->createStorePickupTimeAction->execute($eventMessage->attributes->toArray());

                break;
            case StorePickupTimeEventMessage::UPDATE:
                $this->patchStorePickupTimeAction->execute(
                    $eventMessage->attributes->external_id,
                    Arr::only($eventMessage->attributes->toArray(), $eventMessage->dirty)
                );

                break;
            case StorePickupTimeEventMessage::DELETE:
                $this->deleteStorePickupTimeAction->execute($eventMessage->attributes->external_id);
        }
    }
}
