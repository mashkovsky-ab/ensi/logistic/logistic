<?php

namespace App\Domain\Support\Data;

use App\Domain\Support\Data\Tests\Factories\AddressFactory;
use Ensi\BuClient\Dto\StoreFillablePropertiesAddress;
use Ensi\OmsClient\Dto\Address as OmsAddress;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;

/**
 * Class Address
 * @package App\Domain\Support\Data
 * @property string $address_string - полный адрес одной строкой*
 * @property string $country_code - код страны в соответствии с ISO 3166-1 alpha-2
 * @property string $post_index - почтовый индекс*
 * @property string $region - область или республика или край*
 * @property string $region_guid - ID области/республики/края в базе ФИАС*
 * @property string $area - район
 * @property string $area_guid - ID района в базе ФИАС*
 * @property string $city - город или населенный пункт*
 * @property string $city_guid - ID города в базе ФИАС*
 * @property string $street - улица*
 * @property string $house - дом
 * @property string $block - строение/корпус
 * @property string $flat - офис/квартира
 * @property string $floor - этаж
 * @property string $porch - подъезд
 * @property string $intercom - домофон
 * @property string $geo_lat - широта
 * @property string $geo_lon - долгота
 */
class Address extends Fluent
{
    public static function rules(): array
    {
        return [
            "address_string" => "required|string",
            "country_code" => 'nullable|string',
            "post_index" => 'nullable|string',
            "region" => 'nullable|string',
            "region_guid" => 'nullable|string',
            "area" => 'nullable|string',
            "area_guid" => 'nullable|string',
            "city" => 'nullable|string',
            "city_guid" => 'nullable|string',
            "street" => 'nullable|string',
            "house" => 'nullable|string',
            "block" => 'nullable|string',
            "flat" => 'nullable|string',
            "porch" => 'nullable|string',
            "intercom" => 'nullable|string',
            "geo_lat" => "required|string",
            "geo_lon" => "required|string",
        ];
    }

    public static function validateOrFail(array $address): self
    {
        $validator = Validator::make($address, self::rules());

        return new self($validator->validate());
    }

    /**
     * Формирует строку с полным адресом.
     *
     * @param bool $addNewLine Добавить перенос строки перед домофоном (если он задан)
     * @return string
     */
    public function getFullString(bool $addNewLine = false): string
    {
        $addressParts = [
            $this->address_string ?? '',
            $this->porch ? 'под. ' . $this->porch : null,
            $this->floor ? 'эт. ' . $this->floor : null,
            $this->flat ? 'кв. ' . $this->flat : null,
        ];

        $intercomLine = $this->intercom ? 'домофон ' . $this->intercom : null;
        if ($intercomLine) {
            $intercomLine = ($addNewLine ? "\n" : '') . $intercomLine;
        }
        $addressParts[] = $intercomLine;

        return join(', ', array_filter($addressParts));
    }

    public static function factory(): AddressFactory
    {
        return AddressFactory::new();
    }

    public static function makeFromBuStoreAddress(StoreFillablePropertiesAddress $storeAddress): self
    {
        $self = new self();
        $self->address_string = $storeAddress->getAddressString();
        $self->country_code = $storeAddress->getCountryCode();
        $self->post_index = $storeAddress->getPostIndex();
        $self->region = $storeAddress->getAddressString();
        $self->region_guid = $storeAddress->getRegionGuid();
        $self->area = $storeAddress->getArea();
        $self->area_guid = $storeAddress->getAreaGuid();
        $self->city = $storeAddress->getCity();
        $self->city_guid = $storeAddress->getCityGuid();
        $self->street = $storeAddress->getStreet();
        $self->house = $storeAddress->getHouse();
        $self->block = $storeAddress->getBlock();
        $self->flat = $storeAddress->getFlat();
        $self->porch = $storeAddress->getPorch();
        $self->intercom = $storeAddress->getIntercom();
        $self->geo_lat = $storeAddress->getGeoLat();
        $self->geo_lon = $storeAddress->getGeoLon();

        return $self;
    }

    public static function makeFromOmsDeliveryAddress(OmsAddress $address): self
    {
        $self = new self();
        $self->address_string = $address->getAddressString();
        $self->country_code = $address->getCountryCode();
        $self->post_index = $address->getPostIndex();
        $self->region = $address->getAddressString();
        $self->region_guid = $address->getRegionGuid();
        $self->area = $address->getArea();
        $self->area_guid = $address->getAreaGuid();
        $self->city = $address->getCity();
        $self->city_guid = $address->getCityGuid();
        $self->street = $address->getStreet();
        $self->house = $address->getHouse();
        $self->block = $address->getBlock();
        $self->flat = $address->getFlat();
        $self->porch = $address->getPorch();
        $self->intercom = $address->getIntercom();
        $self->geo_lat = $address->getGeoLat();
        $self->geo_lon = $address->getGeoLon();

        return $self;
    }
}
