<?php

namespace App\Domain\Support\Exceptions;

use Exception;
use Throwable;

/**
 * Class DeliveryServiceApiConnectorNotFound
 * @package App\Domain\DeliveryOrders\Exceptions
 */
class DeliveryServiceApiConnectorNotFound extends Exception
{
    /**
     * DeliveryServiceApiConnectorNotFound constructor.
     * @param  int  $deliveryServiceId
     * @param  Throwable|null  $previous
     */
    public function __construct(int $deliveryServiceId, Throwable $previous = null)
    {
        parent::__construct("Не найден API-обработчик для службы доставки с id={$deliveryServiceId}", 500, $previous);
    }
}
