<?php

namespace App\Domain\Support\Actions;

abstract class CalcModelSizesAction
{
    protected float $volume = 0;
    protected int $maxSide = 0;
    protected string $maxSideName;

    public const MAX_WIDTH = 'width';
    public const MAX_HEIGHT = 'height';
    public const MAX_LENGTH = 'length';

    public function fillSizes()
    {
        $this->maxSideName = static::MAX_WIDTH;
        $this->setWeight();

        $this->calcVolume();
        $this->calcMaxSide();
        $avgSide = $this->getAvg();

        if ($this->maxSide <= $avgSide) {
            $this->setWidth($avgSide);
            $this->setHeight($avgSide);
            $this->setLength($avgSide);
        } else {
            $otherSide = sqrt($this->volume/$this->maxSide);
            $this->setWidth($this->maxSideName == static::MAX_WIDTH ? $this->maxSide : $otherSide);
            $this->setHeight($this->maxSideName == static::MAX_HEIGHT ? $this->maxSide : $otherSide);
            $this->setLength($this->maxSideName == static::MAX_LENGTH ? $this->maxSide : $otherSide);
        }
    }

    /**
     * Рассчитать объем
     */
    abstract protected function calcVolume(): void;

    /**
     * Рассчитать значение максимальной стороны (длины, ширины или высоты)
     */
    abstract protected function calcMaxSide(): void;

    protected function getAvg(): float
    {
        return (float)pow($this->volume, 1/3);
    }

    protected function checkMaxSize($value, $name): void
    {
        if ($value > $this->maxSide) {
            $this->maxSide = $value;
            $this->maxSideName = $name;
        }
    }

    abstract protected function setWeight(): void;

    abstract protected function setWidth(float $width): void;

    abstract protected function setHeight(float $height): void;

    abstract protected function setLength(float $length): void;
}
