<?php

namespace App\Domain\Support\Tests;

use Ensi\TestFactories\Factory;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

abstract class BaseDtoFactory extends Factory
{
    protected int $count = 1;

    public function count(?int $count): self
    {
        $this->count = $count ?? 1;

        return $this;
    }

    public function create(array $extra = [])
    {
      return $this->makeFormat($extra, isArray: false);
    }

    /**
     * Метод для создания вложенных элементов dto
     */
    public function createArray(array $extra = [])
    {
        return $this->makeFormat($extra, isArray: true);
    }

    private function makeFormat(array $extra = [], bool $isArray = false)
    {
        if ($this->count == 1) {
            return $isArray ? $this->convertToArray($this->make($extra)) : $this->make($extra);
        }

        for ($i = 0; $i < $this->count; $i++) {
            $objects[] = $isArray ? $this->convertToArray($this->make($extra)) : $this->make($extra);
        }

        return $objects ?? [];
    }

    protected function collect(BaseDtoFactory $factory): array
    {
        for ($i = 0; $i < $this->count; $i++) {
            $objects[] = $this->convertToArray($factory->make());
        }

        return $objects ?? [];
    }

    private function convertToArray($data)
    {
        if ($data instanceof Collection || $data instanceof Fluent) {
            $data = $data->toArray();
        }
        if (is_array($data)) {
            foreach ($data as $key => $attribute) {
                $convert = $this->convertToArray($attribute);
                $data[$key] = $convert;
            }
        }

        return $data;
    }
}
