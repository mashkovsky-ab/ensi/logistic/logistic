<?php

namespace App\Domain\Support\Traits;

use Illuminate\Support\Collection;

/**
 * Trait WithCollect
 * @package App\Domain\Support\Traits
 */
trait WithCollect
{
    /**
     * @param  array|null  $items
     * @return Collection|static[]
     */
    public static function collect(?array $items): Collection
    {
        return $items ?
            collect($items)->map(function ($item) {
                return new static((array)$item);
            }) :
            collect();
    }
}
