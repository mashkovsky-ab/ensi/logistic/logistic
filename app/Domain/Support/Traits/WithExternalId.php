<?php

namespace App\Domain\Support\Traits;

/**
 * Trait WithExternalId
 * @package App\Domain\Support\Traits
 */
trait WithExternalId
{
    /**
     * @param  string  $externalId
     * @return $this|null
     */
    public static function findByExternalId(string $externalId): ?self
    {
        $items = self::all();

        foreach ($items as $item) {
            if ($item->external_id == $externalId) {
                return $item;
            }
        }

        return null;
    }
}
