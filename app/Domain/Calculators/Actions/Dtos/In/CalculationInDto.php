<?php

namespace App\Domain\Calculators\Actions\Dtos\In;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationBasketItemDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreDto;
use App\Domain\Calculators\Actions\Dtos\Tests\CalculationInFactory;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationInDto
 * @package App\Domain\Calculators\Actions\Dtos\In
 *
 * @property string $city_guid_to - id города получателя в базе ФИАС
 * @property Collection|CalculationBasketItemDto[] $basket_items - содержимое корзины для доставки
 * @property Collection|CalculationStoreDto[] $stores - информация о складах, с которых идет доставка
 */
class CalculationInDto extends Fluent
{
    /**
     * CalculationInDto constructor.
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->basket_items = CalculationBasketItemDto::collect($this->basket_items);
        $this->stores = CalculationStoreDto::collect($this->stores);
    }

    public static function factory(): CalculationInFactory
    {
        return CalculationInFactory::new();
    }
}
