<?php

namespace App\Domain\Calculators\Actions\Dtos\In\Part\Calculation;

use App\Domain\Calculators\Actions\Dtos\Tests\CalculationBasketItemFactory;
use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Fluent;

/**
 * Class CalculationBasketItemDto
 * @package App\Domain\Calculators\Actions\Dtos\In\Part\Calculation
 *
 * @property int $offer_id - id предложения продавца
 * @property int $qty - кол-во предложения продавца
 * @property float $weight - вес (в кг)
 * @property int $width - ширина (в мм)
 * @property int $height - высота (в мм)
 * @property int $length - длина (в мм)
 * @property bool $is_explosive - содержит взрывоопасные элементы?
 * @property int $store_id - id склада продавца
 * @property int $seller_id - id продавца
 */
class CalculationBasketItemDto extends Fluent
{
    use WithCollect;

    public static function factory(): CalculationBasketItemFactory
    {
        return CalculationBasketItemFactory::new();
    }
}
