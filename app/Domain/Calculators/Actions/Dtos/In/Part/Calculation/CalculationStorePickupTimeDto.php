<?php

namespace App\Domain\Calculators\Actions\Dtos\In\Part\Calculation;

use App\Domain\Calculators\Actions\Dtos\Tests\CalculationStorePickupTimeFactory;
use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

/**
 * Class CalculationStorePickupTimeDto
 * @package App\Domain\Calculators\Actions\Dtos\In\Part\Calculation
 *
 * @property int $day - день недели (1-7)
 * @property string $pickup_time_code - код времени отгрузки у службы доставки
 * @property Carbon $pickup_time_start - время начала отгрузки
 * @property Carbon $pickup_time_end - время окончания отгрузки
 * @property Carbon $cargo_export_time - время выгрузки информации о грузе в службу доставки
 * @property int $delivery_service - служба доставки
 */
class CalculationStorePickupTimeDto extends Fluent
{
    use WithCollect;

    /** @var string */
    public const TIME_FORMAT = 'H:i:s';

    public static function factory(): CalculationStorePickupTimeFactory
    {
        return CalculationStorePickupTimeFactory::new();
    }
}
