<?php

namespace App\Domain\Calculators\Actions\Dtos\In\Part\Calculation;

use App\Domain\Calculators\Actions\Dtos\Tests\CalculationStoreWorkingFactory;
use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

/**
 * Class CalculationStoreWorkingDto
 * @package App\Domain\Calculators\Actions\Dtos\In\Part\Calculation
 *
 * @property int $day - день недели (1-7)
 * @property Carbon $working_start_time - время начала работы склада (00:00)
 * @property Carbon $working_end_time - время окончания работы склада (00:00)
 */
class CalculationStoreWorkingDto extends Fluent
{
    use WithCollect;

    /** @var string */
    public const TIME_FORMAT = 'H:i';

    public static function factory(): CalculationStoreWorkingFactory
    {
        return CalculationStoreWorkingFactory::new();
    }
}
