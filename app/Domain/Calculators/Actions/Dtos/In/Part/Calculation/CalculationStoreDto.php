<?php

namespace App\Domain\Calculators\Actions\Dtos\In\Part\Calculation;

use App\Domain\Calculators\Actions\Dtos\Tests\CalculationStoreFactory;
use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationStoreDto
 * @package App\Domain\Calculators\Actions\Dtos\In\Part\Calculation
 *
 * @property int $store_id - id склада продавца
 * @property string $city_guid - id города склада в базе ФИАС
 * @property Collection|CalculationStoreWorkingDto[] $working - график работы склада
 * @property Collection|CalculationStorePickupTimeDto[] $pickup_times - график отгрузки склада
 */
class CalculationStoreDto extends Fluent
{
    use WithCollect;

    /**
     * CalculationStoreDto constructor.
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->working = CalculationStoreWorkingDto::collect($this->working);
        $this->pickup_times = CalculationStorePickupTimeDto::collect($this->pickup_times);
    }

    public static function factory(): CalculationStoreFactory
    {
        return CalculationStoreFactory::new();
    }
}
