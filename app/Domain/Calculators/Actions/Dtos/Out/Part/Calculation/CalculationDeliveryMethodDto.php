<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationDeliveryMethodDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property bool $can_merge - можно ли консолидировать все доставки в одну
 * @property int $delivery_cost - стоимость доставки для клиента
 * @property int $delivery_method_id - способ доставки
 * @property Collection|CalculationDeliveryDto[] $deliveries - доставки
 */
class CalculationDeliveryMethodDto extends Fluent
{
    use WithCollect;

    /**
     * CalculationDeliveryMethodDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->deliveries = CalculationDeliveryDto::collect($this->deliveries);
    }
}
