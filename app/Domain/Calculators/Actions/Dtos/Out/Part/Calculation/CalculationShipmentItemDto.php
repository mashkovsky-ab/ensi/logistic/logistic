<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Fluent;

/**
 * Class CalculationShipmentItemDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property int $offer_id - id предложения продавца
 * @property int $qty - кол-во предложения продавца
 * @property int $weight - вес (в граммах)
 * @property int $width - ширина (в мм)
 * @property int $height - высота (в мм)
 * @property int $length - длина (в мм)
 * @property bool $is_explosive - содержит взрывоопасные элементы?
 * @property int $store_id - id склада продавца
 * @property int $seller_id - id продавца
 */
class CalculationShipmentItemDto extends Fluent
{
    use WithCollect;
}
