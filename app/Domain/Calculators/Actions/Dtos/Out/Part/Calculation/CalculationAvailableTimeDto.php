<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Fluent;

/**
 * Class CalculationAvailableTimeDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property string $code - код интервала доставки
 * @property int $from - начальный час доставки «С»
 * @property int $to - конечный час доставки «ДО»
 */
class CalculationAvailableTimeDto extends Fluent
{
    use WithCollect;
}
