<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreDto;
use App\Domain\External\Dto\Response\Part\Calculator\CalculatorDeliveryMethodDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationComplexShipmentDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property string $city_guid_from - id города отправителя в базе ФИАС
 * @property string $city_guid_to - id города получателя в базе ФИАС
 * @property int $seller_id - id продавца
 * @property int $store_id - id склада отправления
 * @property int $weight - вес отправления (в граммах)
 * @property int $volume - объем отправления (в мм3)
 * @property int $width - ширина отправления (в мм)
 * @property int $height - высота отправления (в мм)
 * @property int $length - длина отправления (в мм)
 * @property Collection|CalculationShipmentItemDto[] $items - товары отправления
 * @property Collection|CalculatorDeliveryMethodDto[] $methods - способы доставки
 * @property CalculatorDeliveryMethodDto $optimalMethod - оптимальный способ доставки
 * @property CalculationStoreDto $store - информация о складе
 */
class CalculationComplexShipmentDto extends Fluent
{
    /**
     * CalculationComplexShipmentDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->methods = CalculatorDeliveryMethodDto::collect($this->methods);
        $this->items = CalculationShipmentItemDto::collect($this->items);
    }
}
