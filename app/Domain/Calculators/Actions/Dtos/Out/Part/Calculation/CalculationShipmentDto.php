<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationShipmentDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property int $seller_id - id продавца
 * @property int $store_id - id склада отправления
 * @property int $weight - вес отправления (в граммах)
 * @property int $width - ширина отправления (в сантиметрах)
 * @property int $height - высота отправления (в сантиметрах)
 * @property int $length - длина отправления (в сантиметрах)
 * @property Carbon $psd - planned shipment date - плановая дата и время, когда отправление должно быть собрано
 * (получит статус "Готово к отгрузке")
 * @property Collection|CalculationShipmentItemDto[] $items - товары отправления
 */
class CalculationShipmentDto extends Fluent
{
    use WithCollect;
    /** @var string */
    public const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * CalculationShipmentDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->items = CalculationShipmentItemDto::collect($this->items);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        $data['psd'] = $this->psd->format(self::DATE_TIME_FORMAT);

        return $data;
    }
}
