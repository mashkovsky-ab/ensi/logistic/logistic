<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationDeliveryDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property int $delivery_service - служба доставки
 * @property int $dt - delivery time - время доставки в днях, которое отдаёт ЛО
 * @property Carbon $pdd - planned delivery date - плановая дата,
 * начиная с которой отправление может быть доставлено клиенту
 * @property Collection|CalculationShipmentDto[] $shipments - отправления
 * @property Collection|CalculationTariffDto[] $tariffs - тарифы на доставку
 */
class CalculationDeliveryDto extends Fluent
{
    use WithCollect;
    /** @var string */
    public const DATE_FORMAT = 'Y-m-d';

    /**
     * CalculationDeliveryDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->shipments = CalculationShipmentDto::collect($this->shipments);
        $this->tariffs = CalculationTariffDto::collect($this->tariffs);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        $data['pdd'] = $this->pdd->format(self::DATE_FORMAT);

        return $data;
    }
}
