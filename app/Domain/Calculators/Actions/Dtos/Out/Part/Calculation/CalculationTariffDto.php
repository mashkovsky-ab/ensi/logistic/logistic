<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationTariffDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property string $external_id - id тарифа в службе доставки
 * @property string $name - наименование тарифа
 * @property int $delivery_service_cost - стоимость доставки от службы доставки @todo - использовать копейки
 * @property int $days_min - минимальное количество дней на доставку
 * @property int $days_max - максимальное количество дней на доставку
 * @property Collection|CalculationAvailableDateDto[] $available_dates - доступные даты для доставки
 * @property array|int[] $point_ids - список Id точек выдачи заказа
 */
class CalculationTariffDto extends Fluent
{
    use WithCollect;

    /**
     * CalculationTariffDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->available_dates = CalculationAvailableDateDto::collect($this->available_dates);
    }
}
