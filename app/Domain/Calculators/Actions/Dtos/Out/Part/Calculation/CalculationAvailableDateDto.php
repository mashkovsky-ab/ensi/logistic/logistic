<?php

namespace App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation;

use App\Domain\Support\Traits\WithCollect;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationAvailableDateDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation
 *
 * @property Carbon $date - дата доставки
 * @property Collection|CalculationAvailableTimeDto[] $available_times - доступные интервалы времени доставки
 */
class CalculationAvailableDateDto extends Fluent
{
    use WithCollect;
    /** @var string */
    public const DATE_FORMAT = 'Y-m-d';

    /**
     * CalculationAvailableDateDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->available_times = CalculationAvailableTimeDto::collect($this->available_times);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        $data['date'] = $this->date->format(self::DATE_FORMAT);

        return $data;
    }
}
