<?php

namespace App\Domain\Calculators\Actions\Dtos\Out;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationDeliveryMethodDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

/**
 * Class CalculationOutDto
 * @package App\Domain\Calculators\Actions\Dtos\Out\Part
 *
 * @property Collection|CalculationDeliveryMethodDto[] $methods - способы получения товара
 */
class CalculationOutDto extends Fluent
{
    /**
     * CalculationOutDto constructor.
     * @param  array  $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->methods = CalculationDeliveryMethodDto::collect($this->methods);
    }
}
