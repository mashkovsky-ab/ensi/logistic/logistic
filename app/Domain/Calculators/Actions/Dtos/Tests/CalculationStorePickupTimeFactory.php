<?php

namespace App\Domain\Calculators\Actions\Dtos\Tests;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStorePickupTimeDto;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use App\Domain\Support\Tests\BaseDtoFactory;

class CalculationStorePickupTimeFactory extends BaseDtoFactory
{
    protected function definition(): array
    {
        return [
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}",
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'cargo_export_time' => $this->faker->time('H:i'),
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::toValues()),
        ];
    }

    public function make(array $extra = []): CalculationStorePickupTimeDto
    {
        return new CalculationStorePickupTimeDto($this->makeArray($extra));
    }
}
