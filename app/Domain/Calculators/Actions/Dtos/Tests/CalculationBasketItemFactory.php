<?php

namespace App\Domain\Calculators\Actions\Dtos\Tests;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationBasketItemDto;
use App\Domain\Support\Tests\BaseDtoFactory;

class CalculationBasketItemFactory extends BaseDtoFactory
{
    protected function definition(): array
    {
        return [
            'offer_id' => $this->faker->randomNumber(),
            'qty' => $this->faker->numberBetween(1, 20),

            'weight' => $this->faker->randomFloat(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'length' => $this->faker->randomNumber(),

            'is_explosive' => $this->faker->boolean(),

            'store_id' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): CalculationBasketItemDto
    {
        return new CalculationBasketItemDto($this->makeArray($extra));
    }
}
