<?php

namespace App\Domain\Calculators\Actions\Dtos\Tests;

use App\Domain\Calculators\Actions\Dtos\In\CalculationInDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationBasketItemDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreDto;
use App\Domain\Support\Tests\BaseDtoFactory;

class CalculationInFactory extends BaseDtoFactory
{

    protected function definition(): array
    {
        return [
            'city_guid_to' => $this->faker->bothify('********-****-****-****-********'),
            'basket_items' => $this->collect(CalculationBasketItemDto::factory()),
            'stores' => $this->collect(CalculationStoreDto::factory()),
        ];
    }

    public function make(array $extra = []): CalculationInDto
    {
        return new CalculationInDto($this->makeArray($extra));
    }
}
