<?php

namespace App\Domain\Calculators\Actions\Dtos\Tests;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreWorkingDto;
use App\Domain\Support\Tests\BaseDtoFactory;

class CalculationStoreWorkingFactory extends BaseDtoFactory
{
    protected function definition(): array
    {
        return [
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->time('H:i'),
            'working_end_time' => $this->faker->time('H:i'),
        ];
    }

    public function make(array $extra = []): CalculationStoreWorkingDto
    {
        return new CalculationStoreWorkingDto($this->makeArray($extra));
    }
}
