<?php

namespace App\Domain\Calculators\Actions\Dtos\Tests;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStorePickupTimeDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreWorkingDto;
use App\Domain\Support\Tests\BaseDtoFactory;

class CalculationStoreFactory extends BaseDtoFactory
{
    protected function definition(): array
    {
        return [
            'store_id' => $this->faker->randomNumber(),
            'city_guid' => $this->faker->bothify('********-****-****-****-********'),
            'working' => $this->collect(CalculationStoreWorkingDto::factory()),
            'pickup_times' => $this->collect(CalculationStorePickupTimeDto::factory()),
        ];
    }

    public function make(array $extra = []): CalculationStoreDto
    {
        return new CalculationStoreDto($this->makeArray($extra));
    }
}
