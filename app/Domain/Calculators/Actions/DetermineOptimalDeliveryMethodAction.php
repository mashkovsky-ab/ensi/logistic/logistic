<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationComplexShipmentDto;
use App\Domain\DeliveryKpis\Actions\CalcPddAction;
use App\Domain\DeliveryKpis\Actions\CalcPsdAction;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\Dto\Response\Part\Calculator\CalculatorDeliveryMethodDto;
use Exception;
use Illuminate\Support\Collection;

/**
 * Определить оптимальный способ доставки
 * Class DetermineOptimalDeliveryMethodAction
 * @package App\Domain\Calculators\Actions
 */
class DetermineOptimalDeliveryMethodAction
{
    /**
     * DetermineOptimalDeliveryMethodAction constructor.
     * @param  CalcPsdAction  $calcPsdAction
     * @param  CalcPddAction  $calcPddAction
     */
    public function __construct(
        protected CalcPsdAction $calcPsdAction,
        protected CalcPddAction $calcPddAction
    ) {
    }

    /**
     * @param  Collection|DeliveryService[]  $deliveryServices
     * @param  CalculationComplexShipmentDto  $complexShipment
     */
    public function execute(Collection $deliveryServices, CalculationComplexShipmentDto $complexShipment): void
    {
        $sellerId = $complexShipment->seller_id;
        $calculationStoreDto = $complexShipment->store;
        $deliveryMethodDtos = $complexShipment->methods;

        if ($deliveryMethodDtos->isEmpty()) {
            return;
        }
        $deliveryMethodDtos = $deliveryMethodDtos->keyBy('delivery_service');
        $availableDeliveryServicesIds = $deliveryMethodDtos->pluck('delivery_service')->unique()->values()->all();
        $availableDeliveryServices = $deliveryServices->filter(
            function (DeliveryService $deliveryService) use ($availableDeliveryServicesIds) {
                return in_array($deliveryService->id, $availableDeliveryServicesIds);
            }
        )->sortBy('priority')->groupBy('priority');
        /**
         * @var Collection|DeliveryService[] $deliveryServicesByPriority - службы доставки с максимальным приоритетом
         */
        $deliveryServicesByPriority = $availableDeliveryServices->first();
        /**
         * Если несколько служб доставок имеют одинаковый приоритет, то отбираем одну единственную
         * сначала по min PDD,
         * затем по min delivery_service_cost (стоимость доставки от службы доставки),
         * и в конце берем первую попавшуюся службу доставки
         */
        if ($deliveryServicesByPriority->count() > 1) {
            $deliveryServicesIdsByPriority = $deliveryServicesByPriority->pluck('id')->all();
            /** @var Collection|CalculatorDeliveryMethodDto[] $deliveryMethodDtos */
            $deliveryMethodDtos = $deliveryMethodDtos->filter(
                function (CalculatorDeliveryMethodDto $calculatorItemDto) use ($deliveryServicesIdsByPriority) {
                    return in_array($calculatorItemDto->delivery_service, $deliveryServicesIdsByPriority);
                }
            )->values();
            /**
             * Рассчитываем PDD для каждого тарифа каждой службы доставки
             */
            foreach ($deliveryMethodDtos as $key => $deliveryMethodDto) {
                try {
                    $deliveryMethodDto->psd = $this->calcPsdAction->execute(
                        $sellerId,
                        $deliveryMethodDto->delivery_service,
                        $calculationStoreDto
                    );
                    $maxDt = $this->getMaxDt($deliveryMethodDto);
                    $deliveryMethodDto->pdd = $this->calcPddAction->execute(
                        $deliveryMethodDto->psd,
                        $deliveryMethodDto->delivery_service,
                        $maxDt
                    );
                    /**
                     * Т.к. у одном службы доставки может быть несколько тарифов на доставку (обычно это у самовывоза),
                     * а какой именно тариф будет выбран зависит от пользователя (какой ПВЗ выберет)
                     * то считаем среднюю стоимость доставки по всем тарифам
                     */
                    $deliveryMethodDto->averageCost = $deliveryMethodDto->tariffs->average('delivery_service_cost');
                } catch (Exception $e) {
                    if (is_debug()) {
                        logger($e->getMessage(), [$e->getFile(), $e->getLine(), $e->getCode(), $complexShipment->toArray()]);
                    }
                    $deliveryMethodDtos->forget($key);
                }
            }
            /**
             * Отбираем одну единственную службу доставки
             * сначала по min PDD,
             * затем по min delivery_service_cost (стоимость доставки от службы доставки),
             * и в конце берем первую попавшуюся службу доставки
             */
            $optimalDeliveryMethodDto = $deliveryMethodDtos->sort(
                function (CalculatorDeliveryMethodDto $a, CalculatorDeliveryMethodDto $b) {
                    if ($a->pdd == $b->pdd) {
                        if ($a->averageCost == $b->averageCost) {
                            return 0;
                        }

                        return $a->averageCost < $b->averageCost ? -1 : 1;
                    }

                    return $a->pdd < $b->pdd ? -1 : 1;
                }
            )->first();
        } else {
            try {
                $optimalDeliveryServiceId = $deliveryServicesByPriority[0]->id;
                /** @var CalculatorDeliveryMethodDto $optimalDeliveryMethodDto */
                $optimalDeliveryMethodDto = $deliveryMethodDtos->filter(
                    function (CalculatorDeliveryMethodDto $calculatorItemDto) use ($optimalDeliveryServiceId) {
                        return $calculatorItemDto->delivery_service == $optimalDeliveryServiceId;
                    }
                )->first();
                $optimalDeliveryMethodDto->psd = $this->calcPsdAction->execute(
                    $sellerId,
                    $optimalDeliveryMethodDto->delivery_service,
                    $calculationStoreDto
                );
                $maxDt = $this->getMaxDt($optimalDeliveryMethodDto);
                $optimalDeliveryMethodDto->pdd = $this->calcPddAction->execute(
                    $optimalDeliveryMethodDto->psd,
                    $optimalDeliveryMethodDto->delivery_service,
                    $maxDt
                );
            } catch (Exception $e) {
                if (is_debug()) {
                    logger($e->getMessage(), [$e->getFile(), $e->getLine(), $e->getCode(), $complexShipment->toArray()]);
                }

                return;
            }
        }

        $complexShipment->optimalMethod = $optimalDeliveryMethodDto;
    }

    /**
     * Получить максимальное время доставки из всех возможных тарифов
     * @param  CalculatorDeliveryMethodDto  $calculatorItemDto
     * @return int
     */
    protected function getMaxDt(CalculatorDeliveryMethodDto $calculatorItemDto): int
    {
        return (int)$calculatorItemDto->tariffs->max('days_max');
    }
}
