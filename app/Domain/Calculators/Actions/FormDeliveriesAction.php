<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationAvailableDateDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationAvailableTimeDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationComplexShipmentDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationDeliveryDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationShipmentDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationTariffDto;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\Dto\Response\Part\Calculator\CalculatorDeliveryMethodDto;
use App\Domain\External\Dto\Response\Part\Calculator\TariffDto;
use Exception;
use Illuminate\Support\Collection;

/**
 * Получить доставки для заказа
 * Class FormDeliveriesAction
 * @package App\Domain\Calculators\Actions
 */
class FormDeliveriesAction
{
    //todo Заменить использование константы ниже на получение значения из БД
    /** @var int - максимальное кол-во дней доставки разницы для консолидации отправлений в одну доставку */
    protected const DAYS_MAX_FOR_CONSOLIDATION = 2;
    //todo Заменить использование константы ниже на получение значения из БД
    /** @var int - максимальное кол-во доступных дней для доставки от первой даты доставки */
    public const MAX_AVAILABLE_DAYS = 14;

    public function __construct(protected DetermineOptimalDeliveryMethodAction $determineOptimalDeliveryMethodAction)
    {
    }

    /**
     * @param int $deliveryMethod
     * @param  Collection|DeliveryService[]  $deliveryServices
     * @param  Collection|CalculationComplexShipmentDto[]  $originalComplexShipments
     * @return Collection|CalculationDeliveryDto[]
     * @throws Exception
     */
    public function execute(int $deliveryMethod, Collection $deliveryServices, Collection $originalComplexShipments): Collection
    {
        $deliveries = collect();
        /*
         * Создаем глубокую копию объекта, т.к. он будет модифицироваться в рамках метода,
         * но нам эти изменения не нужный при следующем вызове
         */
        /** @var Collection|CalculationComplexShipmentDto[] $complexShipments */
        $complexShipments = unserialize(serialize($originalComplexShipments));

        foreach ($complexShipments as $complexShipment) {
            $complexShipment->methods = $complexShipment->methods->filter(
                function (CalculatorDeliveryMethodDto $methodDto) use ($deliveryMethod) {
                    return $methodDto->delivery_method == $deliveryMethod;
                }
            );
            /**
             * Определяем оптимальный способ доставки для отправления
             */
            $this->determineOptimalDeliveryMethodAction->execute($deliveryServices, $complexShipment);
            if (!$complexShipment->optimalMethod) {
                return collect();
            }
        }

        $qtyComplexShipments = $complexShipments->count();
        for ($i = 0; $i < $qtyComplexShipments; $i++) {
            if (!$complexShipments->has($i)) {
                continue;
            }
            $complexShipment = $complexShipments[$i];
            $calculationDeliveryDto = new CalculationDeliveryDto();
            $calculationDeliveryDto->delivery_service = $complexShipment->optimalMethod->delivery_service;
            /**
             * Сохраняем исходный PDD, т.к. при мини-консолидации он может увеличиваться.
             * Но мы для решения, можно ли отправление добавить в текущую доставку, должны использовать
             * исходный PDD.
             * Например, имеются 3 отправления со следующими PDD (время опущено):
             * 28.04.2020, 30.04.2020 и 02.05.2020
             * Исходный PDD = 28.04.2020.
             * Когда отправление 2 будет мини-консолидировано с отправлением 1, PDD для доставки станет 30.04.2020.
             * Но при решении, можно ли мини-консолидировать отправление 3 с отправлениями 1 и 2
             * нужно сравнивать PDD 28.04.2020 и 02.05.2020, а не 30.04.2020 и 02.05.2020!
             * Для этого нам и нужно знать исходный PDD 28.04.2020
             */
            $optimalMethodPdd = clone $complexShipment->optimalMethod->pdd;
            //Добавляем первое отправление в доставку
            $deliveries->push($calculationDeliveryDto);
            $this->addShipment2Delivery($calculationDeliveryDto, $complexShipment);

            /**
             * Если служба доставки делает консолидацию,
             * то пытаемся добавить остальные отправления в текущую доставку (мини-консолидация)
             */
            if ($deliveryServices[$calculationDeliveryDto->delivery_service]->do_consolidation) {
                for ($j = $i + 1; $j < $qtyComplexShipments; $j++) {
                    if (!$complexShipments->has($j)) {
                        continue;
                    }
                    $nextComplexShipment = $complexShipments[$j];
                    if ($nextComplexShipment->optimalMethod->delivery_service
                        == $complexShipment->optimalMethod->delivery_service &&
                        $nextComplexShipment->optimalMethod->pdd->diff($optimalMethodPdd)->days
                        <= self::DAYS_MAX_FOR_CONSOLIDATION
                    ) {
                        /**
                         * Обновляем значение pdd - берем максимум
                         */
                        if ($nextComplexShipment->optimalMethod->pdd > $complexShipment->optimalMethod->pdd) {
                            $complexShipment->optimalMethod->pdd = $nextComplexShipment->optimalMethod->pdd;
                        }
                        $this->addShipment2Delivery($calculationDeliveryDto, $nextComplexShipment);
                        $complexShipments->forget($j);
                    }
                }
            }
            $calculationDeliveryDto->pdd = $complexShipment->optimalMethod->pdd;

            //Добавляем информацию о тарифах для данной доставки
            $tariffs = clone $complexShipment->optimalMethod->tariffs;
            $pdd = clone $complexShipment->optimalMethod->pdd->setTime(0, 0);
            if ($deliveryMethod == DeliveryMethod::METHOD_DELIVERY) {
                /**
                 * Для курьерской доставки выбираем первый по минимальным срокам и стоимости тариф.
                 * Для самовывоза этого не делаем, т.к. разные тарифы обусловлены разным набором доступных ПВЗ
                 */
                $tariffs = collect()->push($tariffs->sort(function (TariffDto $a, TariffDto $b) {
                    if ($a->days_max == $b->days_max) {
                        if ($a->delivery_service_cost == $b->delivery_service_cost) {
                            return 0;
                        }

                        return $a->delivery_service_cost < $b->delivery_service_cost ? -1 : 1;
                    }

                    return $a->days_max < $b->days_max ? -1 : 1;
                })->first());
            }
            $calculationDeliveryDto->dt = (int)$tariffs->max('days_max');
            foreach ($tariffs as $tariffDto) {
                $calculationTariffDto = new CalculationTariffDto();
                $calculationDeliveryDto->tariffs->push($calculationTariffDto);
                $calculationTariffDto->external_id = $tariffDto->external_id;
                $calculationTariffDto->name = $tariffDto->name;
                $calculationTariffDto->delivery_service_cost = $tariffDto->delivery_service_cost;

                if ($tariffDto->available_dates) {
                    $qtyAvailableDates = 1;
                    foreach ($tariffDto->available_dates as $availableDateDto) {
                        if ($availableDateDto->date < $pdd) {
                            continue;
                        }
                        if ($qtyAvailableDates > self::MAX_AVAILABLE_DAYS) {
                            break;
                        }
                        $calculationAvailableDateDto = new CalculationAvailableDateDto();
                        $calculationTariffDto->available_dates->push($calculationAvailableDateDto);

                        $calculationAvailableDateDto->date = $availableDateDto->date;

                        if ($availableDateDto->available_times) {
                            foreach ($availableDateDto->available_times as $availableTimeDto) {
                                $calculationAvailableTimeDto = new CalculationAvailableTimeDto();
                                $calculationAvailableDateDto->available_times->push($calculationAvailableTimeDto);

                                $calculationAvailableTimeDto->code = $availableTimeDto->code;
                                $calculationAvailableTimeDto->to = $availableTimeDto->to;
                                $calculationAvailableTimeDto->from = $availableTimeDto->from;
                            }
                        }
                        $qtyAvailableDates++;
                    }
                }

                if ($tariffDto->days_min) {
                    $calculationTariffDto->days_min = $pdd->diff(now()->setTime(0, 0))->days;
                }
                if ($tariffDto->days_max) {
                    $calculationTariffDto->days_max = $pdd->diff(now()->setTime(0, 0))->days;
                }

                if ($tariffDto->point_ids) {
                    $calculationTariffDto->point_ids = $tariffDto->point_ids;
                }
            }
        }

        return $deliveries;
    }

    /**
     * Добавить отправление в доставку
     * @param  CalculationDeliveryDto  $calculationDeliveryDto
     * @param  CalculationComplexShipmentDto  $complexShipmentDto
     */
    protected function addShipment2Delivery(
        CalculationDeliveryDto $calculationDeliveryDto,
        CalculationComplexShipmentDto $complexShipmentDto
    ): void {
        $calculationShipmentDto = new CalculationShipmentDto();
        $calculationDeliveryDto->shipments->push($calculationShipmentDto);

        $calculationShipmentDto->seller_id = $complexShipmentDto->seller_id;
        $calculationShipmentDto->store_id = $complexShipmentDto->store ? $complexShipmentDto->store->store_id : 0;
        $calculationShipmentDto->weight = $complexShipmentDto->weight;
        $calculationShipmentDto->width = $complexShipmentDto->width;
        $calculationShipmentDto->height = $complexShipmentDto->height;
        $calculationShipmentDto->length = $complexShipmentDto->length;
        $calculationShipmentDto->psd = $complexShipmentDto->optimalMethod->psd;
        $calculationShipmentDto->items = $complexShipmentDto->items;
    }
}
