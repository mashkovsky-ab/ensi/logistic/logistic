<?php

use App\Domain\Calculators\Actions\Dtos\In\CalculationInDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationBasketItemDto;
use App\Domain\Calculators\Actions\GetSuitableDeliveryServicesAction;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryServices\Enums\DeliveryServiceStatus;
use App\Domain\DeliveryServices\Models\DeliveryService;
use Illuminate\Support\Carbon;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("action GetSuitableDeliveryServicesAction exceeded max shipments", function () {
    $calculationInDto = CalculationInDto::factory()->create();

    /** @var DeliveryService[] $deliveryServices */
    $deliveryServices = DeliveryService::factory()->count(5)->create([
        'status' => DeliveryServiceStatus::ACTIVE,
        'do_dangerous_products_delivery' => true,
    ]);

    /** @var DeliveryService $deliveryService */
    $unSuitableDeliveryService = DeliveryService::factory()->create([
        'status' => DeliveryServiceStatus::ACTIVE,
        'do_dangerous_products_delivery' => true,
        'max_shipments_per_day' => 1,
    ]);

    DeliveryOrder::factory()->count(20)->create([
        'delivery_date' => Carbon::today(),
        'delivery_service_id' => $unSuitableDeliveryService->id,
    ]);

    $suitableDeliveryServicesTest = collect($deliveryServices)->pluck('id')->toArray();

    $suitableDeliveryServices = resolve(GetSuitableDeliveryServicesAction::class)->execute($calculationInDto);

    expect(
        array_filter($suitableDeliveryServicesTest,
            fn($key) => !in_array($key, $suitableDeliveryServices)
        )
    )->toBeEmpty();

    expect(in_array($unSuitableDeliveryService->id, $suitableDeliveryServices))->toBeFalse();
});


test("action GetSuitableDeliveryServicesAction dangerous", function (bool $dangerous) {
    $basketItems = CalculationBasketItemDto::factory()->count(2)->createArray(['is_explosive' => $dangerous]);
    $calculationInDto = CalculationInDto::factory()->create(['basket_items' => $basketItems]);

    $deliveryServices = DeliveryService::factory()
        ->sequence(
            ['do_dangerous_products_delivery' => $dangerous],
        )
        ->count(10)->create(['status' => DeliveryServiceStatus::ACTIVE]);

    $suitableDeliveryServicesTest = collect($deliveryServices)->pluck('id')->toArray();

    $suitableDeliveryServices = resolve(GetSuitableDeliveryServicesAction::class)->execute($calculationInDto);

    expect(
        array_filter($suitableDeliveryServicesTest,
            fn($key) => !in_array($key, $suitableDeliveryServices)
        )
    )->toBeEmpty();
})->with([true, false]);
