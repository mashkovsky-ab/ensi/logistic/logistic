<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationComplexShipmentDto;

/**
 * Вычислить объем отправления
 * Class CalcShipmentVolumeAction
 * @package App\Domain\Calculators\Actions
 */
class CalcShipmentVolumeAction
{
    /**
     * @param  CalculationComplexShipmentDto  $complexShipment  - отправление
     */
    public function execute(CalculationComplexShipmentDto $complexShipment): void
    {
        $volume = 0.0;

        foreach ($complexShipment->items as $shipmentItem) {
            $volume +=
                $shipmentItem->qty * ($shipmentItem->width * $shipmentItem->height * $shipmentItem->length);
        }

        $complexShipment->volume = $volume;
    }
}
