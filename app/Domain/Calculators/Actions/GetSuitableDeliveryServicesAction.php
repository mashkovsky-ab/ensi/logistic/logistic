<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\In\CalculationInDto;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryServices\Enums\DeliveryServiceStatus;
use App\Domain\DeliveryServices\Models\DeliveryService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Получить подходящие под ограничения службы доставки для расчета:
 * - по активности службы доставки
 * - проверка по возможности транспортировки опасных грузов
 * - проверка по максимальному кол-ву отправлений (точнее доставок) в день
 *
 * Class GetSuitableDeliveryServicesAction
 * @package App\Domain\Calculators\Actions
 */
class GetSuitableDeliveryServicesAction
{
    /**
     * @param CalculationInDto $calculationInDto
     * @return array|int[]
     */
    public function execute(CalculationInDto $calculationInDto): array
    {
        $hasDangerousProducts = false;
        foreach ($calculationInDto->basket_items as $basketItemDto) {
            if ($basketItemDto->is_explosive) {
                $hasDangerousProducts = true;

                break;
            }
        }
        $query = DeliveryService::query()
            ->where('status', DeliveryServiceStatus::ACTIVE);
        if ($hasDangerousProducts) {
            $query->where('do_dangerous_products_delivery', true);
        }
        /** @var Collection|DeliveryService[] $deliveryServices */
        $deliveryServices = $query->get();
        $suitableDeliveryServices = [];

        $number = $this->getCountDeliveryOrdersForTodayByDeliveryServices();

        foreach ($deliveryServices as $deliveryService) {
            if (is_null($deliveryService->max_shipments_per_day) || $deliveryService->max_shipments_per_day > $number->get($deliveryService->id)) {
                $suitableDeliveryServices[] = $deliveryService->id;
            }
        }

        return $suitableDeliveryServices;
    }

    public function getCountDeliveryOrdersForTodayByDeliveryServices(): Collection
    {
        return DeliveryOrder::isDeliveryServiceActive()
            ->whereDate('delivery_date', Carbon::today())
            ->groupBy('delivery_service_id')
            ->selectRaw('count(*), delivery_service_id')
            ->get()
            ->keyBy('delivery_service_id')
            ->map(fn($item) => $item->count);
    }
}
