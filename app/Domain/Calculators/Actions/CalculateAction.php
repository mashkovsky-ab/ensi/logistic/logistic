<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\In\CalculationInDto;
use App\Domain\Calculators\Actions\Dtos\Out\CalculationOutDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationDeliveryDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationDeliveryMethodDto;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\GetCtBySellerAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\GetPptBySellerAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\GetDeliveryCostAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\GetDeliveryPricesAction;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External;
use App\Domain\External\CalculatorInterface;
use App\Domain\External\Dto\Request\CalculatorRequestDto;
use App\Domain\External\Dto\Response\CalculatorResponseDto;
use Exception;
use Illuminate\Support\Collection;

/**
 * Class CalculateAction
 * @package App\Domain\Calculators\Actions
 */
class CalculateAction
{
    /** @var Collection|DeliveryService[] */
    protected Collection|array $deliveryServices;
    /** @var array */
    protected const CALCULATORS = [
        DeliveryService::SERVICE_B2CPL => External\B2Cpl\Api\CalculatorApi::class,
        DeliveryService::SERVICE_CDEK => External\Cdek\Api\CalculatorApi::class,
    ];

    public function __construct(
        protected GetSuitableDeliveryServicesAction $getSuitableDeliveryServicesAction,
        protected FormComplexShipmentsAction $formComplexShipmentsAction,
        protected GetDeliveryPricesAction $getDeliveryPricesAction,
        protected GetCtBySellerAction $getCtBySellerAction,
        protected GetPptBySellerAction $getPptBySellerAction,
        protected FormDeliveriesAction $formDeliveriesAction,
        protected GetDeliveryCostAction $getDeliveryCostAction
    ) {
    }

    /**
     * @param  CalculationInDto  $calculationInDto
     * @return CalculationOutDto
     * @throws Exception
     */
    public function execute(CalculationInDto $calculationInDto): CalculationOutDto
    {
        $calculationOutDto = new CalculationOutDto();
        $deliveryServicesIds = $this->getSuitableDeliveryServicesAction->execute($calculationInDto);
        if (!$deliveryServicesIds) {
            return $calculationOutDto;
        }

        $complexShipments = $this->formComplexShipmentsAction->execute($calculationInDto);

        $availableDeliveryServices = [];
        $deliveryServicesByCalculators = $this->deliveryServicesByCalculators($deliveryServicesIds);
        foreach ($complexShipments as $complexShipment) {
            $calculatorRequestDto = new CalculatorRequestDto();
            $calculatorRequestDto->city_guid_from = $complexShipment->city_guid_from;
            $calculatorRequestDto->city_guid_to = $complexShipment->city_guid_to;
            $calculatorRequestDto->weight = $complexShipment->weight;
            $calculatorRequestDto->volume = $complexShipment->volume;
            $calculatorRequestDto->width = $complexShipment->width;
            $calculatorRequestDto->height = $complexShipment->height;
            $calculatorRequestDto->length = $complexShipment->length;

            $calculatorResponseDto = new CalculatorResponseDto();
            foreach ($deliveryServicesByCalculators as $calculatorClass => $deliveryServicesIds) {
                /** @var CalculatorInterface $calculator */
                $calculator = resolve($calculatorClass);
                $calculatorRequestDto->delivery_services_ids = $deliveryServicesIds;

                try {
                    $response = $calculator->calculate($calculatorRequestDto);
                } catch (Exception $e) {
                    if (is_debug()) {
                        logger($e->getMessage(), [$e->getFile(), $e->getLine(), $e->getCode(), $calculationInDto->toArray(), $calculatorRequestDto->toArray()]);
                    }

                    continue;
                }
                $availableDeliveryServices = array_merge(
                    $availableDeliveryServices,
                    $response->methods->pluck('delivery_service')->unique()->values()->all()
                );

                $calculatorResponseDto->merge($response);
            }

            $complexShipment->methods = $calculatorResponseDto->methods;
        }
        $availableDeliveryServices = array_unique($availableDeliveryServices);
        $this->loadDeliveryServices($availableDeliveryServices);

        $deliveryPrices = $this->getDeliveryPricesAction->execute($calculationInDto->city_guid_to, $availableDeliveryServices);
        $sellersIds = $complexShipments->pluck('seller_id')->unique()->values()->all();
        $this->getCtBySellerAction->loadCt($sellersIds);
        $this->getPptBySellerAction->loadPpt($sellersIds);
        $deliveryMethods = DeliveryMethod::validValues();

        foreach ($deliveryMethods as $deliveryMethodId) {
            $deliveries = $this->formDeliveriesAction->execute(
                $deliveryMethodId,
                $this->deliveryServices,
                $complexShipments
            );
            if ($deliveries->isNotEmpty()) {
                $calculationDeliveryMethodDto = new CalculationDeliveryMethodDto();
                $calculationOutDto->methods->push($calculationDeliveryMethodDto);
                $calculationDeliveryMethodDto->delivery_method_id = $deliveryMethodId;
                $calculationDeliveryMethodDto->deliveries = $deliveries;
                $calculationDeliveryMethodDto->can_merge = $this->isCanMerge($deliveries);
                /**
                 * Доставки одного заказа могут принадлежать разным службам доставки, у каждой из которых своя цена.
                 * Поэтому считаем среднее значение цен от всех служб доставки
                 */
                $delivery_costs = collect();
                foreach ($deliveries as $delivery) {
                    $delivery_costs->push(
                        $this->getDeliveryCostAction->execute(
                            $deliveryPrices,
                            $deliveryMethodId,
                            $delivery->delivery_service
                        )
                    );
                }
                $calculationDeliveryMethodDto->delivery_cost = $delivery_costs->avg();
            }
        }

        return $calculationOutDto;
    }

    /**
     * @param  array  $deliveryServices
     * @return array
     */
    protected function deliveryServicesByCalculators(array $deliveryServices): array
    {
        $byCalculators = [];

        foreach ($deliveryServices as $deliveryService) {
            if (isset(static::CALCULATORS[$deliveryService])) {
                $calculator = static::CALCULATORS[$deliveryService];

                if (!isset($byCalculators[$calculator])) {
                    $byCalculators[$calculator] = [];
                }
                $byCalculators[$calculator][] = $deliveryService;
            }
        }

        return $byCalculators;
    }

    /**
     * Можно ли объединить доставки в одну?
     * @param  Collection|CalculationDeliveryDto[]  $deliveries
     * @return bool
     */
    protected function isCanMerge(Collection $deliveries): bool
    {
        if ($deliveries->count() <= 1) {
            return false;
        }

        /**
         * Если у доставок разные службы доставки, то объединить в одну их нельзя
         */
        $deliveryServices = $deliveries->pluck('delivery_service')->unique()->values();
        if ($deliveryServices->count() != 1) {
            return false;
        }

        /**
         * Если служба доставки не делает консолидацию, то объединить доставки в одну нельзя
         */
        $deliveryServiceId = $deliveryServices[0];
        if (!$this->deliveryServices[$deliveryServiceId]->do_consolidation) {
            return false;
        }

        return true;
    }

    /**
     * @param  array  $deliveryServicesIds
     */
    protected function loadDeliveryServices(array $deliveryServicesIds): void
    {
        $this->deliveryServices = DeliveryService::query()
            ->whereIn('id', $deliveryServicesIds)
            ->select(['id', 'pct', 'priority', 'do_consolidation'])
            ->get()
            ->keyBy('id');
    }
}
