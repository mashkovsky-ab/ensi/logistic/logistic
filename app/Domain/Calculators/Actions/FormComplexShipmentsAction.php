<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\In\CalculationInDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationComplexShipmentDto;
use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationShipmentItemDto;
use Illuminate\Support\Collection;

/**
 * Сформировать комплексные отправления для расчета доставки
 * Class FormComplexShipmentsAction
 * @package App\Domain\Calculators\Actions
 */
class FormComplexShipmentsAction implements WithSides
{
    /**
     * FormComplexShipmentsAction constructor.
     * @param  CalcShipmentSizesAction  $calcShipmentSizesAction
     */
    public function __construct(protected CalcShipmentSizesAction $calcShipmentSizesAction)
    {
    }

    /**
     * @param  CalculationInDto  $calculationInDto
     * @return Collection|CalculationComplexShipmentDto[]
     */
    public function execute(CalculationInDto $calculationInDto): Collection
    {
        /** @var Collection|CalculationComplexShipmentDto[] $complexShipments */
        $complexShipments = collect();
        $storesById = $calculationInDto->stores->keyBy('store_id');

        foreach ($calculationInDto->basket_items as $basketItem) {
            if (!$complexShipments->has($basketItem->store_id)) {
                $calculationShipmentDto = new CalculationComplexShipmentDto();
                $calculationShipmentDto->city_guid_from = $storesById->has($basketItem->store_id) ?
                    $storesById[$basketItem->store_id]->city_guid : '';
                $calculationShipmentDto->city_guid_to = $calculationInDto->city_guid_to;
                $calculationShipmentDto->seller_id = $basketItem->seller_id;
                $calculationShipmentDto->store = $storesById->has($basketItem->store_id) ?
                    $storesById[$basketItem->store_id] : null;
                $calculationShipmentDto->weight = 0;
                $calculationShipmentDto->volume = 0;
                $calculationShipmentDto->width = 0;
                $calculationShipmentDto->height = 0;
                $calculationShipmentDto->length = 0;

                $complexShipments->put($basketItem->store_id, $calculationShipmentDto);
            }

            $calculationShipmentDto = $complexShipments[$basketItem->store_id];
            $calculationShipmentDto->weight += ceil($basketItem->qty * $basketItem->weight);

            $calculationShipmentItemDto = new CalculationShipmentItemDto();
            $calculationShipmentDto->items->push($calculationShipmentItemDto);

            $calculationShipmentItemDto->offer_id = $basketItem->offer_id;
            $calculationShipmentItemDto->qty = $basketItem->qty;
            $calculationShipmentItemDto->weight = $basketItem->weight;
            $calculationShipmentItemDto->width = $basketItem->width;
            $calculationShipmentItemDto->height = $basketItem->height;
            $calculationShipmentItemDto->length = $basketItem->length;
            $calculationShipmentItemDto->is_explosive = $basketItem->is_explosive;
            $calculationShipmentItemDto->store_id = $basketItem->store_id;
            $calculationShipmentItemDto->seller_id = $basketItem->seller_id;
        }
        $complexShipments = $complexShipments->values();

        foreach ($complexShipments as $complexShipment) {
            $this->calcShipmentSizesAction->execute($complexShipment);

            //Округляем габариты отправления до целого, т.к. службы доставки не работаю с дробными
            foreach (self::SIDES as $side) {
                $complexShipment[$side] = ceil($complexShipment[$side]);
            }
        }

        return $complexShipments;
    }
}
