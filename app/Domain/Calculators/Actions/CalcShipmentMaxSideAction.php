<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationComplexShipmentDto;

/**
 * Вычислить максимальную сторону отправления среди всех сторон его товаров
 * Class CalcShipmentMaxSideAction
 * @package App\Domain\Calculators\Actions
 */
class CalcShipmentMaxSideAction implements WithSides
{
    /**
     * @param  CalculationComplexShipmentDto  $complexShipment  - отправление
     * @return array
     */
    public function execute(CalculationComplexShipmentDto $complexShipment): array
    {
        $maxSide = 0;
        $maxSideName = '';

        foreach ($complexShipment->items as $shipmentItem) {
            foreach (self::SIDES as $side) {
                if ($shipmentItem[$side] > $maxSide) {
                    $maxSide = $shipmentItem[$side];
                    $maxSideName = $side;
                }
            }
        }

        return [$maxSide, $maxSideName];
    }
}
