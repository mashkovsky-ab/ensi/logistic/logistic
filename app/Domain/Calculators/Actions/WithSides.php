<?php

namespace App\Domain\Calculators\Actions;

/**
 * Interface WithSides
 * @package App\Domain\Calculators\Actions
 */
interface WithSides
{
    /** @var array */
    const SIDES = ['width', 'height', 'length'];
}
