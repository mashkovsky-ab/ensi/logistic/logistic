<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationComplexShipmentDto;

/**
 * Вычислить общие габариты отправления по габаритам его товаров
 * Class CalcShipmentSizesAction
 * @package App\Domain\Calculators\Actions
 */
class CalcShipmentSizesAction implements WithSides
{
    /**
     * CalcShipmentSizesAction constructor.
     * @param  CalcShipmentVolumeAction  $calcShipmentVolumeAction
     * @param  CalcShipmentMaxSideAction  $calcShipmentMaxSideAction
     */
    public function __construct(
        protected CalcShipmentVolumeAction $calcShipmentVolumeAction,
        protected CalcShipmentMaxSideAction $calcShipmentMaxSideAction,
    ) {
    }

    /**
     * @param  CalculationComplexShipmentDto  $complexShipment
     */
    public function execute(CalculationComplexShipmentDto $complexShipment): void
    {
        $width = $height = $length = 0;

        $this->calcShipmentVolumeAction->execute($complexShipment);
        [$maxSide, $maxSideName] = $this->calcShipmentMaxSideAction->execute($complexShipment);
        $avgSide = pow($complexShipment->volume, 1 / 3);

        if ($maxSide <= $avgSide) {
            foreach (self::SIDES as $side) {
                $$side = $avgSide;
            }
        } else {
            $otherSide = sqrt($complexShipment->volume / $maxSide);
            foreach (self::SIDES as $side) {
                if ($side == $maxSideName) {
                    $$side = $maxSide;
                } else {
                    $$side = $otherSide;
                }
            }
        }

        $complexShipment->width = $width;
        $complexShipment->height = $height;
        $complexShipment->length = $length;
    }
}
