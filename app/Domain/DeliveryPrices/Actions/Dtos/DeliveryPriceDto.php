<?php

namespace App\Domain\DeliveryPrices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryPriceDto
 * @package App\Domain\DeliveryPrices\Actions\Dtos
 *
 * @property int $federal_district_id - id федерального округа
 * @property int $region_id - id региона
 * @property string $region_guid - id ФИАС региона
 * @property int $delivery_service - служба доставки
 * @property int $delivery_method - способ доставки на последней миле (доставка до места получения заказа)
 * @property int $price - цена доставки @todo - использовать копейки
 */
class DeliveryPriceDto extends Fluent
{
}
