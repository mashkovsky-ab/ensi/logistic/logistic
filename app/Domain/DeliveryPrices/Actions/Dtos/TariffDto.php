<?php

namespace App\Domain\DeliveryPrices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class TariffDto
 * @package App\Domain\DeliveryPrices\Actions\Dtos
 *
 * @property int $delivery_service_id - id службы доставки
 * @property int $delivery_method_id - id способа доставки на последней миле (доставка до места получения заказа)
 * @property int $shipment_method_id - id способа доставки на нулевой миле
 * (доставка от продавца до распределительного центра)
 * @property string $name - название
 * @property string $description - описание
 * @property string $external_id - id тарифа у службы доставки
 * @property string $apiship_external_id - id тарифа у службы ApiShip
 * @property int $weight_min - минимальный вес заказа
 * @property int $weight_max - максимальный вес заказа
 */
class TariffDto extends Fluent
{
}
