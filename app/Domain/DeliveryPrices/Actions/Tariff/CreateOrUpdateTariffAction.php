<?php

namespace App\Domain\DeliveryPrices\Actions\Tariff;

use App\Domain\DeliveryPrices\Actions\Dtos\TariffDto;
use App\Domain\DeliveryPrices\Models\Tariff;

class CreateOrUpdateTariffAction
{
    public function execute(TariffDto $tariffDto): Tariff
    {
        $tariff = Tariff::query()
            ->where('delivery_service', $tariffDto->delivery_service_id)
            ->where('external_id', $tariffDto->external_id)
            ->first();
        if (is_null($tariff)) {
            $tariff = new Tariff();
            $tariff->delivery_service = $tariffDto->delivery_service_id;
            $tariff->external_id = $tariffDto->external_id;
        }
        $tariff->name = $tariffDto->name;
        $tariff->description = $tariffDto->description;
        $tariff->apiship_external_id = $tariffDto->apiship_external_id;
        $tariff->delivery_method = $tariffDto->delivery_method_id;
        $tariff->shipment_method = $tariffDto->shipment_method_id;
        $tariff->weight_min = $tariffDto->weight_min;
        $tariff->weight_max = $tariffDto->weight_max;

        $tariff->save();

        return $tariff;
    }
}
