<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use function get_region_by_city_guid;

class GetDeliveryPricesAction
{
    public function execute(string $cityGuidTo, array $availableDeliveryServices): array
    {
        $deliveryPrices = [];
        $region = get_region_by_city_guid($cityGuidTo);

        if ($region) {
            /*
             * Получаем цены доставки в регион, с учетом того, что цена доставки, указанная конкретно для региона
             * переопределяет цену, указанную для федерального округа
             */
            /** @var Collection|DeliveryPrice[] $items */
            $items = DeliveryPrice::query()->where(function (Builder $query) use ($region) {
                $query->where(function (Builder $query) use ($region) {
                    $query->where('federal_district_id', $region->federal_district_id)
                        ->whereNull('region_id');
                })
                    ->orWhere('region_id', $region->id);
            })
                ->whereIn('delivery_service', $availableDeliveryServices)
                ->orderBy('federal_district_id')
                ->orderBy('region_id')
                ->get();
            foreach ($items as $item) {
                $deliveryPrices[$item->delivery_method][$item->delivery_service] = $item->price;
            }
        }

        return $deliveryPrices;
    }
}
