<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;

class DeleteDeliveryPriceAction
{
    public function execute(int $deliveryPriceId)
    {
        $deliveryPrice = DeliveryPrice::query()->findOrFail($deliveryPriceId);
        $deliveryPrice->delete();
    }
}
