<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Actions\Dtos\DeliveryPriceDto;
use App\Domain\DeliveryPrices\Models\DeliveryPrice;

class CreateDeliveryPriceAction
{
    public function execute(DeliveryPriceDto $deliveryPriceDto): DeliveryPrice
    {
        $deliveryPrice = new DeliveryPrice();
        $deliveryPrice->federal_district_id = $deliveryPriceDto->federal_district_id;
        $deliveryPrice->region_id = $deliveryPriceDto->region_id;
        $deliveryPrice->region_guid = $deliveryPriceDto->region_guid;
        $deliveryPrice->delivery_service = $deliveryPriceDto->delivery_service;
        $deliveryPrice->delivery_method = $deliveryPriceDto->delivery_method;
        $deliveryPrice->price = $deliveryPriceDto->price;

        $deliveryPrice->save();

        return $deliveryPrice;
    }
}
