<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

/**
 * Получить стоимость доставки для покупателя
 */
class GetDeliveryCostAction
{
    /**
     * @param  array  $deliveryPrices
     * @param  int  $deliveryMethod  - способ доставки (курьерская доставка, самовывоз)
     * @param  int  $deliveryService  - служба доставки
     * @return int
     */
    public function execute(array $deliveryPrices, int $deliveryMethod, int $deliveryService): int
    {
        return $deliveryPrices[$deliveryMethod][$deliveryService] ?? 0;
    }
}
