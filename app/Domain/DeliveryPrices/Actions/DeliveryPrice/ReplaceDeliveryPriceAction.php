<?php

namespace App\Domain\DeliveryPrices\Actions\DeliveryPrice;

use App\Domain\DeliveryPrices\Actions\Dtos\DeliveryPriceDto;
use App\Domain\DeliveryPrices\Models\DeliveryPrice;

class ReplaceDeliveryPriceAction
{
    public function execute(int $deliveryPriceId, DeliveryPriceDto $deliveryPriceDto): DeliveryPrice
    {
        /** @var DeliveryPrice $deliveryPrice */
        $deliveryPrice = DeliveryPrice::query()->findOrFail($deliveryPriceId);

        $deliveryPrice->federal_district_id = $deliveryPriceDto->federal_district_id;
        $deliveryPrice->region_id = $deliveryPriceDto->region_id;
        $deliveryPrice->region_guid = $deliveryPriceDto->region_guid;
        $deliveryPrice->delivery_service = $deliveryPriceDto->delivery_service;
        $deliveryPrice->delivery_method = $deliveryPriceDto->delivery_method;
        $deliveryPrice->price = $deliveryPriceDto->price;

        $deliveryPrice->save();

        return $deliveryPrice;
    }
}
