<?php

namespace App\Domain\DeliveryPrices\Models;

use App\Domain\DeliveryPrices\Models\Tests\Factories\TariffFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Тариф на доставку/самовывоз
 * Class Tariff
 * @package App\Domain\DeliveryPrices\Models
 *
 * @property int $id - id
 * @property int $delivery_service - id службы доставки
 * @property int $delivery_method - id способа доставки на последней миле (доставка до места получения заказа)
 * @property int $shipment_method - id способа доставки на нулевой миле (доставка от продавца до распределительного центра)
 * @property string $name - название
 * @property string $description - описание
 * @property string $external_id - id тарифа у службы доставки
 * @property string $apiship_external_id - id тарифа у службы ApiShip
 * @property int $weight_min - минимальное ограничение по весу для тарифа (граммы)
 * @property int $weight_max - максимальное ограничение по весу для тарифа (граммы)
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 */
class Tariff extends Model
{
    public static function factory(): TariffFactory
    {
        return TariffFactory::new();
    }
}
