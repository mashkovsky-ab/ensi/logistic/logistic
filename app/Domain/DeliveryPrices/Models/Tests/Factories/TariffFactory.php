<?php

namespace App\Domain\DeliveryPrices\Models\Tests\Factories;

use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use App\Domain\DeliveryServices\Enums\ShipmentMethod;
use Illuminate\Database\Eloquent\Factories\Factory;

class TariffFactory extends Factory
{
    protected $model = Tariff::class;

    public function definition()
    {
        return [
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::toValues()),
            'delivery_method' => $this->faker->randomElement(DeliveryMethod::validValues()),
            'shipment_method' => $this->faker->randomElement(ShipmentMethod::validValues()),
            'name' => $this->faker->text(50),
            'description' => $this->faker->text(),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'apiship_external_id' => $this->faker->unique()->numerify('#########'),
            'weight_min' => (int)$this->faker->randomFloat(500, 10000),
            'weight_max' => (int)$this->faker->randomFloat(10000, 20000),
        ];
    }
}
