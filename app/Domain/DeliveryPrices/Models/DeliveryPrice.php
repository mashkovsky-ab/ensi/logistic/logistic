<?php

namespace App\Domain\DeliveryPrices\Models;

use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\Geos\Models\FederalDistrict;
use App\Domain\Geos\Models\Region;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Стоимость доставки по регионам
 * Class DeliveryPrice
 * @package App\Domain\DeliveryPrices\Models
 *
 * @property int $id - id
 * @property int $federal_district_id - id федерального округа
 * @property int $region_id - id региона
 * @property string $region_guid - id ФИАС региона
 * @property int $delivery_service - id службы доставки
 * @property int $delivery_method - способ доставки на последней миле (доставка до места получения заказа)
 * @property int $price - цена доставки
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 * @property FederalDistrict $federalDistrict - федеральный округ
 * @property Region $region - федеральный округ
 */
class DeliveryPrice extends Model
{
    /**
     * @return BelongsTo
     */
    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    /**
     * @return BelongsTo
     */
    public function federalDistrict(): BelongsTo
    {
        return $this->belongsTo(FederalDistrict::class);
    }

    /**
     * @return BelongsTo
     */
    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }
}
