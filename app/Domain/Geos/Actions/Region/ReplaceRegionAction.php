<?php

namespace App\Domain\Geos\Actions\Region;

use App\Domain\Geos\Actions\Dtos\RegionDto;
use App\Domain\Geos\Models\Region;

class ReplaceRegionAction
{
    public function execute(int $regionId, RegionDto $regionDto): Region
    {
        /** @var Region $region */
        $region = Region::query()->findOrFail($regionId);

        $region->federal_district_id = $regionDto->federal_district_id;
        $region->name = $regionDto->name;
        $region->guid = $regionDto->guid;

        $region->save();

        return $region;
    }
}
