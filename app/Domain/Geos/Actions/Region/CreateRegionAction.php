<?php

namespace App\Domain\Geos\Actions\Region;

use App\Domain\Geos\Actions\Dtos\RegionDto;
use App\Domain\Geos\Models\Region;

class CreateRegionAction
{
    public function execute(RegionDto $regionDto): Region
    {
        $region = new Region();
        $region->federal_district_id = $regionDto->federal_district_id;
        $region->name = $regionDto->name;
        $region->guid = $regionDto->guid;

        $region->save();

        return $region;
    }
}
