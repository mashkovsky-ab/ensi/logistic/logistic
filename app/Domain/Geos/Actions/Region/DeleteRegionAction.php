<?php

namespace App\Domain\Geos\Actions\Region;

use App\Domain\Geos\Models\Region;

class DeleteRegionAction
{
    public function execute(int $regionId)
    {
        $region = Region::query()->findOrFail($regionId);
        $region->delete();
    }
}
