<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Actions\Dtos\FederalDistrictDto;
use App\Domain\Geos\Models\FederalDistrict;

class ReplaceFederalDistrictAction
{
    public function execute(int $federalDistrictId, FederalDistrictDto $regionDto): FederalDistrict
    {
        /** @var FederalDistrict $region */
        $region = FederalDistrict::query()->findOrFail($federalDistrictId);

        $region->name = $regionDto->name;

        $region->save();

        return $region;
    }
}
