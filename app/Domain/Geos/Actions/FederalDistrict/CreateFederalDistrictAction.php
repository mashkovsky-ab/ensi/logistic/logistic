<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Actions\Dtos\FederalDistrictDto;
use App\Domain\Geos\Models\FederalDistrict;

class CreateFederalDistrictAction
{
    public function execute(FederalDistrictDto $regionDto): FederalDistrict
    {
        $federalDistrict = new FederalDistrict();
        $federalDistrict->name = $regionDto->name;

        $federalDistrict->save();

        return $federalDistrict;
    }
}
