<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Models\FederalDistrict;

class DeleteFederalDistrictAction
{
    public function execute(int $federalDistrictId)
    {
        $federalDistrict = FederalDistrict::query()->findOrFail($federalDistrictId);
        $federalDistrict->delete();
    }
}
