<?php

namespace App\Domain\Geos\Actions\FederalDistrict;

use App\Domain\Geos\Actions\Dtos\FederalDistrictDto;
use App\Domain\Geos\Models\FederalDistrict;

class PatchFederalDistrictAction
{
    public function execute(int $federalDistrictId, FederalDistrictDto $regionDto): FederalDistrict
    {
        /** @var FederalDistrict $federalDistrict */
        $federalDistrict = FederalDistrict::query()->findOrFail($federalDistrictId);

        $federalDistrict->name = $regionDto['name'] ?? $federalDistrict->name;

        $federalDistrict->save();

        return $federalDistrict;
    }
}
