<?php

namespace App\Domain\Geos\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class FederalDistrictDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property string $name - название
 */
class FederalDistrictDto extends Fluent
{
}
