<?php

namespace App\Domain\Geos\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class RegionDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property int $federal_district_id - id федерального округа
 * @property string $name - название
 * @property string $guid - ФИАС id
 */
class RegionDto extends Fluent
{
}
