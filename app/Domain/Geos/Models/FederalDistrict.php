<?php

namespace App\Domain\Geos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Федеральный округ
 * Class FederalDistrict
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property string $name - название
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property Collection|Region[] $regions
 */
class FederalDistrict extends Model
{
    /**
     * @return HasMany
     */
    public function regions(): HasMany
    {
        return $this->hasMany(Region::class);
    }
}
