<?php

namespace App\Domain\Geos\Models;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\DeliveryServices\Models\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Регион
 * Class Region
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property int $federal_district_id - id федерального округа
 * @property string $name - название
 * @property string $guid - id ФИАС региона
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property FederalDistrict $federalDistrict
 * @property Collection|DeliveryPrice[] $deliveryPrices
 * @property Collection|City[] $cities
 */
class Region extends Model
{
    /**
     * @return BelongsTo
     */
    public function federalDistrict(): BelongsTo
    {
        return $this->belongsTo(FederalDistrict::class);
    }

    /**
     * @return HasMany
     */
    public function deliveryPrices(): HasMany
    {
        return $this->hasMany(DeliveryPrice::class);
    }

    /**
     * @return HasMany
     */
    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }
}
