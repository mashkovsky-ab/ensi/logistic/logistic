<?php

namespace App\Domain\DeliveryOrders\Events;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CancelDeliveryOrderEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public function __construct(public DeliveryOrder $deliveryOrder)
    {
    }
}
