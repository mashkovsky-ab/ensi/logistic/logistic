<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;

class ReplaceDeliveryOrderStatusMappingAction
{
    public function execute(int $deliveryOrderStatusMappingId, array $fields): DeliveryOrderStatusMapping
    {
        $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::findOrFail($deliveryOrderStatusMappingId);

        $newAttributes = $fields;
        foreach (DeliveryOrderStatusMapping::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $deliveryOrderStatusMapping->update($newAttributes);

        return $deliveryOrderStatusMapping;
    }
}
