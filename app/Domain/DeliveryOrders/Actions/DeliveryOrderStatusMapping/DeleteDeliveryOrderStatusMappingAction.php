<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;

class DeleteDeliveryOrderStatusMappingAction
{
    public function execute(int $deliveryOrderStatusMappingId)
    {
        $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::query()->findOrFail($deliveryOrderStatusMappingId);
        $deliveryOrderStatusMapping->delete();
    }
}
