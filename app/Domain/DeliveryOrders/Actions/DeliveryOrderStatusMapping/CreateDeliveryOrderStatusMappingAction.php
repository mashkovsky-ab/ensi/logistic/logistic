<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use Illuminate\Support\Arr;

class CreateDeliveryOrderStatusMappingAction
{
    public function execute(array $fields): DeliveryOrderStatusMapping
    {
        return DeliveryOrderStatusMapping::create(Arr::only($fields, DeliveryOrderStatusMapping::FILLABLE));
    }
}
