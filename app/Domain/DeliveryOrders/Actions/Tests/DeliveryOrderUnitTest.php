<?php

use App\Domain\DeliveryOrders\Actions\DeliveryOrder\CancelDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\CreateDeliveryOrderAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrder\PatchDeliveryOrderAction;
use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Events\CancelDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Events\CreateDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Events\PatchDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use App\Domain\External\Actions\DeliveryOrder\CancelDeliveryOrderAction as CancelExternalDeliveryOrderAction;
use App\Domain\External\Actions\DeliveryOrder\CreateDeliveryOrderAction as CreateExternalDeliveryOrderAction;
use App\Domain\External\Actions\DeliveryOrder\UpdateDeliveryOrderAction as UpdateExternalDeliveryOrderAction;
use App\Domain\External\Dto\Response\DeliveryOrderResponseDto;
use Tests\IntegrationTestCase;
use function Pest\Laravel\assertDatabaseHas;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region CreateDeliveryOrderAction
test("CreateDeliveryOrderAction success", function () {
    /** @var IntegrationTestCase $this */
    $deliveryOrderFields = DeliveryOrder::factory()->make()->toArray();
    $deliveryOrderPlacesFields = DeliveryOrderPlace::factory()->count(3)->make()->toArray();
    $deliveryOrderFields['places'] = $deliveryOrderPlacesFields;
    foreach ($deliveryOrderFields['places'] as &$place) {
        $deliveryOrderPlaceItemsFields = DeliveryOrderPlaceItem::factory()->count(3)->make()->toArray();
        $place['items'] = $deliveryOrderPlaceItemsFields;
    }

    $this->mock(CreateExternalDeliveryOrderAction::class)
        ->allows([
            'execute' => new DeliveryOrderResponseDto(['external_id' => 1]),
        ]);

    Event::fake([CreateDeliveryOrderEvent::class]);
    $deliveryOrder = resolve(CreateDeliveryOrderAction::class)->execute($deliveryOrderFields);
    Event::assertDispatched(CreateDeliveryOrderEvent::class);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
    ]);
});
// endregion

// region PatchDeliveryOrderAction
test("PatchDeliveryAction success", function () {
    /** @var IntegrationTestCase $this */
    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create(['status' => DeliveryOrderStatus::NEW]);

    $this->mock(UpdateExternalDeliveryOrderAction::class)
        ->allows([
            'execute' => new DeliveryOrderResponseDto(),
        ]);
    Event::fake([PatchDeliveryOrderEvent::class]);
    $fields = [
        'status' => DeliveryOrderStatus::SHIPPED,
        'places' => DeliveryOrderPlace::factory()->count(5)->make([
            'items' => DeliveryOrderPlaceItem::factory()->count(3)->make()->toArray()
        ])->toArray(),
    ];
    $deliveryOrder = resolve(PatchDeliveryOrderAction::class)->execute($deliveryOrder->id, $fields);

    Event::assertDispatched(PatchDeliveryOrderEvent::class);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => $deliveryOrder->status,
    ]);
});
// endregion

// region CancelDeliveryOrderAction
test("CancelDeliveryAction success", function () {
    /** @var IntegrationTestCase $this */
    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()->create(['status' => DeliveryOrderStatus::NEW]);

    $this->mock(CancelExternalDeliveryOrderAction::class)->shouldReceive('execute');
    Event::fake();
    $deliveryOrder = resolve(CancelDeliveryOrderAction::class)->execute($deliveryOrder->id);

    Event::assertDispatched(CancelDeliveryOrderEvent::class);

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => $deliveryOrder->status,
    ]);
});
// endregion
