<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\Support\Actions\CalcModelSizesAction;

class CalcDeliveryOrderPlaceSizesAction extends CalcModelSizesAction
{
    protected DeliveryOrderPlace $deliveryOrderPlace;

    public function execute(DeliveryOrderPlace $place)
    {
        $this->deliveryOrderPlace = $place;
        $this->fillSizes();
        $this->deliveryOrderPlace->save();
    }

    protected function calcVolume(): void
    {
        foreach ($this->deliveryOrderPlace->items as $item) {
            $this->volume += $item->width * $item->height * $item->length;
        }
    }

    protected function calcMaxSide(): void
    {
        foreach ($this->deliveryOrderPlace->items as $item) {
            $this->checkMaxSize($item->weight, static::MAX_WIDTH);
            $this->checkMaxSize($item->height, static::MAX_HEIGHT);
            $this->checkMaxSize($item->length, static::MAX_LENGTH);
        }
    }

    protected function setWeight(): void
    {
        $weight = 0;

        foreach ($this->deliveryOrderPlace->items as $item) {
            $weight += $item->weight;
        }

        $this->deliveryOrderPlace->weight = $weight;
    }

    protected function setWidth(float $width): void
    {
        $this->deliveryOrderPlace->width = (int)round($width);
    }

    protected function setHeight(float $height): void
    {
        $this->deliveryOrderPlace->height = (int)round($height);
    }

    protected function setLength(float $length): void
    {
        $this->deliveryOrderPlace->length = (int)round($length);
    }
}
