<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Events\PatchDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Messages\Listen\Dtos\DeliveryDto;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Illuminate\Support\Facades\DB;
use Throwable;

class PatchDeliveryOrderAction
{
    public function __construct(
        protected CreateDeliveryOrderPlaceAction $createPlaceAction,
        protected PatchDeliveryOrderPlaceAction $patchPlaceAction,
        protected DeleteDeliveryOrderPlaceAction $deletePlaceAction,
    ) {
    }

    /**
     * @param int $deliveryOrderId
     * @param array $fields
     * @return DeliveryOrder
     * @throws Throwable
     */
    public function execute(int $deliveryOrderId, array $fields): DeliveryOrder
    {
        return DB::transaction(function () use ($deliveryOrderId, $fields) {
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = DeliveryOrder::query()->findOrFail($deliveryOrderId);
            $deliveryOrder->update($fields);

            if (isset($fields['places'])) {
                $placeIds = [];
                //Создаем/патчим существующие места
                foreach ($fields['places'] as $place) {
                    if (isset($place['id'])) {
                        $place = $this->patchPlaceAction->execute($place['id'], $place);
                    } else {
                        $place = $this->createPlaceAction->execute($deliveryOrder, $place);
                    }
                    $placeIds[] = $place->id;
                }

                //Удаляем места, которых больше нет
                foreach ($deliveryOrder->places as $place) {
                    if (!in_array($place->id, $placeIds)) {
                        $this->deletePlaceAction->execute($place->id);
                    }
                }
            }

            PatchDeliveryOrderEvent::dispatch($deliveryOrder);

            return $deliveryOrder;
        });
    }

    public function patchFromDeliveryDto(int $deliveryOrderId, DeliveryDto $deliveryDto): ?DeliveryOrder
    {
        if ($deliveryDto->status == DeliveryStatusEnum::ASSEMBLED) {
            return $this->execute($deliveryOrderId, $deliveryDto->toDeliveryOrderFields());
        }

        return null;
    }
}
