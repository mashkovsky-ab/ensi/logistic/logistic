<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Events\CancelDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;

class CancelDeliveryOrderAction
{
    public function execute(int $orderId): DeliveryOrder
    {
        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = DeliveryOrder::query()->findOrFail($orderId);

        $deliveryOrder->status = DeliveryOrderStatus::CANCELED;
        $deliveryOrder->save();

        CancelDeliveryOrderEvent::dispatch($deliveryOrder);

        return $deliveryOrder;
    }
}
