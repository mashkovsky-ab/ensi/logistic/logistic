<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use Illuminate\Support\Facades\DB;
use Throwable;

class PatchDeliveryOrderPlaceAction
{
    public function __construct(
        protected CreateDeliveryOrderPlaceItemAction $createItemAction,
        protected PatchDeliveryOrderPlaceItemAction $patchItemAction,
        protected DeleteDeliveryOrderPlaceItemAction $deleteItemAction,
        protected CalcDeliveryOrderPlaceSizesAction $calcDeliveryOrderPlaceSizesAction
    ) {
    }

    /**
     * @param int $deliveryPlaceId
     * @param array $fields
     * @return DeliveryOrderPlace
     * @throws Throwable
     */
    public function execute(int $deliveryPlaceId, array $fields): DeliveryOrderPlace
    {
        return DB::transaction(function () use ($deliveryPlaceId, $fields) {
            /** @var DeliveryOrderPlace $deliveryOrderPlace */
            $deliveryOrderPlace = DeliveryOrderPlace::query()->findOrFail($deliveryPlaceId);
            $deliveryOrderPlace->update($fields);

            if (isset($fields['items'])) {
                $itemIds = [];
                //Создаем/патчим существующие товары места
                foreach ($fields['items'] as $item) {
                    if (isset($item['id'])) {
                        $item = $this->patchItemAction->execute($item['id'], $item);
                    } else {
                        $item = $this->createItemAction->execute($deliveryOrderPlace, $item);
                    }
                    $itemIds[] = $item->id;
                }

                //Удаляем товары места, которых больше нет
                foreach ($deliveryOrderPlace->items as $item) {
                    if (!in_array($item->id, $itemIds)) {
                        $this->deleteItemAction->execute($item->id);
                    }
                }
            }

            $this->calcDeliveryOrderPlaceSizesAction->execute($deliveryOrderPlace);

            return $deliveryOrderPlace;
        });
    }
}
