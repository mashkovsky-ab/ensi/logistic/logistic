<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class CreateDeliveryOrderPlaceAction
{
    public function __construct(
        protected CreateDeliveryOrderPlaceItemAction $createItemAction,
        protected CalcDeliveryOrderPlaceSizesAction $calcDeliveryOrderPlaceSizesAction
    ) {
    }

    public function execute(DeliveryOrder $deliveryOrder, array $fields): DeliveryOrderPlace
    {
        return DB::transaction(function () use ($deliveryOrder, $fields) {
            $fields['delivery_order_id'] = $deliveryOrder->id;
            /** @var DeliveryOrderPlace $deliveryOrderPlace */
            $deliveryOrderPlace = DeliveryOrderPlace::create(Arr::only($fields, DeliveryOrderPlace::FILLABLE));
            $deliveryOrder->places->push($deliveryOrderPlace);
            $deliveryOrderPlace->number = $this->generatePlaceNumber($deliveryOrder);
            $deliveryOrderPlace->saveQuietly();

            foreach ($fields['items'] as $item) {
                $this->createItemAction->execute($deliveryOrderPlace, $item);
            }

            $this->calcDeliveryOrderPlaceSizesAction->execute($deliveryOrderPlace);

            return $deliveryOrderPlace;
        });
    }

    protected function generatePlaceNumber(DeliveryOrder $deliveryOrder): string
    {
        return $deliveryOrder->number . '-' . $deliveryOrder->places->count();
    }
}
