<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;

class DeleteDeliveryOrderPlaceItemAction
{
    /**
     * @param int $deliveryOrderPlaceItemId
     */
    public function execute(int $deliveryOrderPlaceItemId): void
    {
        /** @var DeliveryOrderPlaceItem $deliveryOrderPlaceItem */
        $deliveryOrderPlaceItem = DeliveryOrderPlaceItem::query()->findOrFail($deliveryOrderPlaceItemId);
        $deliveryOrderPlaceItem->delete();
    }
}
