<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;

class DeleteDeliveryOrderPlaceAction
{
    /**
     * @param int $deliveryPlaceId
     */
    public function execute(int $deliveryPlaceId): void
    {
        /** @var DeliveryOrderPlace $deliveryOrderPlace */
        $deliveryOrderPlace = DeliveryOrderPlace::query()->findOrFail($deliveryPlaceId);
        $deliveryOrderPlace->delete();
    }
}
