<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\External\Actions\DeliveryOrder\GetDeliveryOrderStatusAction;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class UpdateDeliveryOrderStatusesAction
{
    protected LoggerInterface $logger;

    public function __construct(protected GetDeliveryOrderStatusAction $getDeliveryOrderStatusAction)
    {
        $this->logger = Log::channel('update:delivery_order_statuses');
    }

    public function execute(): void
    {
        $deliveryOrders = DeliveryOrder::inDelivery()->get()->keyBy('number');

        if ($deliveryOrders->isNotEmpty()) {
            $deliveryOrdersByDeliveryService = $deliveryOrders->groupBy('delivery_service_id');
            foreach ($deliveryOrdersByDeliveryService as $deliveryServiceId => $chunkDeliveryOrders) {
                try {
                    /** @var Collection|DeliveryOrder[] $chunkDeliveryOrders */
                    $deliveryOrderStatusOutputDtos = $this->getDeliveryOrderStatusAction->execute(
                        $deliveryServiceId,
                        $chunkDeliveryOrders->pluck('xml_id')->all()
                    )->items;
                    foreach ($deliveryOrderStatusOutputDtos as $deliveryOrderStatusOutputDto) {
                        if ($deliveryOrders->has($deliveryOrderStatusOutputDto->number)) {
                            $deliveryOrder = $deliveryOrders[$deliveryOrderStatusOutputDto->number];
                            if ($deliveryOrderStatusOutputDto->success) {
                                if ($deliveryOrderStatusOutputDto->status && $deliveryOrder->status != $deliveryOrderStatusOutputDto->status) {
                                    $deliveryOrder->status = $deliveryOrderStatusOutputDto->status;
                                }
                                $deliveryOrder->external_status = $deliveryOrderStatusOutputDto->status_external_id;
                                $deliveryOrder->external_status_at = new Carbon($deliveryOrderStatusOutputDto->status_date);

                                $deliveryOrder->save();
                            }
                        }
                    }
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [
                        'code' => $e->getCode(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'trace' => $e->getTraceAsString(),
                    ]);
                }
            }
        }
    }
}
