<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Events\CreateDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\Kafka\Messages\Listen\Dtos\DeliveryDto;
use App\Domain\Support\Data\Address;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Throwable;

class CreateDeliveryOrderAction
{
    public function __construct(
        protected CreateDeliveryOrderPlaceAction $createPlaceAction
    ) {
    }

    /**
     * @param array $fields
     * @return DeliveryOrder
     * @throws Throwable
     */
    public function execute(array $fields): DeliveryOrder
    {
        return DB::transaction(function () use ($fields) {
            if (is_array($fields['sender_address'])) {
                $fields['sender_address'] = new Address($fields['sender_address']);
            }
            if (is_array($fields['recipient_address'])) {
                $fields['recipient_address'] = new Address($fields['recipient_address']);
            }
            $fields['status'] = $fields['status'] ?? DeliveryOrderStatus::NEW;
            $deliveryOrder = DeliveryOrder::create(Arr::only($fields, DeliveryOrder::FILLABLE));

            foreach ($fields['places'] as $place) {
                $this->createPlaceAction->execute($deliveryOrder, $place);
            }

            CreateDeliveryOrderEvent::dispatch($deliveryOrder);

            return $deliveryOrder;
        });
    }

    public function createFromDeliveryDto(DeliveryDto $deliveryDto): ?DeliveryOrder
    {
        if ($deliveryDto->status == DeliveryStatusEnum::ASSEMBLED) {
            return $this->execute($deliveryDto->toDeliveryOrderFields());
        }

        return null;
    }
}
