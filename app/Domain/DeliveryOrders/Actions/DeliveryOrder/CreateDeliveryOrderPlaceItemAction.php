<?php

namespace App\Domain\DeliveryOrders\Actions\DeliveryOrder;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use Illuminate\Support\Arr;

class CreateDeliveryOrderPlaceItemAction
{
    public function execute(DeliveryOrderPlace $deliveryOrderPlace, array $fields): DeliveryOrderPlaceItem
    {
        $fields['delivery_order_place_id'] = $deliveryOrderPlace->id;
        /** @var DeliveryOrderPlaceItem $deliveryOrderPlaceItem */
        $deliveryOrderPlaceItem = DeliveryOrderPlaceItem::create(Arr::only($fields, DeliveryOrderPlaceItem::FILLABLE));
        $deliveryOrderPlace->items->push($deliveryOrderPlaceItem);

        return $deliveryOrderPlaceItem;
    }
}
