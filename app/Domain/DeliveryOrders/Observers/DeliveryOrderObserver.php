<?php

namespace App\Domain\DeliveryOrders\Observers;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;

class DeliveryOrderObserver
{
    public function saving(DeliveryOrder $deliveryOrder)
    {
        if ($deliveryOrder->status != $deliveryOrder->getOriginal('status')) {
            $deliveryOrder->status_at = now();
        }
        if ($deliveryOrder->external_status != $deliveryOrder->getOriginal('external_status') && !$deliveryOrder->external_status_at) {
            $deliveryOrder->external_status_at = now();
        }
    }
}
