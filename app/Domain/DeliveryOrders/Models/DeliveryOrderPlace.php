<?php

namespace App\Domain\DeliveryOrders\Models;

use App\Domain\DeliveryOrders\Models\Tests\Factories\DeliveryOrderPlaceFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Место (коробка) в заказе на доставку
 * Class DeliveryOrderPlace
 * @package App\Domain\DeliveryOrders\Models
 *
 * @property int $id - id
 * @property int $delivery_order_id - id заказа на доставку*
 *
 * @property string $number - номер места*
 * @property string|null $barcode - штрихкод места
 * @property int $height - высота места (в мм)* (рассчитывается автоматически)
 * @property int $length - длина места (в мм)* (рассчитывается автоматически)
 * @property int $width - ширина места (в мм)* (рассчитывается автоматически)
 * @property int $weight - вес места (в граммах)* (рассчитывается автоматически)
 *
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryOrder $order - заказ на доставку
 * @property Collection|DeliveryOrderPlaceItem[] $items - товары места
 */
class DeliveryOrderPlace extends Model
{
    const FILLABLE = [
        'delivery_order_id',
        'barcode',
    ];

    protected $attributes = [
        'height' => 0,
        'length' => 0,
        'width' => 0,
        'weight' => 0,
    ];

    protected $fillable = self::FILLABLE;

    public function order(): BelongsTo
    {
        return $this->belongsTo(DeliveryOrder::class, 'delivery_order_id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(DeliveryOrderPlaceItem::class, 'delivery_order_place_id');
    }

    public static function factory(): DeliveryOrderPlaceFactory
    {
        return DeliveryOrderPlaceFactory::new();
    }
}
