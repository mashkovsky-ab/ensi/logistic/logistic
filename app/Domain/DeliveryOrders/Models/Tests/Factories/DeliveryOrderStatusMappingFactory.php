<?php

namespace App\Domain\DeliveryOrders\Models\Tests\Factories;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryOrderStatusMappingFactory extends Factory
{
    protected $model = DeliveryOrderStatusMapping::class;

    public function definition()
    {
        return [
            'delivery_service_id' => $this->faker->randomElement(DeliveryServiceEnum::toValues()),
            'status' => $this->faker->randomElement(DeliveryOrderStatus::validValues()),
            'external_status' => $this->faker->text(50),
        ];
    }
}
