<?php

namespace App\Domain\DeliveryOrders\Models;

use App\Domain\DeliveryOrders\Models\Tests\Factories\DeliveryOrderStatusMappingFactory;
use App\Domain\DeliveryServices\Models\DeliveryService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Маппинг статусов служб доставок со статусами доставки внутри платформы
 * Class DeliveryOrderStatusMapping
 * @package App\Domain\DeliveryOrders\Models
 *
 * @property int $id - id
 * @property int $delivery_service_id - id службы доставки
 * @property int $status - id статуса заказа на доставку внутри платформы (см. \App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus)
 * @property string $external_status - статус заказа на доставку у службы доставки
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 */
class DeliveryOrderStatusMapping extends Model
{
    protected $table = 'delivery_order_status_mapping';

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'delivery_service_id',
        'status',
        'external_status',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @return BelongsTo
     */
    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    public static function factory(): DeliveryOrderStatusMappingFactory
    {
        return DeliveryOrderStatusMappingFactory::new();
    }
}
