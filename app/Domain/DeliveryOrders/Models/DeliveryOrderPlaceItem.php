<?php

namespace App\Domain\DeliveryOrders\Models;

use App\Domain\DeliveryOrders\Models\Tests\Factories\DeliveryOrderPlaceItemFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Товар места (коробки) в заказе на доставку
 * Class DeliveryOrderPlaceItem
 * @package App\Domain\DeliveryOrders\Models
 *
 * @property int $id - id
 * @property int $delivery_order_place_id - id места в заказе на доставку*
 *
 * @property string|null $vendor_code - артикул товара
 * @property string $name - наименование товара*
 * @property float $qty - кол-во товара*
 * @property float|null $qty_delivered - заполняется только при частичной доставке и показывает сколько вложимых выкуплено
 * @property int $height - высота единицы товара (в мм)*
 * @property int $length - длина единицы товара (в мм)*
 * @property int $width - ширина единицы товара (в мм)*
 * @property int $weight - вес единицы товара (в граммах)*
 * @property int $assessed_cost - оценочная стоимость единицы товара (в коп.)
 * @property int $cost - стоимость единицы товара с учетом НДС (в коп.)*
 * @property int $cost_vat - процентная ставка НДС
 * @property int $price - цена единицы товара к оплате с учетом НДС (в коп.)*
 * @property string|null $barcode - штрихкод на товаре
 *
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryOrderPlace $place - место в заказе на доставку
 */
class DeliveryOrderPlaceItem extends Model
{
    protected $casts = [
        'qty' => 'float',
        'qty_delivered' => 'float',
    ];

    const FILLABLE = [
        'delivery_order_place_id',
        'vendor_code',
        'barcode',
        'name',
        'qty',
        'qty_delivered',
        'height',
        'length',
        'width',
        'weight',
        'assessed_cost',
        'cost',
        'cost_vat',
        'price',
    ];

    protected $fillable = self::FILLABLE;

    public function place(): BelongsTo
    {
        return $this->belongsTo(DeliveryOrderPlace::class, 'delivery_order_place_id');
    }

    public static function factory(): DeliveryOrderPlaceItemFactory
    {
        return DeliveryOrderPlaceItemFactory::new();
    }
}
