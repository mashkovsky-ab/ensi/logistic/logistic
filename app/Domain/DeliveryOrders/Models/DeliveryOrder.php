<?php

namespace App\Domain\DeliveryOrders\Models;

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\Tests\Factories\DeliveryOrderFactory;
use App\Domain\DeliveryPrices\Models\Tariff;
use App\Domain\DeliveryServices\Enums\DeliveryServiceStatus;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\Support\Data\Address;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Заказ на доставку
 * Class DeliveryOrder
 * @package App\Domain\DeliveryOrders\Models
 *
 * @property int $id - id
 * @property int $delivery_id - id Отправления в OMS*
 * @property int $delivery_service_id - id службы доставки*
 *
 * @property string $number - номер заказа на доставку внутри платформы (совпадает с номером Отправления в OMS)*
 * @property string|null $external_id - номер заказа на доставку у службы доставки
 * @property string|null $error_external_id - текст последней ошибки при создании заказа на доставку в службе доставки
 * @property int $status - id статуса заказа на доставку внутри платформы (см. \App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus)*
 * @property Carbon $status_at - дата и время изменения статуса заказа на доставку внутри платформы*
 * @property string $external_status - статус заказа на доставку у службы доставки
 * @property Carbon $external_status_at - дата и время изменения статуса заказа на доставку у службы доставки
 * @property int $height - высота заказа (в мм)*
 * @property int $length - длина заказа (в мм)*
 * @property int $width - ширина заказа (в мм)*
 * @property int $weight - вес всего заказа (в граммах)*
 * @property int $shipment_method - способ доставки на нулевой миле (доставка от продавца до распределительного центра)*
 * @property int $delivery_method - способ доставки на последней миле (доставка до места получения заказа)*
 * @property string $tariff_id - тариф службы доставки по которому осуществляется доставка*
 * @property Carbon|null $delivery_date - желаемая дата доставки
 * @property int|null $point_in_id - ID пункта приема заказа
 * @property int|null $point_out_id - ID пункта выдачи заказа
 * @property Carbon|null $shipment_time_start - начальное время забора груза (чч:мм)
 * @property Carbon|null $shipment_time_end - конечное время забора груза (чч:мм)
 * @property Carbon|null $delivery_time_start - начальное время доставки (чч:мм)
 * @property Carbon|null $delivery_time_end - конечное время доставки (чч:мм)
 * @property string|null $delivery_time_code - код времени доставки (чч:мм)
 * @property string|null $description - комментарий
 *
 * @property int|null $assessed_cost - оценочная стоимость / сумма страховки (коп.)
 * @property int $delivery_cost - стоимость доставки с учетом НДС (коп.)*
 * @property int|null $delivery_cost_vat - процентная ставка НДС
 * @property int $delivery_cost_pay - стоимость доставки к оплате с учетом НДС (коп.)*
 * @property int|null $cod_cost - сумма наложенного платежа с учетом НДС (коп.)ensi_logistic_logistic_test_1
 * @property bool $is_delivery_payed_by_recipient - флаг для указания стороны, которая платит за услуги доставки (0-отправитель, 1-получатель)
 *
 * @property bool $sender_is_seller - отправитель является истинным продавцом, а не маркетплейсом?
 * @property string|null $sender_inn - инн отправителя
 * @property Address $sender_address - адрес отправителя
 * @property string|null $sender_company_name - название компании отправителя
 * @property string|null $sender_contact_name - ФИО контактного лица отправителя
 * @property string|null $sender_email - контактный email отправителя
 * @property string|null $sender_phone - контактный телефон отправителя
 * @property string|null $sender_comment - комментарий отправителя
 *
 * @property Address|null $recipient_address - адрес получателя
 * @property string|null $recipient_company_name - название компании получателя
 * @property string|null $recipient_contact_name - ФИО контактного лица получателя
 * @property string|null $recipient_email - контактный email получателя
 * @property string|null $recipient_phone - контактный телефон получателя
 * @property string|null $recipient_comment - комментарий получателя
 *
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 * @property Tariff $tariff - тариф на доставку/самовывоз
 * @property Point|null $pointIn - пункт приема заказа
 * @property Point|null $pointOut - пункт выдачи заказа
 * @property Collection|DeliveryOrderPlace[] $places - места в заказе на доставку
 *
 * @method static Builder isDeliveryServiceActive() - заказы для активных служб доставки
 * @method static Builder inDelivery() - заказы в доставке
 */
class DeliveryOrder extends Model
{
    protected $casts = [
        'status_at' => 'datetime',
        'external_status_at' => 'datetime',
        'delivery_date' => 'date',
        'shipment_time_start' => 'datetime',
        'shipment_time_end' => 'datetime',
        'delivery_time_start' => 'datetime',
        'delivery_time_end' => 'datetime',
    ];

    const FILLABLE = [
        'delivery_id',
        'delivery_service_id',
        'number',
        'status',
        'height',
        'length',
        'width',
        'weight',
        'shipment_method',
        'delivery_method',
        'tariff_id',
        'delivery_date',
        'point_in_id',
        'point_out_id',
        'shipment_time_start',
        'shipment_time_end',
        'delivery_time_start',
        'delivery_time_end',
        'delivery_time_code',
        'description',
        'assessed_cost',
        'delivery_cost',
        'delivery_cost_vat',
        'delivery_cost_pay',
        'cod_cost',
        'is_delivery_payed_by_recipient',
        'sender_is_seller',
        'sender_inn',
        'sender_address',
        'sender_company_name',
        'sender_contact_name',
        'sender_email',
        'sender_phone',
        'sender_comment',
        'recipient_address',
        'recipient_company_name',
        'recipient_contact_name',
        'recipient_email',
        'recipient_phone',
        'recipient_comment',
    ];

    protected $fillable = self::FILLABLE;

    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    public function tariff(): BelongsTo
    {
        return $this->belongsTo(Tariff::class);
    }

    public function pointIn(): BelongsTo
    {
        return $this->belongsTo(Point::class);
    }

    public function pointOut(): BelongsTo
    {
        return $this->belongsTo(Point::class);
    }

    public function places(): HasMany
    {
        return $this->hasMany(DeliveryOrderPlace::class);
    }

    public static function factory(): DeliveryOrderFactory
    {
        return DeliveryOrderFactory::new();
    }

    protected function getSenderAddressAttribute(): ?Address
    {
        return $this->attributes['sender_address']
            ? new Address(json_decode($this->attributes['sender_address']))
            : null;
    }

    protected function setSenderAddressAttribute(?Address $address)
    {
        $this->attributes['sender_address'] = $address?->toJson();
    }

    protected function getRecipientAddressAttribute(): ?Address
    {
        return $this->attributes['recipient_address']
            ? new Address(json_decode($this->attributes['recipient_address']))
            : null;
    }

    protected function setRecipientAddressAttribute(?Address $address)
    {
        $this->attributes['recipient_address'] = $address?->toJson();
    }

    protected function scopeIsDeliveryServiceActive(Builder $query): Builder
    {
        return $query->whereHas('deliveryService', function (Builder $query) {
                $query->where('status', DeliveryServiceStatus::ACTIVE);
        });
    }

    /**
     * Получить заказы в доставке: выгружены в СД и еще не доставлены
     * @param Builder $query
     * @return Builder
     */
    public static function scopeInDelivery(Builder $query): Builder
    {
        return $query
            ->whereNotNull('external_id')
            ->whereNotIn('status', DeliveryOrderStatus::done());
    }
}
