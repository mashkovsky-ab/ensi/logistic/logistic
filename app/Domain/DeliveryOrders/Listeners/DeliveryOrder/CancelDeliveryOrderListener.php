<?php

namespace App\Domain\DeliveryOrders\Listeners\DeliveryOrder;

use App\Domain\DeliveryOrders\Events\CancelDeliveryOrderEvent;
use App\Domain\External\Actions\DeliveryOrder\CancelDeliveryOrderAction;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

class CancelDeliveryOrderListener
{
    public function __construct(protected CancelDeliveryOrderAction $cancelDeliveryOrderAction)
    {
    }

    /**
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function handle(CancelDeliveryOrderEvent $event)
    {
        $deliveryOrder = $event->deliveryOrder;

        $this->cancelDeliveryOrderAction->execute($deliveryOrder->delivery_service_id, $deliveryOrder->external_id);
    }
}
