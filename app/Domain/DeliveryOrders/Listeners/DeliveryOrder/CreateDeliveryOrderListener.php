<?php

namespace App\Domain\DeliveryOrders\Listeners\DeliveryOrder;

use App\Domain\DeliveryOrders\Events\CreateDeliveryOrderEvent;
use App\Domain\External\Actions\DeliveryOrder\CreateDeliveryOrderAction;
use App\Domain\External\Dto\Request\DeliveryOrderRequestDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

class CreateDeliveryOrderListener
{
    public function __construct(protected CreateDeliveryOrderAction $createDeliveryOrderAction)
    {
    }

    /**
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function handle(CreateDeliveryOrderEvent $event)
    {
        $deliveryOrder = $event->deliveryOrder;

        $deliveryOrderResponseDto = $this->createDeliveryOrderAction->execute($deliveryOrder->delivery_service_id, DeliveryOrderRequestDto::makeFromDeliveryOrder($deliveryOrder));

        $deliveryOrder->external_id = $deliveryOrderResponseDto->external_id;
        $deliveryOrder->error_external_id = $deliveryOrderResponseDto->message;

        $deliveryOrder->save();
    }
}
