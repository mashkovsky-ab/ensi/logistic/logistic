<?php

namespace App\Domain\DeliveryOrders\Listeners\DeliveryOrder;

use App\Domain\DeliveryOrders\Events\PatchDeliveryOrderEvent;
use App\Domain\External\Actions\DeliveryOrder\UpdateDeliveryOrderAction;
use App\Domain\External\Dto\Request\DeliveryOrderRequestDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

class PatchDeliveryOrderListener
{
    public function __construct(protected UpdateDeliveryOrderAction $updateDeliveryOrderAction)
    {
    }

    /**
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function handle(PatchDeliveryOrderEvent $event)
    {
        $deliveryOrder = $event->deliveryOrder;

        $deliveryOrderResponseDto = $this->updateDeliveryOrderAction->execute($deliveryOrder->delivery_service_id, DeliveryOrderRequestDto::makeFromDeliveryOrder($deliveryOrder));

        $deliveryOrder->error_external_id = $deliveryOrderResponseDto->message;

        $deliveryOrder->save();
    }
}
