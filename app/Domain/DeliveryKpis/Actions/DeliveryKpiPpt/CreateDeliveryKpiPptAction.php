<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt;

use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiPptDto;
use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use App\Exceptions\CreateException;

class CreateDeliveryKpiPptAction
{
    public function execute(int $sellerId, DeliveryKpiPptDto $deliveryKpiPptDto): DeliveryKpiPpt
    {
        /** @var DeliveryKpiPpt $deliveryKpiPpt */
        $deliveryKpiPpt = DeliveryKpiPpt::query()
            ->where('seller_id', $sellerId)
            ->first();
        if ($deliveryKpiPpt) {
            throw new CreateException('DeliveryKpiPpt are exist for this seller', 400);
        }
        $deliveryKpiPpt = new DeliveryKpiPpt();
        $deliveryKpiPpt->seller_id = $sellerId;
        $deliveryKpiPpt->ppt = $deliveryKpiPptDto->ppt;

        $deliveryKpiPpt->save();

        return $deliveryKpiPpt;
    }
}
