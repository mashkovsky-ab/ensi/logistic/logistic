<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt;

use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiPptDto;
use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;

class ReplaceDeliveryKpiPptAction
{
    public function execute(int $sellerId, DeliveryKpiPptDto $deliveryKpiPptDto): DeliveryKpiPpt
    {
        /** @var DeliveryKpiPpt $deliveryKpiPpt */
        $deliveryKpiPpt = DeliveryKpiPpt::query()
            ->where('seller_id', $sellerId)
            ->firstOrNew();
        $deliveryKpiPpt->seller_id = $sellerId;
        $deliveryKpiPpt->ppt = $deliveryKpiPptDto->ppt;

        $deliveryKpiPpt->save();

        return $deliveryKpiPpt;
    }
}
