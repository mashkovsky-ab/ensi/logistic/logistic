<?php

namespace App\Domain\DeliveryKpis\Actions;

use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStorePickupTimeDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreWorkingDto;
use App\Domain\DeliveryKpis\Actions\DeliveryKpi\GetRtgAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\GetCtBySellerAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\GetPptBySellerAction;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use function now;

/**
 * Рассчитать PSD - planned shipment date - плановая дата и время, когда отправление должно быть собрано
 * (получить статус "Готово к отгрузке")
 */
class CalcPsdAction
{
    public function __construct(
        protected GetRtgAction $getRtgAction,
        protected GetCtBySellerAction $getCtBySellerAction,
        protected GetPptBySellerAction $getPptBySellerAction
    ) {
    }

    public function execute(
        int $sellerId,
        int $deliveryServiceId,
        CalculationStoreDto $calculationStoreDto
    ): Carbon {
        /** @var Collection|CalculationStorePickupTimeDto[] $workingByDays - график работы склада по дням */
        $workingByDays = $calculationStoreDto->working->sortBy('day')->keyBy('day');
        /**
         * @var Collection|CalculationStorePickupTimeDto[] $pickUpTimesByDelivery - время отгрузки со склада по дням,
         * с учетом того, что значения для конкретной службы доставки переопределяют значения для всех служб доставки
         */
        $pickUpTimesByDelivery = $calculationStoreDto
            ->pickup_times
            ->filter(function (CalculationStorePickupTimeDto $storePickupTimeDto) use ($deliveryServiceId) {
                return $storePickupTimeDto->delivery_service == $deliveryServiceId ||
                    !$storePickupTimeDto->delivery_service;
            })
            ->groupBy('day')
            ->map(function ($group, $day) use ($deliveryServiceId) {
                $storePickupTimeDtoAgg = new CalculationStorePickupTimeDto();
                $storePickupTimeDtoAgg->day = $day;
                $storePickupTimeDtoAgg->pickup_time_code = null;
                $storePickupTimeDtoAgg->pickup_time_start = null;
                $storePickupTimeDtoAgg->pickup_time_end = null;
                $storePickupTimeDtoAgg->cargo_export_time = null;
                $storePickupTimeDtoAgg->delivery_service = $deliveryServiceId;

                /** @var Collection|CalculationStorePickupTimeDto[] $group */
                $group->each(function (CalculationStorePickupTimeDto $storePickupTimeDto) use ($storePickupTimeDtoAgg) {
                    if ($storePickupTimeDto->pickup_time_code) {
                        $storePickupTimeDtoAgg->pickup_time_code = $storePickupTimeDto->pickup_time_code;
                    }
                    if ($storePickupTimeDto->pickup_time_start) {
                        $storePickupTimeDtoAgg->pickup_time_start = $storePickupTimeDto->pickup_time_start;
                    }
                    if ($storePickupTimeDto->pickup_time_end) {
                        $storePickupTimeDtoAgg->pickup_time_end = $storePickupTimeDto->pickup_time_end;
                    }
                    if ($storePickupTimeDto->cargo_export_time) {
                        $storePickupTimeDtoAgg->cargo_export_time = $storePickupTimeDto->cargo_export_time;
                    }
                });

                return $storePickupTimeDtoAgg;
            })
            ->filter(function (CalculationStorePickupTimeDto $storePickupTimeDto) {
                return (
                        $storePickupTimeDto->pickup_time_code ||
                        $storePickupTimeDto->pickup_time_start &&
                        $storePickupTimeDto->pickup_time_end
                    ) && $storePickupTimeDto->cargo_export_time;
            });

        /**
         * Плановое время сборки отправления продавцом в минутах
         * с учетом времени проверки АОЗ и без учета графика работы склада продавца
         */
        $psdWithoutBt = $this->getRtgAction->execute() + $this->getCtBySellerAction->execute($sellerId) + $this->getPptBySellerAction->execute($sellerId);

        $dayPlus = 0;
        $date = now();
        //Дата и время начала обработки отправления
        $startDateTime = null;
        /**
         * Определяем дату и время начала обработки отправления
         */
        while ($dayPlus <= 6) {
            $date->modify('+'.$dayPlus.'day'.($dayPlus > 1 ? 's' : ''));
            $dayPlus++;
            //Получаем номер дня недели (1 - понедельник, ..., 7 - воскресенье)
            $dayOfWeek = $date->format('N');
            if (!$workingByDays->has($dayOfWeek)) {
                continue;
            }
            $storeWorkingStartTime = $workingByDays[$dayOfWeek]->working_start_time;
            $storeWorkingEndTime = $workingByDays[$dayOfWeek]->working_end_time;
            if (!$storeWorkingStartTime || !$storeWorkingEndTime) {
                continue;
            }

            if ($dayPlus == 1) {
                /**
                 * Если склад сегодня работает,
                 * то получаем текущее время
                 */
                $currentTime = Carbon::createFromFormat(
                    CalculationStoreWorkingDto::TIME_FORMAT,
                    $date->format(CalculationStoreWorkingDto::TIME_FORMAT)
                );

                if ($currentTime < $storeWorkingStartTime) {
                    /**
                     * Если текущее время меньше времени начала работы склада,
                     * то устанавливаем время начала сборки = времени начала работы склада
                     */
                    $currentTime = clone $storeWorkingStartTime;
                    /**
                     * Определяем время окончания сборки отправления
                     * и сравниваем его с временем окончания работы склада
                     */
                    $endTime = $currentTime->modify('+'.$psdWithoutBt.' minutes');
                    if ($endTime > $storeWorkingEndTime) {
                        /**
                         * Если время окончания сборки отправления больше времени окончания работы склада,
                         * то переходим на следующий день
                         */
                        continue;
                    } else {
                        /**
                         * Иначе время начала работы склада и будет временем начала сборки
                         */
                        $startDateTime = (new Carbon())->setDateTime(
                            $date->format('Y'),
                            $date->format('m'),
                            $date->format('d'),
                            $storeWorkingStartTime->format('H'),
                            $storeWorkingStartTime->format('i'),
                            $storeWorkingStartTime->format('s'),
                        );

                        break;
                    }
                } elseif ($currentTime > $storeWorkingEndTime) {
                    /**
                     * Если текущее время больше времени окончания работы склада,
                     * то переходим на следующий день
                     */
                    continue;
                } else {
                    /**
                     * Иначе определеяем время окончания сборки отправления
                     * и сравниваем его с временем окончания работы склада
                     */
                    $endTime = $currentTime->modify('+'.$psdWithoutBt.' minutes');
                    if ($endTime > $storeWorkingEndTime) {
                        /**
                         * Если время окончания сборки отправления больше времени окончания работы склада,
                         * то переходим на следующий день
                         */
                        continue;
                    } else {
                        /**
                         * Иначе текущее время и будет временем начала сборки
                         */
                        $startDateTime = $date;

                        break;
                    }
                }
            } else {
                $startTime = clone $storeWorkingStartTime;
                /**
                 * Определяем время окончания сборки отправления
                 * и сравниваем его с временем окончания работы склада
                 */
                $endTime = $startTime->modify('+'.$psdWithoutBt.' minutes');
                if ($endTime > $storeWorkingEndTime) {
                    /**
                     * Если время окончания сборки отправления больше времени окончания работы склада,
                     * то переходим на следующий день
                     */
                    continue;
                } else {
                    /**
                     * Иначе время начала работы склада и будет временем начала сборки
                     */
                    $startDateTime = (new Carbon())->setDateTime(
                        $date->format('Y'),
                        $date->format('m'),
                        $date->format('d'),
                        $storeWorkingStartTime->format('H'),
                        $storeWorkingStartTime->format('i'),
                        $storeWorkingStartTime->format('s'),
                    );

                    break;
                }
            }
        }

        /**
         * Если дата и время начала обработки отправления равна null,
         * значит отправление собрать невозможно
         */
        if (is_null($startDateTime)) {
            throw new Exception('Не возможно собрать отправление с текущим графиком работы склада');
        }

        /**
         * Определяем PSD - плановую дату и время, когда отправление должно быть собрано.
         * Изначально PSD равно:
         * дате и времени начала обработки отправления + время на его проверку (RTG) + подтверждение (CT) + сборку (PPT)
         */
        $startDateTime->modify('+'.$psdWithoutBt.' minutes');
        $endDateTime = clone $startDateTime;
        $dayPlus = 0;
        $isCargoCanBeExported = false;
        while ($dayPlus <= 6) {
            $endDateTime->modify('+'.$dayPlus.'day'.($dayPlus > 1 ? 's' : ''));
            $dayPlus++;
            //Получаем номер дня недели (1 - понедельник, ..., 7 - воскресенье)
            $dayOfWeek = $date->format('N');
            if (!$pickUpTimesByDelivery->has($dayOfWeek)) {
                continue;
            }
            $storeCargoExportTime = $pickUpTimesByDelivery[$dayOfWeek]->cargo_export_time;
            if ($dayPlus == 1) {
                $endTime = Carbon::createFromFormat(
                    CalculationStorePickupTimeDto::TIME_FORMAT,
                    $endDateTime->format(CalculationStorePickupTimeDto::TIME_FORMAT)
                );

                if ($endTime > $storeCargoExportTime) {
                    /**
                     * Если время окончания сборки отправления больше времени создания заявки на забор груза,
                     * то переходим на следующий день
                     */
                    continue;
                }
            }

            /**
             * В качестве времени, когда отправление должно быть собрано,
             * устанавливаем время создания заявки на забор груза
             */
            $endDateTime = (new Carbon())->setDateTime(
                $endDateTime->format('Y'),
                $endDateTime->format('m'),
                $endDateTime->format('d'),
                $storeCargoExportTime->format('H'),
                $storeCargoExportTime->format('i'),
                $storeCargoExportTime->format('s'),
            );
            $isCargoCanBeExported = true;

            break;
        }

        if (!$isCargoCanBeExported) {
            throw new Exception('Не возможно отгрузить собранное отправление с текущим графиком отгрузки для склада');
        }

        return $endDateTime;
    }
}
