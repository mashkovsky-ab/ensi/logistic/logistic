<?php

namespace App\Domain\DeliveryKpis\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryKpiDto
 * @package App\Domain\DeliveryKpis\Actions\Dtos
 *
 * @property int $rtg - ready-to-go time - время для проверки заказа АОЗ до его передачи в MAS (мин)
 * @property int $ct - confirmation time - время перехода Отправления из статуса “Ожидает подтверждения”
 * в статус “На комплектации” (мин)
 * @property int $ppt - planned processing time - плановое время для прохождения Отправлением статусов
 * от “На комплектации” до “Готов к передаче ЛО” (мин)
 */
class DeliveryKpiDto extends Fluent
{
}
