<?php

namespace App\Domain\DeliveryKpis\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryKpiCtDto
 * @package App\Domain\DeliveryKpis\Actions\Dtos
 *
 * @property int $ct - confirmation time - время перехода Отправления из статуса “Ожидает подтверждения”
 * в статус “На комплектации (мин)
 */
class DeliveryKpiCtDto extends Fluent
{
}
