<?php

namespace App\Domain\DeliveryKpis\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryKpiPptDto
 * @package App\Domain\DeliveryKpis\Actions\Dtos
 *
 * @property int $ppt - planned processing time - плановое время для прохождения Отправлением статусов
 * от “На комплектации” до “Готов к передаче ЛО” (мин)
 */
class DeliveryKpiPptDto extends Fluent
{
}
