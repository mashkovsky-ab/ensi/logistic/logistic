<?php

namespace App\Domain\DeliveryKpis\Actions;

use App\Domain\DeliveryServices\Models\DeliveryService;

class GetPctByDeliveryServiceAction
{
    public function execute(int $deliveryServiceId): int
    {
        /** @var DeliveryService $deliveryServices */
        $deliveryServices = DeliveryService::query()->find('id', $deliveryServiceId);

        return $deliveryServices ? $deliveryServices->pct : 0;
    }
}
