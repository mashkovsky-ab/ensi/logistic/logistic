<?php

namespace App\Domain\DeliveryKpis\Actions;

use Illuminate\Support\Carbon;

/**
 * Рассчитать PDD - planned delivery date - плановая дата,
 * начиная с которой отправление может быть доставлено клиенту
 */
class CalcPddAction
{
    public function __construct(protected GetPctByDeliveryServiceAction $getPctByDeliveryServiceAction)
    {
    }

    /**
     * @param  Carbon  $psd
     * @param  int  $deliveryServiceId
     * @param  int  $dt  - delivery time - кол-во дней доставки, которое возвращает служба доставки
     * @return Carbon
     */
    public function execute(
        Carbon $psd,
        int $deliveryServiceId,
        int $dt
    ): Carbon {
        $pdd = clone $psd;
        /**
         * Planned consolidation time плановое время доставки заказа от склада продавца
         * до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО
         */
        $pct = $this->getPctByDeliveryServiceAction->execute($deliveryServiceId);

        $pdd->modify('+'.$pct.' minute'.($pct > 1 ? 's' : ''))
            ->modify('+'.$dt.' day'.($dt > 1 ? 's' : ''));
        /**
         * Доступные интервалы времени доставки берутся
         * из \App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationAvailableTimeDto
         */
        $pdd->setTime(0, 0);

        return $pdd;
    }
}
