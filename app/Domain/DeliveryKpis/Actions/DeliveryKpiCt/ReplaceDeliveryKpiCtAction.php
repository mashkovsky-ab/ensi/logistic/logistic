<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpiCt;

use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiCtDto;
use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;

class ReplaceDeliveryKpiCtAction
{
    public function execute(int $sellerId, DeliveryKpiCtDto $deliveryKpiCtDto): DeliveryKpiCt
    {
        /** @var DeliveryKpiCt $deliveryKpiCt */
        $deliveryKpiCt = DeliveryKpiCt::query()
            ->where('seller_id', $sellerId)
            ->firstOrNew();
        $deliveryKpiCt->seller_id = $sellerId;
        $deliveryKpiCt->ct = $deliveryKpiCtDto->ct;

        $deliveryKpiCt->save();

        return $deliveryKpiCt;
    }
}
