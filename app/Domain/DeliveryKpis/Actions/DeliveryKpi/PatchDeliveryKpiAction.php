<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpi;

use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiDto;
use App\Domain\DeliveryKpis\Models\DeliveryKpi;

class PatchDeliveryKpiAction
{
    public function execute(DeliveryKpiDto $deliveryKpiDto): DeliveryKpi
    {
        /** @var DeliveryKpi $deliveryKpi */
        $deliveryKpi = DeliveryKpi::query()->first();
        if (is_null($deliveryKpi)) {
            $deliveryKpi = new DeliveryKpi();
        }
        $deliveryKpi->rtg = (int)($deliveryKpiDto['rtg'] ?? $deliveryKpi->rtg);
        $deliveryKpi->ct = (int)($deliveryKpiDto['ct'] ?? $deliveryKpi->ct);
        $deliveryKpi->ppt = (int)($deliveryKpiDto['ppt'] ?? $deliveryKpi->ppt);

        $deliveryKpi->save();

        return $deliveryKpi;
    }
}
