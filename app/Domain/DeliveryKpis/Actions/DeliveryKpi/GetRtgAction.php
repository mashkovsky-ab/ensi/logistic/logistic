<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpi;

/**
 * Получить RTG - ready-to-go time - время для проверки заказа АОЗ до его передачи в ЛК продавца
 */
class GetRtgAction
{
    public function __construct(protected GetDeliveryKpiAction $getDeliveryKpiAction)
    {
    }

    public function execute(): int
    {
        $deliveryKpi = $this->getDeliveryKpiAction->execute();

        return $deliveryKpi ? $deliveryKpi->rtg : 0;
    }
}
