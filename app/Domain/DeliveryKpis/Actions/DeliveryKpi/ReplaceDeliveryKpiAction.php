<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpi;

use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiDto;
use App\Domain\DeliveryKpis\Models\DeliveryKpi;

class ReplaceDeliveryKpiAction
{
    public function execute(DeliveryKpiDto $deliveryKpiDto): DeliveryKpi
    {
        /** @var DeliveryKpi $deliveryKpi */
        $deliveryKpi = DeliveryKpi::query()->first();
        if (is_null($deliveryKpi)) {
            $deliveryKpi = new DeliveryKpi();
        }
        $deliveryKpi->rtg = $deliveryKpiDto->rtg;
        $deliveryKpi->ct = $deliveryKpiDto->ct;
        $deliveryKpi->ppt = $deliveryKpiDto->ppt;

        $deliveryKpi->save();

        return $deliveryKpi;
    }
}
