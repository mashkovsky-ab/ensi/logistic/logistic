<?php

namespace App\Domain\DeliveryKpis\Actions\DeliveryKpi;

use App\Domain\DeliveryKpis\Models\DeliveryKpi;

class GetDeliveryKpiAction
{
    /** @var DeliveryKpi|null - KPI для расчета сроков доставки */
    protected ?DeliveryKpi $deliveryKpi = null;

    public function execute(): ?DeliveryKpi
    {
        if (is_null($this->deliveryKpi)) {
            $this->deliveryKpi = DeliveryKpi::get();
        }

        return $this->deliveryKpi;
    }
}
