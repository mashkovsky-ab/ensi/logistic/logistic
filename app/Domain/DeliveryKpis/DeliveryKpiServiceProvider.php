<?php

namespace App\Domain\DeliveryKpis;

use App\Domain\DeliveryKpis\Actions\DeliveryKpi\GetDeliveryKpiAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\GetCtBySellerAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\GetPptBySellerAction;
use Illuminate\Support\ServiceProvider;

/**
 * Class DeliveryKpiServiceProvider
 * @package App\Domain\DeliveryKpis
 */
class DeliveryKpiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->fillingContainer();
    }

    protected function fillingContainer(): void
    {
        $this->app->singleton(
            GetCtBySellerAction::class,
            function ($app) {
                return new GetCtBySellerAction($app->make(GetDeliveryKpiAction::class));
            }
        );

        $this->app->singleton(
            GetDeliveryKpiAction::class,
            function () {
                return new GetDeliveryKpiAction();
            }
        );

        $this->app->singleton(
            GetPptBySellerAction::class,
            function ($app) {
                return new GetPptBySellerAction($app->make(GetDeliveryKpiAction::class));
            }
        );
    }
}
