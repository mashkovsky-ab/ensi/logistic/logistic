<?php

namespace App\Domain\DeliveryKpis\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * KPI для расчета сроков доставки
 * Class DeliveryKpi
 * @package App\Domain\DeliveryKpis\Models
 *
 * @property int $id - id
 * @property int $rtg - ready-to-go time - время для проверки заказа АОЗ до его передачи в MAS (мин)
 * @property int $ct - confirmation time - время перехода Отправления из статуса “Ожидает подтверждения”
 * в статус “На комплектации” (мин)
 * @property int $ppt - planned processing time - плановое время для прохождения Отправлением статусов
 * от “На комплектации” до “Готов к передаче ЛО” (мин)
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 */
class DeliveryKpi extends Model
{
    /** @var string */
    protected $table = 'delivery_kpi';

    /**
     * @return DeliveryKpi|null
     */
    public static function get(): ?DeliveryKpi
    {
        /** @var DeliveryKpi|null $deliveryKpi */
        $deliveryKpi = self::query()->first();

        return $deliveryKpi;
    }
}
