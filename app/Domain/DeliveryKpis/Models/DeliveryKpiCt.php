<?php

namespace App\Domain\DeliveryKpis\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class DeliveryKpiCt
 * @package App\Domain\DeliveryKpis\Models
 *
 * @property int $id - id
 * @property int $seller_id - id продавца
 * @property int $ct - confirmation time - время перехода Отправления из статуса “Ожидает подтверждения”
 * в статус “На комплектации (мин)
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 */
class DeliveryKpiCt extends Model
{
    /** @var string */
    protected $table = 'delivery_kpi_ct';
}
