<?php

namespace App\Domain\DeliveryKpis\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class DeliveryKpiPpt
 * @package App\Domain\DeliveryKpis\Models
 *
 * @property int $id - id
 * @property int $seller_id - id продавца
 * @property int $ppt - planned processing time - плановое время для прохождения Отправлением статусов
 * от “На комплектации” до “Готов к передаче ЛО” (мин)
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 */
class DeliveryKpiPpt extends Model
{
    /** @var string */
    protected $table = 'delivery_kpi_ppt';
}
