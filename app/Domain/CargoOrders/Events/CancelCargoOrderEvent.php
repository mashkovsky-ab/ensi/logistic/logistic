<?php

namespace App\Domain\CargoOrders\Events;

use App\Domain\CargoOrders\Models\CargoOrder;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CancelCargoOrderEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public function __construct(public CargoOrder $cargoOrder)
    {
    }
}
