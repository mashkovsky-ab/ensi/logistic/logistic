<?php

namespace App\Domain\CargoOrders\Models;

use App\Domain\CargoOrders\Models\Tests\Factories\CargoShipmentLinkFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Ссылка на отправления в OMS
 * Class CargoShipmentLink
 * @package App\Domain\CargoOrders\Models
 *
 * @property int $id - идентификатор ссылки на отправление
 * @property int $cargo_id - идентификатор груза
 * @property int $shipment_id - идентификатор отправления в OMS
 *
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property-read Cargo $cargo
 */
class CargoShipmentLink extends Model
{
    /**
     * @return BelongsTo
     */
    public function cargo(): BelongsTo
    {
        return $this->belongsTo(Cargo::class);
    }

    public static function factory(): CargoShipmentLinkFactory
    {
        return CargoShipmentLinkFactory::new();
    }
}
