<?php

namespace App\Domain\CargoOrders\Models;

use App\Domain\CargoOrders\Models\Tests\Factories\StorePickupTimeFactory;
use App\Domain\DeliveryServices\Models\DeliveryService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Класс-модель для сущности "Склады - время отгрузки грузов"
 * Class StorePickupTime
 *
 * @property int $id - id времени отгрузки груза
 * @property int $external_id - идентификатор во внешней системе (bu)
 * @property int $store_id - id склада
 * @property int $day - день недели (1-7)
 * @property string $pickup_time_code - код времени отгрузки у службы доставки
 * @property Carbon $pickup_time_start - время начала отгрузки
 * @property Carbon $pickup_time_end - время окончания отгрузки
 * @property Carbon $cargo_export_time - время выгрузки информации о грузе в службу доставки
 * @property int $delivery_service_id - идентификатор службы доставки
 *
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 */
class StorePickupTime extends Model
{
    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'external_id',
        'store_id',
        'day',
        'pickup_time_code',
        'pickup_time_start',
        'pickup_time_end',
        'cargo_export_time',
        'delivery_service_id',
    ];

    protected $fillable = self::FILLABLE;

    /**
     * @return BelongsTo
     */
    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }

    public static function factory(): StorePickupTimeFactory
    {
        return StorePickupTimeFactory::new();
    }
}
