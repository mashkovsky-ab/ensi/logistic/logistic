<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoShipmentLink;
use Illuminate\Database\Eloquent\Factories\Factory;

class CargoShipmentLinkFactory extends Factory
{
    protected $model = CargoShipmentLink::class;

    public function definition()
    {
        return [
            'cargo_id' => Cargo::factory(),
            'shipment_id' => $this->faker->randomNumber(),
        ];
    }
}
