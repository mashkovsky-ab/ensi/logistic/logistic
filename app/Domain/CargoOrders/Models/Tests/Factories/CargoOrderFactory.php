<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Enums\CargoOrderStatusEnum;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoOrder;
use Illuminate\Database\Eloquent\Factories\Factory;

class CargoOrderFactory extends Factory
{
    protected $model = CargoOrder::class;

    public function definition()
    {
        return [
            'cargo_id' => Cargo::factory(),

            'timeslot_id' => $this->faker->uuid(),
            'timeslot_from' => $this->faker->time(),
            'timeslot_to' => $this->faker->time(),

            'cdek_intake_number' => $this->faker->unique()->numerify('######'),
            'external_id' => $this->faker->unique()->numerify('######'),
            'error_external_id' => $this->faker->text(20),
            'date' => $this->faker->date(),
            'status' => $this->faker->randomElement(CargoOrderStatusEnum::toValues())
        ];
    }
}
