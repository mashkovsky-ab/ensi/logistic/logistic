<?php

namespace App\Domain\CargoOrders\Models\Tests\Factories;

use App\Domain\CargoOrders\Enums\CargoStatusEnum;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class CargoFactory extends Factory
{
    protected $model = Cargo::class;

    public function definition()
    {
        return [
            'seller_id' => $this->faker->randomNumber(),
            'store_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(CargoStatusEnum::toValues()),
            'status_at' => $this->faker->date(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->date(),
            'shipping_problem_comment' => $this->faker->text(200),
            'is_canceled' => $this->faker->boolean(),
            'is_canceled_at' => $this->faker->date(),
            'delivery_service_id' => $this->faker->randomElement(DeliveryServiceEnum::toValues()),

            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
        ];
    }
}
