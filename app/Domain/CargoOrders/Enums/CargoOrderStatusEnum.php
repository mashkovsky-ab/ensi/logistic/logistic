<?php

namespace App\Domain\CargoOrders\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self NEW() - Новый
 * @method static self DONE() - Выполнен
 * @method static self CANCELED() - Отменен
 */
class CargoOrderStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'NEW' => 1,
            'DONE' => 2,
            'CANCELED' => 3,
        ];
    }
}
