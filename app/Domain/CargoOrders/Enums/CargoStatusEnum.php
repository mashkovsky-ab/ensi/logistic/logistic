<?php

namespace App\Domain\CargoOrders\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self NEW() - Новый
 * @method static self AWAIT_SHIPMENT() - Ожидает отгрузки
 */
class CargoStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'NEW' => 1,
            'AWAIT_SHIPMENT' => 2,
        ];
    }
}
