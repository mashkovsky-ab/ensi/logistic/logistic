<?php

namespace App\Domain\CargoOrders\Observers;

use App\Domain\CargoOrders\Models\Cargo;

class CargoObserver
{
    public function saving(Cargo $cargo)
    {
        if ($cargo->status != $cargo->getOriginal('status')) {
            $cargo->status_at = now();
        }

        if ($cargo->is_problem != $cargo->getOriginal('is_problem')) {
            $cargo->is_problem_at = now();
        }

        if ($cargo->is_canceled != $cargo->getOriginal('is_canceled')) {
            $cargo->is_canceled_at = now();
        }
    }
}
