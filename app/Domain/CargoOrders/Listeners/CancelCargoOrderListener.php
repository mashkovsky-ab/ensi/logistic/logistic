<?php

namespace App\Domain\CargoOrders\Listeners;

use App\Domain\CargoOrders\Actions\CancelCourierCallAction;
use App\Domain\CargoOrders\Events\CancelCargoOrderEvent;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

class CancelCargoOrderListener
{
    public function __construct(protected CancelCourierCallAction $cancelCourierCallAction)
    {
    }

    /**
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function handle(CancelCargoOrderEvent $event)
    {
        $cargoOrder = $event->cargoOrder;
        $deliveryServiceId = $cargoOrder->cargo->deliveryService->id;

        $this->cancelCourierCallAction->execute($deliveryServiceId, $cargoOrder->external_id);
    }

}
