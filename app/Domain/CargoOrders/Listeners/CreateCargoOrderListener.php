<?php

namespace App\Domain\CargoOrders\Listeners;

use App\Domain\CargoOrders\Actions\CreateCourierCallAction;
use App\Domain\CargoOrders\Events\CreateCargoOrderEvent;
use App\Domain\External\Dto\Request\CourierCallRequestDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;


class CreateCargoOrderListener
{
    public function __construct(protected CreateCourierCallAction $createCourierCallAction)
    {
    }

    /**
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function handle(CreateCargoOrderEvent $event)
    {
        $cargoOrder = $event->cargoOrder;
        $deliveryServiceId = $cargoOrder->cargo->delivery_service_id;

        $courierCallResponseDto = $this->createCourierCallAction->execute($deliveryServiceId, CourierCallRequestDto::makeFromCargoOrder($cargoOrder));

        $cargoOrder->external_id = $courierCallResponseDto->external_id;
        $cargoOrder->error_external_id = $courierCallResponseDto->message;
        $cargoOrder->cdek_intake_number = $courierCallResponseDto->cdek_intake_number;

        $cargoOrder->save();
    }
}
