<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Actions\Dtos\CargoDto;
use App\Domain\CargoOrders\Models\Cargo;

/**
 * Создание сущности груз
 * Class CreateCargoAction
 * @package App\Domain\CargoOrders\Actions
 */
class CreateCargoAction
{
    /**
     * @param CargoDto $cargoDto
     * @return Cargo
     */
    public function execute(CargoDto $cargoDto): Cargo
    {
        $cargo = new Cargo();
        $cargo->seller_id = $cargoDto->seller_id;
        $cargo->store_id = $cargoDto->store_id;

        $cargo->status = $cargoDto->status;
        $cargo->is_problem = $cargoDto->is_problem ?? false;
        $cargo->is_canceled = $cargoDto->is_canceled ?? false;

        $cargo->delivery_service_id = $cargoDto->delivery_service_id;

        $cargo->width = $cargoDto->width;
        $cargo->height = $cargoDto->height;
        $cargo->length = $cargoDto->length;
        $cargo->weight = $cargoDto->weight;

        $cargo->shipping_problem_comment = $cargoDto->shipping_problem_comment ?? '';

        $cargo->save();

        return $cargo;
    }
}
