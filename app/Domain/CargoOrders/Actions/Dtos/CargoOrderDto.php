<?php

namespace App\Domain\CargoOrders\Actions\Dtos;

use App\Domain\CargoOrders\Enums\CargoOrderStatusEnum;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\StorePickupTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

/**
 * @property int $cargo_id - идентификатор груза
 * @property string $timeslot_id - идентификатор таймслота
 * @property Carbon $timeslot_from - время начала таймлоста
 * @property Carbon $timeslot_to - время окончания таймслота
 *
 * @property Carbon $date - дата забора груза
 * @property int $status - статус заказа на забор груза
 */
class CargoOrderDto extends Fluent
{
    public static function makeFromCargo(Cargo $cargo, StorePickupTime $storePickupTime): CargoOrderDto
    {
        return new self([
            'cargo_id' => $cargo->id,
            'timeslot_id' => $storePickupTime->pickup_time_code,
            'timeslot_from' => $storePickupTime->pickup_time_start,
            'timeslot_to' => $storePickupTime->pickup_time_end,
            'date' => Carbon::parse($storePickupTime->cargo_export_time),
            'status' => CargoOrderStatusEnum::NEW()->value,
        ]);
    }
}
