<?php

namespace App\Domain\CargoOrders\Actions\Dtos;

use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

/**
 * Груз - совокупность отправлений для доставки на нулевой миле (доставка от мерчанта до распределительного центра)
 * Class CargoDto
 * @package App\Domain\CargoOrders\Actions\Dtos
 *
 * @property int $seller_id - id продавца
 * @property int $store_id - id склада продав
 * @property int $status - статус
 * @property int $is_problem - флаг, что у груза проблемы при отгрузке
 * @property int $is_canceled - флаг, что груз отменен
 * @property int $delivery_service_id - идентификатор сервиса доставки
 *
 * @property string $cdek_intake_number - Номер заявки СДЭК на вызов курьера
 * @property string $xml_id
 * @property string $error_xml_id - текст последней ошибки при создании заявки на вызов курьера для забора груза в службе доставки
 * @property float $width - ширина
 * @property float $height - высота
 * @property float $length - длина
 * @property float $weight - вес
 * @property string $shipping_problem_comment - последнее сообщение мерчанта о проблеме с отгрузкой
 */
class CargoDto extends Fluent
{

}
