<?php

namespace App\Domain\CargoOrders\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Ссылка на отправления в OMS
 * Class CargoShipmentLinkDto
 * @package App\Domain\CargoOrders\Actions\Dtos
 *
 * @property int $cargo_id - идентификатор груза
 * @property int $shipment_id - идентификатор отправления в OMS
 *
 */
class CargoShipmentLinkDto extends Fluent
{

}
