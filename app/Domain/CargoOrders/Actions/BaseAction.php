<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\B2Cpl;
use App\Domain\External\Cdek;
use App\Domain\External\CourierCallInterface;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Class BaseAction
 * @package App\Domain\CargoOrders\Actions
 */
abstract class BaseAction
{
    /** @var array */
    protected const API = [
        DeliveryService::SERVICE_B2CPL => B2Cpl\Api\CourierCallApi::class,
        DeliveryService::SERVICE_CDEK => Cdek\Api\CourierCallApi::class,
    ];

    /**
     * @param  int  $deliveryServiceId
     * @return CourierCallInterface
     * @throws DeliveryServiceApiConnectorNotFound
     */
    protected function getApi(int $deliveryServiceId): CourierCallInterface
    {
        if (isset(static::API[$deliveryServiceId])) {
            $apiClass = static::API[$deliveryServiceId];
            /** @var CourierCallInterface $api */
            return resolve($apiClass);
        } else {
            throw new DeliveryServiceApiConnectorNotFound($deliveryServiceId);
        }
    }
}
