<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Actions\Dtos\CargoDto;
use App\Domain\CargoOrders\Models\Cargo;

/**
 * Обновление сущности груз
 * Class PatchCargoAction
 * @package App\Domain\CargoOrders\Actions
 */
class PatchCargoAction
{
    /**
     * @param int $cargoId
     * @param CargoDto $cargoDto
     * @return Cargo
     */
    public function execute(int $cargoId, CargoDto $cargoDto): Cargo
    {
        /** @var Cargo $cargo */
        $cargo = Cargo::query()->findOrFail($cargoId);

        $cargo->status = $cargoDto->status ?? $cargo->status;
        $cargo->is_problem = $cargoDto->is_problem ?? $cargo->is_problem;
        $cargo->is_canceled = $cargoDto->is_canceled ?? $cargo->is_canceled;

        $cargo->width = $cargoDto->width ?? $cargo->width;
        $cargo->height = $cargoDto->height ?? $cargo->height;
        $cargo->length = $cargoDto->length ?? $cargo->length;
        $cargo->weight = $cargoDto->weight ?? $cargo->weight;

        $cargo->shipping_problem_comment = $cargoDto->shipping_problem_comment ?? $cargo->shipping_problem_comment;

        $cargo->save();

        return $cargo;
    }

}
