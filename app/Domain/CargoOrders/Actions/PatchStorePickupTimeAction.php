<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\StorePickupTime;
use Illuminate\Support\Arr;

class PatchStorePickupTimeAction
{
    public function execute(int $storePickupTimeExternalId, array $fields): StorePickupTime
    {
        /** @var StorePickupTime $storePickupTime */
        $storePickupTime = StorePickupTime::query()
            ->where('external_id', $storePickupTimeExternalId)
            ->firstOrFail();

        $storePickupTime->update(Arr::only($fields, StorePickupTime::FILLABLE));

        return $storePickupTime;
    }
}
