<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Actions\Dtos\CargoShipmentLinkDto;
use App\Domain\CargoOrders\Models\CargoShipmentLink;

/**
 * Создание отгрузок
 * Class CreateCargoShipmentLinkAction
 * @package App\Domain\CargoOrders\Actions
 */
class CreateCargoShipmentLinkAction
{
    /**
     * @param CargoShipmentLinkDto $cargoShipmentLinkDto
     * @return CargoShipmentLink
     */
    public function execute(CargoShipmentLinkDto $cargoShipmentLinkDto): CargoShipmentLink
    {
        $cargoShipmentLink = new CargoShipmentLink();
        $cargoShipmentLink->shipment_id = $cargoShipmentLinkDto->shipment_id;
        $cargoShipmentLink->cargo_id = $cargoShipmentLinkDto->cargo_id;

        $cargoShipmentLink->save();

        return $cargoShipmentLink;
    }
}
