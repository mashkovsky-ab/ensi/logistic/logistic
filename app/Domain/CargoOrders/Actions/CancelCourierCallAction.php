<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\External\Dto\Response\CourierCallCancelResponseDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Отменить заявку на вызов курьера
 * Class CancelCourierCallAction
 * @package App\Domain\CargoOrders\Actions
 */
class CancelCourierCallAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  string  $externalId  - id заявки на вызов курьера во внешней системе
     * @return CourierCallCancelResponseDto
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(int $deliveryServiceId, string $externalId): CourierCallCancelResponseDto
    {
        $api = $this->getApi($deliveryServiceId);

        return $api->cancel($externalId);
    }
}
