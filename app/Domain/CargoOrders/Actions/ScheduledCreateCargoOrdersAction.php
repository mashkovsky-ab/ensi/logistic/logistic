<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Actions\Dtos\CargoDto;
use App\Domain\CargoOrders\Actions\Dtos\CargoOrderDto;
use App\Domain\CargoOrders\Enums\CargoStatusEnum;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\StorePickupTime;
use Illuminate\Support\Collection;

class ScheduledCreateCargoOrdersAction
{
    public function __construct(protected CreateCargoOrderAction $cargoOrderAction, protected PatchCargoAction $patchCargoAction)
    {
    }

    public function execute()
    {
        $currentDay = date("N");
        /** @var Collection|StorePickupTime[] $storePickupTimes */
        $storePickupTimes = StorePickupTime::query()->where("day", $currentDay)->get()->keyBy("delivery_service_id");

        /** @var Collection|Cargo[] $freeCargo */
        $freeCargo = Cargo::query()->freeCargoByDeliveryServices($storePickupTimes->keys()->all())->get();

        foreach ($freeCargo as $cargo) {
            $storePickupTime = $storePickupTimes->get($cargo->delivery_service_id);
            $this->cargoOrderAction->execute(CargoOrderDto::makeFromCargo($cargo, $storePickupTime));
            $this->patchCargoAction->execute($cargo->id, new CargoDto(['status' => CargoStatusEnum::AWAIT_SHIPMENT()->value]));
        }
    }
}
