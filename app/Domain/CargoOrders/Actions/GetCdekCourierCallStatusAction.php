<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Проверить ошибки в заявке на вызов курьера во внешнем сервисе;
 * Если ошибок нет - возвращается также актуальный номер заявки на вызов курьера
 * @note Метод актуален только для CDEK
 * Class CreateCourierCallAction
 * @package App\Domain\CargoOrders\Actions
 */
class GetCdekCourierCallStatusAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  string  $externalId  - id заявки на вызов курьера во внешней системе
     * @return mixed
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(int $deliveryServiceId, string $externalId): mixed
    {
        $api = $this->getApi($deliveryServiceId);

        return $api->check($externalId);
    }
}
