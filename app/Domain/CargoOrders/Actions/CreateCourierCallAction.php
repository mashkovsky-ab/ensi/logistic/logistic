<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\External\Dto\Request\CourierCallRequestDto;
use App\Domain\External\Dto\Response\CourierCallResponseDto;
use App\Domain\Support\Exceptions\DeliveryServiceApiConnectorNotFound;

/**
 * Создать заявку на вызов курьера
 * Class CreateCourierCallAction
 * @package App\Domain\CargoOrders\Actions
 */
class CreateCourierCallAction extends BaseAction
{
    /**
     * @param  int  $deliveryServiceId
     * @param  CourierCallRequestDto  $courierCallRequestDto
     * @return CourierCallResponseDto
     * @throws DeliveryServiceApiConnectorNotFound
     */
    public function execute(int $deliveryServiceId, CourierCallRequestDto $courierCallRequestDto): CourierCallResponseDto
    {
        $api = $this->getApi($deliveryServiceId);

        return $api->create($courierCallRequestDto);
    }
}
