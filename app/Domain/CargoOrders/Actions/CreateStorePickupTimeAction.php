<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\StorePickupTime;
use Illuminate\Support\Arr;

class CreateStorePickupTimeAction
{
    public function execute(array $fields): StorePickupTime
    {
        return StorePickupTime::create(Arr::only($fields, StorePickupTime::FILLABLE));
    }
}
