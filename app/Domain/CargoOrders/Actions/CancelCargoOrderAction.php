<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Enums\CargoOrderStatusEnum;
use App\Domain\CargoOrders\Events\CancelCargoOrderEvent;
use App\Domain\CargoOrders\Models\CargoOrder;

class CancelCargoOrderAction
{
    public function execute(int $cargoOrderId): CargoOrder
    {
        /** @var CargoOrder $cargoOrder */
        $cargoOrder = CargoOrder::query()->findOrFail($cargoOrderId);

        $cargoOrder->status = CargoOrderStatusEnum::CANCELED()->value;
        $cargoOrder->save();

        CancelCargoOrderEvent::dispatch($cargoOrder);

        return $cargoOrder;
    }
}
