<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Models\Cargo;

/**
 * Отмена груза
 * Class CancelCargoAction
 * @package App\Domain\CargoOrders\Actions
 */
class CancelCargoAction
{
    /**
     * @param int $cargoId
     * @return Cargo
     */
    public function execute(int $cargoId): Cargo
    {
        /** @var Cargo $cargo */
        $cargo = Cargo::query()->findOrFail($cargoId);

        $cargo->is_canceled = true;
        $cargo->save();

        return $cargo;
    }
}
