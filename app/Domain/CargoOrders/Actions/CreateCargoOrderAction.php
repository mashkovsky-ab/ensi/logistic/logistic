<?php

namespace App\Domain\CargoOrders\Actions;

use App\Domain\CargoOrders\Actions\Dtos\CargoOrderDto;
use App\Domain\CargoOrders\Events\CreateCargoOrderEvent;
use App\Domain\CargoOrders\Models\CargoOrder;

class CreateCargoOrderAction
{
    public function execute(CargoOrderDto $cargoOrderDto): CargoOrder
    {
        $cargoOrder = new CargoOrder();
        $cargoOrder->cargo_id = $cargoOrderDto->cargo_id;

        $cargoOrder->timeslot_id = $cargoOrderDto->timeslot_id;
        $cargoOrder->timeslot_from = $cargoOrderDto->timeslot_from;
        $cargoOrder->timeslot_to = $cargoOrderDto->timeslot_to;

        $cargoOrder->date = $cargoOrderDto->date;
        $cargoOrder->status = $cargoOrderDto->status;

        $cargoOrder->save();

        CreateCargoOrderEvent::dispatch($cargoOrder);

        return $cargoOrder;
    }
}
