<?php

namespace App\Domain\DeliveryServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Станции метро, где есть точки приема/выдачи заказов
 * Class MetroStation
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property int $metro_line_id - id линии метро
 * @property string $name - название
 * @property string $city_guid - ФИАС id города
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property-read MetroLine $metroLine
 * @property-read Collection|PointMetroStationLink[] $pointMetroStationLinks
 * @property-read Collection|Point[] $points
 */
class MetroStation extends Model
{
    /**
     * @return BelongsTo
     */
    public function metroLine(): BelongsTo
    {
        return $this->belongsTo(MetroLine::class);
    }

    /**
     * @return HasMany
     */
    public function pointMetroStationLinks(): HasMany
    {
        return $this->hasMany(PointMetroStationLink::class);
    }

    /**
     * @return BelongsToMany
     */
    public function points(): BelongsToMany
    {
        return $this->belongsToMany(PointMetroStationLink::class, (new Point())->getTable());
    }
}
