<?php

namespace App\Domain\DeliveryServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Связь населенного пункта и службы доставки
 * Class CityDeliveryServiceLink
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property int $delivery_service - id службы доставки
 * @property int $city_id - id населенного пункта
 * @property string $city_guid - ФИАС id населенного пункта
 * @property array $payload - дополнительная информация (id населенного пункта СДЭК и т.д.)
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property City $city - населенный пункт
 */
class CityDeliveryServiceLink extends Model
{
    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Получить id населенного пункта у СДЭК
     * @return string
     */
    public function getCdekId(): string
    {
        return $this->payload['cdek_id'] ?? '';
    }
}
