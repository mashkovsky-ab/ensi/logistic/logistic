<?php

namespace App\Domain\DeliveryServices\Models;

use App\Domain\Geos\Models\Region;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Населенный пункт, в который возможна доставка
 * Class City
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property int $region_id - id региона
 * @property string $name - название
 * @property string $guid - ФИАС id
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property Collection|CityDeliveryServiceLink[] $cityDeliveryServiceLinks - связи населенного пункта и служб доставок
 * @property Region $region - регион
 */
class City extends Model
{
    /** @var string - ФИАС id для Москвы */
    const MOSCOW_GUID = '0c5b2444-70a0-4932-980c-b4dc0d3f02b5';

    /** @var string - ФИАС id для Московской обл. */
    public const MOSCOW_DISTRICT_GUID = '29251dcf-00a1-4e34-98d4-5c47484a36d4';

    /** @var string - ФИАС id для Санкт-Петербурга */
    const SPB_GUID = 'c2deb16a-0330-4f05-821f-1d09c93331e6';

    /**
     * @return HasMany
     */
    public function cityDeliveryServiceLinks(): HasMany
    {
        return $this->hasMany(CityDeliveryServiceLink::class);
    }

    /**
     * @return BelongsTo
     */
    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }
}
