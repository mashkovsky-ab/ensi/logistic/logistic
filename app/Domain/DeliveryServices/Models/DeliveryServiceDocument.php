<?php

namespace App\Domain\DeliveryServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Документы службы доставки
 * Class DeliveryServiceDocument
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property int $delivery_service_id - id службы доставки
 * @property string $name - название
 * @property string $file - ссылка на файл с документом
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 */
class DeliveryServiceDocument extends Model
{
    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'delivery_service_id',
        'name',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @return BelongsTo
     */
    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }
}
