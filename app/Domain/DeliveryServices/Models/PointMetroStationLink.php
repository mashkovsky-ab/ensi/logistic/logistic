<?php

namespace App\Domain\DeliveryServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Связь точки приема/выдачи заказов со станцией метро
 * Class PointMetroStationLink
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property int $point_id - id пункта приема/выдачи заказов
 * @property int $metro_station_id - id станции метро
 * @property float $distance - расстояние (км)
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property-read MetroStation $metroStation
 */
class PointMetroStationLink extends Model
{
    /** @var string */
    protected $table = 'point_metro_station_links';

    /**
     * @return BelongsTo
     */
    public function metroStation(): BelongsTo
    {
        return $this->belongsTo(MetroStation::class);
    }
}
