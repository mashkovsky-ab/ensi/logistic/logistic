<?php

namespace App\Domain\DeliveryServices\Models;

use App\Domain\DeliveryServices\Models\Tests\Factories\DeliveryServiceFactory;
use App\Domain\Support\Data\Address;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Служба доставки (логистический оператор) на последней миле
 * (доставка от распределительного центра до места получения заказа)
 * Class DeliveryService
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property string $name - название
 * @property Carbon $registered_at - дата регистрации
 *
 * @property string $legal_info_company_name - юр. название компании
 * @property Address $legal_info_company_address - юр. адрес компании
 * @property string $legal_info_inn - ИНН
 * @property string $legal_info_payment_account - р/с
 * @property string $legal_info_bik - БИК
 * @property string $legal_info_bank - банк
 * @property string $legal_info_bank_correspondent_account - к/с банка
 *
 * @property string $general_manager_name - ФИО ген. директора
 *
 * @property string $contract_number - номер договора
 * @property Carbon $contract_date - дата договора
 *
 * @property int $vat_rate - ставка НДС
 * @property int $taxation_type - тип налогообложения
 *
 * @property int $status - статус (см. \App\Domain\DeliveryServices\Enums\DeliveryServiceStatus)
 *
 * @property string $comment - комментарий
 * @property string $apiship_key - код в системе ApiShip
 *
 * @property int $priority - приоритет (чем меньше, тем выше)
 * @property int $max_shipments_per_day - максимальное кол-во отправлений в день
 * @property Carbon $max_cargo_export_time - время создания заявки для забора отправления день-в-день
 *
 * @property bool $do_consolidation - консолидация многоместных отправлений?
 * @property bool $do_deconsolidation - расконсолидация многоместных отправлений?
 * @property bool $do_zero_mile - осуществляет нулевую милю?
 * @property bool $do_express_delivery - осуществляет экспресс-доставку?
 * @property bool $do_return - принимает возвраты?
 * @property bool $do_dangerous_products_delivery - осуществляет доставку опасных грузов?
 * @property bool $do_transportation_oversized_cargo - перевозка крупногабаритных грузов?
 *
 * @property bool $add_partial_reject_service - добавлять услугу частичного отказ в заказ на доставку?
 * @property bool $add_insurance_service - добавлять услугу страхования груза в заказ на доставку?
 * @property bool $add_fitting_service - добавлять услугу примерки в заказ на доставку?
 * @property bool $add_return_service - добавлять услугу возврата в заказ на доставку?
 * @property bool $add_open_service - добавлять услугу вскрытия при получении в заказ на доставку?
 *
 * @property int $pct - planned сonsolidation time - плановое время доставки заказа от склада продавца
 * до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО (мин)
 *
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property Collection|DeliveryServiceDocument[] $deliveryServiceDocuments - документы
 * @property Collection|DeliveryServiceManager[] $deliveryServiceManagers - менеджеры
 * @property Collection|DeliveryServicePaymentMethodLink[] $paymentMethods - доступные способы оплаты
 */
class DeliveryService extends Model
{
    /** @var int - B2Cpl */
    public const SERVICE_B2CPL = 1;
    /** @var int - Boxberry */
    public const SERVICE_BOXBERRY = 2;
    /** @var int - СДЭК */
    public const SERVICE_CDEK = 3;
    /** @var int - Dostavista */
    public const SERVICE_DOSTAVISTA = 4;
    /** @var int - DPD */
    public const SERVICE_DPD = 5;
    /** @var int - IML */
    public const SERVICE_IML = 6;
    /** @var int - MaxiPost */
    public const SERVICE_MAXIPOST = 7;
    /** @var int - PickPoint */
    public const SERVICE_PICKPOINT = 8;
    /** @var int - PONY EXPRESS */
    public const SERVICE_PONY_EXPRESS = 9;
    /** @var int - Почта России */
    public const SERVICE_RU_POST = 10;

    /** @var array */
    protected $casts = [
        'registered_at' => 'datetime',
        'legal_info_company_address' => 'array',
        'contract_date' => 'date',
        'max_cargo_export_time' => 'datetime',
    ];

    protected function getLegalInfoCompanyAddressAttribute(): ?Address
    {
        return $this->attributes['legal_info_company_address']
            ? new Address(json_decode($this->attributes['legal_info_company_address']))
            : null;
    }

    protected function setLegalInfoCompanyAddressAttribute(?Address $address)
    {
        if ($address && !$address->address_string) {
            $address->address_string = $address->getFullString();
        }
        $this->attributes['legal_info_company_address'] = $address?->toJson();
    }

    /**
     * @return HasMany
     */
    public function deliveryServiceDocuments(): HasMany
    {
        return $this->hasMany(DeliveryServiceDocument::class);
    }

    /**
     * @return HasMany
     */
    public function deliveryServiceManagers(): HasMany
    {
        return $this->hasMany(DeliveryServiceManager::class);
    }

    /**
     * @return HasMany
     */
    public function paymentMethods(): HasMany
    {
        return $this->hasMany(DeliveryServicePaymentMethodLink::class);
    }

    public static function factory(): DeliveryServiceFactory
    {
        return DeliveryServiceFactory::new();
    }
}
