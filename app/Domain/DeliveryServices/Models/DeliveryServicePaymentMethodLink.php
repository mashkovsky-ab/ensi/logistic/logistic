<?php

namespace App\Domain\DeliveryServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Доступные способы оплаты для службы доставки
 * Class DeliveryServicePaymentMethodLink
 * @package App\Domain\DeliveryServices\Models
 *
 * @property int $id - id
 * @property int $delivery_service_id - id службы доставки
 * @property int $payment_method - id способа оплаты из OMS
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property DeliveryService $deliveryService - служба доставки
 */
class DeliveryServicePaymentMethodLink extends Model
{
    /**
     * @return BelongsTo
     */
    public function deliveryService(): BelongsTo
    {
        return $this->belongsTo(DeliveryService::class);
    }
}
