<?php

namespace App\Domain\DeliveryServices\Models\Tests\Factories;

use App\Domain\DeliveryServices\Enums\DeliveryServiceStatus;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\Support\Data\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryServiceFactory extends Factory
{
    protected $model = DeliveryService::class;

    public function definition()
    {
        return [
            'id' => $this->faker->unique()->numberBetween(100, 200),
            'name' => $this->faker->text(50),
            'registered_at' => $this->faker->date(),

            'legal_info_company_name' => $this->faker->company(),
            'legal_info_company_address' => Address::factory()->make(),
            'legal_info_inn' =>  $this->faker->unique()->numerify('############'),
            'legal_info_payment_account' => $this->faker->unique()->numerify('####################'),
            'legal_info_bik' => $this->faker->unique()->numerify('04#######'),
            'legal_info_bank' => $this->faker->company(),
            'legal_info_bank_correspondent_account' => $this->faker->unique()->numerify('301#################'),

            'general_manager_name' => $this->faker->name(),

            'contract_number' => $this->faker->unique()->numerify('############'),
            'contract_date' => $this->faker->date(),

            'vat_rate' => $this->faker->numberBetween(1, 20),
            'taxation_type' => $this->faker->numberBetween(1, 5),

            'status' => $this->faker->randomElement(DeliveryServiceStatus::validValues()),

            'comment' => $this->faker->text(20),
            'apiship_key' => $this->faker->text(20),

            'priority' =>  $this->faker->numberBetween(1, 5),
            'max_shipments_per_day' =>  $this->faker->numberBetween(100, 200),
            'max_cargo_export_time' => $this->faker->time(),

            'do_consolidation' => $this->faker->boolean(),
            'do_deconsolidation' => $this->faker->boolean(),
            'do_zero_mile' => $this->faker->boolean(),
            'do_express_delivery' => $this->faker->boolean(),
            'do_return' => $this->faker->boolean(),
            'do_dangerous_products_delivery' => $this->faker->boolean(),

            'add_partial_reject_service' => $this->faker->boolean(),
            'add_insurance_service' => $this->faker->boolean(),
            'add_fitting_service' => $this->faker->boolean(),
            'add_return_service' => $this->faker->boolean(),
            'add_open_service' => $this->faker->boolean(),

            'pct' => $this->faker->randomNumber(),
        ];
    }
}
