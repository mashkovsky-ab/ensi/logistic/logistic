<?php

namespace App\Domain\DeliveryServices\Models\Tests\Factories;

use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\Support\Data\Address;
use App\Http\ApiV1\OpenApiGenerated\Enums\PointTypeEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointFactory extends Factory
{
    protected $model = Point::class;

    public function definition()
    {
        return [
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::toValues()),
            'active' => $this->faker->boolean(),
            'type' => $this->faker->randomElement(PointTypeEnum::cases()),
            'name' => $this->faker->text(50),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'apiship_external_id' => $this->faker->unique()->numerify('#########'),
            'has_payment_card' => $this->faker->boolean(),
            'address' => Address::factory()->make(),
            'city_guid' => $this->faker->uuid(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'timetable' => $this->faker->text(),
        ];
    }
}
