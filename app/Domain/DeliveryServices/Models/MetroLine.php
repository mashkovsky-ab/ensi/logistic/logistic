<?php

namespace App\Domain\DeliveryServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Линии метро, где есть точки приема/выдачи заказов
 * Class MetroLine
 * @package App\Domain\Geos\Models
 *
 * @property int $id - id
 * @property string $name - название
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property Collection|MetroStation[] $metroStations
 */
class MetroLine extends Model
{
    /**
     * @return HasMany
     */
    public function metroStations(): HasMany
    {
        return $this->hasMany(MetroStation::class);
    }
}
