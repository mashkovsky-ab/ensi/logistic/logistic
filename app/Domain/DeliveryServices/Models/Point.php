<?php

namespace App\Domain\DeliveryServices\Models;

use App\Domain\DeliveryServices\Models\Tests\Factories\PointFactory;
use App\Domain\Support\Data\Address;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Пункт приема/выдачи заказов
 * Class Point
 * @package App\Domain\Geos\Models
 *
 * @property int $id
 * @property int $delivery_service
 * @property bool $active
 * @property int $type
 * @property string $name
 * @property string $external_id
 * @property string $apiship_external_id
 * @property bool $has_payment_card
 * @property Address $address
 * @property string $city_guid
 * @property string $email
 * @property string $phone
 * @property string $timetable
 * @property Carbon|null $created_at - дата создания
 * @property Carbon|null $updated_at - дата обновления
 *
 * @property-read Collection|PointMetroStationLink[] $pointMetroStationLinks
 * @property-read Collection|MetroStation[] $metroStations
 */
class Point extends Model
{
    /** @var array */
    protected $casts = [
        'address' => 'array',
    ];

    /** @var array */
    protected $with = [
        'pointMetroStationLinks.metroStation',
    ];

    /**
     * @return HasMany
     */
    public function pointMetroStationLinks(): HasMany
    {
        return $this->hasMany(PointMetroStationLink::class);
    }

    /**
     * @return BelongsToMany
     */
    public function metroStations(): BelongsToMany
    {
        return $this->belongsToMany(MetroStation::class, (new PointMetroStationLink())->getTable());
    }

    protected function getAddressAttribute(): ?Address
    {
        return $this->attributes['address']
            ? new Address(json_decode($this->attributes['address']))
            : null;
    }

    protected function setAddressAttribute(?Address $address)
    {
        if ($address && !$address->address_string) {
            $address->address_string = $address->getFullString();
        }
        $this->attributes['address'] = $address?->toJson();
    }

    /**
     * @return bool
     */
    public function deactivate(): bool
    {
        $this->active = false;

        return $this->save();
    }

    public static function factory(): PointFactory
    {
        return PointFactory::new();
    }
}
