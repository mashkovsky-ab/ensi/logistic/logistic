<?php

namespace App\Domain\DeliveryServices\Actions\Metro;

use App\Domain\DeliveryServices\Actions\Dtos\MetroStationDto;
use App\Domain\DeliveryServices\Models\MetroStation;

class CreateMetroStationAction
{
    public function execute(MetroStationDto $metroStationDto): MetroStation
    {
        $metroStation = new MetroStation();
        $metroStation->metro_line_id = $metroStationDto->metro_line_id;
        $metroStation->name = $metroStationDto->name;
        $metroStation->city_guid = $metroStationDto->city_guid;

        $metroStation->save();

        return $metroStation;
    }
}
