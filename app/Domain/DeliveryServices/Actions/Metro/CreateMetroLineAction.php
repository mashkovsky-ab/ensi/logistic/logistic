<?php

namespace App\Domain\DeliveryServices\Actions\Metro;

use App\Domain\DeliveryServices\Actions\Dtos\MetroLineDto;
use App\Domain\DeliveryServices\Models\MetroLine;

class CreateMetroLineAction
{
    public function execute(MetroLineDto $metroLineDto): MetroLine
    {
        $metroLine = new MetroLine();
        $metroLine->name = $metroLineDto->name;

        $metroLine->save();

        return $metroLine;
    }
}
