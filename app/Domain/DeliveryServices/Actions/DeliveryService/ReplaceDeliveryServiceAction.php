<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryService;

use App\Domain\DeliveryServices\Actions\Dtos\DeliveryServiceDto;
use App\Domain\DeliveryServices\Models\DeliveryService;

class ReplaceDeliveryServiceAction
{
    public function execute(int $deliveryServiceId, DeliveryServiceDto $deliveryServiceDto): DeliveryService
    {
        /** @var DeliveryService $deliveryService */
        $deliveryService = DeliveryService::query()->findOrFail($deliveryServiceId);

        $deliveryService->name = $deliveryServiceDto->name;
        $deliveryService->registered_at = $deliveryServiceDto->registered_at;
        $deliveryService->legal_info_company_name = $deliveryServiceDto->legal_info_company_name;
        $deliveryService->legal_info_company_address = $deliveryServiceDto->legal_info_company_address;
        $deliveryService->legal_info_inn = $deliveryServiceDto->legal_info_inn;
        $deliveryService->legal_info_payment_account = $deliveryServiceDto->legal_info_payment_account;
        $deliveryService->legal_info_bik = $deliveryServiceDto->legal_info_bik;
        $deliveryService->legal_info_bank = $deliveryServiceDto->legal_info_bank;
        $deliveryService->legal_info_bank_correspondent_account = $deliveryServiceDto->legal_info_bank_correspondent_account;
        $deliveryService->general_manager_name = $deliveryServiceDto->general_manager_name;
        $deliveryService->contract_number = $deliveryServiceDto->contract_number;
        $deliveryService->contract_date = $deliveryServiceDto->contract_date;
        $deliveryService->vat_rate = $deliveryServiceDto->vat_rate;
        $deliveryService->taxation_type = $deliveryServiceDto->taxation_type;
        $deliveryService->status = $deliveryServiceDto->status;
        $deliveryService->comment = $deliveryServiceDto->comment;
        $deliveryService->apiship_key = $deliveryServiceDto->apiship_key;
        $deliveryService->priority = $deliveryServiceDto->priority;
        $deliveryService->max_shipments_per_day = $deliveryServiceDto->max_shipments_per_day;
        $deliveryService->max_cargo_export_time = $deliveryServiceDto->max_cargo_export_time;
        $deliveryService->do_consolidation = $deliveryServiceDto->do_consolidation;
        $deliveryService->do_deconsolidation = $deliveryServiceDto->do_deconsolidation;
        $deliveryService->do_zero_mile = $deliveryServiceDto->do_zero_mile;
        $deliveryService->do_express_delivery = $deliveryServiceDto->do_express_delivery;
        $deliveryService->do_return = $deliveryServiceDto->do_return;
        $deliveryService->do_dangerous_products_delivery = $deliveryServiceDto->do_dangerous_products_delivery;
        $deliveryService->do_transportation_oversized_cargo = $deliveryServiceDto->do_transportation_oversized_cargo;
        $deliveryService->add_partial_reject_service = $deliveryServiceDto->add_partial_reject_service;
        $deliveryService->add_insurance_service = $deliveryServiceDto->add_insurance_service;
        $deliveryService->add_fitting_service = $deliveryServiceDto->add_fitting_service;
        $deliveryService->add_return_service = $deliveryServiceDto->add_return_service;
        $deliveryService->add_open_service = $deliveryServiceDto->add_open_service;
        $deliveryService->pct = $deliveryServiceDto->pct;

        $deliveryService->save();

        return $deliveryService;
    }
}
