<?php

namespace App\Domain\DeliveryServices\Actions\City;

use App\Domain\DeliveryServices\Actions\Dtos\CityDeliveryServiceLinkDto;
use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;

class CreateOrUpdateCityDeliveryServiceLinkAction
{
    public function execute(CityDeliveryServiceLinkDto $cityDeliveryServiceLinkDto): CityDeliveryServiceLink
    {
        $cityDeliveryServiceLink = CityDeliveryServiceLink::query()
            ->where('delivery_service', $cityDeliveryServiceLinkDto->delivery_service_id)
            ->where('city_guid', $cityDeliveryServiceLinkDto->city_guid)
            ->first();
        if (is_null($cityDeliveryServiceLink)) {
            $cityDeliveryServiceLink = new CityDeliveryServiceLink();
            $cityDeliveryServiceLink->delivery_service = $cityDeliveryServiceLinkDto->delivery_service_id;
            $cityDeliveryServiceLink->city_guid = $cityDeliveryServiceLinkDto->city_guid;
        }
        $cityDeliveryServiceLink->city_id = $cityDeliveryServiceLinkDto->city_id;
        $cityDeliveryServiceLink->payload = $cityDeliveryServiceLinkDto->payload ? : [];

        $cityDeliveryServiceLink->save();

        return $cityDeliveryServiceLink;
    }
}
