<?php

namespace App\Domain\DeliveryServices\Actions\City;

use App\Domain\DeliveryServices\Actions\Dtos\CityDto;
use App\Domain\DeliveryServices\Models\City;

class CreateOrUpdateCityAction
{
    public function execute(CityDto $cityDto): City
    {
        $city = City::query()
            ->where('guid', $cityDto->guid)
            ->first() ?: new City();
        $city->region_id = $cityDto->region_id;
        $city->name = $cityDto->name;
        $city->guid = $cityDto->guid;

        $city->save();

        return $city;
    }
}
