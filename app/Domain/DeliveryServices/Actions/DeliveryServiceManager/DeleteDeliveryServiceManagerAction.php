<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceManager;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;

class DeleteDeliveryServiceManagerAction
{
    public function execute(int $deliveryServiceManagerId)
    {
        $deliveryServiceManager = DeliveryServiceManager::query()->findOrFail($deliveryServiceManagerId);
        $deliveryServiceManager->delete();
    }
}
