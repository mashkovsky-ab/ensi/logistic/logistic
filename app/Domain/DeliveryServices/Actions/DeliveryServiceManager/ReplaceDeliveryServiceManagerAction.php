<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceManager;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;

class ReplaceDeliveryServiceManagerAction
{
    public function execute(int $deliveryServiceManagerId, array $fields): DeliveryServiceManager
    {
        $deliveryServiceManager = DeliveryServiceManager::findOrFail($deliveryServiceManagerId);

        $newAttributes = $fields;
        foreach (DeliveryServiceManager::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $deliveryServiceManager->update($newAttributes);

        return $deliveryServiceManager;
    }
}
