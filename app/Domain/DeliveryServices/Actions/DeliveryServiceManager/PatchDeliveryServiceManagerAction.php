<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceManager;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;

class PatchDeliveryServiceManagerAction
{
    public function execute(int $deliveryServiceManagerId, array $fields): DeliveryServiceManager
    {
        $deliveryServiceManager = DeliveryServiceManager::findOrFail($deliveryServiceManagerId);
        $deliveryServiceManager->update($fields);

        return $deliveryServiceManager;
    }
}
