<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceManager;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;
use Illuminate\Support\Arr;

class CreateDeliveryServiceManagerAction
{
    public function execute(array $fields): DeliveryServiceManager
    {
        return DeliveryServiceManager::create(Arr::only($fields, DeliveryServiceManager::FILLABLE));
    }
}
