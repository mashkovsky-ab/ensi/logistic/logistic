<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class PointDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property int $delivery_service_id
 * @property boolean $active
 * @property int $type
 * @property string $name
 * @property string $external_id
 * @property string $apiship_external_id
 * @property bool $has_payment_card
 * @property string $geo_lat
 * @property string $geo_lon
 * @property string $country_code
 * @property string $post_index
 * @property string $region
 * @property string $region_type
 * @property string $area
 * @property string $city
 * @property string $city_type
 * @property string $street
 * @property string $street_type
 * @property string $house
 * @property string $block
 * @property string $flat
 * @property string $city_guid
 * @property string $email
 * @property string $phone
 * @property string $timetable
 * @property string $description
 */
class PointDto extends Fluent
{
}
