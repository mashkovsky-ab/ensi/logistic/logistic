<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class MetroLineDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property string $name - название
 */
class MetroLineDto extends Fluent
{
}
