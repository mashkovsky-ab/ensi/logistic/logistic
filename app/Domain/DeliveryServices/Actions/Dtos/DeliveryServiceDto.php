<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use App\Domain\Support\Data\Address;
use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

/**
 * Class DeliveryServiceDto
 * @package App\Domain\DeliveryServices\Actions\Dtos
 *
 * @property string $name - название
 * @property Carbon $registered_at - дата регистрации
 *
 * @property string $legal_info_company_name - юр. название компании
 * @property Address $legal_info_company_address - юр. адрес компании
 * @property string $legal_info_inn - ИНН
 * @property string $legal_info_payment_account - р/с
 * @property string $legal_info_bik - БИК
 * @property string $legal_info_bank - банк
 * @property string $legal_info_bank_correspondent_account - к/с банка
 *
 * @property string $general_manager_name - ФИО ген. директора
 *
 * @property string $contract_number - номер договора
 * @property Carbon $contract_date - дата договора
 *
 * @property int $vat_rate - ставка НДС
 * @property int $taxation_type - тип налогообложения
 *
 * @property int $status - статус
 *
 * @property string $comment - комментарий
 * @property string $apiship_key - код в системе ApiShip
 *
 * @property int $priority - приоритет (чем меньше, тем выше)
 * @property int $max_shipments_per_day - максимальное кол-во отправлений в день
 * @property Carbon $max_cargo_export_time - время создания заявки для забора отправления день-в-день
 *
 * @property bool $do_consolidation - консолидация многоместных отправлений?
 * @property bool $do_deconsolidation - расконсолидация многоместных отправлений?
 * @property bool $do_zero_mile - осуществляет нулевую милю?
 * @property bool $do_express_delivery - осуществляет экспресс-доставку?
 * @property bool $do_return - принимает возвраты?
 * @property bool $do_dangerous_products_delivery - осуществляет доставку опасных грузов?
 * @property bool $do_transportation_oversized_cargo - перевозка крупногабаритных грузов?
 *
 * @property bool $add_partial_reject_service - добавлять услугу частичного отказ в заказ на доставку?
 * @property bool $add_insurance_service - добавлять услугу страхования груза в заказ на доставку?
 * @property bool $add_fitting_service - добавлять услугу примерки в заказ на доставку?
 * @property bool $add_return_service - добавлять услугу возврата в заказ на доставку?
 * @property bool $add_open_service - добавлять услугу вскрытия при получении в заказ на доставку?
 *
 * @property int $pct - planned сonsolidation time - плановое время доставки заказа от склада продавца
 * до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО (мин)
 */
class DeliveryServiceDto extends Fluent
{
}
