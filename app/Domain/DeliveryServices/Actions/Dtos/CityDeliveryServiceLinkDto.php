<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class CityDeliveryServiceLinkDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property int $delivery_service_id - id службы доставки
 * @property int $city_id - id населенного пункта
 * @property string $city_guid - ФИАС id населенного пункта
 * @property array $payload - дополнительная информация (id населенного пункта СДЭК и т.д.)
 */
class CityDeliveryServiceLinkDto extends Fluent
{
}
