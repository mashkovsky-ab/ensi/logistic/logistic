<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class MetroStationDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property int $metro_line_id - id линии метро
 *
 * @property string $name - название
 * @property string $city_guid - ФИАС id города
 */
class MetroStationDto extends Fluent
{
}
