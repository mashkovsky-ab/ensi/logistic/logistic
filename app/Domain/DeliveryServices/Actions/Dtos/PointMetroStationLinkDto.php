<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class PointMetroStationLinkDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property int $point_id - id пункта приема/выдачи заказов
 * @property int $metro_station_id - id станции метро
 * @property float $distance - расстояние (км)
 */
class PointMetroStationLinkDto extends Fluent
{
}
