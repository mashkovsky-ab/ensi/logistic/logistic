<?php

namespace App\Domain\DeliveryServices\Actions\Dtos;

use Illuminate\Support\Fluent;

/**
 * Class CityDto
 * @package App\Domain\Geos\Actions\Dtos
 *
 * @property int $region_id - id региона
 * @property string $name - название
 * @property string $guid - ФИАС id
 */
class CityDto extends Fluent
{
}
