<?php

namespace App\Domain\DeliveryServices\Actions\Point;

use App\Domain\DeliveryServices\Actions\Dtos\PointMetroStationLinkDto;
use App\Domain\DeliveryServices\Models\PointMetroStationLink;

class CreatePointMetroStationLinkAction
{
    public function execute(PointMetroStationLinkDto $pointMetroStationLinkDto): PointMetroStationLink
    {
        $pointMetroStationLink = new PointMetroStationLink();
        $pointMetroStationLink->point_id = $pointMetroStationLinkDto->point_id;
        $pointMetroStationLink->metro_station_id = $pointMetroStationLinkDto->metro_station_id;
        $pointMetroStationLink->distance = $pointMetroStationLinkDto->distance;

        $pointMetroStationLink->save();

        return $pointMetroStationLink;
    }
}
