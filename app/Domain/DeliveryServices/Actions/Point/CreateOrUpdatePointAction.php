<?php

namespace App\Domain\DeliveryServices\Actions\Point;

use App\Domain\DeliveryServices\Actions\Dtos\PointDto;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\Support\Data\Address;

class CreateOrUpdatePointAction
{
    public function execute(PointDto $pointDto): Point
    {
        /** @var Point $point */
        $point = Point::query()
            ->where('delivery_service', $pointDto->delivery_service_id)
            ->where('external_id', $pointDto->external_id)
            ->first();
        if (is_null($point)) {
            $point = new Point();
            $point->delivery_service = $pointDto->delivery_service_id;
            $point->external_id = $pointDto->external_id;
        }
        $point->type = $pointDto->type;
        $point->name = $pointDto->name;
        $point->apiship_external_id = $pointDto->apiship_external_id;
        $point->has_payment_card = $pointDto->has_payment_card;

        $address = new Address();
        $address->country_code = $pointDto->country_code;
        $address->post_index = $pointDto->post_index;
        $address->region = join(' ', array_filter([$pointDto->region, $pointDto->region_type]));
        $address->area = $pointDto->area;
        $address->city = join(' ', array_filter([$pointDto->city_type, $pointDto->city]));
        $address->street = join(' ', array_filter([$pointDto->street_type, $pointDto->street]));
        $address->house = $pointDto->house;
        $address->block = $pointDto->block;
        $address->flat = $pointDto->flat ? 'офис' . $pointDto->flat : '';
        $address->geo_lat = $pointDto->geo_lat;
        $address->geo_lon = $pointDto->geo_lon;
        $point->address = $address;

        $point->city_guid = $pointDto->city_guid;
        $point->email = $pointDto->email;
        $point->phone = $pointDto->phone;
        $point->timetable = $pointDto->timetable;

        $point->save();

        return $point;
    }
}
