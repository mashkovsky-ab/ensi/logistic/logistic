<?php

namespace App\Domain\DeliveryServices\Actions\Point;

use App\Domain\DeliveryServices\Actions\Dtos\PointMetroStationLinkDto;
use App\Domain\DeliveryServices\Models\PointMetroStationLink;

class PatchPointMetroStationLinkAction
{
    public function execute(
        PointMetroStationLink $pointMetroStationLink,
        PointMetroStationLinkDto $pointMetroStationLinkDto
    ): PointMetroStationLink {
        $pointMetroStationLink->distance = $pointMetroStationLinkDto->distance;

        $pointMetroStationLink->save();

        return $pointMetroStationLink;
    }
}
