<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

class SaveDeliveryServiceDocumentFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $deliveryServiceDocumentId, UploadedFile $file, ?string $name = null): DeliveryServiceDocument
    {
        /** @var DeliveryServiceDocument $deliveryServiceDocument */
        $deliveryServiceDocument = DeliveryServiceDocument::findOrFail($deliveryServiceDocumentId);

        $hash = Str::random(20);
        $extension = $name ? pathinfo($name)['extension'] : $file->getClientOriginalExtension();
        $fileName = "file_{$deliveryServiceDocumentId}_{$hash}.{$extension}";
        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->fileManager->protectedDiskName());

        $path = $disk->putFileAs("delivery-service-document-files/{$hashedSubDirs}", $file, $fileName);
        if (!$path) {
            throw new RuntimeException("Unable to save file $fileName to directory delivery-service-document-files/{$hashedSubDirs}");
        }

        if ($deliveryServiceDocument->file) {
            $disk->delete($deliveryServiceDocument->file);
        }

        $deliveryServiceDocument->file = $path;
        $deliveryServiceDocument->save();

        return $deliveryServiceDocument;
    }
}
