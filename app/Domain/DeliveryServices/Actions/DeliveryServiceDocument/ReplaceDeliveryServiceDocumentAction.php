<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;

class ReplaceDeliveryServiceDocumentAction
{
    public function execute(int $deliveryServiceDocumentId, array $fields): DeliveryServiceDocument
    {
        $deliveryServiceDocument = DeliveryServiceDocument::findOrFail($deliveryServiceDocumentId);

        $newAttributes = $fields;
        foreach (DeliveryServiceDocument::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $deliveryServiceDocument->update($newAttributes);

        return $deliveryServiceDocument;
    }
}
