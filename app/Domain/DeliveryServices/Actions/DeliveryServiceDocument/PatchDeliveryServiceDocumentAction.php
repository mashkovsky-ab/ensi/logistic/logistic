<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;

class PatchDeliveryServiceDocumentAction
{
    public function execute(int $deliveryServiceDocumentId, array $fields): DeliveryServiceDocument
    {
        $deliveryServiceDocument = DeliveryServiceDocument::findOrFail($deliveryServiceDocumentId);
        $deliveryServiceDocument->update($fields);

        return $deliveryServiceDocument;
    }
}
