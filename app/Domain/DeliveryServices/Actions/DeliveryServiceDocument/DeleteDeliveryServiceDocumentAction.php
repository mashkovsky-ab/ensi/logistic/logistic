<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;

class DeleteDeliveryServiceDocumentAction
{
    public function execute(int $deliveryServiceDocumentId)
    {
        $deliveryServiceDocument = DeliveryServiceDocument::query()->findOrFail($deliveryServiceDocumentId);
        $deliveryServiceDocument->delete();
    }
}
