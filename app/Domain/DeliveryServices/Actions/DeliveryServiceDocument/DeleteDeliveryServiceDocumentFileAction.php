<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class DeleteDeliveryServiceDocumentFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $deliveryServiceDocumentId): DeliveryServiceDocument
    {
        /** @var DeliveryServiceDocument $deliveryServiceDocument */
        $deliveryServiceDocument = DeliveryServiceDocument::findOrFail($deliveryServiceDocumentId);

        if ($deliveryServiceDocument->file) {
            $disk = Storage::disk($this->fileManager->protectedDiskName());
            if ($disk->exists($deliveryServiceDocument->file) && !$disk->delete($deliveryServiceDocument->file)) {
                throw new RuntimeException("Unable to delete file {$deliveryServiceDocument->file}");
            }

            $deliveryServiceDocument->file = null;
            $deliveryServiceDocument->save();
        }

        return $deliveryServiceDocument;
    }
}
