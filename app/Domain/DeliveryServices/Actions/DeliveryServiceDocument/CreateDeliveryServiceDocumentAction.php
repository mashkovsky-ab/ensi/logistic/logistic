<?php

namespace App\Domain\DeliveryServices\Actions\DeliveryServiceDocument;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use Illuminate\Support\Arr;

class CreateDeliveryServiceDocumentAction
{
    public function execute(array $fields): DeliveryServiceDocument
    {
        return DeliveryServiceDocument::create(Arr::only($fields, DeliveryServiceDocument::FILLABLE));
    }
}
