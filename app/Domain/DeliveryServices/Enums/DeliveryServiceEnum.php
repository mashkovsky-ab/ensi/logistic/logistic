<?php

namespace App\Domain\DeliveryServices\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * todo Убрать константы со службами доставки из \App\Domain\DeliveryServices\Models\DeliveryService
 * @method static self B2CPL() - B2Cpl
 * @method static self BOXBERRY() - Boxberry
 * @method static self CDEK() - СДЭК
 * @method static self DOSTAVISTA() - Dostavista
 * @method static self DPD() - DPD
 * @method static self IML() - IML
 * @method static self MAXIPOST() - MaxiPost
 * @method static self PICKPOINT() - PickPoint
 * @method static self PONY_EXPRESS() - PONY EXPRESS
 * @method static self RU_POST() - Почта России
 */
class DeliveryServiceEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'B2CPL' => 1,
            'BOXBERRY' => 2,
            'CDEK' => 3,
            'DOSTAVISTA' => 4,
            'DPD' => 5,
            'IML' => 6,
            'MAXIPOST' => 7,
            'PICKPOINT' => 8,
            'PONY_EXPRESS' => 9,
            'RU_POST' => 10,
        ];
    }
}
