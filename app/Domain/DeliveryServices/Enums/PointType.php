<?php

namespace App\Domain\DeliveryServices\Enums;

use App\Domain\Support\Traits\WithExternalId;

/**
 * Типы точек приема/выдачи заказов
 * Class PointType
 * @package App\Domain\Geos\Enums
 */
class PointType
{
    use WithExternalId;

    /** @var int - пункт выдачи заказа */
    public const TYPE_PICKUP_POINT = 1;
    /** @var int - постамат */
    public const TYPE_POSTOMAT = 2;
    /** @var int - отделение Почты России */
    public const TYPE_RU_POST_OFFICE = 3;
    /** @var int - терминал */
    public const SERVICE_TERMINAL = 4;

    /** @var int */
    public $id;
    /** @var string */
    public $name;
    /** @var string - идентификатор службы в интеграторе служб доставки */
    public $external_id;

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            new self(self::TYPE_PICKUP_POINT, 'Пункт выдачи заказа', '1'),
            new self(self::TYPE_POSTOMAT, 'Постамат', '2'),
            new self(self::TYPE_RU_POST_OFFICE, 'Отделение Почты России', '3'),
            new self(self::SERVICE_TERMINAL, 'Терминал', '4'),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return [
            self::TYPE_PICKUP_POINT,
            self::TYPE_POSTOMAT,
            self::TYPE_RU_POST_OFFICE,
            self::SERVICE_TERMINAL,
        ];
    }

    /**
     * PointType constructor.
     * @param  int  $id
     * @param  string  $name
     * @param  string  $external_id  - идентификатор службы в интеграторе служб доставки
     */
    public function __construct(int $id, string $name, string $external_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->external_id = $external_id;
    }
}
