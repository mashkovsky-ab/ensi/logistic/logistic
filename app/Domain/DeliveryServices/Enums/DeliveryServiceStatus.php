<?php

namespace App\Domain\DeliveryServices\Enums;

/**
 * Статус службы доставки (логистического оператора)
 * Class DeliveryServiceStatus
 * @package App\Domain\DeliveryServices\Enums
 */
class DeliveryServiceStatus
{
    /** @var int - активен */
    public const ACTIVE = 1;
    /** @var int - не активен */
    public const INACTIVE = 2;

    /** @var int */
    public int $id;
    /** @var string */
    public string $name;

    /**
     * @return DeliveryServiceStatus[]
     */
    public static function all(): array
    {
        return [
            new self(self::ACTIVE, 'Активен'),
            new self(self::INACTIVE, 'Не активен'),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return [
            self::ACTIVE,
            self::INACTIVE,
        ];
    }

    /**
     * DeliveryServiceStatus constructor.
     * @param  int  $id
     * @param  string  $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}
