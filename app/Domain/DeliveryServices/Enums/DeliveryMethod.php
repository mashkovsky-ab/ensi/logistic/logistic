<?php

namespace App\Domain\DeliveryServices\Enums;

use App\Domain\Support\Traits\WithExternalId;

/**
 * Способ доставки на последней миле (доставка до места получения заказа)
 * Class DeliveryMethod
 * @package App\Domain\DeliveryServices\Enums
 */
class DeliveryMethod
{
    use WithExternalId;

    /** @var int - курьерская доставка */
    public const METHOD_DELIVERY = 1;
    /** @var int - самовывоз */
    public const METHOD_PICKUP = 2;

    /** @var int */
    public int $id;
    /** @var string */
    public string $name;
    /** @var string - идентификатор типа доставки в интеграторе служб доставки ApiShip */
    public string $external_id;

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            new self(self::METHOD_DELIVERY, 'Курьерская доставка', 1),
            new self(self::METHOD_PICKUP, 'Самовывоз', 2),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return [
            self::METHOD_DELIVERY,
            self::METHOD_PICKUP,
        ];
    }

    /**
     * DeliveryMethod constructor.
     * @param  int  $id
     * @param  string  $name
     * @param  string  $external_id - идентификатор службы в интеграторе служб доставки
     */
    public function __construct(int $id, string $name, string $external_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->external_id = $external_id;
    }
}
