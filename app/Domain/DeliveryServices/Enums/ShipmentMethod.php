<?php

namespace App\Domain\DeliveryServices\Enums;

use App\Domain\Support\Traits\WithExternalId;

/**
 * Способы доставки на нулевой миле (доставка от продавца до распределительного центра)
 * Class ShipmentMethod
 * @package App\Domain\DeliveryServices\Enums
 */
class ShipmentMethod
{
    use WithExternalId;

    /** @var int - курьер службы доставки */
    public const METHOD_DS_COURIER = 1;
    /** @var int - собственный курьер */
    public const METHOD_OWN_COURIER = 2;

    /** @var int */
    public int $id;
    /** @var string */
    public string $name;
    /** @var string - идентификатор типа приема в интеграторе служб доставки ApiShip */
    public string $external_id;

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            new self(self::METHOD_DS_COURIER, 'Курьер службы доставки', 1),
            new self(self::METHOD_OWN_COURIER, 'Собственный курьер', 2),
        ];
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        return [
            self::METHOD_DS_COURIER,
            self::METHOD_OWN_COURIER,
        ];
    }

    /**
     * ShipmentMethod constructor.
     * @param  int  $id
     * @param  string  $name
     * @param  string  $external_id - идентификатор службы в интеграторе служб доставки
     */
    public function __construct(int $id, string $name, string $external_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->external_id = $external_id;
    }
}
