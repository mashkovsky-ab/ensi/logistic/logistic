<?php

namespace App\Domain\DeliveryServices\Observers;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

/**
 * Class DeliveryServiceDocumentObserver
 * @package App\Domain\DeliveryServices\Observers
 */
class DeliveryServiceDocumentObserver
{
    public function __construct(protected EnsiFilesystemManager $filesystemManager)
    {
    }

    public function deleted(DeliveryServiceDocument $deliveryServiceDocument)
    {
        if ($deliveryServiceDocument->file) {
            Storage::disk($this->filesystemManager->protectedDiskName())->delete($deliveryServiceDocument->file);
        }
    }
}
