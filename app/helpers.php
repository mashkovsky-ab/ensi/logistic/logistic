<?php

use App\Domain\DeliveryServices\Models\City;
use App\Domain\Geos\Models\Region;

if (!function_exists('is_debug')) {
    /**
     * Писать ли в лог?
     * @return bool
     */
    function is_debug(): bool
    {
        return !app()->isProduction() || config('app.debug');
    }
}

if (!function_exists('g2kg')) {

    /**
     * Перевести граммы в килограммы
     * @param  float  $value - значение в граммах
     * @return float
     */
    function g2kg(float $value): float
    {
        return $value / 1000;
    }
}

if (!function_exists('cm2mm')) {

    /**
     * Перевести сантиметры в миллиметры
     * @param  float  $value - значение в сантиметрах
     * @return float
     */
    function cm2mm(float $value): float
    {
        return $value * 10;
    }
}

if (!function_exists('mm2cm')) {

    /**
     * Перевести миллиметры в сантиметры
     * @param  float  $value - значение в миллиметрах
     * @return float
     */
    function mm2cm(float $value): float
    {
        return $value / 10;
    }
}

if (!function_exists('penny2rub')) {

    /**
     * Перевести копейки в рубли
     * @param  int|null  $value - значение в копейках
     * @return float
     */
    function penny2rub(?int $value): float
    {
        return (int)$value / 100;
    }
}

if (!function_exists('get_region_by_city_guid')) {

    /**
     * Получить регион для населенного пункта по его ФИАС id
     * @param  string $cityGuid - ФИАС id населенного пункта
     * @return Region|null
     */
    function get_region_by_city_guid(string $cityGuid): ?Region
    {
        /** @var City $city */
        $city = City::query()->where('guid', $cityGuid)->with('region')->first();

        return $city ? $city->region : null;
    }
}

if (!function_exists('is_valid_http_status')) {
    function is_valid_http_status(int $code): bool
    {
        return in_array($code, array_keys(\Illuminate\Http\Response::$statusTexts));
    }
}

if (!function_exists('topic')) {
    function topic(string $name): string
    {
        return config('kafka.contour') . ".$name";
    }
}
