<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Queries;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DeliveryPricesQuery
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Queries
 */
class DeliveryPricesQuery extends QueryBuilder
{
    /**
     * DeliveryPricesQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = DeliveryPrice::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'price']);
        $this->allowedIncludes([
            AllowedInclude::relationship('federal_district', 'federalDistrict'),
            'region',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('federal_district_id'),
            AllowedFilter::exact('region_id'),
            AllowedFilter::exact('region_guid'),
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('delivery_method'),
        ]);

        $this->defaultSort('-id');
    }
}
