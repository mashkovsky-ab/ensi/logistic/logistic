<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Queries;

use App\Domain\DeliveryPrices\Models\Tariff;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class TariffsQuery
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Queries
 */
class TariffsQuery extends QueryBuilder
{
    /**
     * TariffsQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = Tariff::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'name']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('delivery_method'),
            AllowedFilter::exact('shipment_method'),
            AllowedFilter::partial('name'),
            AllowedFilter::partial('description'),
            AllowedFilter::exact('external_id'),
        ]);

        $this->defaultSort('-id');
    }
}
