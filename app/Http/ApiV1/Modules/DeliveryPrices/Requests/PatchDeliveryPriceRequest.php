<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PatchDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Requests
 */
class PatchDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['nullable', 'integer'],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['nullable', Rule::in(DeliveryServiceEnum::cases())],
            'delivery_method' => ['nullable', Rule::in(DeliveryMethodEnum::cases())],
            'price' => ['nullable', 'integer'],
      ];
    }
}
