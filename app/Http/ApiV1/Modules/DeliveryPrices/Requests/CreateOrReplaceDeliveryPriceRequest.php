<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Requests;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateOrReplaceDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Requests
 */
class CreateOrReplaceDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $table = (new DeliveryPrice())->getTable();
        $id = (int) $this->route('deliveryPriceId');

        return [
            'federal_district_id' => [
                'required',
                'integer',
                Rule::unique($table)->where(function ($query) {
                    return $query->where('region_id', $this->request->get('region_id'))
                        ->where('delivery_service', $this->request->get('delivery_service'))
                        ->where('delivery_method', $this->request->get('delivery_method'));
                })->ignore($id),
            ],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['required', Rule::in(DeliveryServiceEnum::cases())],
            'delivery_method' => ['required', Rule::in(DeliveryMethodEnum::cases())],
            'price' => ['required', 'integer'],
      ];
    }
}
