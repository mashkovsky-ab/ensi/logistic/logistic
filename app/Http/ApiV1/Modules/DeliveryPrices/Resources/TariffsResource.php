<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Resources;

use App\Domain\DeliveryPrices\Models\Tariff;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class TariffsResource
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Resources
 *
 * @mixin Tariff
 */
class TariffsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service' => $this->delivery_service,
            'delivery_method' => $this->delivery_method,
            'shipment_method' => $this->shipment_method,
            'name' => $this->name,
            'description' => $this->description,
            'external_id' => $this->external_id,
            'apiship_external_id' => $this->apiship_external_id,
            'weight_min' => $this->weight_min,
            'weight_max' => $this->weight_max,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
