<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Resources;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Http\ApiV1\Modules\Geos\Resources\FederalDistrictsResource;
use App\Http\ApiV1\Modules\Geos\Resources\RegionsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryPricesResource
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Resources
 *
 * @mixin DeliveryPrice
 */
class DeliveryPricesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'federal_district_id' => $this->federal_district_id,
            'region_id' => $this->region_id,
            'region_guid' => $this->region_guid,
            'delivery_service' => $this->delivery_service,
            'delivery_method' => $this->delivery_method,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'federal_district' => FederalDistrictsResource::make($this->whenLoaded('federalDistrict')),
            'region' => RegionsResource::make($this->whenLoaded('region')),
        ];
    }
}
