<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Controllers;

use App\Http\ApiV1\Modules\DeliveryPrices\Queries\TariffsQuery;
use App\Http\ApiV1\Modules\DeliveryPrices\Resources\TariffsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class TariffsController
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Controllers
 */
class TariffsController
{
    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  TariffsQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, TariffsQuery $query): AnonymousResourceCollection
    {
        return TariffsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
