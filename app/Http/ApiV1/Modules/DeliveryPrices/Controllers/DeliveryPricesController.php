<?php

namespace App\Http\ApiV1\Modules\DeliveryPrices\Controllers;

use App\Domain\DeliveryPrices\Actions\DeliveryPrice\CreateDeliveryPriceAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\DeleteDeliveryPriceAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\PatchDeliveryPriceAction;
use App\Domain\DeliveryPrices\Actions\DeliveryPrice\ReplaceDeliveryPriceAction;
use App\Domain\DeliveryPrices\Actions\Dtos\DeliveryPriceDto;
use App\Http\ApiV1\Modules\DeliveryPrices\Queries\DeliveryPricesQuery;
use App\Http\ApiV1\Modules\DeliveryPrices\Requests\CreateOrReplaceDeliveryPriceRequest;
use App\Http\ApiV1\Modules\DeliveryPrices\Requests\PatchDeliveryPriceRequest;
use App\Http\ApiV1\Modules\DeliveryPrices\Resources\DeliveryPricesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryPricesController
 * @package App\Http\ApiV1\Modules\DeliveryPrices\Controllers
 */
class DeliveryPricesController
{
    /**
     * @param  CreateOrReplaceDeliveryPriceRequest  $request
     * @param  CreateDeliveryPriceAction  $action
     * @return DeliveryPricesResource
     */
    public function create(CreateOrReplaceDeliveryPriceRequest $request, CreateDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute(new DeliveryPriceDto($request->validated())));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  CreateOrReplaceDeliveryPriceRequest  $request
     * @param  ReplaceDeliveryPriceAction  $action
     * @return DeliveryPricesResource
     */
    public function replace(
        int $deliveryPriceId,
        CreateOrReplaceDeliveryPriceRequest $request,
        ReplaceDeliveryPriceAction $action
    ): DeliveryPricesResource {
        return new DeliveryPricesResource($action->execute($deliveryPriceId, new DeliveryPriceDto($request->validated())));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  PatchDeliveryPriceRequest  $request
     * @param  PatchDeliveryPriceAction  $action
     * @return DeliveryPricesResource
     */
    public function patch(int $deliveryPriceId, PatchDeliveryPriceRequest $request, PatchDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($deliveryPriceId, new DeliveryPriceDto($request->validated())));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  DeleteDeliveryPriceAction  $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(int $deliveryPriceId, DeleteDeliveryPriceAction $action): EmptyResource
    {
        $action->execute($deliveryPriceId);

        return new EmptyResource();
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  DeliveryPricesQuery  $query
     * @return DeliveryPricesResource
     */
    public function get(int $deliveryPriceId, DeliveryPricesQuery $query): DeliveryPricesResource
    {
        return new DeliveryPricesResource($query->findOrFail($deliveryPriceId));
    }

    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  DeliveryPricesQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryPricesQuery $query): AnonymousResourceCollection
    {
        return DeliveryPricesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  DeliveryPricesQuery  $query
     * @return DeliveryPricesResource
     */
    public function searchOne(DeliveryPricesQuery $query): DeliveryPricesResource
    {
        return new DeliveryPricesResource($query->firstOrFail());
    }
}
