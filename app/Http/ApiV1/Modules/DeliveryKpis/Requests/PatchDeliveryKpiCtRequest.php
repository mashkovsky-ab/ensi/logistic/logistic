<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchDeliveryKpiCtRequest
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Requests
 */
class PatchDeliveryKpiCtRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'ct' => ['nullable', 'integer', 'min:0'],
        ];
    }
}
