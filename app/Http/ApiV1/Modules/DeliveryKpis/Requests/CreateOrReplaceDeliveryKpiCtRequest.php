<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceDeliveryKpiCtRequest
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Requests
 */
class CreateOrReplaceDeliveryKpiCtRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'ct' => ['required', 'integer', 'min:0'],
        ];
    }
}
