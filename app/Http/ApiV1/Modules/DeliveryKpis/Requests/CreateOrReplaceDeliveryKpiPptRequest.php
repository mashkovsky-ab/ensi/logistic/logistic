<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceDeliveryKpiPptRequest
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Requests
 */
class CreateOrReplaceDeliveryKpiPptRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'ppt' => ['required', 'integer', 'min:0'],
        ];
    }
}
