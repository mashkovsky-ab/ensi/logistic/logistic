<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/delivery-kpis/delivery-kpi-ct:search 200', function () {
    postJson('/api/v1/delivery-kpis/delivery-kpi-ct:search')
        ->assertStatus(200);
});
