<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/delivery-kpis/delivery-kpi 200', function () {
    getJson('/api/v1/delivery-kpis/delivery-kpi')
        ->assertStatus(200);
});
