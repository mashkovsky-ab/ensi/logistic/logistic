<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Controllers;

use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\CreateDeliveryKpiPptAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\DeleteDeliveryKpiPptAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiPpt\ReplaceDeliveryKpiPptAction;
use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiPptDto;
use App\Http\ApiV1\Modules\DeliveryKpis\Queries\DeliveryKpiPptQuery;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\CreateOrReplaceDeliveryKpiPptRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Resources\DeliveryKpiPptResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryKpiPptController
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Controllers
 */
class DeliveryKpiPptController
{
    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  DeliveryKpiPptQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryKpiPptQuery $query): AnonymousResourceCollection
    {
        return DeliveryKpiPptResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  DeliveryKpiPptQuery  $query
     * @return DeliveryKpiPptResource
     */
    public function searchOne(DeliveryKpiPptQuery $query): DeliveryKpiPptResource
    {
        return new DeliveryKpiPptResource($query->firstOrFail());
    }

    /**
     * @param  int  $sellerId
     * @param  DeliveryKpiPptQuery  $query
     * @return DeliveryKpiPptResource
     */
    public function get(int $sellerId, DeliveryKpiPptQuery $query): DeliveryKpiPptResource
    {
        return new DeliveryKpiPptResource($query->where('seller_id', $sellerId)->firstOrFail());
    }

    /**
     * @param  int  $sellerId
     * @param  CreateOrReplaceDeliveryKpiPptRequest  $request
     * @param  CreateDeliveryKpiPptAction  $action
     * @return DeliveryKpiPptResource
     * @throws Exception
     */
    public function create(
        int $sellerId,
        CreateOrReplaceDeliveryKpiPptRequest $request,
        CreateDeliveryKpiPptAction $action
    ): DeliveryKpiPptResource {
        return new DeliveryKpiPptResource($action->execute($sellerId, new DeliveryKpiPptDto($request->validated())));
    }

    /**
     * @param  int  $sellerId
     * @param  CreateOrReplaceDeliveryKpiPptRequest  $request
     * @param  ReplaceDeliveryKpiPptAction  $action
     * @return DeliveryKpiPptResource
     */
    public function replace(
        int $sellerId,
        CreateOrReplaceDeliveryKpiPptRequest $request,
        ReplaceDeliveryKpiPptAction $action
    ): DeliveryKpiPptResource {
        return new DeliveryKpiPptResource($action->execute($sellerId, new DeliveryKpiPptDto($request->validated())));
    }

    /**
     * @param  int  $sellerId
     * @param  DeleteDeliveryKpiPptAction  $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(int $sellerId, DeleteDeliveryKpiPptAction $action): EmptyResource
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
