<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Controllers;

use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\CreateDeliveryKpiCtAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\DeleteDeliveryKpiCtAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpiCt\ReplaceDeliveryKpiCtAction;
use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiCtDto;
use App\Http\ApiV1\Modules\DeliveryKpis\Queries\DeliveryKpiCtQuery;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\CreateOrReplaceDeliveryKpiCtRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Resources\DeliveryKpiCtResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryKpiCtController
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Controllers
 */
class DeliveryKpiCtController
{
    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  DeliveryKpiCtQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryKpiCtQuery $query): AnonymousResourceCollection
    {
        return DeliveryKpiCtResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  DeliveryKpiCtQuery  $query
     * @return DeliveryKpiCtResource
     */
    public function searchOne(DeliveryKpiCtQuery $query): DeliveryKpiCtResource
    {
        return new DeliveryKpiCtResource($query->firstOrFail());
    }

    /**
     * @param  int  $sellerId
     * @param  DeliveryKpiCtQuery  $query
     * @return DeliveryKpiCtResource
     */
    public function get(int $sellerId, DeliveryKpiCtQuery $query): DeliveryKpiCtResource
    {
        return new DeliveryKpiCtResource($query->where('seller_id', $sellerId)->firstOrFail());
    }

    /**
     * @param  int  $sellerId
     * @param  CreateOrReplaceDeliveryKpiCtRequest  $request
     * @param  CreateDeliveryKpiCtAction  $action
     * @return DeliveryKpiCtResource
     * @throws Exception
     */
    public function create(
        int $sellerId,
        CreateOrReplaceDeliveryKpiCtRequest $request,
        CreateDeliveryKpiCtAction $action
    ): DeliveryKpiCtResource {
        return new DeliveryKpiCtResource($action->execute($sellerId, new DeliveryKpiCtDto($request->validated())));
    }

    /**
     * @param  int  $sellerId
     * @param  CreateOrReplaceDeliveryKpiCtRequest  $request
     * @param  ReplaceDeliveryKpiCtAction  $action
     * @return DeliveryKpiCtResource
     */
    public function replace(
        int $sellerId,
        CreateOrReplaceDeliveryKpiCtRequest $request,
        ReplaceDeliveryKpiCtAction $action
    ): DeliveryKpiCtResource {
        return new DeliveryKpiCtResource($action->execute($sellerId, new DeliveryKpiCtDto($request->validated())));
    }

    /**
     * @param  int  $sellerId
     * @param  DeleteDeliveryKpiCtAction  $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(int $sellerId, DeleteDeliveryKpiCtAction $action): EmptyResource
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
