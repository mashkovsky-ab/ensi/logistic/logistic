<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Controllers;

use App\Domain\DeliveryKpis\Actions\DeliveryKpi\PatchDeliveryKpiAction;
use App\Domain\DeliveryKpis\Actions\DeliveryKpi\ReplaceDeliveryKpiAction;
use App\Domain\DeliveryKpis\Actions\Dtos\DeliveryKpiDto;
use App\Domain\DeliveryKpis\Models\DeliveryKpi;
use App\Http\ApiV1\Modules\DeliveryKpis\Queries\DeliveryKpiQuery;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\PatchDeliveryKpiRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Requests\ReplaceDeliveryKpiRequest;
use App\Http\ApiV1\Modules\DeliveryKpis\Resources\DeliveryKpiResource;
use Exception;

/**
 * Class DeliveryKpiController
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Controllers
 */
class DeliveryKpiController
{
    /**
     * @param  DeliveryKpiQuery  $query
     * @return DeliveryKpiResource
     */
    public function get(DeliveryKpiQuery $query): DeliveryKpiResource
    {
        return new DeliveryKpiResource($query->first() ? : new DeliveryKpi());
    }

    /**
     * @param  PatchDeliveryKpiRequest  $request
     * @param  PatchDeliveryKpiAction  $action
     * @return DeliveryKpiResource
     * @throws Exception
     */
    public function patch(PatchDeliveryKpiRequest $request, PatchDeliveryKpiAction $action): DeliveryKpiResource
    {
        return new DeliveryKpiResource($action->execute(new DeliveryKpiDto($request->validated())));
    }

    /**
     * @param  ReplaceDeliveryKpiRequest  $request
     * @param  ReplaceDeliveryKpiAction  $action
     * @return DeliveryKpiResource
     * @throws Exception
     */
    public function replace(ReplaceDeliveryKpiRequest $request, ReplaceDeliveryKpiAction $action): DeliveryKpiResource
    {
        return new DeliveryKpiResource($action->execute(new DeliveryKpiDto($request->validated())));
    }
}
