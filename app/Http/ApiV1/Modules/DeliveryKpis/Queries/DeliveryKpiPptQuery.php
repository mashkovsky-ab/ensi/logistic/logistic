<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Queries;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DeliveryKpiPptQuery
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Queries
 */
class DeliveryKpiPptQuery extends QueryBuilder
{
    /**
     * DeliveryKpiPptQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = DeliveryKpiPpt::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['ppt']);

        $this->allowedFilters([
            AllowedFilter::exact('seller_id'),
        ]);

        $this->defaultSort('-id');
    }
}
