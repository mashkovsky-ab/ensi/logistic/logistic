<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Queries;

use App\Domain\DeliveryKpis\Models\DeliveryKpi;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DeliveryKpiQuery
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Queries
 */
class DeliveryKpiQuery extends QueryBuilder
{
    /**
     * DeliveryKpiQuery constructor.
     */
    public function __construct()
    {
        $query = DeliveryKpi::query();

        parent::__construct($query);
    }
}
