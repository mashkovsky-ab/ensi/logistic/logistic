<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Queries;

use App\Domain\DeliveryKpis\Models\DeliveryKpiCt;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DeliveryKpiCtQuery
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Queries
 */
class DeliveryKpiCtQuery extends QueryBuilder
{
    /**
     * DeliveryKpiCtQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = DeliveryKpiCt::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['ct']);

        $this->allowedFilters([
            AllowedFilter::exact('seller_id'),
        ]);

        $this->defaultSort('-id');
    }
}
