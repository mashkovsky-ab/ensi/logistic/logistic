<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Resources;

use App\Domain\DeliveryKpis\Models\DeliveryKpi;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryKpiResource
 * @package App\Http\ApiV1\Modules\Lists\Resources
 *
 * @mixin DeliveryKpi
 */
class DeliveryKpiResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'rtg' => $this->rtg,
            'ct' => $this->ct,
            'ppt' => $this->ppt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
