<?php

namespace App\Http\ApiV1\Modules\DeliveryKpis\Resources;

use App\Domain\DeliveryKpis\Models\DeliveryKpiPpt;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryKpiResource
 * @package App\Http\ApiV1\Modules\Lists\Resources
 *
 * @mixin DeliveryKpiPpt
 */
class DeliveryKpiPptResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'seller_id' => $this->seller_id,
            'ppt' => $this->ppt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
