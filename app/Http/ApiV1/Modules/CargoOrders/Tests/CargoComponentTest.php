<?php

use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoShipmentLink;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Http\ApiV1\Modules\CargoOrders\Tests\Factories\PatchCargoRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoStatusEnum;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

// cargo-orders/cargo/{id}
test("PATCH /api/v1/cargo-orders/cargo/{id} success", function () {
    $isProblem = true;
    $deliveryService = DeliveryService::factory()->create();
    /** @var Cargo $cargo */
    $cargo = Cargo::factory()->for($deliveryService)->create([
        'is_problem' => $isProblem,
    ]);

    $cargoData = PatchCargoRequestFactory::new()->only(['shipping_problem_comment'])->make(['shipping_problem_comment' => 'test']);

    patchJson("/api/v1/cargo-orders/cargo/{$cargo->id}", $cargoData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $cargo->id)
        ->assertJsonPath('data.is_problem', $isProblem);

    assertDatabaseHas((new Cargo())->getTable(), [
        'id' => $cargo->id,
        'shipping_problem_comment' => $cargoData['shipping_problem_comment'],
        'is_problem' => $isProblem,
    ]);
});

// cargo-orders/cargo{id}:cancel
test("POST /api/v1/cargo-orders/cargo/{id}:cancel success", function () {
    $deliveryService = DeliveryService::factory()->create();
    /** @var Cargo $cargo */
    $cargo = Cargo::factory()->for($deliveryService)->create([
        'is_canceled' => false,
    ]);

    postJson("/api/v1/cargo-orders/cargo/{$cargo->id}:cancel")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $cargo->id)
        ->assertJsonPath('data.is_canceled', true);
});

// cargo-orders/cargo:search
test("POST /api/v1/cargo-orders/cargo:search success", function () {
    $deliveryService = DeliveryService::factory()->create();
    $cargo = Cargo::factory()->for($deliveryService)
        ->count(10)
        ->sequence(
            ['status' => CargoStatusEnum::NEW],
            ['status' => CargoStatusEnum::AWAIT_SHIPMENT],
        )->create();


    postJson("/api/v1/cargo-orders/cargo:search", [
        "filter" => ["status" => CargoStatusEnum::NEW],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.status', CargoStatusEnum::NEW);
});

test("POST /api/v1/cargo-orders/cargo:search?include='shipment_links,delivery_service' success", function () {
    /** @var DeliveryService $deliveryService */
    $deliveryService = DeliveryService::factory()->create();
    /** @var Cargo $cargo */
    $cargo = Cargo::factory()->for($deliveryService)->create();
    /** @var CargoShipmentLink $cargoShipmentLinks */
    $cargoShipmentLinks = CargoShipmentLink::factory()->for($cargo)->create();

    postJson("/api/v1/cargo-orders/cargo:search", [
        "include" => ["shipment_links", "delivery_service", "shipment_links_count"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.delivery_service.id', $deliveryService->id)
        ->assertJsonPath('data.0.shipment_links.0.id', $cargoShipmentLinks->id)
        ->assertJsonPath('data.0.shipment_links_count', 1);
});
