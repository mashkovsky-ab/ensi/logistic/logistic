<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\CargoStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class PatchCargoRequestFactory extends BaseApiFactory
{

    protected function definition(): array
    {
        return [
            'status' => $this->faker->randomElement(CargoStatusEnum::cases()),
            'is_problem' => $this->faker->boolean(),
            'is_canceled' => $this->faker->boolean(),
            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
            'shipping_problem_comment' => $this->faker->text(100),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
