<?php

use App\Domain\CargoOrders\Events\CancelCargoOrderEvent;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoOrder;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Http\ApiV1\OpenApiGenerated\Enums\CargoOrderStatusEnum;
use Illuminate\Support\Facades\Event;
use Tests\ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ComponentTestCase::class);
uses()->group('component');

// cargo-orders/cargo-orders:cancel
test("POST /api/v1/cargo-orders/cargo-orders/{id}:cancel success", function () {
    $deliveryService = DeliveryService::factory()->create();
    $cargo = Cargo::factory()->for($deliveryService)->create();
    /** @var CargoOrder $cargoOrder */
    $cargoOrder = CargoOrder::factory()->for($cargo)->create([
        'status' => CargoOrderStatusEnum::NEW
    ]);
    Event::fake();

    postJson("/api/v1/cargo-orders/cargo-orders/{$cargoOrder->id}:cancel")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $cargoOrder->id)
        ->assertJsonPath('data.status', CargoOrderStatusEnum::CANCELED);

    Event::assertDispatched(CancelCargoOrderEvent::class);
});

// cargo-orders/cargo-orders:search
test("POST /api/v1/cargo-orders/cargo-orders:search success", function () {
    $deliveryService = DeliveryService::factory()->create();
    $cargo = Cargo::factory()->for($deliveryService)->create();
    CargoOrder::factory()->for($cargo)
        ->count(10)
        ->sequence(
            ['status' => CargoOrderStatusEnum::NEW],
            ['status' => CargoOrderStatusEnum::DONE],
        )->create();

    postJson("/api/v1/cargo-orders/cargo-orders:search", [
        "filter" => ["status" => CargoOrderStatusEnum::NEW],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.status', CargoOrderStatusEnum::NEW);
});

test("POST /api/v1/cargo-orders/cargo-orders:search?include=cargo success", function () {
    $deliveryService = DeliveryService::factory()->create();
    /** @var Cargo $cargo */
    $cargo = Cargo::factory()->for($deliveryService)->create();
    /** @var CargoOrder $cargoOrder */
    CargoOrder::factory()->for($cargo)->create();

    postJson("/api/v1/cargo-orders/cargo-orders:search?include=cargo")
        ->assertStatus(200)
        ->assertJsonPath('data.0.cargo.id', $cargo->id);
});
