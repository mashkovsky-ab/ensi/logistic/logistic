<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Resources;

use App\Domain\CargoOrders\Models\Cargo;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServicesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class CargoResource
 * @package App\Http\ApiV1\Modules\CargoOrders\Resources
 *
 * @mixin Cargo
 */
class CargoResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'seller_id' => $this->seller_id,
            'store_id' => $this->store_id,
            'delivery_service_id' => $this->delivery_service_id,
            'status' => $this->status,
            'status_at' => $this->status_at,
            'is_problem' => $this->is_problem,
            'is_problem_at' => $this->is_problem_at,
            'is_canceled' => $this->is_canceled,
            'is_canceled_at' => $this->is_canceled_at,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'weight' => $this->weight,
            'shipping_problem_comment' => $this->shipping_problem_comment,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'shipment_links' => CargoShipmentLinksResource::collection($this->whenLoaded('shipmentLinks')),
            'shipment_links_count' => $this->shipment_links_count ?? new MissingValue(),
            'delivery_service' => DeliveryServicesResource::make($this->whenLoaded('deliveryService')),
        ];
    }
}
