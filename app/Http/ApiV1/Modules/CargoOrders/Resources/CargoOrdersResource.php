<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Resources;

use App\Domain\CargoOrders\Models\CargoOrder;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CargoOrder
 */
class CargoOrdersResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cargo_id' => $this->cargo_id,
            'timeslot_id' => $this->timeslot_id,
            'timeslot_from' => $this->timeslot_from,
            'timeslot_to' => $this->timeslot_to,
            'cdek_intake_number' => $this->cdek_intake_number,
            'external_id' => $this->external_id,
            'error_external_id' => $this->error_external_id,
            'date' => $this->date,
            'status' => $this->status,
            'cargo' => CargoResource::make($this->whenLoaded('cargo')),
        ];
    }
}

