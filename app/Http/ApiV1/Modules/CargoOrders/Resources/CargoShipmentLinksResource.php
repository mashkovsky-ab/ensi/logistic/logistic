<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Resources;

use App\Domain\CargoOrders\Models\CargoShipmentLink;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CargoResource
 * @package App\Http\ApiV1\Modules\CargoOrders\Resources
 *
 * @mixin CargoShipmentLink
 */
class CargoShipmentLinksResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cargo_id' => $this->cargo_id,
            'shipment_id' => $this->shipment_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
