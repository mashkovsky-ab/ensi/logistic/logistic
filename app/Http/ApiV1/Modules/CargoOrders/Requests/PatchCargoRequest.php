<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\CargoStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PathCargoRequest
 * @package App\Http\ApiV1\Modules\CargoOrders\Requests
 */
class PatchCargoRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'status' => [Rule::in(CargoStatusEnum::cases())],
            'is_problem' => ['sometimes', 'boolean', 'nullable'],
            'is_canceled' => ['sometimes', 'boolean', 'nullable'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'weight' => ['nullable', 'numeric'],
            'shipping_problem_comment' => ['sometimes', 'string', 'nullable'],
        ];
    }

}
