<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Controllers;

use App\Domain\CargoOrders\Actions\CancelCargoOrderAction;
use App\Http\ApiV1\Modules\CargoOrders\Queries\CargoOrderQuery;
use App\Http\ApiV1\Modules\CargoOrders\Resources\CargoOrdersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Контроллер для работы с заказами на забор груза (от продавца до РЦ)
 */
class CargoOrdersController
{
    public function cancel(int $cargoOrderId, CancelCargoOrderAction $action): CargoOrdersResource
    {
        return CargoOrdersResource::make($action->execute($cargoOrderId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CargoOrderQuery $query): AnonymousResourceCollection
    {
        return CargoOrdersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
