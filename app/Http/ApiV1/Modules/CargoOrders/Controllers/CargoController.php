<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Controllers;

use App\Domain\CargoOrders\Actions\CancelCargoAction;
use App\Domain\CargoOrders\Actions\Dtos\CargoDto;
use App\Domain\CargoOrders\Actions\PatchCargoAction;
use App\Http\ApiV1\Modules\CargoOrders\Queries\CargoQuery;
use App\Http\ApiV1\Modules\CargoOrders\Requests\PatchCargoRequest;
use App\Http\ApiV1\Modules\CargoOrders\Resources\CargoResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CargoController
{
    public function patch(int $cargoId, PatchCargoRequest $request, PatchCargoAction $action): CargoResource
    {
        return CargoResource::make($action->execute($cargoId, new CargoDto($request->validated())));
    }

    public function cancel(int $cargoId, CancelCargoAction $action): CargoResource
    {
        return CargoResource::make($action->execute($cargoId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CargoQuery $query): AnonymousResourceCollection
    {
        return CargoResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
