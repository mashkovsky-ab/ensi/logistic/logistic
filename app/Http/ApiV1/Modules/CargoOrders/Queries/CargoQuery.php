<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Queries;

use App\Domain\CargoOrders\Models\Cargo;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class CargoQuery extends QueryBuilder
{
    /**
     * CargoQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = Cargo::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id']);
        $this->allowedSorts(['status']);

        $this->allowedSorts(['is_problem']);
        $this->allowedSorts(['is_problem_at']);
        $this->allowedSorts(['is_canceled']);
        $this->allowedSorts(['is_canceled_at']);

        $this->allowedSorts(['created_at']);
        $this->allowedSorts(['updated_at']);

        $this->allowedIncludes([
            AllowedInclude::relationship('shipment_links', 'shipmentLinks'),
            AllowedInclude::relationship('delivery_service', 'deliveryService'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('status'),
        ]);

        $this->defaultSort('-id');
    }
}
