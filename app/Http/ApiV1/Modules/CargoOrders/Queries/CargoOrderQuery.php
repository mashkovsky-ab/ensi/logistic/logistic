<?php

namespace App\Http\ApiV1\Modules\CargoOrders\Queries;

use App\Domain\CargoOrders\Models\CargoOrder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CargoOrderQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = CargoOrder::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id']);
        $this->allowedSorts(['cargo_id']);
        $this->allowedSorts(['date']);
        $this->allowedSorts(['status']);

        $this->allowedIncludes(['cargo']);

        $this->allowedFilters([
            AllowedFilter::exact('date'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('timeslot_id'),
        ]);

        $this->defaultSort('-id');
    }
}
