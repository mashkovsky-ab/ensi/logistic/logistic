<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/delivery-services/delivery-methods 200', function () {
    getJson('/api/v1/delivery-services/delivery-methods')
        ->assertStatus(200);
});

test('GET /api/v1/delivery-services/delivery-service-statuses 200', function () {
    getJson('/api/v1/delivery-services/delivery-service-statuses')
        ->assertStatus(200);
});

test('GET /api/v1/delivery-services/shipment-methods 200', function () {
    getJson('/api/v1/delivery-services/shipment-methods')
        ->assertStatus(200);
});
