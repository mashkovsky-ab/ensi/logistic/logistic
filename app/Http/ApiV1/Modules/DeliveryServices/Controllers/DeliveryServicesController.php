<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryService\PatchDeliveryServiceAction;
use App\Domain\DeliveryServices\Actions\DeliveryService\ReplaceDeliveryServiceAction;
use App\Domain\DeliveryServices\Actions\Dtos\DeliveryServiceDto;
use App\Http\ApiV1\Modules\DeliveryServices\Queries\DeliveryServicesQuery;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\PatchDeliveryServiceRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\ReplaceDeliveryServiceRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServicesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryServicesController
 * @package App\Http\ApiV1\Modules\DeliveryServices\Controllers
 */
class DeliveryServicesController
{
    /**
     * @param  int  $deliveryServiceId
     * @param  ReplaceDeliveryServiceRequest  $request
     * @param  ReplaceDeliveryServiceAction  $action
     * @return DeliveryServicesResource
     */
    public function replace(
        int $deliveryServiceId,
        ReplaceDeliveryServiceRequest $request,
        ReplaceDeliveryServiceAction $action
    ): DeliveryServicesResource {
        return new DeliveryServicesResource($action->execute(
            $deliveryServiceId,
            new DeliveryServiceDto($request->validated())
        ));
    }

    /**
     * @param  int  $deliveryServiceId
     * @param  PatchDeliveryServiceRequest  $request
     * @param  PatchDeliveryServiceAction  $action
     * @return DeliveryServicesResource
     */
    public function patch(
        int $deliveryServiceId,
        PatchDeliveryServiceRequest $request,
        PatchDeliveryServiceAction $action
    ): DeliveryServicesResource {
        return new DeliveryServicesResource($action->execute(
            $deliveryServiceId,
            new DeliveryServiceDto($request->validated())
        ));
    }

    /**
     * @param  int  $deliveryServiceId
     * @param  DeliveryServicesQuery  $query
     * @return DeliveryServicesResource
     */
    public function get(int $deliveryServiceId, DeliveryServicesQuery $query): DeliveryServicesResource
    {
        return new DeliveryServicesResource($query->findOrFail($deliveryServiceId));
    }

    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  DeliveryServicesQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryServicesQuery $query): AnonymousResourceCollection
    {
        return DeliveryServicesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  DeliveryServicesQuery  $query
     * @return DeliveryServicesResource
     */
    public function searchOne(DeliveryServicesQuery $query): DeliveryServicesResource
    {
        return new DeliveryServicesResource($query->firstOrFail());
    }
}
