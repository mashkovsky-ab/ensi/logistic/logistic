<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentFileAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\SaveDeliveryServiceDocumentFileAction;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\UploadDeliveryServiceDocumentFileRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServiceDocumentsResource;

class DeliveryServiceDocumentFilesController
{
    public function upload(
        int $deliveryServiceDocumentId,
        UploadDeliveryServiceDocumentFileRequest $request,
        SaveDeliveryServiceDocumentFileAction $action
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($action->execute(
            $deliveryServiceDocumentId,
            $request->file('file'),
            $request->validated()['name'] ?? null
        ));
    }

    public function delete(
        int $deliveryServiceDocumentId,
        DeleteDeliveryServiceDocumentFileAction $action
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($action->execute($deliveryServiceDocumentId));
    }
}
