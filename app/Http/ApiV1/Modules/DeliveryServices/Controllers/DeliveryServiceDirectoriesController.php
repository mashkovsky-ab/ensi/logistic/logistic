<?php


namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\DeliveryServiceStatus;
use App\Domain\DeliveryServices\Enums\ShipmentMethod;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryMethodsResource;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServiceStatusesResource;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\ShipmentMethodsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryServiceDirectoriesController
 * @package App\Http\ApiV1\Modules\DeliveryServices\Controllers
 */
class DeliveryServiceDirectoriesController
{
    /**
     * @return AnonymousResourceCollection
     */
    public function getDeliveryMethods(): AnonymousResourceCollection
    {
        return DeliveryMethodsResource::collection(DeliveryMethod::all());
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getDeliveryServiceStatuses(): AnonymousResourceCollection
    {
        return DeliveryServiceStatusesResource::collection(DeliveryServiceStatus::all());
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getShipmentMethods(): AnonymousResourceCollection
    {
        return ShipmentMethodsResource::collection(ShipmentMethod::all());
    }
}
