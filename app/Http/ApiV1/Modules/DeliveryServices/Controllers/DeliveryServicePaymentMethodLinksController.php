<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryService\AddPaymentMethodsToDeliveryServiceAction;
use App\Domain\DeliveryServices\Actions\DeliveryService\DeletePaymentMethodFromDeliveryServiceAction;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\AddPaymentMethodsToDeliveryServiceRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\DeletePaymentMethodFromDeliveryServiceRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;

class DeliveryServicePaymentMethodLinksController
{
    public function add(
        int $deliveryServiceId,
        AddPaymentMethodsToDeliveryServiceRequest $request,
        AddPaymentMethodsToDeliveryServiceAction $action
    ): EmptyResource {
        $validated = $request->validated();
        $action->execute($deliveryServiceId, $validated['payment_methods']);

        return new EmptyResource();
    }

    /**
     * @param int $deliveryServiceId
     * @param DeletePaymentMethodFromDeliveryServiceRequest $request
     * @param DeletePaymentMethodFromDeliveryServiceAction $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(
        int $deliveryServiceId,
        DeletePaymentMethodFromDeliveryServiceRequest $request,
        DeletePaymentMethodFromDeliveryServiceAction $action
    ): EmptyResource {
        $validated = $request->validated();
        $action->execute($deliveryServiceId, $validated['payment_method']);

        return new EmptyResource();
    }
}
