<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\CreateDeliveryServiceManagerAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\DeleteDeliveryServiceManagerAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\PatchDeliveryServiceManagerAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceManager\ReplaceDeliveryServiceManagerAction;
use App\Http\ApiV1\Modules\DeliveryServices\Queries\DeliveryServiceManagersQuery;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\CreateOrReplaceDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\PatchDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServiceManagersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DeliveryServiceManagersController
{
    public function create(CreateOrReplaceDeliveryServiceManagerRequest $request, CreateDeliveryServiceManagerAction $action): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($action->execute($request->validated()));
    }

    public function replace(
        int $deliveryServiceManagerId,
        CreateOrReplaceDeliveryServiceManagerRequest $request,
        ReplaceDeliveryServiceManagerAction $action
    ): DeliveryServiceManagersResource {
        return new DeliveryServiceManagersResource($action->execute($deliveryServiceManagerId, $request->validated()));
    }

    public function patch(int $deliveryServiceManagerId, PatchDeliveryServiceManagerRequest $request, PatchDeliveryServiceManagerAction $action): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($action->execute($deliveryServiceManagerId, $request->validated()));
    }

    public function delete(int $deliveryServiceManagerId, DeleteDeliveryServiceManagerAction $action): EmptyResource
    {
        $action->execute($deliveryServiceManagerId);

        return new EmptyResource();
    }

    public function get(int $deliveryServiceManagerId, DeliveryServiceManagersQuery $query): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($query->findOrFail($deliveryServiceManagerId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryServiceManagersQuery $query): AnonymousResourceCollection
    {
        return DeliveryServiceManagersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryServiceManagersQuery $query): DeliveryServiceManagersResource
    {
        return new DeliveryServiceManagersResource($query->firstOrFail());
    }
}
