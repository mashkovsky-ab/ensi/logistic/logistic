<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Controllers;

use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\CreateDeliveryServiceDocumentAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\PatchDeliveryServiceDocumentAction;
use App\Domain\DeliveryServices\Actions\DeliveryServiceDocument\ReplaceDeliveryServiceDocumentAction;
use App\Http\ApiV1\Modules\DeliveryServices\Queries\DeliveryServiceDocumentsQuery;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\CreateOrReplaceDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Requests\PatchDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServiceDocumentsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DeliveryServiceDocumentsController
{
    public function create(CreateOrReplaceDeliveryServiceDocumentRequest $request, CreateDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($request->validated()));
    }

    public function replace(
        int $deliveryServiceDocumentId,
        CreateOrReplaceDeliveryServiceDocumentRequest $request,
        ReplaceDeliveryServiceDocumentAction $action
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($action->execute($deliveryServiceDocumentId, $request->validated()));
    }

    public function patch(int $deliveryServiceDocumentId, PatchDeliveryServiceDocumentRequest $request, PatchDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($deliveryServiceDocumentId, $request->validated()));
    }

    public function delete(int $deliveryServiceDocumentId, DeleteDeliveryServiceDocumentAction $action): EmptyResource
    {
        $action->execute($deliveryServiceDocumentId);

        return new EmptyResource();
    }

    public function get(int $deliveryServiceDocumentId, DeliveryServiceDocumentsQuery $query): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($query->findOrFail($deliveryServiceDocumentId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryServiceDocumentsQuery $query): AnonymousResourceCollection
    {
        return DeliveryServiceDocumentsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(DeliveryServiceDocumentsQuery $query): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($query->firstOrFail());
    }
}
