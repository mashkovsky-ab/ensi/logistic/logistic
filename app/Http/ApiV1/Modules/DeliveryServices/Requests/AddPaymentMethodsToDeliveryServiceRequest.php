<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class AddPaymentMethodsToDeliveryServiceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'payment_methods' => ['required', 'array', 'min:1'],
            'payment_methods.*' => ['integer', 'required'],
        ];
    }
}
