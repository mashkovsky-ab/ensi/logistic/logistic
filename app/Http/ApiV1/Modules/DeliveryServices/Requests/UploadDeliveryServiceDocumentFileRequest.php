<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class UploadDeliveryServiceDocumentFileRequest
 * @package App\Http\ApiV1\Modules\DeliveryServices\Requests
 */
class UploadDeliveryServiceDocumentFileRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:102400'],
            'name' => ['nullable'],
        ];
    }
}
