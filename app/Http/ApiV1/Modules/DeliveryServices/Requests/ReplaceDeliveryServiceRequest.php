<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Domain\Support\Data\Address;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ReplaceDeliveryServiceRequest
 * @package App\Http\ApiV1\Modules\DeliveryServices\Requests
 */
class ReplaceDeliveryServiceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'registered_at' => ['sometimes', 'date'],

            'legal_info_company_name' => ['sometimes', 'string'],
            'legal_info_company_address' => ['sometimes', 'array'],
            'legal_info_inn' => ['sometimes', 'string'],
            'legal_info_payment_account' => ['sometimes', 'string'],
            'legal_info_bik' => ['sometimes', 'string'],
            'legal_info_bank' => ['sometimes', 'string'],
            'legal_info_bank_correspondent_account' => ['sometimes', 'string'],

            'general_manager_name' => ['sometimes', 'string'],

            'contract_number' => ['sometimes', 'string'],
            'contract_date' => ['sometimes', 'date'],

            'vat_rate' => ['sometimes', 'integer'],
            'taxation_type' => ['sometimes', 'integer'],

            'status' => [Rule::in(DeliveryServiceStatusEnum::cases())],

            'comment' => ['sometimes', 'string'],
            'apiship_key' => ['sometimes', 'string'],

            'priority' => ['sometimes', 'integer'],
            'max_shipments_per_day' => ['sometimes', 'nullable', 'integer', 'min:0'],
            'max_cargo_export_time' => ['sometimes', 'nullable', 'date_format:H:i'],

            'do_consolidation' => ['sometimes', 'boolean'],
            'do_deconsolidation' => ['sometimes', 'boolean'],
            'do_zero_mile' => ['sometimes', 'boolean'],
            'do_express_delivery' => ['sometimes', 'boolean'],
            'do_return' => ['sometimes', 'boolean'],
            'do_dangerous_products_delivery' => ['sometimes', 'boolean'],
            'do_transportation_oversized_cargo' => ['sometimes', 'boolean'],

            'add_partial_reject_service' => ['sometimes', 'boolean'],
            'add_insurance_service' => ['sometimes', 'boolean'],
            'add_fitting_service' => ['sometimes', 'boolean'],
            'add_return_service' => ['sometimes', 'boolean'],
            'add_open_service' => ['sometimes', 'boolean'],

            'pct' => ['sometimes', 'integer'],
        ];
    }

    public function validated($key = null, $default = null): array
    {
        $validated = parent::validated();

        $legalInfoCompanyAddress = $this->get('legal_info_company_address');
        if ($legalInfoCompanyAddress) {
            // Приходится валидировать отдельно от основной валидации из-за бага https://github.com/laravel/ideas/issues/1704
            $validated['legal_info_company_address'] = Address::validateOrFail($legalInfoCompanyAddress);
        }

        return $validated;
    }
}
