<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceDeliveryServiceManagerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['required', Rule::in(DeliveryServiceEnum::cases())],
            'name' => ['required', 'string'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/'],
            'email' => ['required', 'email'],
      ];
    }
}
