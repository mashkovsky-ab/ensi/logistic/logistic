<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Resources;

use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class DeliveryServicesResource
 * @package App\Http\ApiV1\Modules\DeliveryServices\Resources
 *
 * @mixin DeliveryService
 */
class DeliveryServicesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'registered_at' => $this->registered_at,
            'legal_info_company_name' => $this->legal_info_company_name,
            'legal_info_company_address' => $this->legal_info_company_address,
            'legal_info_inn' => $this->legal_info_inn,
            'legal_info_payment_account' => $this->legal_info_payment_account,
            'legal_info_bik' => $this->legal_info_bik,
            'legal_info_bank' => $this->legal_info_bank,
            'legal_info_bank_correspondent_account' => $this->legal_info_bank_correspondent_account,
            'general_manager_name' => $this->general_manager_name,
            'contract_number' => $this->contract_number,
            'contract_date' => $this->contract_date,
            'vat_rate' => $this->vat_rate,
            'taxation_type' => $this->taxation_type,
            'status' => $this->status,
            'comment' => $this->comment,
            'apiship_key' => $this->apiship_key,
            'priority' => $this->priority,
            'max_shipments_per_day' => $this->max_shipments_per_day,
            'max_cargo_export_time' => $this->max_cargo_export_time,
            'do_consolidation' => $this->do_consolidation,
            'do_deconsolidation' => $this->do_deconsolidation,
            'do_zero_mile' => $this->do_zero_mile,
            'do_express_delivery' => $this->do_express_delivery,
            'do_return' => $this->do_return,
            'do_dangerous_products_delivery' => $this->do_dangerous_products_delivery,
            'do_transportation_oversized_cargo' => $this->do_transportation_oversized_cargo,
            'add_partial_reject_service' => $this->add_partial_reject_service,
            'add_insurance_service' => $this->add_insurance_service,
            'add_fitting_service' => $this->add_fitting_service,
            'add_return_service' => $this->add_return_service,
            'add_open_service' => $this->add_open_service,
            'pct' => $this->pct,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'delivery_service_documents' => DeliveryServiceDocumentsResource::collection(
                $this->whenLoaded('deliveryServiceDocuments')
            ),
            'delivery_service_managers' => DeliveryServiceManagersResource::collection(
                $this->whenLoaded('deliveryServiceManagers')
            ),
            'payment_methods' => $this->resource->relationLoaded('paymentMethods') ?
                $this->paymentMethods->pluck('payment_method')->all() : new MissingValue(),
        ];
    }
}
