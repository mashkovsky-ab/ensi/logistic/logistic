<?php


namespace App\Http\ApiV1\Modules\DeliveryServices\Resources;

use App\Domain\DeliveryServices\Enums\DeliveryServiceStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryServiceStatusesResource
 * @package App\Http\ApiV1\Modules\DeliveryServices\Resources
 *
 * @mixin DeliveryServiceStatus
 */
class DeliveryServiceStatusesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
