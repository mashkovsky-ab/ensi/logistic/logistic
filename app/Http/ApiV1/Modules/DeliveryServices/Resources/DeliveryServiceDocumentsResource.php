<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Resources;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DeliveryServiceDocumentsResource
 * @package App\Http\ApiV1\Modules\DeliveryServices\Resources
 *
 * @mixin DeliveryServiceDocument
 */
class DeliveryServiceDocumentsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service_id' => $this->delivery_service_id,
            'name' => $this->name,
            'file' => $this->mapProtectedFileToResponse($this->file),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'delivery_service' => DeliveryServicesResource::make($this->whenLoaded('deliveryService')),
        ];
    }
}
