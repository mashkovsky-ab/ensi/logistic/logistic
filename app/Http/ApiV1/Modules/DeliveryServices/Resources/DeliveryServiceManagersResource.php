<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Resources;

use App\Domain\DeliveryServices\Models\DeliveryServiceManager;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DeliveryServiceManagersResource
 * @package App\Http\ApiV1\Modules\DeliveryServices\Resources
 *
 * @mixin DeliveryServiceManager
 */
class DeliveryServiceManagersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service_id' => $this->delivery_service_id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'delivery_service' => DeliveryServicesResource::make($this->whenLoaded('deliveryService')),
        ];
    }
}
