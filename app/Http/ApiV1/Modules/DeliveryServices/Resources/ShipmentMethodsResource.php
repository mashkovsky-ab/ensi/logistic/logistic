<?php


namespace App\Http\ApiV1\Modules\DeliveryServices\Resources;

use App\Domain\DeliveryServices\Enums\ShipmentMethod;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class ShipmentMethodsResource
 * @package App\Http\ApiV1\Modules\DeliveryServices\Resources
 *
 * @mixin ShipmentMethod
 */
class ShipmentMethodsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
