<?php


namespace App\Http\ApiV1\Modules\DeliveryServices\Resources;

use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryMethodsResource
 * @package App\Http\ApiV1\Modules\DeliveryServices\Resources
 *
 * @mixin DeliveryMethod
 */
class DeliveryMethodsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
