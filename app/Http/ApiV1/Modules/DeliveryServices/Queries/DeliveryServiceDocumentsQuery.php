<?php

namespace App\Http\ApiV1\Modules\DeliveryServices\Queries;

use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryServiceDocumentsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = DeliveryServiceDocument::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes([AllowedInclude::relationship('delivery_service', 'deliveryService')]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service_id'),
            AllowedFilter::exact('name'),
        ]);

        $this->defaultSort('-id');
    }
}
