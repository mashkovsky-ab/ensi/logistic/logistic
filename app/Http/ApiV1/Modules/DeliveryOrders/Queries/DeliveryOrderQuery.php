<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Queries;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveryOrderQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = DeliveryOrder::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'number', 'delivery_date', 'assessed_cost']);
        $this->allowedIncludes([
            'places',
            'places.items',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_id'),
            AllowedFilter::exact('delivery_service_id'),
            AllowedFilter::exact('number'),
            AllowedFilter::exact('status'),
        ]);

        $this->defaultSort('-id');
    }
}
