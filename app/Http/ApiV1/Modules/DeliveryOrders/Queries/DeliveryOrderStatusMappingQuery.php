<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Queries;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DeliveryOrderStatusMappingQuery
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Queries
 */
class DeliveryOrderStatusMappingQuery extends QueryBuilder
{
    /**
     * DeliveryOrderStatusMappingQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = DeliveryOrderStatusMapping::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'delivery_service_id', 'status']);
        $this->allowedIncludes([AllowedInclude::relationship('delivery_service', 'deliveryService'),]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service_id'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('external_status'),
        ]);

        $this->defaultSort('-id');
    }
}
