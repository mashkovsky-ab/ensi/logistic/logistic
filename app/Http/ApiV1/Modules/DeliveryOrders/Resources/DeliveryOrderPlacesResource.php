<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Resources;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlace;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryOrderPlacesResource
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Resources
 *
 * @mixin DeliveryOrderPlace
 */
class DeliveryOrderPlacesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_order_id' => $this->delivery_order_id,
            'number' => $this->number,
            'barcode' => $this->barcode,
            'height' => $this->height,
            'length' => $this->length,
            'width' => $this->width,
            'weight' => $this->weight,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'items' => DeliveryOrderPlaceItemsResource::collection($this->whenLoaded('items')),
        ];
    }
}
