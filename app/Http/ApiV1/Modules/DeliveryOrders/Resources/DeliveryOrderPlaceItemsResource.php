<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Resources;

use App\Domain\DeliveryOrders\Models\DeliveryOrderPlaceItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryOrderPlacesResource
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Resources
 *
 * @mixin DeliveryOrderPlaceItem
 */
class DeliveryOrderPlaceItemsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_order_place_id' => $this->delivery_order_place_id,
            'vendor_code' => $this->vendor_code,
            'name' => $this->name,
            'qty' => $this->qty,
            'qty_delivered' => $this->qty_delivered,
            'height' => $this->height,
            'length' => $this->length,
            'width' => $this->width,
            'weight' => $this->weight,
            'assessed_cost' => $this->assessed_cost,
            'cost' => $this->cost,
            'cost_vat' => $this->cost_vat,
            'price' => $this->price,
            'barcode' => $this->barcode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
