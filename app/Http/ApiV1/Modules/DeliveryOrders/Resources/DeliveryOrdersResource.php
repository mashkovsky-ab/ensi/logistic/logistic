<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Resources;

use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Http\ApiV1\Support\Resources\AddressResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryOrdersResource
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Resources
 *
 * @mixin DeliveryOrder
 */
class DeliveryOrdersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_id' => $this->delivery_id,
            'delivery_service_id' => $this->delivery_service_id,
            'number' => $this->number,
            'external_id' => $this->external_id,
            'error_external_id' => $this->error_external_id,
            'status' => $this->status,
            'status_at' => $this->status_at,
            'external_status' => $this->external_status,
            'external_status_at' => $this->external_status_at,
            'height' => $this->height,
            'length' => $this->length,
            'width' => $this->width,
            'weight' => $this->weight,
            'shipment_method' => $this->shipment_method,
            'delivery_method' => $this->delivery_method,
            'tariff_id' => $this->tariff_id,
            'delivery_date' => $this->dateToIso($this->delivery_date),
            'point_in_id' => $this->point_in_id,
            'point_out_id' => $this->point_out_id,
            'shipment_time_start' => $this->shipment_time_start,
            'shipment_time_end' => $this->shipment_time_end,
            'delivery_time_start' => $this->delivery_time_start,
            'delivery_time_end' => $this->delivery_time_end,
            'description' => $this->description,
            'assessed_cost' => $this->assessed_cost,
            'delivery_cost_vat' => $this->delivery_cost_vat,
            'delivery_cost_pay' => $this->delivery_cost_pay,
            'cod_cost' => $this->cod_cost,
            'is_delivery_payed_by_recipient' => $this->is_delivery_payed_by_recipient,
            'sender_is_seller' => $this->sender_is_seller,
            'sender_inn' => $this->sender_inn,
            'sender_address' => AddressResource::make($this->sender_address),
            'sender_company_name' => $this->sender_company_name,
            'sender_contact_name' => $this->sender_company_name,
            'sender_email' => $this->sender_email,
            'sender_phone' => $this->sender_phone,
            'sender_comment' => $this->sender_comment,
            'recipient_address' => AddressResource::make($this->recipient_address),
            'recipient_company_name' => $this->recipient_company_name,
            'recipient_contact_name' => $this->recipient_contact_name,
            'recipient_email' => $this->recipient_email,
            'recipient_phone' => $this->recipient_phone,
            'recipient_comment' => $this->recipient_comment,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'places' => DeliveryOrderPlacesResource::collection($this->whenLoaded('places')),
        ];
    }
}
