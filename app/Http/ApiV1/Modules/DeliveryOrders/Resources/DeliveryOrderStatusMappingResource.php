<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Resources;

use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Http\ApiV1\Modules\DeliveryServices\Resources\DeliveryServicesResource;
use App\Http\ApiV1\Modules\Geos\Resources\FederalDistrictsResource;
use App\Http\ApiV1\Modules\Geos\Resources\RegionsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class DeliveryPricesResource
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Resources
 *
 * @mixin DeliveryOrderStatusMapping
 */
class DeliveryOrderStatusMappingResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service_id' => $this->delivery_service_id,
            'status' => $this->status,
            'external_status' => $this->external_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'delivery_service' => DeliveryServicesResource::make($this->whenLoaded('deliveryService')),
        ];
    }
}
