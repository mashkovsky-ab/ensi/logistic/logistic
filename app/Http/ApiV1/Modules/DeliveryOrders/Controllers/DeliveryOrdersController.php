<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Controllers;

use App\Http\ApiV1\Modules\DeliveryOrders\Queries\DeliveryOrderQuery;
use App\Http\ApiV1\Modules\DeliveryOrders\Resources\DeliveryOrdersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Контроллер для работы с заказами на доставки (от РЦ до места получения заказа)
 * Class DeliveryOrdersController
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Controllers
 */
class DeliveryOrdersController
{
    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  DeliveryOrderQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryOrderQuery $query): AnonymousResourceCollection
    {
        return DeliveryOrdersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  DeliveryOrderQuery  $query
     * @return DeliveryOrdersResource
     */
    public function searchOne(DeliveryOrderQuery $query): DeliveryOrdersResource
    {
        return new DeliveryOrdersResource($query->firstOrFail());
    }
}
