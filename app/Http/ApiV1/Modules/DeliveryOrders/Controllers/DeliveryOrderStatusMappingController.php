<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Controllers;

use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\CreateDeliveryOrderStatusMappingAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\DeleteDeliveryOrderStatusMappingAction;
use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\ReplaceDeliveryOrderStatusMappingAction;
use App\Http\ApiV1\Modules\DeliveryOrders\Queries\DeliveryOrderStatusMappingQuery;
use App\Http\ApiV1\Modules\DeliveryOrders\Requests\CreateOrReplaceDeliveryOrderStatusMappingRequest;
use App\Http\ApiV1\Modules\DeliveryOrders\Resources\DeliveryOrderStatusMappingResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryOrderStatusMappingController
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Controllers
 */
class DeliveryOrderStatusMappingController
{
    /**
     * @param  CreateOrReplaceDeliveryOrderStatusMappingRequest  $request
     * @param  CreateDeliveryOrderStatusMappingAction  $action
     * @return DeliveryOrderStatusMappingResource
     */
    public function create(CreateOrReplaceDeliveryOrderStatusMappingRequest $request, CreateDeliveryOrderStatusMappingAction $action): DeliveryOrderStatusMappingResource
    {
        return new DeliveryOrderStatusMappingResource($action->execute($request->validated()));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  CreateOrReplaceDeliveryOrderStatusMappingRequest  $request
     * @param  ReplaceDeliveryOrderStatusMappingAction  $action
     * @return DeliveryOrderStatusMappingResource
     */
    public function replace(
        int $deliveryPriceId,
        CreateOrReplaceDeliveryOrderStatusMappingRequest $request,
        ReplaceDeliveryOrderStatusMappingAction $action
    ): DeliveryOrderStatusMappingResource {
        return new DeliveryOrderStatusMappingResource($action->execute($deliveryPriceId, $request->validated()));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  DeleteDeliveryOrderStatusMappingAction  $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(int $deliveryPriceId, DeleteDeliveryOrderStatusMappingAction $action): EmptyResource
    {
        $action->execute($deliveryPriceId);

        return new EmptyResource();
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  DeliveryOrderStatusMappingQuery  $query
     * @return DeliveryOrderStatusMappingResource
     */
    public function get(int $deliveryPriceId, DeliveryOrderStatusMappingQuery $query): DeliveryOrderStatusMappingResource
    {
        return new DeliveryOrderStatusMappingResource($query->findOrFail($deliveryPriceId));
    }

    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  DeliveryOrderStatusMappingQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveryOrderStatusMappingQuery $query): AnonymousResourceCollection
    {
        return DeliveryOrderStatusMappingResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  DeliveryOrderStatusMappingQuery  $query
     * @return DeliveryOrderStatusMappingResource
     */
    public function searchOne(DeliveryOrderStatusMappingQuery $query): DeliveryOrderStatusMappingResource
    {
        return new DeliveryOrderStatusMappingResource($query->firstOrFail());
    }
}
