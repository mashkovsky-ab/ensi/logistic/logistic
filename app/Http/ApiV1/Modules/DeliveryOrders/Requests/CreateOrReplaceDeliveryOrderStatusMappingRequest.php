<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class CreateOrReplaceDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\DeliveryOrders\Requests
 */
class CreateOrReplaceDeliveryOrderStatusMappingRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['delivery_service_id' => "integer", 'status' => "integer", 'external_status' => "string"])]
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['required', Rule::in(DeliveryServiceEnum::cases())],
            'status' => ['required', Rule::in(DeliveryOrderStatusEnum::cases())],
            'external_status' => ['required'],
      ];
    }
}
