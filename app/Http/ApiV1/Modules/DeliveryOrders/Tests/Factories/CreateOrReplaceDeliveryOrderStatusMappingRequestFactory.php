<?php

namespace App\Http\ApiV1\Modules\DeliveryOrders\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use JetBrains\PhpStorm\ArrayShape;

class CreateOrReplaceDeliveryOrderStatusMappingRequestFactory extends BaseApiFactory
{
    #[ArrayShape(['delivery_service_id' => "integer", 'status' => "integer", 'external_status' => "string"])]
    protected function definition(): array
    {
        return [
            'delivery_service_id' => $this->faker->randomElement(DeliveryServiceEnum::cases()),
            'status' => $this->faker->randomElement(DeliveryOrderStatusEnum::cases()),
            'external_status' => $this->faker->name(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
