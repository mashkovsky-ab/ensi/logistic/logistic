<?php

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use App\Http\ApiV1\Modules\DeliveryOrders\Tests\Factories\CreateOrReplaceDeliveryOrderStatusMappingRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryOrderStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/delivery-orders/delivery-order-status-mapping success', function () {
    $deliveryOrderStatusMappingData = CreateOrReplaceDeliveryOrderStatusMappingRequestFactory::new()->make();

    postJson('/api/v1/delivery-orders/delivery-order-status-mapping', $deliveryOrderStatusMappingData)
        ->assertStatus(201)
        ->assertJsonPath('data.delivery_service_id', $deliveryOrderStatusMappingData['delivery_service_id'])
        ->assertJsonPath('data.status', $deliveryOrderStatusMappingData['status'])
        ->assertJsonPath('data.external_status', $deliveryOrderStatusMappingData['external_status']);

    assertDatabaseHas((new DeliveryOrderStatusMapping())->getTable(), [
        'delivery_service_id' => $deliveryOrderStatusMappingData['delivery_service_id'],
        'status' => $deliveryOrderStatusMappingData['status'],
        'external_status' => $deliveryOrderStatusMappingData['external_status'],
    ]);
});

test('GET /api/v1/delivery-orders/delivery-order-status-mapping/{id}?include=delivery_service success', function () {
    /** @var DeliveryOrderStatusMapping $deliveryOrderStatusMapping */
    $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::factory()->create();
    $id = $deliveryOrderStatusMapping->id;

    getJson("/api/v1/delivery-orders/delivery-order-status-mapping/$id?include=delivery_service")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.delivery_service.id', $deliveryOrderStatusMapping->delivery_service_id);
});

test('GET /api/v1/delivery-orders/delivery-order-status-mapping/{id} 404', function () {
    getJson("/api/v1/delivery-orders/delivery-order-status-mapping/404")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PUT /api/v1/delivery-orders/delivery-order-status-mapping/{id} success', function () {
    /** @var DeliveryOrderStatusMapping $deliveryOrderStatusMapping */
    $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::factory()->create();
    $id = $deliveryOrderStatusMapping->id;

    $deliveryOrderStatusMappingData = CreateOrReplaceDeliveryOrderStatusMappingRequestFactory::new()->make();

    putJson("/api/v1/delivery-orders/delivery-order-status-mapping/$id", $deliveryOrderStatusMappingData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.delivery_service_id', $deliveryOrderStatusMappingData['delivery_service_id'])
        ->assertJsonPath('data.status', $deliveryOrderStatusMappingData['status'])
        ->assertJsonPath('data.external_status', $deliveryOrderStatusMappingData['external_status']);

    assertDatabaseHas((new DeliveryOrderStatusMapping())->getTable(), [
        'id' => $id,
        'delivery_service_id' => $deliveryOrderStatusMappingData['delivery_service_id'],
        'status' => $deliveryOrderStatusMappingData['status'],
        'external_status' => $deliveryOrderStatusMappingData['external_status'],
    ]);
});

test('PUT /api/v1/delivery-orders/delivery-order-status-mapping/{id} 404', function () {
    putJson("/api/v1/delivery-orders/delivery-order-status-mapping/404", DeliveryOrderStatusMapping::factory()->make()->toArray())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/delivery-orders/delivery-order-status-mapping/{id} success', function () {
    /** @var DeliveryOrderStatusMapping $deliveryOrderStatusMapping */
    $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::factory()->create();
    $id = $deliveryOrderStatusMapping->id;

    deleteJson("/api/v1/delivery-orders/delivery-order-status-mapping/$id")->assertStatus(200);

    assertModelMissing($deliveryOrderStatusMapping);
});

test("POST /api/v1/delivery-orders/delivery-order-status-mapping:search success", function () {
    $deliveryOrderStatusMappings = DeliveryOrderStatusMapping::factory()
        ->count(10)
        ->sequence(
            ['status' => DeliveryOrderStatus::SHIPPED],
            ['status' => DeliveryOrderStatus::NEW],
        )
        ->create();
    $lastId = $deliveryOrderStatusMappings->last()->id;

    $requestBody = [
        "filter" => [
            "status" => DeliveryOrderStatusEnum::NEW,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/delivery-orders/delivery-order-status-mapping:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.status', DeliveryOrderStatusEnum::NEW);
});

test("POST /api/v1/delivery-orders/delivery-order-status-mapping:search-one success", function () {
    $deliveryOrderStatusMapping = DeliveryOrderStatusMapping::factory()->create();

    $requestBody = [
        "filter" => [
            "id" => $deliveryOrderStatusMapping['id'],
        ],
    ];

    postJson("/api/v1/delivery-orders/delivery-order-status-mapping:search-one", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryOrderStatusMapping['id'])
        ->assertJsonPath('data.delivery_service_id', $deliveryOrderStatusMapping['delivery_service_id'])
        ->assertJsonPath('data.status', $deliveryOrderStatusMapping['status'])
        ->assertJsonPath('data.external_status', $deliveryOrderStatusMapping['external_status']);
});
