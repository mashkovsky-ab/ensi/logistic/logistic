<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationShipmentItemDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationShipmentItemsResource
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationShipmentItemDto
 */
class CalculationShipmentItemsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'offer_id' => $this->offer_id,
            'qty' => $this->qty,
            'weight' => $this->weight,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'is_explosive' => $this->is_explosive,
            'store_id' => $this->store_id,
            'seller_id' => $this->seller_id,
        ];
    }
}
