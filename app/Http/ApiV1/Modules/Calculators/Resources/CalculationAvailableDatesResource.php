<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationAvailableDateDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationAvailableDatesResource
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationAvailableDateDto
 */
class CalculationAvailableDatesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'date' => $this->date,
            'available_times' => CalculationAvailableTimesResource::collection($this->available_times),
        ];
    }
}
