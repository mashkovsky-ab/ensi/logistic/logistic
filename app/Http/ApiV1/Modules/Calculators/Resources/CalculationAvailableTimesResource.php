<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationAvailableTimeDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationAvailableTimesResource
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationAvailableTimeDto
 */
class CalculationAvailableTimesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'code' => $this->code,
            'from' => $this->from,
            'to' => $this->to,
        ];
    }
}
