<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationTariffDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationTariffsResources
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationTariffDto
 */
class CalculationTariffsResources extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'external_id' => $this->external_id,
            'name' => $this->name,
            'delivery_service_cost' => $this->delivery_service_cost,
            'days_min' => $this->days_min,
            'days_max' => $this->days_max,
            'available_dates' => CalculationAvailableDatesResource::collection($this->available_dates),
            'point_ids' => $this->point_ids,
        ];
    }
}
