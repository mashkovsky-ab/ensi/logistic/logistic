<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationDeliveryDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationDeliveriesResource
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationDeliveryDto
 */
class CalculationDeliveriesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'delivery_service' => $this->delivery_service,
            'dt' => $this->dt,
            'pdd' => $this->pdd,
            'shipments' => CalculationShipmentsResource::collection($this->shipments),
            'tariffs' => CalculationTariffsResources::collection($this->tariffs),
        ];
    }
}
