<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationDeliveryMethodDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationDeliveryMethodsResources
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationDeliveryMethodDto
 */
class CalculationDeliveryMethodsResources extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'can_merge' => $this->can_merge,
            'delivery_cost' => $this->delivery_cost,
            'delivery_method_id' => $this->delivery_method_id,
            'deliveries' => CalculationDeliveriesResource::collection($this->deliveries),
        ];
    }
}
