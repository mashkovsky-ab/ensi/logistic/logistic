<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\Part\Calculation\CalculationShipmentDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculationShipmentsResource
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationShipmentDto
 */
class CalculationShipmentsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'seller_id' => $this->seller_id,
            'store_id' => $this->store_id,
            'weight' => $this->weight,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'psd' => $this->psd,
            'items' => CalculationShipmentItemsResource::collection($this->items),
        ];
    }
}
