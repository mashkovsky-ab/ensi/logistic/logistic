<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Dtos\Out\CalculationOutDto;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CalculateResource
 * @package App\Http\ApiV1\Modules\Calculators\Resources
 *
 * @mixin CalculationOutDto
 */
class CalculationsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'methods' => CalculationDeliveryMethodsResources::collection($this->methods),
        ];
    }
}
