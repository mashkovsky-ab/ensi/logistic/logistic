<?php

namespace App\Http\ApiV1\Modules\Calculators\Controllers;

use App\Domain\Calculators\Actions\CalculateAction;
use App\Domain\Calculators\Actions\Dtos\In\CalculationInDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationBasketItemDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStorePickupTimeDto;
use App\Domain\Calculators\Actions\Dtos\In\Part\Calculation\CalculationStoreWorkingDto;
use App\Http\ApiV1\Modules\Calculators\Requests\CalculateRequest;
use App\Http\ApiV1\Modules\Calculators\Resources\CalculationsResource;
use Exception;
use Illuminate\Support\Carbon;

/**
 * Контроллер для расчета сроков и стоимости доставки
 * Class CalculatorController
 * @package App\Http\ApiV1\Modules\Calculators\Controllers
 */
class CalculatorController
{
    /**
     * Рассчитать сроки и стоимость доставки
     * @param CalculateRequest $request
     * @param CalculateAction $action
     * @return CalculationsResource
     * @throws Exception
     */
    public function calculate(CalculateRequest $request, CalculateAction $action): CalculationsResource
    {
        $data = $request->validated();

        $calculationInDto = new CalculationInDto();
        $calculationInDto->city_guid_to = $data['city_guid_to'];

        foreach ($data['basket_items'] as $basketItem) {
            $calculationBasketItemDto = new CalculationBasketItemDto();
            $calculationInDto->basket_items->push($calculationBasketItemDto);
            $calculationBasketItemDto->offer_id = $basketItem['offer_id'];
            $calculationBasketItemDto->qty = $basketItem['qty'];
            $calculationBasketItemDto->weight = (float)$basketItem['weight'];
            $calculationBasketItemDto->width = (int)ceil($basketItem['width']);
            $calculationBasketItemDto->height = (int)ceil($basketItem['height']);
            $calculationBasketItemDto->length = (int)ceil($basketItem['length']);
            $calculationBasketItemDto->is_explosive = $basketItem['is_explosive'] ?? false;
            $calculationBasketItemDto->store_id = $basketItem['store_id'];
            $calculationBasketItemDto->seller_id = $basketItem['seller_id'];
        }
        foreach ($data['stores'] as $store) {
            $calculationStoreDto = new CalculationStoreDto();
            $calculationInDto->stores->push($calculationStoreDto);
            $calculationStoreDto->store_id = $store['store_id'];
            $calculationStoreDto->city_guid = $store['city_guid'];

            if (isset($store['working'])) {
                foreach ($store['working'] as $datum) {
                    $calculationStoreWorkingDto = new CalculationStoreWorkingDto();
                    $calculationStoreDto->working->push($calculationStoreWorkingDto);
                    $calculationStoreWorkingDto->day = $datum['day'];
                    $calculationStoreWorkingDto->working_start_time =
                        isset($datum['working_start_time']) && $datum['working_start_time']
                            ? Carbon::createFromFormat(
                                CalculationStoreWorkingDto::TIME_FORMAT,
                                $datum['working_start_time']
                            ) : null;
                    $calculationStoreWorkingDto->working_end_time =
                        isset($datum['working_end_time']) && $datum['working_end_time']
                            ? Carbon::createFromFormat(
                                CalculationStoreWorkingDto::TIME_FORMAT,
                                $datum['working_end_time']
                            ) : null;
                }
            }

            if (isset($store['pickup_times'])) {
                foreach ($store['pickup_times'] as $datum) {
                    $calculationStorePickupTimeDto = new CalculationStorePickupTimeDto();
                    $calculationStoreDto->pickup_times->push($calculationStorePickupTimeDto);
                    $calculationStorePickupTimeDto->day = $datum['day'];
                    $calculationStorePickupTimeDto->delivery_service = $datum['delivery_service'];
                    $calculationStorePickupTimeDto->pickup_time_code = $datum['pickup_time_code'] ?? '';
                    $calculationStorePickupTimeDto->pickup_time_start =
                        isset($datum['pickup_time_start']) && $datum['pickup_time_start']
                            ? Carbon::createFromFormat(
                                CalculationStorePickupTimeDto::TIME_FORMAT,
                                $datum['pickup_time_start']
                            ) : null;
                    $calculationStorePickupTimeDto->pickup_time_end =
                        isset($datum['pickup_time_end']) && $datum['pickup_time_end']
                            ? Carbon::createFromFormat(
                                CalculationStorePickupTimeDto::TIME_FORMAT,
                                $datum['pickup_time_end']
                            ) : null;
                    $calculationStorePickupTimeDto->cargo_export_time =
                        isset($datum['cargo_export_time']) && $datum['cargo_export_time']
                            ? Carbon::createFromFormat(
                                CalculationStorePickupTimeDto::TIME_FORMAT,
                                $datum['cargo_export_time']
                            ) : null;
                }
            }
        }

        return new CalculationsResource($action->execute($calculationInDto));
    }
}
