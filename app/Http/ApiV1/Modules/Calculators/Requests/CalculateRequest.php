<?php

namespace App\Http\ApiV1\Modules\Calculators\Requests;

use App\Http\ApiV1\OpenApiGenerated\Dto\DeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CalculateRequest
 * @package App\Http\ApiV1\Modules\Calculators\Requests
 */
class CalculateRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'city_guid_to' => 'required',

            'basket_items' => ['required', 'array', 'min:1'],
            'basket_items.*.offer_id' => ['required', 'integer'],
            'basket_items.*.qty' => ['required', 'numeric'],
            'basket_items.*.weight' => ['required', 'numeric'],
            'basket_items.*.width' => ['required', 'numeric'],
            'basket_items.*.height' => ['required', 'numeric'],
            'basket_items.*.length' => ['required', 'numeric'],
            'basket_items.*.is_explosive' => ['sometimes', 'boolean', 'nullable'],
            'basket_items.*.store_id' => ['required', 'integer'],
            'basket_items.*.seller_id' => ['required', 'integer'],

            'stores' => ['required', 'array', 'min:1'],
            'stores.*.store_id' => ['required', 'integer'],
            'stores.*.city_guid' => 'required',

            'stores.*.working' => ['sometimes', 'array', 'min:1'],
            'stores.*.working.*.day' => ['required', 'integer'],
            'stores.*.working.*.working_start_time' => 'sometimes',
            'stores.*.working.*.working_end_time' => 'sometimes',

            'stores.*.pickup_times' => ['sometimes', 'array', 'min:1'],
            'stores.*.pickup_times.*.day' => ['required', 'integer'],
            'stores.*.pickup_times.*.pickup_time_code' => 'sometimes',
            'stores.*.pickup_times.*.pickup_time_start' => 'sometimes',
            'stores.*.pickup_times.*.pickup_time_end' => 'sometimes',
            'stores.*.pickup_times.*.cargo_export_time' => 'sometimes',
            'stores.*.pickup_times.*.delivery_service' => 'required',
        ];
    }
}
