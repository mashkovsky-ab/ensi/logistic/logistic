<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\City;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CitiesResource
 * @package App\Http\ApiV1\Modules\Geos\Resources
 *
 * @mixin City
 */
class CitiesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'region_id' => $this->region_id,
            'name' => $this->name,
            'guid' => $this->guid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'region' => RegionsResource::make($this->whenLoaded('region')),
        ];
    }
}
