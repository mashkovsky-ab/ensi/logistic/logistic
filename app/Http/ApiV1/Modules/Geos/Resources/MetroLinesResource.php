<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\MetroLine;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class MetroLinesResource
 * @package App\Http\ApiV1\Modules\Geos\Resources
 *
 * @mixin MetroLine
 */
class MetroLinesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'metro_stations' => MetroStationsResource::collection($this->whenLoaded('metroStations')),
        ];
    }
}
