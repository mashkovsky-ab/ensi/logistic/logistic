<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\MetroStation;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class MetroStationsResource
 * @package App\Http\ApiV1\Modules\Geos\Resources
 *
 * @mixin MetroStation
 */
class MetroStationsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'metro_line_id' => $this->metro_line_id,
            'city_guid' => $this->city_guid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'points' => PointsResource::collection($this->whenLoaded('points')),
            'metro_line' => MetroLinesResource::make($this->whenLoaded('metroLine')),
        ];
    }
}
