<?php

namespace App\Http\ApiV1\Modules\Geos\Resources;

use App\Domain\DeliveryServices\Models\Point;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class PointsResource
 * @package App\Http\ApiV1\Modules\Geos\Resources
 *
 * @mixin Point
 */
class PointsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'delivery_service' => $this->delivery_service,
            'active' => $this->active,
            'type' => $this->type,
            'name' => $this->name,
            'external_id' => $this->external_id,
            'apiship_external_id' => $this->apiship_external_id,
            'has_payment_card' => $this->has_payment_card,
            'address' => $this->address,
            'city_guid' => $this->city_guid,
            'email' => $this->email,
            'phone' => $this->phone,
            'timetable' => $this->timetable,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'metro_stations' => MetroStationsResource::collection($this->whenLoaded('metroStations')),
        ];
    }
}
