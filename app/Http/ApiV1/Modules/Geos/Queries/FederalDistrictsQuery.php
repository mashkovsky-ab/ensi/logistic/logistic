<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\Geos\Models\FederalDistrict;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class FederalDistrictsQuery
 * @package App\Http\ApiV1\Modules\Geos\Queries
 */
class FederalDistrictsQuery extends QueryBuilder
{
    /**
     * FederalDistrictsQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = FederalDistrict::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes(['regions']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
        ]);

        $this->defaultSort('-id');
    }
}
