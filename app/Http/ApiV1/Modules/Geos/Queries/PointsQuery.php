<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\DeliveryServices\Models\Point;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PointsQuery
 * @package App\Http\ApiV1\Modules\Geos\Queries
 */
class PointsQuery extends QueryBuilder
{
    /**
     * PointsQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = Point::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes([
            AllowedInclude::relationship('metro_stations', 'metroStations'),
            AllowedInclude::relationship('metro_stations.metro_line', 'metroStations.metroLine'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('type'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('external_id'),
            AllowedFilter::exact('has_payment_card'),
            AllowedFilter::exact('email'),
            AllowedFilter::exact('phone'),
            AllowedFilter::exact('active'),
        ]);

        $this->defaultSort('-id');
    }
}
