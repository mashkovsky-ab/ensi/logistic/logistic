<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class CityDeliveryServiceLinksQuery
 * @package App\Http\ApiV1\Modules\Geos\Queries
 */
class CityDeliveryServiceLinksQuery extends QueryBuilder
{
    /**
     * CityDeliveryServiceLinksQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = CityDeliveryServiceLink::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes(['city']);

        $this->allowedFilters([
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('city_id'),
            AllowedFilter::exact('city_guid'),
        ]);

        $this->defaultSort('-id');
    }
}
