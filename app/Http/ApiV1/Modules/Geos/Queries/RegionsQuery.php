<?php

namespace App\Http\ApiV1\Modules\Geos\Queries;

use App\Domain\Geos\Models\Region;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class RegionsQuery
 * @package App\Http\ApiV1\Modules\Geos\Queries
 */
class RegionsQuery extends QueryBuilder
{
    /**
     * RegionsQuery constructor.
     * @param  Request  $request
     */
    public function __construct(Request $request)
    {
        $query = Region::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'name']);
        $this->allowedIncludes([
            AllowedInclude::relationship('federal_district', 'federalDistrict'),
            AllowedInclude::relationship('delivery_prices', 'deliveryPrices'),
            'cities',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('federal_district_id'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('guid'),
        ]);

        $this->defaultSort('-id');
    }
}
