<?php

namespace App\Http\ApiV1\Modules\Geos\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceRegionRequest
 * @package App\Http\ApiV1\Modules\Geos\Requests
 */
class CreateOrReplaceRegionRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['required', 'integer'],
            'name' => ['required', 'string'],
            'guid' => ['required', 'string'],
      ];
    }
}
