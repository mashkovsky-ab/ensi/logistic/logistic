<?php

namespace App\Http\ApiV1\Modules\Geos\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceFederalDistrictRequest
 * @package App\Http\ApiV1\Modules\Geos\Requests
 */
class CreateOrReplaceFederalDistrictRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
      ];
    }
}
