<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Http\ApiV1\Modules\Geos\Queries\CityDeliveryServiceLinksQuery;
use App\Http\ApiV1\Modules\Geos\Resources\PointsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryCitiesController
 * @package App\Http\ApiV1\Modules\Geos\Controllers
 */
class DeliveryCitiesController
{
    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  CityDeliveryServiceLinksQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, CityDeliveryServiceLinksQuery $query): AnonymousResourceCollection
    {
        return PointsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
