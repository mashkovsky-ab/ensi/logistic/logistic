<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Domain\Geos\Actions\Dtos\RegionDto;
use App\Domain\Geos\Actions\Region\CreateRegionAction;
use App\Domain\Geos\Actions\Region\DeleteRegionAction;
use App\Domain\Geos\Actions\Region\PatchRegionAction;
use App\Domain\Geos\Actions\Region\ReplaceRegionAction;
use App\Http\ApiV1\Modules\Geos\Queries\RegionsQuery;
use App\Http\ApiV1\Modules\Geos\Requests\CreateOrReplaceRegionRequest;
use App\Http\ApiV1\Modules\Geos\Requests\PatchRegionRequest;
use App\Http\ApiV1\Modules\Geos\Resources\RegionsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class RegionsController
 * @package App\Http\ApiV1\Modules\Geos\Controllers
 */
class RegionsController
{
    /**
     * @param  CreateOrReplaceRegionRequest  $request
     * @param  CreateRegionAction  $action
     * @return RegionsResource
     */
    public function create(CreateOrReplaceRegionRequest $request, CreateRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute(new RegionDto($request->validated())));
    }

    /**
     * @param  int  $regionId
     * @param  CreateOrReplaceRegionRequest  $request
     * @param  ReplaceRegionAction  $action
     * @return RegionsResource
     */
    public function replace(
        int $regionId,
        CreateOrReplaceRegionRequest $request,
        ReplaceRegionAction $action
    ): RegionsResource {
        return new RegionsResource($action->execute($regionId, new RegionDto($request->validated())));
    }

    /**
     * @param  int  $regionId
     * @param  PatchRegionRequest  $request
     * @param  PatchRegionAction  $action
     * @return RegionsResource
     */
    public function patch(int $regionId, PatchRegionRequest $request, PatchRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute($regionId, new RegionDto($request->validated())));
    }

    /**
     * @param  int  $regionId
     * @param  DeleteRegionAction  $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(int $regionId, DeleteRegionAction $action): EmptyResource
    {
        $action->execute($regionId);

        return new EmptyResource();
    }

    /**
     * @param  int  $regionId
     * @param  RegionsQuery  $query
     * @return RegionsResource
     */
    public function get(int $regionId, RegionsQuery $query): RegionsResource
    {
        return new RegionsResource($query->findOrFail($regionId));
    }

    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  RegionsQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, RegionsQuery $query): AnonymousResourceCollection
    {
        return RegionsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  RegionsQuery  $query
     * @return RegionsResource
     */
    public function searchOne(RegionsQuery $query): RegionsResource
    {
        return new RegionsResource($query->firstOrFail());
    }
}
