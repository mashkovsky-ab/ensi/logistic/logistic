<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Domain\Geos\Actions\Dtos\FederalDistrictDto;
use App\Domain\Geos\Actions\FederalDistrict\CreateFederalDistrictAction;
use App\Domain\Geos\Actions\FederalDistrict\DeleteFederalDistrictAction;
use App\Domain\Geos\Actions\FederalDistrict\PatchFederalDistrictAction;
use App\Domain\Geos\Actions\FederalDistrict\ReplaceFederalDistrictAction;
use App\Http\ApiV1\Modules\Geos\Queries\FederalDistrictsQuery;
use App\Http\ApiV1\Modules\Geos\Requests\CreateOrReplaceFederalDistrictRequest;
use App\Http\ApiV1\Modules\Geos\Requests\PatchFederalDistrictRequest;
use App\Http\ApiV1\Modules\Geos\Resources\FederalDistrictsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class FederalDistrictsController
 * @package App\Http\ApiV1\Modules\Geos\Controllers
 */
class FederalDistrictsController
{
    /**
     * @param  CreateOrReplaceFederalDistrictRequest  $request
     * @param  CreateFederalDistrictAction  $action
     * @return FederalDistrictsResource
     */
    public function create(CreateOrReplaceFederalDistrictRequest $request, CreateFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute(new FederalDistrictDto($request->validated())));
    }

    /**
     * @param  int  $federalDistrictId
     * @param  CreateOrReplaceFederalDistrictRequest  $request
     * @param  ReplaceFederalDistrictAction  $action
     * @return FederalDistrictsResource
     */
    public function replace(
        int $federalDistrictId,
        CreateOrReplaceFederalDistrictRequest $request,
        ReplaceFederalDistrictAction $action
    ): FederalDistrictsResource {
        return new FederalDistrictsResource($action->execute($federalDistrictId, new FederalDistrictDto($request->validated())));
    }

    /**
     * @param  int  $federalDistrictId
     * @param  PatchFederalDistrictRequest  $request
     * @param  PatchFederalDistrictAction  $action
     * @return FederalDistrictsResource
     */
    public function patch(int $federalDistrictId, PatchFederalDistrictRequest $request, PatchFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute($federalDistrictId, new FederalDistrictDto($request->validated())));
    }

    /**
     * @param  int  $federalDistrictId
     * @param  DeleteFederalDistrictAction  $action
     * @return EmptyResource
     * @throws Exception
     */
    public function delete(int $federalDistrictId, DeleteFederalDistrictAction $action): EmptyResource
    {
        $action->execute($federalDistrictId);

        return new EmptyResource();
    }

    /**
     * @param  int  $federalDistrictId
     * @param  FederalDistrictsQuery  $query
     * @return FederalDistrictsResource
     */
    public function get(int $federalDistrictId, FederalDistrictsQuery $query): FederalDistrictsResource
    {
        return new FederalDistrictsResource($query->findOrFail($federalDistrictId));
    }

    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  FederalDistrictsQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, FederalDistrictsQuery $query): AnonymousResourceCollection
    {
        return FederalDistrictsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @param  FederalDistrictsQuery  $query
     * @return FederalDistrictsResource
     */
    public function searchOne(FederalDistrictsQuery $query): FederalDistrictsResource
    {
        return new FederalDistrictsResource($query->firstOrFail());
    }
}
