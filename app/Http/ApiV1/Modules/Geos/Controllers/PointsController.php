<?php

namespace App\Http\ApiV1\Modules\Geos\Controllers;

use App\Http\ApiV1\Modules\Geos\Queries\PointsQuery;
use App\Http\ApiV1\Modules\Geos\Resources\PointsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class PointsController
 * @package App\Http\ApiV1\Modules\Geos\Controllers
 */
class PointsController
{
    /**
     * @param  PageBuilderFactory  $pageBuilderFactory
     * @param  PointsQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(PageBuilderFactory $pageBuilderFactory, PointsQuery $query): AnonymousResourceCollection
    {
        return PointsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
