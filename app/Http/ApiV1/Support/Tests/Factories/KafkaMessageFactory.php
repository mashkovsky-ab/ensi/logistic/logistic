<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\AbstractEventMessage;
use Ensi\TestFactories\Factory;
use RdKafka\Message;

abstract class KafkaMessageFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'event' => $this->faker->randomElement([
                AbstractEventMessage::CREATE,
                AbstractEventMessage::UPDATE,
                AbstractEventMessage::DELETE,
            ]),
        ];
    }

    protected function mergeDefinitionWithExtra(array $extra): array
    {
        $array = parent::prepareDefinition();

        $extraAttributes = $extra['attributes'] ?? [];
        $extraDirty = $extra['dirty'] ?? [];

        $array['event'] = $extra['event'] ?? $array['event'];
        $array['attributes'] = array_merge($array['attributes'], $extraAttributes);

        $dirty = match ($array['event']) {
            AbstractEventMessage::CREATE => array_keys($array['attributes']),
            AbstractEventMessage::UPDATE => $this->faker->randomElements(
                array_diff(array_keys($array['attributes']), ["id"])
            ),
            AbstractEventMessage::DELETE => [],
        };

        $array['dirty'] = array_unique(array_merge($dirty, $extraDirty));

        return $array;
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }
}
