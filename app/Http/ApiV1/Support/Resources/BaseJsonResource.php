<?php

namespace App\Http\ApiV1\Support\Resources;

use App\Http\ApiV1\Support\Pagination\Page;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * Class BaseJsonResource
 * @package App\Http\ApiV1\Support\Resources
 */
abstract class BaseJsonResource extends JsonResource
{
    public const DATE_FORMAT = 'Y-m-d';

    /**
     * @param $resource
     * @param  array  $pagination
     * @return AnonymousResourceCollection
     */
    public static function collectionWithPagination($resource, array $pagination): AnonymousResourceCollection
    {
        $collection = static::collection($resource);
        $currentAdditional = $collection->additional ?: [];
        $append = ['meta' => ['pagination' => $pagination]];

        return static::collection($resource)->additional(array_merge_recursive($currentAdditional, $append));
    }

    /**
     * @return null|array
     */
    protected function mapFileToResponse(string $fieldName)
    {
        $value = $this->$fieldName;
        if (!$value) {
            return null;
        }

        $fileManager = resolve(EnsiFilesystemManager::class);

        return [
            'path' => $value->path ?? '',
            'name' => $value->name ?? '',
            'url' => $value->path ? $fileManager->public()->url($value->path) : '',
        ];
    }

    /**
     * @param  Page  $page
     * @return AnonymousResourceCollection
     */
    public static function collectPage(Page $page): AnonymousResourceCollection
    {
        return static::collectionWithPagination($page->items, $page->pagination);
    }

    protected function mapPublicFileToResponse(?string $filePath): ?EnsiFile
    {
        return $filePath ? EnsiFile::public($filePath) : null;
    }

    protected function mapProtectedFileToResponse(?string $filePath): ?EnsiFile
    {
        return $filePath ? EnsiFile::protected($filePath) : null;
    }

    public function dateToIso(?Carbon $date): ?string
    {
        return $date?->format(static::DATE_FORMAT);
    }
}
