<?php

namespace App\Http\ApiV1\Support\Resources;

use Illuminate\Http\Request;

class AddressResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'address_string' => $this['address_string'] ?? null,
            'post_index' => $this['post_index'] ?? null,
            'country_code' => $this['country_code'] ?? null,
            'region' => $this['region'] ?? null,
            'area' => $this['area'] ?? null,
            'city' => $this['city'] ?? null,
            'city_guid' => $this['city_guid'] ?? null,
            'street' => $this['street'] ?? null,
            'house' => $this['house'] ?? null,
            'block' => $this['block'] ?? null,
            'flat' => $this['flat'] ?? null,
            'company_name' => $this['company_name'] ?? null,
            'contact_name' => $this['contact_name'] ?? null,
            'email' => $this['email'] ?? null,
            'phone' => $this['phone'] ?? null,
            'comment' => $this['comment'] ?? null,
        ];
    }
}
