<?php

namespace App\Http\ApiV1\Support\Pagination;

use Illuminate\Database\Eloquent\Builder as EloquentQueryBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder as SpatieQueryBuilder;

/**
 * Class AbstractPageBuilder
 * @package App\Http\ApiV1\Support\Pagination
 */
abstract class AbstractPageBuilder
{
    protected bool $forbidToBypassPagination = false;

    protected ?int $maxLimit = null;

    /**
     * AbstractPageBuilder constructor.
     * @param  QueryBuilder|EloquentQueryBuilder|SpatieQueryBuilder  $query
     * @param  Request  $request
     */
    public function __construct(protected QueryBuilder|EloquentQueryBuilder|SpatieQueryBuilder $query, protected Request $request)
    {
    }

    abstract public function build(): Page;

    /**
     * @param  bool  $value
     * @return $this
     */
    public function forbidToBypassPagination(bool $value = true): static
    {
        $this->forbidToBypassPagination = $value;

        return $this;
    }

    /**
     * @param  int|null  $maxLimit
     * @return $this
     */
    public function maxLimit(?int $maxLimit): static
    {
        $this->maxLimit = $maxLimit;

        return $this;
    }

    /**
     * @param  int  $limit
     * @return int
     */
    protected function applyMaxLimit(int $limit): int
    {
        return $this->maxLimit !== null && $this->maxLimit > 0 ? min($limit, $this->maxLimit) : $limit;
    }

    /**
     * @return int
     */
    protected function getDefaultLimit(): int
    {
        return config('pagination.default_limit');
    }
}
