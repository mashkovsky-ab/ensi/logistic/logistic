<?php

namespace App\Http\ApiV1\Support\Pagination;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use Illuminate\Database\Eloquent\Builder as EloquentQueryBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder as SpatieQueryBuilder;

/**
 * Class PageBuilderFactory
 * @package App\Http\ApiV1\Support\Pagination
 */
class PageBuilderFactory
{
    /**
     * @param  QueryBuilder|EloquentQueryBuilder|SpatieQueryBuilder  $query
     * @param  Request|null  $request
     * @return AbstractPageBuilder
     */
    public function fromQuery(QueryBuilder|EloquentQueryBuilder|SpatieQueryBuilder $query, ?Request $request = null): AbstractPageBuilder
    {
        $request = $request ?: resolve(Request::class);

        return $request->input('pagination.type', config('pagination.default_type')) === PaginationTypeEnum::CURSOR
            ? new CursorPageBuilder($query, $request)
            : new OffsetPageBuilder($query, $request);
    }
}
