<?php

namespace App\Http\ApiV1\Support\Pagination;

use Illuminate\Support\Collection;

/**
 * Class Page
 * @package App\Http\ApiV1\Support\Pagination
 */
class Page
{
    /**
     * @var array|Collection
     */
    public array $items;

    /**
     * Page constructor.
     * @param  array|Collection  $items
     * @param  array  $pagination
     */
    public function __construct(array|Collection $items, public array $pagination)
    {
        $this->items = is_object($items) ? $items->all() : $items;
    }
}
