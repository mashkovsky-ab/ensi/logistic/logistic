<?php

namespace App\Console;

use App\Console\Commands\CargoOrders\ScheduledCreateCargoOrdersCommand;
use App\Console\Commands\DeliveryOrders\UpdateDeliveryOrderStatusesCommand;
use Ensi\LaravelInitialEventPropagation\SetInitialEventArtisanMiddleware;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    public function bootstrap()
    {
        parent::bootstrap();
        (new SetInitialEventArtisanMiddleware())->handle();
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command(PointsImportCommand::class)->dailyAt('01:00');
        // $schedule->command(TariffsImportCommand::class)->dailyAt('01:30');
        // $schedule->command(DeliveryCitiesImportCommand::class)->dailyAt('02:00');
        $schedule->command(ScheduledCreateCargoOrdersCommand::class)->everyMinute();
        $schedule->command(UpdateDeliveryOrderStatusesCommand::class)->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
    }
}
