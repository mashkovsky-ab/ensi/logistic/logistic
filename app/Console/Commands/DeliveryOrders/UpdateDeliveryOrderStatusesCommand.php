<?php

namespace App\Console\Commands\DeliveryOrders;

use App\Domain\DeliveryOrders\Actions\DeliveryOrder\UpdateDeliveryOrderStatusesAction;
use Illuminate\Console\Command;

class UpdateDeliveryOrderStatusesCommand extends Command
{
    protected $signature = 'delivery-orders:update-statuses';
    protected $description = 'Обновить статусы активных заказов на доставку';

    public function handle(UpdateDeliveryOrderStatusesAction $action)
    {
        $action->execute();
    }
}
