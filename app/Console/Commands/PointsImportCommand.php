<?php

namespace App\Console\Commands;

use App\Domain\DeliveryServices\Actions\Dtos\MetroLineDto;
use App\Domain\DeliveryServices\Actions\Dtos\MetroStationDto;
use App\Domain\DeliveryServices\Actions\Dtos\PointDto;
use App\Domain\DeliveryServices\Actions\Dtos\PointMetroStationLinkDto;
use App\Domain\DeliveryServices\Actions\Metro\CreateMetroLineAction;
use App\Domain\DeliveryServices\Actions\Metro\CreateMetroStationAction;
use App\Domain\DeliveryServices\Actions\Point\CreateOrUpdatePointAction;
use App\Domain\DeliveryServices\Actions\Point\CreatePointMetroStationLinkAction;
use App\Domain\DeliveryServices\Actions\Point\PatchPointMetroStationLinkAction;
use App\Domain\DeliveryServices\Enums\PointType;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\DeliveryServices\Models\MetroLine;
use App\Domain\DeliveryServices\Models\MetroStation;
use App\Domain\DeliveryServices\Models\Point;
use App\Domain\DeliveryServices\Models\PointMetroStationLink;
use App\Domain\External\ApiShip\ApiShipService;
use App\Domain\External\ApiShip\Dto\Lists\Point\PointDto as ApiShipPointDto;
use App\Domain\External\ApiShip\Responses\Lists\PointListsResponse;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Log\Logger;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Импорт пунктов приема/выдачи заказов из ApiShip по подключенным службам доставки.
 * P.S. Подключение служб доставки происходит по адресу https://app.apiship.ru/params/index
 * Class PointsImportCommand
 * @package App\Console\Commands
 */
class PointsImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:points';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import points from ApiShip';

    /** @var array [сity_guid][name] => id линий метро */
    protected $metroLinesCache = [];

    /** @var array [сity_guid][name] => id станций метро */
    protected $metroStationsCache = [];

    /** @var Logger */
    protected $logger;

    /** @var int - кол-во записей, получаемых за 1 шаг */
    protected const STEP_LIMIT = 500;

    /**
     * PointsImportCommand constructor.
     */
    public function __construct(
        protected ApiShipService $apiShipService,
        protected CreateOrUpdatePointAction $createOrUpdatePointAction,
        protected CreateMetroLineAction $createMetroLineAction,
        protected CreateMetroStationAction $createMetroStationAction,
        protected CreatePointMetroStationLinkAction $createPointMetroStationLinkAction,
        protected PatchPointMetroStationLinkAction $patchPointMetroStationLinkAction
    ) {
        parent::__construct();

        $this->logger = Log::channel('import:points');
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $message = "Начат импорт пунктов приема/выдачи заказов";
        $this->getOutput()->note($message);
        $this->logger->info($message);

        $startAt = now();
        $this->loadMetro();
        $this->importPoints();
        $this->deactivateOld($startAt);

        $message = "Закончен импорт пунктов приема/выдачи заказов";
        $this->logger->info($message);
        $this->getOutput()->success($message);
    }

    protected function loadMetro(): void
    {
        /** @var Collection|MetroLine[] $metroLines */
        $metroLines = MetroLine::query()->select(['id', 'name'])->with('metroStations')->get();

        foreach ($metroLines as $metroLine) {
            $this->addMetroLine2Cache($metroLine);

            foreach ($metroLine->metroStations as $metroStation) {
                $this->addMetroStation2Cache($metroStation);
            }
        }
    }

    protected function importPoints(): void
    {
        $deliveryServices = DeliveryService::query()->select(['id', 'apiship_key'])->get();

        $listsApi = $this->apiShipService->lists();
        $result = $listsApi->getPoints();
        $pointListsResponse = new PointListsResponse($result->getOriginJson());
        $countPages = (int)ceil($pointListsResponse->getTotal() / static::STEP_LIMIT);

        $this->getOutput()->note("Получено {$pointListsResponse->getTotal()} пункт(ов) приема/выдачи заказов");
        $bar = $this->getOutput()->createProgressBar($pointListsResponse->getTotal());
        $this->logger->info("Получено {$pointListsResponse->getTotal()} пункт(ов) приема/выдачи заказов");

        for ($page = 0; $page < $countPages; $page++) {
            $result = $listsApi->getPoints(static::STEP_LIMIT, $page * static::STEP_LIMIT);
            $pointDtos = (new PointListsResponse($result->getOriginJson()))->getResults();

            foreach ($pointDtos as $pointDto) {
                try {
                    /** @var DeliveryService $deliveryService */
                    $deliveryService = $deliveryServices->where('apiship_key', $pointDto->providerKey)->first();
                    if (!$deliveryService) {
                        continue;
                    }
                    $pointType = PointType::findByExternalId($pointDto->type);

                    /** @var Point $point */
                    $point = $this->createOrUpdatePointAction->execute(new PointDto([
                        'delivery_service_id' => $deliveryService->id,
                        'external_id' => $pointDto->code,
                        'type' => $pointType->id,
                        'name' => $pointType->name,
                        'apiship_external_id' => $pointDto->id,
                        'has_payment_card' => $pointDto->paymentCard,
                        'geo_lat' => $pointDto->lat,
                        'geo_lon' => $pointDto->lng,
                        'country_code' => $pointDto->countryCode,
                        'post_index' => $pointDto->postIndex,
                        'region' => $pointDto->region,
                        'region_type' => $pointDto->regionType,
                        'area' => $pointDto->area,
                        'city' => $pointDto->city,
                        'city_type' => $pointDto->cityType,
                        'street' => $pointDto->street,
                        'street_type' => $pointDto->streetType,
                        'house' => $pointDto->house,
                        'block' => $pointDto->block,
                        'flat' => $pointDto->flat,
                        'city_guid' => $pointDto->cityGuid,
                        'email' => $pointDto->email,
                        'phone' => $pointDto->phone,
                        'timetable' => $pointDto->timetable,
                        'description' => $pointDto->description,
                    ]));

                    $this->importPointMetro($point, $pointDto);

                    $bar->advance();
                } catch (Exception $e) {
                    $message = "Ошибка при сохранении пункта приема/выдачи заказов c кодом " .
                        "{$pointDto->name} ({$pointDto->code}): {$e->getMessage()}";
                    $this->getOutput()->warning($message);
                    $this->logger->warning($message, $pointDto->toArray());
                }
            }
        }

        $bar->finish();
        $this->getOutput()->newLine();
    }

    /**
     * @param  Point  $point
     * @param  ApiShipPointDto  $pointDto
     */
    protected function importPointMetro(Point $point, ApiShipPointDto $pointDto): void
    {
        try {
            /** @var Collection|PointMetroStationLink[] $pointMetroStationLinks */
            $pointMetroStationLinks = $point->pointMetroStationLinks->keyBy('metro_station_id');

            DB::transaction(function () use ($point, $pointDto, $pointMetroStationLinks) {
                if ($pointDto->metro) {
                    foreach ($pointDto->metro as $pointMetroDto) {
                        if (!isset($this->metroLinesCache[$pointMetroDto->line])) {
                            $metroLine = $this->createMetroLineAction->execute(new MetroLineDto([
                                'name' => $pointMetroDto->line,
                            ]));
                            $this->addMetroLine2Cache($metroLine);
                        }
                        $metroLineId = $this->metroLinesCache[$pointMetroDto->line];

                        if (!isset($this->metroStationsCache[$metroLineId][$pointMetroDto->name])) {
                            $metroStation = $this->createMetroStationAction->execute(new MetroStationDto([
                                'metro_line_id' => $metroLineId,
                                'name' => $pointMetroDto->name,
                                'city_guid' => $pointDto->cityGuid,
                            ]));
                            $this->addMetroStation2Cache($metroStation);
                        }
                        $metroStationId = $this->metroStationsCache[$metroLineId][$pointMetroDto->name];

                        if ($pointMetroStationLinks->has($metroStationId)) {
                            //Обновляем существующие привязки, если расстояние поменялось
                            $pointMetroStationLink = $pointMetroStationLinks[$metroStationId];
                            $this->patchPointMetroStationLinkAction->execute(
                                $pointMetroStationLink,
                                new PointMetroStationLinkDto(['distance' => $pointMetroDto->distance])
                            );

                            $pointMetroStationLinks->forget($metroStationId);
                        } else {
                            //Создаем новые привязки
                            $this->createPointMetroStationLinkAction->execute(new PointMetroStationLinkDto([
                                'point_id' => $point->id,
                                'metro_station_id' => $metroStationId,
                                'distance' => $pointMetroDto->distance,
                            ]));
                        }
                    }

                    //Удаляем старые привязки, которых больше нет
                    if ($pointMetroStationLinks->isNotEmpty()) {
                        foreach ($pointMetroStationLinks as $metroStationLink) {
                            $metroStationLink->delete();
                        }
                    }
                } else {
                    foreach ($pointMetroStationLinks as $metroStationLink) {
                        $metroStationLink->delete();
                    }
                }
            });
        } catch (Exception $e) {
            $message =
                "Ошибка при сохранении метро для пункта приема/выдачи заказов c id={$point->id}: {$e->getMessage()}";
            $this->getOutput()->warning($message);
            $this->logger->warning($message, $pointDto->toArray());
        } catch (Throwable $e) {
            $message =
                "Ошибка при сохранении метро для пункта приема/выдачи заказов c id={$point->id}: {$e->getMessage()}";
            $this->getOutput()->warning($message);
            $this->logger->warning($message, $pointDto->toArray());
        }
    }

    /**
     * @param  MetroLine  $metroLine
     */
    protected function addMetroLine2Cache(MetroLine $metroLine): void
    {
        $this->metroLinesCache[$metroLine->name] = $metroLine->id;
    }

    /**
     * @param  MetroStation $metroStation
     */
    protected function addMetroStation2Cache(MetroStation $metroStation): void
    {
        $this->metroStationsCache[$metroStation->metro_line_id][$metroStation->name] = $metroStation->id;
    }

    /**
     * Деактивировать неактуальные пункты приема/выдачи заказов
     * @param  Carbon  $startAt
     */
    protected function deactivateOld(Carbon $startAt): void
    {
        /** @var Collection|Point[] $points */
        $points = Point::query()
            ->where('updated_at', '<', $startAt)
            ->get();
        foreach ($points as $point) {
            try {
                $point->deactivate();
            } catch (Exception $e) {
                $message = "Ошибка при деактивации неактуального пункта приема/выдачи заказов связи с id {$point->id}";
                $this->getOutput()->warning($message);
                $this->logger->warning($message);
            }
        }
    }
}
