<?php

namespace App\Console\Commands\CargoOrders;

use App\Domain\CargoOrders\Actions\ScheduledCreateCargoOrdersAction;
use Illuminate\Console\Command;

class ScheduledCreateCargoOrdersCommand extends Command
{
    protected $signature = 'cargo-orders:scheduled-creation';
    protected $description = 'Создать заказ на забор груза по расписанию отгрузок склада';

    public function handle(ScheduledCreateCargoOrdersAction $action)
    {
        $action->execute();
    }
}
