<?php

namespace App\Console\Commands;

use App\Domain\DeliveryServices\Actions\City\CreateOrUpdateCityAction;
use App\Domain\DeliveryServices\Actions\City\CreateOrUpdateCityDeliveryServiceLinkAction;
use App\Domain\DeliveryServices\Actions\Dtos\CityDeliveryServiceLinkDto;
use App\Domain\DeliveryServices\Actions\Dtos\CityDto;
use App\Domain\DeliveryServices\Models\CityDeliveryServiceLink;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\ApiShip\ApiShipService;
use App\Domain\External\ApiShip\Dto\Lists\ProviderCity\AbstractProviderCityDto;
use App\Domain\Geos\Models\Region;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Log\Logger;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * Class DeliveryCitiesImportCommand
 * @package App\Console\Commands
 */
class DeliveryCitiesImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:delivery_cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import delivery cities from ApiShip';

    /** @var Logger */
    protected $logger;

    /** @var int - кол-во записей, получаемых за 1 шаг */
    protected const STEP_LIMIT = 500;

    /** @var array - список служб доставки, для которых необходимо импортировать населенные пункты */
    protected const DELIVERY_SERVICES = [
        DeliveryService::SERVICE_B2CPL,
        DeliveryService::SERVICE_CDEK,
    ];

    /** @var Collection|Region[] */
    protected $regionsCache;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        protected ApiShipService $apiShipService,
        protected CreateOrUpdateCityAction $createOrUpdateCityAction,
        protected CreateOrUpdateCityDeliveryServiceLinkAction $createOrUpdateCityDeliveryServiceLinkAction
    ) {
        parent::__construct();

        $this->logger = Log::channel('import:delivery_cities');
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $message = 'Начат импорт';
        $this->getOutput()->note($message);
        $this->logger->info($message);

        $startAt = now();
        $this->import();
        $this->deleteOld($startAt);

        $message = 'Закончен импорт';
        $this->logger->info($message);
        $this->getOutput()->success($message);
    }

    protected function import(): void
    {
        $listsApi = $this->apiShipService->lists();

        /** @var Collection|DeliveryService[] $deliveryServices */
        $deliveryServices = DeliveryService::query()->whereIn('id', static::DELIVERY_SERVICES)->get();
        foreach ($deliveryServices as $deliveryService) {
            try {
                $providerCityListsResponse = $listsApi->getProviderCities($deliveryService);
                $countPages = (int)ceil($providerCityListsResponse->getTotal() / static::STEP_LIMIT);

                $message = "Получено {$providerCityListsResponse->getTotal()} населенных пунктов " .
                    $deliveryService->apiship_key;
                $this->getOutput()->note($message);
                $bar = $this->getOutput()->createProgressBar($providerCityListsResponse->getTotal());
                $this->logger->info($message);

                for ($page = 0; $page < $countPages; $page++) {
                    $providerCityListsResponse = $listsApi->getProviderCities(
                        $deliveryService,
                        static::STEP_LIMIT,
                        $page * static::STEP_LIMIT
                    );
                    $providerCityDtos = $providerCityListsResponse->getResults();

                    foreach ($providerCityDtos as $providerCityDto) {
                        if (!$providerCityDto->cityGuid) {
                            continue;
                        }

                        try {
                            $region = $this->searchRegionByName($providerCityDto);
                            if (is_null($region)) {
                                $message =
                                    "Не удалось найти регион $providerCityDto->region для населенного пункта " .
                                    "c id $providerCityDto->id для службы доставки $deliveryService->apiship_key";
                                $this->getOutput()->warning($message);
                                $this->logger->warning($message);

                                continue;
                            }

                            $city = $this->createOrUpdateCityAction->execute(new CityDto([
                                'region_id' => $region->id,
                                'name' => $providerCityDto->city,
                                'guid' => $providerCityDto->cityGuid,
                            ]));

                            $this->createOrUpdateCityDeliveryServiceLinkAction->execute(new CityDeliveryServiceLinkDto([
                                'delivery_service_id' => $deliveryService->id,
                                'city_guid' => $providerCityDto->cityGuid,
                                'city_id' => $city->id,
                                'payload' => array_diff_key(
                                    $providerCityDto->toArray(),
                                    array_flip(["id", "cityGuid"])
                                ),
                            ]));

                            $bar->advance();
                        } catch (Exception $e) {
                            $message = "Ошибка при сохранении населенного пункта c id $providerCityDto->id " .
                                "для службы доставки $deliveryService->apiship_key: {$e->getMessage()}";
                            $this->getOutput()->warning($message);
                            $this->logger->warning($message, $providerCityDto->toArray());
                        }
                    }
                }

                $bar->finish();
                $this->getOutput()->newLine();
            } catch (Exception $e) {
                $message = "Непредвиденная ошибка: {$e->getMessage()}";
                $this->getOutput()->warning($message);
                $this->logger->warning($message, $e->getTrace());
            }
        }
    }

    /**
     * Удалить неактуальные связи городов со службами доставки
     * @param  Carbon  $startAt
     */
    protected function deleteOld(Carbon $startAt): void
    {
        /** @var Collection|CityDeliveryServiceLink[] $cityDeliveryServiceLinks */
        $cityDeliveryServiceLinks = CityDeliveryServiceLink::query()
            ->where('updated_at', '<', $startAt)
            ->get();
        foreach ($cityDeliveryServiceLinks as $cityDeliveryServiceLink) {
            try {
                $cityDeliveryServiceLink->delete();
            } catch (Exception $e) {
                $message = "Ошибка при удалении неактуальной связи с id $cityDeliveryServiceLink->id " .
                    "города со службой доставки : {$e->getMessage()}";
                $this->getOutput()->warning($message);
                $this->logger->warning($message);
            }
        }
    }

    /**
     * Поиск региона
     * @param  AbstractProviderCityDto $providerCityDto
     * @return Region|null
     */
    protected function searchRegionByName(AbstractProviderCityDto $providerCityDto): ?Region
    {
        if (is_null($this->regionsCache)) {
            $this->regionsCache = Region::query()->get()->keyBy('guid');
        }

        /**
         * Сначала ищем регион по названию
         */
        $regionName = $providerCityDto->region;
        foreach ([
            'область',
            'республика',
            ' обл',
            ' респ',
            'край',
            'автономный округ',
            'автономная область',
        ] as $search) {
            $regionName = str_ireplace($search, '', $regionName);
        }
        $regionName = trim($regionName);

        foreach ($this->regionsCache as $region) {
            if (stripos($region->name, $regionName) !== false) {
                return $region;
            }
        }

        return null;
    }
}
