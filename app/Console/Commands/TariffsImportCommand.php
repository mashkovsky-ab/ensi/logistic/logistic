<?php

namespace App\Console\Commands;

use App\Domain\DeliveryPrices\Actions\Dtos\TariffDto;
use App\Domain\DeliveryPrices\Actions\Tariff\CreateOrUpdateTariffAction;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\ShipmentMethod;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\ApiShip\ApiShipService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;

/**
 * Импорт тарифов из ApiShip по подключенным службам доставки.
 * P.S. Подключение служб доставки происходит по адресу https://app.apiship.ru/params/index
 * Class TariffsImportCommand
 * @package App\Console\Commands
 */
class TariffsImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tariffs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import tariffs from ApiShip';

    /** @var Logger */
    protected $logger;

    /** @var int - кол-во записей, получаемых за 1 шаг */
    protected const STEP_LIMIT = 500;

    /**
     * PointsImportCommand constructor.
     */
    public function __construct(
        protected ApiShipService $apiShipService,
        protected CreateOrUpdateTariffAction $createOrUpdateTariffAction
    ) {
        parent::__construct();

        $this->logger = Log::channel('import:tariffs');
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->getOutput()->note('Начат импорт тарифов');
        $this->logger->info('Начат импорт');

        $this->importTariffs();

        $this->logger->info('Закончен импорт тарифов');
        $this->getOutput()->success('Закончен импорт');
    }

    protected function importTariffs(): void
    {
        $listsApi = $this->apiShipService->lists();
        $deliveryServices = DeliveryService::query()->select(['id', 'apiship_key'])->get();

        $tariffListsResponse = $listsApi->getTariffs();
        $countPages = (int)ceil($tariffListsResponse->getTotal() / static::STEP_LIMIT);

        $this->getOutput()->note("Получено {$tariffListsResponse->getTotal()} тарифов");
        $bar = $this->getOutput()->createProgressBar($tariffListsResponse->getTotal());
        $this->logger->info("Получено {$tariffListsResponse->getTotal()} тарифов");

        for ($page = 0; $page < $countPages; $page++) {
            $tariffListsResponse = $listsApi->getTariffs(static::STEP_LIMIT, $page * static::STEP_LIMIT);
            $tariffDtos = $tariffListsResponse->getResults();

            foreach ($tariffDtos as $i => $tariffDto) {
                try {
                    /** @var \App\Domain\DeliveryServices\Models\DeliveryService $deliveryService */
                    $deliveryService = $deliveryServices->where('apiship_key', $tariffDto->providerKey)->first();
                    if (!$deliveryService) {
                        continue;
                    }

                    $deliveryMethod = $tariffDto->deliveryType ?
                        DeliveryMethod::findByExternalId($tariffDto->deliveryType) : null;
                    $shipmentMethod = $tariffDto->pickupType ?
                        ShipmentMethod::findByExternalId($tariffDto->pickupType) : null;
                    $externalId = mb_strtolower($tariffDto->aliasName);

                    $this->createOrUpdateTariffAction->execute(new TariffDto([
                        'delivery_service_id' => $deliveryService->id,
                        'external_id' => $externalId,
                        'name' => $tariffDto->name,
                        'description' => $tariffDto->description,
                        'apiship_external_id' => $tariffDto->id,
                        'delivery_method_id' => $deliveryMethod ? $deliveryMethod->id : null,
                        'shipment_method_id' => $shipmentMethod ? $shipmentMethod->id : null,
                        'weight_min' => $tariffDto->weightMin,
                        'weight_max' => $tariffDto->weightMax,
                    ]));

                    $bar->advance();
                } catch (Exception $e) {
                    $this->getOutput()->warning(
                        "Ошибка при сохранении тарифа c кодом
                     {$tariffDto->name} ({$tariffDto->code}): {$e->getMessage()}"
                    );
                    $this->logger->warning(
                        "Ошибка при сохранении тарифа: {$e->getMessage()}",
                        $tariffDto->toArray()
                    );
                }
            }
        }

        $bar->finish();
        $this->getOutput()->newLine();
    }
}
