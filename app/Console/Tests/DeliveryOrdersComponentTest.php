<?php

use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\External\Actions\DeliveryOrder\GetDeliveryOrderStatusAction;
use App\Domain\External\B2Cpl\Lists\B2CplDeliveryOrderStatus;
use App\Domain\External\Dto\Response\DeliveryOrdersStatusResponseDto;
use App\Domain\External\Dto\Response\Part\DeliveryOrderStatus\DeliveryOrderStatusOutputDto;
use Illuminate\Support\Carbon;
use Tests\ComponentTestCase;
use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command delivery-orders:update-statuses success", function () {
    /** @var DeliveryOrder $deliveryOrder */
    $deliveryOrder = DeliveryOrder::factory()
        ->create([
            'status' => DeliveryOrderStatus::SHIPPED,
        ]);

    $deliveryOrderStatusOutputDto = new DeliveryOrderStatusOutputDto([
        'success' => true,
        'number' => $deliveryOrder->number,
        'status_external_id' => B2CplDeliveryOrderStatus::READY_FOR_RECIPIENT,
        'status' => DeliveryOrderStatus::READY_FOR_RECIPIENT,
        'status_date' => (new Carbon())->toDateTimeString(),
    ]);
    $this->mock(GetDeliveryOrderStatusAction::class)
        ->allows([
            'execute' => new DeliveryOrdersStatusResponseDto([
                'items' => collect()->push($deliveryOrderStatusOutputDto)
            ]),
        ]);

    artisan("delivery-orders:update-statuses");

    assertDatabaseHas((new DeliveryOrder())->getTable(), [
        'id' => $deliveryOrder->id,
        'status' => $deliveryOrderStatusOutputDto->status,
        'external_status' => $deliveryOrderStatusOutputDto->status_external_id,
        'external_status_at' => $deliveryOrderStatusOutputDto->status_date,
    ]);
});
