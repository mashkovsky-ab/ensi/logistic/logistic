<?php

use App\Domain\CargoOrders\Events\CreateCargoOrderEvent;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Models\CargoOrder;
use App\Domain\CargoOrders\Models\StorePickupTime;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use Tests\ComponentTestCase;
use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseCount;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command cargo-orders:scheduled-creation success", function () {
    $deliveryServiceEnums = DeliveryServiceEnum::toValues();

    StorePickupTime::factory()
        ->sequence(
            ...collect($deliveryServiceEnums)
            ->map(fn($deliveryServiceEnum) => [
                'delivery_service_id' => $deliveryServiceEnum,
            ])
            ->toArray()
        )
        ->count(count($deliveryServiceEnums))
        ->create([
            'day' => date("N")
        ]);

    $cargo = Cargo::factory()->count(5)->create();
    Event::fake();
    artisan("cargo-orders:scheduled-creation");
    Event::assertDispatched(CreateCargoOrderEvent::class);

    assertDatabaseCount((new CargoOrder())->getTable(), count($cargo));
});

test("Command cargo-orders:scheduled-creation missing slot", function () {
    $cargoCount = 10;
    StorePickupTime::factory()
        ->create([
            'day' => date("N"),
            'delivery_service_id' => DeliveryServiceEnum::B2CPL()->value,
        ]);

    Cargo::factory()
        ->sequence(
            ['delivery_service_id' => DeliveryServiceEnum::B2CPL()->value],
            ['delivery_service_id' => DeliveryServiceEnum::BOXBERRY()->value],
        )
        ->count($cargoCount)
        ->create();

    Event::fake();
    artisan("cargo-orders:scheduled-creation");
    Event::assertDispatched(CreateCargoOrderEvent::class);

    assertDatabaseCount((new CargoOrder())->getTable(), $cargoCount / 2);
});
