<?php

namespace App\Providers;

use App\Domain\CargoOrders\Events\CancelCargoOrderEvent;
use App\Domain\CargoOrders\Events\CreateCargoOrderEvent;
use App\Domain\CargoOrders\Listeners\CancelCargoOrderListener;
use App\Domain\CargoOrders\Listeners\CreateCargoOrderListener;
use App\Domain\CargoOrders\Models\Cargo;
use App\Domain\CargoOrders\Observers\CargoObserver;
use App\Domain\DeliveryOrders\Events\CancelDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Events\CreateDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Events\PatchDeliveryOrderEvent;
use App\Domain\DeliveryOrders\Listeners\DeliveryOrder\CancelDeliveryOrderListener;
use App\Domain\DeliveryOrders\Listeners\DeliveryOrder\CreateDeliveryOrderListener;
use App\Domain\DeliveryOrders\Listeners\DeliveryOrder\PatchDeliveryOrderListener;
use App\Domain\DeliveryOrders\Models\DeliveryOrder;
use App\Domain\DeliveryOrders\Observers\DeliveryOrderObserver;
use App\Domain\DeliveryServices\Models\DeliveryServiceDocument;
use App\Domain\DeliveryServices\Observers\DeliveryServiceDocumentObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        //CargoOrder events
        CreateCargoOrderEvent::class => [
            CreateCargoOrderListener::class,
        ],
        CancelCargoOrderEvent::class => [
            CancelCargoOrderListener::class,
        ],
        //DeliveryOrder events
        CreateDeliveryOrderEvent::class => [
            CreateDeliveryOrderListener::class,
        ],
        PatchDeliveryOrderEvent::class => [
            PatchDeliveryOrderListener::class,
        ],
        CancelDeliveryOrderEvent::class => [
            CancelDeliveryOrderListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        DeliveryOrder::observe(DeliveryOrderObserver::class);
        DeliveryServiceDocument::observe(DeliveryServiceDocumentObserver::class);
        Cargo::observe(CargoObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
