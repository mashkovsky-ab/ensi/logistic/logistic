<?php

namespace App\Providers;

use App\Domain\External\ApiShip\ApiShipService;
use App\Domain\External\B2Cpl\B2CplService;
use App\Domain\External\Cdek\CdekService;
use App\Domain\External\Cdek\CdekService2;
use CdekSDK\CdekClient;
use Dadata\DadataClient;
use Http\Client\Curl\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Psr\Http\Client\ClientInterface;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Model::preventLazyLoading(!app()->isProduction());

        //DaDataService
        $this->app->singleton(
            DaDataClient::class,
            function () {
                return new DaDataClient(config('external.dadata.api_token'), config('external.dadata.api_secret'));
            }
        );

        //CdekService
        $this->app->singleton(
            CdekService::class,
            function () {
                return app()->isProduction() ? new CdekClient(
                    config('external.cdek.account'),
                    config('external.cdek.secure_password')
                ) : new CdekClient(
                    config('external.cdek.account'),
                    config('external.cdek.secure_password'),
                    new \GuzzleHttp\Client(['base_uri' => 'https://integration.edu.cdek.ru'])
                );
            }
        );
        $this->app->instance(
            ClientInterface::class,
            new Client()
        );
        $this->app->singleton(
            CdekService2::class,
            function () {
                $client = new \CdekSDK2\Client(
                    resolve(ClientInterface::class),
                    config('external.cdek.account'),
                    config('external.cdek.secure_password')
                );
                $client->setTest(!app()->isProduction());
                $client->setAccount(config('external.cdek.account'));
                $client->setSecure(config('external.cdek.secure_password'));

                return $client;
            }
        );

        //ApiShipService
        $this->app->instance(
            \Apiship\Adapter\AdapterInterface::class,
            new \Apiship\Adapter\GuzzleTokenAdapter(
                config('delivery-service.apiship.token'),
                !app()->isProduction()
            )
        );
        $this->app->singleton(
            ApiShipService::class,
            function () {
                /** @var \Apiship\Adapter\AdapterInterface $adapter */
                $adapter = $this->app->make(\Apiship\Adapter\AdapterInterface::class);

                return new ApiShipService($adapter);
            }
        );

        //B2CplService
        $b2cplAdapter = app()->isProduction() ? new \Ensi\B2Cpl\Adapter\GuzzleAdapter(
            config('delivery-service.b2cpl.login'),
            config('delivery-service.b2cpl.key')
        ) : new \Ensi\B2Cpl\Adapter\GuzzleAdapter();
        $this->app->instance(
            \Ensi\B2Cpl\Adapter\AdapterInterface::class,
            $b2cplAdapter
        );
        $this->app->singleton(
            B2CplService::class,
            function () {
                /** @var \Ensi\B2Cpl\Adapter\AdapterInterface $adapter */
                $adapter = $this->app->make(\Ensi\B2Cpl\Adapter\AdapterInterface::class);

                return new B2CplService($adapter);
            }
        );
    }
}
