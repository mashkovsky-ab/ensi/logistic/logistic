<?php

namespace App\Providers;

use Ackintosh\Ganesha;
use Ackintosh\Ganesha\Builder;
use Ackintosh\Ganesha\GuzzleMiddleware;
use Ackintosh\Ganesha\Storage\Adapter\Apcu as ApcuAdapter;
use Ensi\BuClient\BuClientProvider;
use Ensi\GuzzleMultibyte\BodySummarizer;
use Ensi\LaravelInitialEventPropagation\PropagateInitialEventLaravelGuzzleMiddleware;
use Ensi\OmsClient\OmsClientProvider;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Handler\CurlMultiHandler;
use GuzzleHttp\Handler\Proxy;
use GuzzleHttp\Handler\StreamHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use LogicException;

/**
 * Class OpenApiClientsServiceProvider
 * @package App\Providers
 */
class OpenApiClientsServiceProvider extends ServiceProvider
{
    /** @var int */
    private const DEFAULT_TIMEOUT = 30;

    public function register(): void
    {
        $handler = $this->configureHandler();

        $this->registerService(
            handler: $handler,
            domain: 'units',
            serviceName: 'bu',
            configurationClassName: BuClientProvider::$configuration,
            apisClassNames: BuClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'orders',
            serviceName: 'oms',
            configurationClassName: OmsClientProvider::$configuration,
            apisClassNames: OmsClientProvider::$apis
        );
    }

    /**
     * @return HandlerStack
     */
    private function configureHandler(): HandlerStack
    {
        $stack = new HandlerStack($this->chooseGuzzleHandler());

        $stack->push(Middleware::httpErrors(new BodySummarizer()), 'http_errors');
        $stack->push(Middleware::redirect(), 'allow_redirects');
        $stack->push(Middleware::prepareBody(), 'prepare_body');
        if (!config('ganesha.disable_middleware', false)) {
            $stack->push($this->configureGaneshaMiddleware());
        }

        $stack->push(new PropagateInitialEventLaravelGuzzleMiddleware());

        return $stack;
    }

    // Not implemented in guzzle <7
    private static function chooseGuzzleHandler(): callable
    {
        $handler = null;
        if (\function_exists('curl_multi_exec') && \function_exists('curl_exec')) {
            $handler = Proxy::wrapSync(new CurlMultiHandler(), new CurlHandler());
        } elseif (\function_exists('curl_exec')) {
            $handler = new CurlHandler();
        } elseif (\function_exists('curl_multi_exec')) {
            $handler = new CurlMultiHandler();
        }

        if (\ini_get('allow_url_fopen')) {
            $handler = $handler
                ? Proxy::wrapStreaming($handler, new StreamHandler())
                : new StreamHandler();
        } elseif (!$handler) {
            throw new \RuntimeException('GuzzleHttp requires cURL, the allow_url_fopen ini setting, or a custom HTTP handler.');
        }

        return $handler;
    }

    /**
     * @return GuzzleMiddleware
     */
    private function configureGaneshaMiddleware(): GuzzleMiddleware
    {
        $config = config('ganesha');

        $ganesha = Builder::withRateStrategy()
            ->timeWindow($config['time_window'])
            ->failureRateThreshold($config['failure_rate_threshold'])
            ->minimumRequests($config['minimum_requests'])
            ->intervalToHalfOpen($config['interval_to_half_open'])
            ->adapter(new ApcuAdapter())
            ->build();


        $ganesha->subscribe(function ($event, $service, $message) {
            switch ($event) {
                case Ganesha::EVENT_TRIPPED:
                    Log::warning(
                        "Ganesha has tripped! It seems that a failure has occurred in {$service}. {$message}."
                    );

                    break;
                case Ganesha::EVENT_CALMED_DOWN:
                    Log::info(
                        "The failure in {$service} seems to have calmed down :). {$message}."
                    );

                    break;
                case Ganesha::EVENT_STORAGE_ERROR:
                    Log::error($message);

                    break;
            }
        });

        return new GuzzleMiddleware($ganesha);
    }

    /**
     * @param  HandlerStack  $handler
     * @param  string  $domain
     * @param  string  $serviceName
     * @param  string  $configurationClassName
     * @param  array  $apisClassNames
     */
    private function registerService(HandlerStack $handler, string $domain, string $serviceName, string $configurationClassName, array $apisClassNames): void
    {
        $config = config("openapi-clients.$domain.$serviceName");
        if (!$config) {
            throw new LogicException("Config openapi-clients.$domain.$serviceName is not found");
        }

        $baseUri = $config['base_uri'];
        $this->app->bind($this->trimFQCN($configurationClassName), fn () => (new $configurationClassName())->setHost($baseUri));
        foreach ($apisClassNames as $api) {
            $this->app->when($this->trimFQCN($api))
                ->needs(ClientInterface::class)
                ->give(fn () => new Client([
                    'handler' => $handler,
                    'base_uri' => $baseUri,
                    'ganesha.service_name' => $domain . '_' . $serviceName,
                    'timeout' => $config['timeout'] ?? self::DEFAULT_TIMEOUT,
                ]));
        }
    }

    /**
     * @param  string  $name
     * @return string
     */
    private function trimFQCN(string $name): string
    {
        return ltrim($name, '\\');
    }
}
