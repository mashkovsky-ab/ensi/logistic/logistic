CalculateDeliveryRequest:
  type: object
  properties:
    city_guid_to:
      type: string
      description: Id города получателя в базе ФИАС
      example: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
    basket_items:
      type: array
      description: Содержимое корзины для доставки
      minLength: 1
      items:
        $ref: '#/CalculationBasketItem'
    stores:
      type: array
      description: Информация о складах, с которых идет доставка
      items:
        $ref: '#/CalculationStore'
  required:
    - city_guid_to
    - delivery_service
    - basket_items
    - stores

BasketItem:
  type: object
  description: Информация о товаре
  properties:
    offer_id:
      type: integer
      description: ID оффера
      example: 4002
    qty:
      type: integer
      description: Кол-во оффера
      example: 1
    weight:
      type: number
      format: float
      description: Вес (в кг)
      example: 10
    width:
      type: integer
      description: Ширина (в мм)
      example: 10
    height:
      type: integer
      description: Высота (в мм)
      example: 10
    length:
      type: integer
      description: Длина (в мм)
      example: 10
    is_explosive:
      type: boolean
      description: Cодержит взрывоопасные элементы?
      example: true
    store_id:
      type: integer
      description: Id склада продавца
      example: 1
    seller_id:
      type: integer
      description: Id продавца
      example: 1
  required:
    - offer_id
    - qty
    - weight
    - width
    - height
    - length
    - store_id
    - seller_id

CalculationBasketItem:
  allOf:
    - $ref: '#/BasketItem'

CalculationStore:
  type: object
  properties:
    store_id:
      type: integer
      description: Id склада продавца
      example: 1
    city_guid:
      type: string
      description: Id города склада в базе ФИАС
      example: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
    working:
      type: array
      items:
        $ref: '#/CalculationStoreWorking'
    pickup_times:
      type: array
      items:
        $ref: '#/CalculationStorePickupTime'
  required:
    - store_id
    - city_guid

CalculationStoreWorking:
  type: object
  properties:
    day:
      type: integer
      description: День недели (1-7)
      example: 1
    working_start_time:
      type: string
      description: Время начала работы склада
      example: 00:00
    working_end_time:
      type: string
      description: Время окончания работы склада
      example: 23:00
  required:
    - day
    - working_start_time
    - working_end_time

CalculationStorePickupTime:
  type: object
  properties:
    day:
      type: integer
      description: День недели (1-7)
      example: 1
    pickup_time_code:
      type: string
      description: Код времени отгрузки у службы доставки
      example: "11-13"
    pickup_time_start:
      type: string
      description: Время начала отгрузки
      example: 11:00:00
    pickup_time_end:
      type: string
      description: Время окончания отгрузки
      example: 13:00:00
    cargo_export_time:
      type: string
      description: Время выгрузки информации о грузе в службу доставки
      example: 10:00:00
    delivery_service:
      allOf:
        - type: integer
        - $ref: '../../delivery-services/enums/delivery_service_enum.yaml'
  required:
    - day
    - delivery_service

CalculateDeliveryResponse:
  type: object
  properties:
    data:
      type: object
      properties:
        methods:
          type: array
          description: Способы получения товара
          items:
            $ref: '#/CalculationDeliveryMethod'
      required:
        - methods
  required:
    - data

CalculationDeliveryMethod:
  type: object
  properties:
    can_merge:
      type: boolean
      description: Можно ли консолидировать все доставки в одну
      example: true
    delivery_cost:
      type: integer
      description: Стоимость доставки для клиента (в копейках)
      example: 100
    delivery_method_id:
      allOf:
        - type: integer
        - $ref: '../../delivery-services/enums/delivery_method_enum.yaml'
    deliveries:
      type: array
      items:
        $ref: '#/CalculationDelivery'
  required:
    - can_merge
    - delivery_cost
    - delivery_method_id
    - deliveries

CalculationDelivery:
  type: object
  properties:
    delivery_service:
      allOf:
        - type: integer
        - $ref: '../../delivery-services/enums/delivery_service_enum.yaml'
    dt:
      type: integer
      description: Delivery Time - время доставки в днях, которое отдаёт ЛО
      example: 3
    pdd:
      type: string
      format: date
      description: Planned Delivery Date - плановая дата, начиная с которой отправление может быть доставлено клиенту
      example: "2021-01-01"
    shipments:
      type: array
      description: Отправления, которые входят в доставку
      items:
        $ref: '#/CalculationShipment'
    tariffs:
      type: array
      description: Тарифы на доставку
      items:
        $ref: '#/CalculationTariff'
  required:
    - delivery_service
    - dt
    - pdd
    - shipments
    - tariffs

CalculationShipment:
  type: object
  properties:
    seller_id:
      type: integer
      description: Id продавца
      example: 1
    store_id:
      type: integer
      description: Id склада отправления
      example: 2
    weight:
      type: number
      format: float
      description: Вес отправления (в граммах)
      example: 10.1
    width:
      type: integer
      description: Ширина отправления (в см)
      example: 10
    height:
      type: integer
      description: Высота отправления (в см)
      example: 10
    length:
      type: integer
      description: Длина отправления (в см)
      example: 10
    psd:
      type: string
      format: date
      description: Planned Shipment Date - плановая дата и время, когда отправление должно быть собрано (получит статус "Готово к отгрузке")
      example: "2021-01-01"
    items:
      type: array
      description: Товары отправления
      items:
        $ref: '#/CalculationShipmentItem'
  required:
    - seller_id
    - store_id
    - weight
    - width
    - height
    - length
    - psd
    - items

CalculationShipmentItem:
  allOf:
    - $ref: '#/BasketItem'

CalculationTariff:
  type: object
  properties:
    external_id:
      type: integer
      description: Id тарифа в службе доставки
      example: 2
    name:
      type: string
      description: Наименование тарифа
      example: "Доставка"
    delivery_service_cost:
      type: integer
      description: Стоимость доставки от службы доставки (в копейках)
      example: 199
    days_min:
      type: integer
      description: Минимальное количество дней на доставку
    days_max:
      type: integer
      description: Максимальное количество дней на доставку
    available_dates:
      type: array
      description: Доступные даты для доставки
      items:
        $ref: '#/CalculationAvailableDate'
    point_ids:
      type: array
      description: Список Id точек выдачи заказа
      items:
        type: integer
  required:
    - external_id
    - name
    - delivery_service_cost

CalculationAvailableDate:
  type: object
  properties:
    date:
      type: string
      format: date
      description: Дата доставки
      example: "2020-05-27"
    available_times:
      type: array
      description: Доступные интервалы времени доставки
      items:
        $ref: '#/CalculationAvailableTime'
  required:
    - date

CalculationAvailableTime:
  type: object
  properties:
    code:
      type: string
      description: Код интервала доставки
      example: "10-20"
    from:
      type: integer
      description: Начальный час доставки «С»
      example: 10
    to:
      type: integer
      description: Конечный час доставки «ДО»
      example: 20
  required:
    - code
    - from
    - to
