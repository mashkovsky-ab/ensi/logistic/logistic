<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class Logistics
 */
return new class extends Migration {
    /** @var array */
    const FEDERAL_DISTRICTS_WITH_REGIONS = [
        [
            'name' => 'Центральный федеральный округ',
            'regions' => [
                [
                    'name' => 'Город федерального значения Москва',
                    'guid' => '0c5b2444-70a0-4932-980c-b4dc0d3f02b5',
                ],
                [
                    'name' => 'Белгородская область',
                    'guid' => '639efe9d-3fc8-4438-8e70-ec4f2321f2a7',
                ],
                [
                    'name' => 'Брянская область',
                    'guid' => 'f5807226-8be0-4ea8-91fc-39d053aec1e2',
                ],
                [
                    'name' => 'Владимирская область',
                    'guid' => 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                ],
                [
                    'name' => 'Воронежская область',
                    'guid' => 'b756fe6b-bbd3-44d5-9302-5bfcc740f46e',
                ],
                [
                    'name' => 'Ивановская область',
                    'guid' => '0824434f-4098-4467-af72-d4f702fed335',
                ],
                [
                    'name' => 'Калужская область',
                    'guid' => '18133adf-90c2-438e-88c4-62c41656de70',
                ],
                [
                    'name' => 'Костромская область',
                    'guid' => '15784a67-8cea-425b-834a-6afe0e3ed61c',
                ],
                [
                    'name' => 'Курская область',
                    'guid' => 'ee594d5e-30a9-40dc-b9f2-0add1be44ba1',
                ],
                [
                    'name' => 'Липецкая область',
                    'guid' => '1490490e-49c5-421c-9572-5673ba5d80c8',
                ],
                [
                    'name' => 'Московская область',
                    'guid' => '29251dcf-00a1-4e34-98d4-5c47484a36d4',
                ],
                [
                    'name' => 'Орловская область',
                    'guid' => '5e465691-de23-4c4e-9f46-f35a125b5970',
                ],
                [
                    'name' => 'Рязанская область',
                    'guid' => '963073ee-4dfc-48bd-9a70-d2dfc6bd1f31',
                ],
                [
                    'name' => 'Тамбовская область',
                    'guid' => 'a9a71961-9363-44ba-91b5-ddf0463aebc2',
                ],
                [
                    'name' => 'Тверская область',
                    'guid' => '61723327-1c20-42fe-8dfa-402638d9b396',
                ],
                [
                    'name' => 'Ярославская область',
                    'guid' => 'a84b2ef4-db03-474b-b552-6229e801ae9b',
                ],
            ],
        ],
        [
            'name' => 'Приволжский федеральный округ',
            'regions' => [
                [
                    'name' => 'Республика Башкортостан',
                    'guid' => '6f2cbfd8-692a-4ee4-9b16-067210bde3fc',
                ],
                [
                    'name' => 'Республика Марий Эл',
                    'guid' => 'de2cbfdf-9662-44a4-a4a4-8ad237ae4a3e',
                ],
                [
                    'name' => 'Республика Мордовия',
                    'guid' => '37a0c60a-9240-48b5-a87f-0d8c86cdb6e1',
                ],
                [
                    'name' => 'Республика Татарстан',
                    'guid' => '0c089b04-099e-4e0e-955a-6bf1ce525f1a',
                ],
                [
                    'name' => 'Удмуртская Республика',
                    'guid' => '52618b9c-bcbb-47e7-8957-95c63f0b17cc',
                ],
                [
                    'name' => 'Чувашская Республика',
                    'guid' => '878fc621-3708-46c7-a97f-5a13a4176b3e',
                ],
                [
                    'name' => 'Пермский край',
                    'guid' => '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                ],
                [
                    'name' => 'Нижегородская область',
                    'guid' => '88cd27e2-6a8a-4421-9718-719a28a0a088',
                ],
                [
                    'name' => 'Оренбургская область',
                    'guid' => '8bcec9d6-05bc-4e53-b45c-ba0c6f3a5c44',
                ],
                [
                    'name' => 'Пензенская область',
                    'guid' => 'c99e7924-0428-4107-a302-4fd7c0cca3ff',
                ],
                [
                    'name' => 'Самарская область',
                    'guid' => 'df3d7359-afa9-4aaa-8ff9-197e73906b1c',
                ],
                [
                    'name' => 'Саратовская область',
                    'guid' => 'df594e0e-a935-4664-9d26-0bae13f904fe',
                ],
                [
                    'name' => 'Ульяновская область',
                    'guid' => 'fee76045-fe22-43a4-ad58-ad99e903bd58',
                ],
            ],
        ],
        [
            'name' => 'Северо-Западный федеральный округ',
            'regions' => [
                [
                    'name' => 'Город федерального значения Санкт-Петербург',
                    'guid' => 'c2deb16a-0330-4f05-821f-1d09c93331e6',
                ],
                [
                    'name' => 'Республика Карелия',
                    'guid' => '248d8071-06e1-425e-a1cf-d1ff4c4a14a8',
                ],
                [
                    'name' => 'Республика Коми',
                    'guid' => 'c20180d9-ad9c-46d1-9eff-d60bc424592a',
                ],
                [
                    'name' => 'Архангельская область',
                    'guid' => '294277aa-e25d-428c-95ad-46719c4ddb44',
                ],
                [
                    'name' => 'Вологодская область',
                    'guid' => 'ed36085a-b2f5-454f-b9a9-1c9a678ee618',
                ],
                [
                    'name' => 'Калининградская область',
                    'guid' => '90c7181e-724f-41b3-b6c6-bd3ec7ae3f30',
                ],
                [
                    'name' => 'Ленинградская область',
                    'guid' => '6d1ebb35-70c6-4129-bd55-da3969658f5d',
                ],
                [
                    'name' => 'Мурманская область',
                    'guid' => '1c727518-c96a-4f34-9ae6-fd510da3be03',
                ],
                [
                    'name' => 'Новгородская область',
                    'guid' => 'e5a84b81-8ea1-49e3-b3c4-0528651be129',
                ],
                [
                    'name' => 'Псковская область',
                    'guid' => 'f6e148a1-c9d0-4141-a608-93e3bd95e6c4',
                ],
                [
                    'name' => 'Ненецкий автономный округ',
                    'guid' => '89db3198-6803-4106-9463-cbf781eff0b8',
                ],
            ],
        ],
        [
            'name' => 'Уральский федеральный округ',
            'regions' => [
                [
                    'name' => 'Курганская область',
                    'guid' => '4a3d970f-520e-46b9-b16c-50d4ca7535a8',
                ],
                [
                    'name' => 'Свердловская область',
                    'guid' => '92b30014-4d52-4e2e-892d-928142b924bf',
                ],
                [
                    'name' => 'Тюменская область',
                    'guid' => '54049357-326d-4b8f-b224-3c6dc25d6dd3',
                ],
                [
                    'name' => 'Челябинская область',
                    'guid' => '27eb7c10-a234-44da-a59c-8b1f864966de',
                ],
                [
                    'name' => 'Ханты-Мансийский автономный округ — Югра',
                    'guid' => 'd66e5325-3a25-4d29-ba86-4ca351d9704b',
                ],
                [
                    'name' => 'Ямало-Ненецкий автономный округ',
                    'guid' => '826fa834-3ee8-404f-bdbc-13a5221cfb6e',
                ],
            ],
        ],
        [
            'name' => 'Южный федеральный округ',
            'regions' => [
                [
                    'name' => 'Город федерального значения Севастополь',
                    'guid' => '6fdecb78-893a-4e3f-a5ba-aa062459463b',
                ],
                [
                    'name' => 'Республика Адыгея',
                    'guid' => 'd8327a56-80de-4df2-815c-4f6ab1224c50',
                ],
                [
                    'name' => 'Республика Калмыкия',
                    'guid' => '491cde9d-9d76-4591-ab46-ea93c079e686',
                ],
                [
                    'name' => 'Республика Крым',
                    'guid' => 'bd8e6511-e4b9-4841-90de-6bbc231a789e',
                ],
                [
                    'name' => 'Краснодарский край',
                    'guid' => 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                ],
                [
                    'name' => 'Астраханская область',
                    'guid' => '83009239-25cb-4561-af8e-7ee111b1cb73',
                ],
                [
                    'name' => 'Волгоградская область',
                    'guid' => 'da051ec8-da2e-4a66-b542-473b8d221ab4',
                ],
                [
                    'name' => 'Ростовская область',
                    'guid' => 'f10763dc-63e3-48db-83e1-9c566fe3092b',
                ],
            ],
        ],
        [
            'name' => 'Сибирский федеральный округ',
            'regions' => [
                [
                    'name' => 'Республика Алтай',
                    'guid' => '5c48611f-5de6-4771-9695-7e36a4e7529d',
                ],
                [
                    'name' => 'Республика Тыва',
                    'guid' => '026bc56f-3731-48e9-8245-655331f596c0',
                ],
                [
                    'name' => 'Республика Хакасия',
                    'guid' => '8d3f1d35-f0f4-41b5-b5b7-e7cadf3e7bd7',
                ],
                [
                    'name' => 'Алтайский край',
                    'guid' => '8276c6a1-1a86-4f0d-8920-aba34d4cc34a',
                ],
                [
                    'name' => 'Красноярский край',
                    'guid' => 'db9c4f8b-b706-40e2-b2b4-d31b98dcd3d1',
                ],
                [
                    'name' => 'Иркутская область',
                    'guid' => '6466c988-7ce3-45e5-8b97-90ae16cb1249',
                ],
                [
                    'name' => 'Кемеровская область',
                    'guid' => '393aeccb-89ef-4a7e-ae42-08d5cebc2e30',
                ],
                [
                    'name' => 'Новосибирская область',
                    'guid' => '1ac46b49-3209-4814-b7bf-a509ea1aecd9',
                ],
                [
                    'name' => 'Омская область',
                    'guid' => '05426864-466d-41a3-82c4-11e61cdc98ce',
                ],
                [
                    'name' => 'Томская область',
                    'guid' => '889b1f3a-98aa-40fc-9d3d-0f41192758ab',
                ],
            ],
        ],
        [
            'name' => 'Северо-Кавказский федеральный округ',
            'regions' => [
                [
                    'name' => 'Республика Дагестан',
                    'guid' => '0bb7fa19-736d-49cf-ad0e-9774c4dae09b',
                ],
                [
                    'name' => 'Республика Ингушетия',
                    'guid' => 'b2d8cd20-cabc-4deb-afad-f3c4b4d55821',
                ],
                [
                    'name' => 'Кабардино-Балкарская Республика',
                    'guid' => '1781f74e-be4a-4697-9c6b-493057c94818',
                ],
                [
                    'name' => 'Карачаево-Черкесская Республика',
                    'guid' => '61b95807-388a-4cb1-9bee-889f7cf811c8',
                ],
                [
                    'name' => 'Республика Северная Осетия',
                    'guid' => 'de459e9c-2933-4923-83d1-9c64cfd7a817',
                ],
                [
                    'name' => 'Чеченская Республика',
                    'guid' => 'de67dc49-b9ba-48a3-a4cc-c2ebfeca6c5e',
                ],
                [
                    'name' => 'Ставропольский край',
                    'guid' => '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                ],
            ],
        ],
        [
            'name' => 'Дальневосточный федеральный округ',
            'regions' => [
                [
                    'name' => 'Республика Бурятия',
                    'guid' => 'a84ebed3-153d-4ba9-8532-8bdf879e1f5a',
                ],
                [
                    'name' => 'Республика Саха (Якутия)',
                    'guid' => 'c225d3db-1db6-4063-ace0-b3fe9ea3805f',
                ],
                [
                    'name' => 'Забайкальский край',
                    'guid' => 'b6ba5716-eb48-401b-8443-b197c9578734',
                ],
                [
                    'name' => 'Камчатский край',
                    'guid' => 'd02f30fc-83bf-4c0f-ac2b-5729a866a207',
                ],
                [
                    'name' => 'Приморский край',
                    'guid' => '43909681-d6e1-432d-b61f-ddac393cb5da',
                ],
                [
                    'name' => 'Хабаровский край',
                    'guid' => '7d468b39-1afa-41ec-8c4f-97a8603cb3d4',
                ],
                [
                    'name' => 'Амурская область',
                    'guid' => '844a80d6-5e31-4017-b422-4d9c01e9942c',
                ],
                [
                    'name' => 'Магаданская область',
                    'guid' => '9c05e812-8679-4710-b8cb-5e8bd43cdf48',
                ],
                [
                    'name' => 'Сахалинская область',
                    'guid' => 'aea6280f-4648-460f-b8be-c2bc18923191',
                ],
                [
                    'name' => 'Еврейская автономная область',
                    'guid' => '1b507b09-48c9-434f-bf6f-65066211c73e',
                ],
                [
                    'name' => 'Чукотский автономный округ',
                    'guid' => 'f136159b-404a-4f1f-8d8d-d169e1374d5c',
                ],
            ],
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metro_lines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');

            $table->timestamps();
        });

        Schema::create('metro_stations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('metro_line_id')->unsigned();
            $table->string('name');
            $table->string('city_guid');

            $table->timestamps();

            $table->foreign('metro_line_id')->references('id')->on('metro_lines')->onDelete('cascade');
        });

        Schema::create('points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('delivery_service')->unsigned();
            $table->integer('type')->unsigned();
            $table->string('name');
            $table->decimal('lat', 10, 7);
            $table->decimal('lng', 10, 7);
            $table->string('external_id');
            $table->string('apiship_external_id');
            $table->boolean('has_payment_card')->default(false);
            $table->json('address');
            $table->string('city_guid')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('timetable')->nullable();

            $table->timestamps();
        });

        Schema::create('point_metro_station_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('point_id')->unsigned();
            $table->bigInteger('metro_station_id')->unsigned();
            $table->float('distance');

            $table->timestamps();
        });

        Schema::create('delivery_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('delivery_service')->unsigned();
            $table->string('city_guid');
            $table->json('payload');

            $table->timestamps();
        });

        Schema::create('tariffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('delivery_service')->unsigned();
            $table->tinyInteger('delivery_method')->unsigned()->nullable();
            $table->tinyInteger('shipment_method')->unsigned()->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('external_id');
            $table->string('apiship_external_id');
            $table->integer('weight_min')->unsigned()->nullable();
            $table->integer('weight_max')->unsigned()->nullable();

            $table->timestamps();
        });

        Schema::create('federal_districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->timestamps();
        });

        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('federal_district_id')->unsigned();
            $table->string('name');
            $table->string('guid');

            $table->timestamps();

            $table->foreign('federal_district_id')->references('id')->on('federal_districts')->onDelete('cascade');
        });

        Schema::create('delivery_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('federal_district_id')->unsigned();
            $table->integer('region_id')->unsigned()->nullable();
            $table->string('region_guid')->nullable();
            $table->tinyInteger('delivery_service')->unsigned();
            $table->tinyInteger('delivery_method')->unsigned();
            $table->decimal('price', 18, 4);

            $table->timestamps();

            $table->unique(
                ['federal_district_id', 'region_id', 'delivery_service', 'delivery_method'],
                'district_region_ds_dm'
            );

            $table->foreign('federal_district_id')->references('id')->on('federal_districts')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
        });

        $this->upData();
    }

    public function upData(): void
    {
        foreach (self::FEDERAL_DISTRICTS_WITH_REGIONS as $datum) {
            $federalDistrictId = DB::table('federal_districts')->insertGetId([
                'name' => $datum['name'],
                'created_at' => now(),
            ]);

            if ($federalDistrictId && isset($datum['regions'])) {
                foreach ($datum['regions'] as $regionDatum) {
                    DB::table('regions')->insert([
                        'federal_district_id' => $federalDistrictId,
                        'name' => $regionDatum['name'],
                        'guid' => $regionDatum['guid'],
                        'created_at' => now(),
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_prices');
        Schema::dropIfExists('regions');
        Schema::dropIfExists('federal_districts');
        Schema::dropIfExists('tariffs');
        Schema::dropIfExists('delivery_cities');
        Schema::dropIfExists('point_metro_station_links');
        Schema::dropIfExists('points');
        Schema::dropIfExists('metro_stations');
        Schema::dropIfExists('metro_lines');
    }
};
