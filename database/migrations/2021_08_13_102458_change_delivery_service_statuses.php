<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('delivery_services')
            ->where('status', '=', 3)
            ->update(['status' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('delivery_services')
            ->where('status', '=', 2)
            ->update(['status' => 3]);
    }
};
