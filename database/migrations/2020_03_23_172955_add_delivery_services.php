<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('delivery_services')
            ->insert([
                'id' => 1,
                'name' => 'B2Cpl',
                'apiship_key' => 'b2cpl',
                'status' => 1,
                'registered_at' => now(),
                'priority' => 1,
                'do_consolidation' => true,
                'do_deconsolidation' => true,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 2,
                'name' => 'Boxberry',
                'apiship_key' => 'boxberry',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 3,
                'name' => 'СДЭК',
                'apiship_key' => 'cdek',
                'status' => 1,
                'registered_at' => now(),
                'priority' => 2,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 4,
                'name' => 'Dostavista',
                'apiship_key' => 'dostavista',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 5,
                'name' => 'DPD',
                'apiship_key' => 'dpd',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 6,
                'name' => 'IML',
                'apiship_key' => 'iml',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 7,
                'name' => 'MaxiPost',
                'apiship_key' => 'maxi',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 8,
                'name' => 'PickPoint',
                'apiship_key' => 'pickpoint',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 9,
                'name' => 'PONY EXPRESS',
                'apiship_key' => 'pony',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
        DB::table('delivery_services')
            ->insert([
                'id' => 10,
                'name' => 'Почта России',
                'apiship_key' => 'rupost',
                'status' => 3,
                'registered_at' => now(),
                'priority' => 3,
                'do_consolidation' => false,
                'do_deconsolidation' => false,
                'do_express_delivery' => false,
                'created_at' => now(),
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('delivery_services')
            ->whereIn(
                'apiship_key',
                ['b2cpl', 'boxberry', 'cdek', 'dostavista', 'dpd', 'iml', 'maxi', 'pickpoint', 'pony', 'rupost']
            )
            ->delete();
    }
};
