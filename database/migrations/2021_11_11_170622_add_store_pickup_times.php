<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_pickup_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('external_id')->unsigned()->unique();

            $table->bigInteger('store_id')->unsigned();
            $table->tinyInteger('day')->unsigned();
            $table->string('pickup_time_code')->nullable();
            $table->time('pickup_time_start')->nullable();
            $table->time('pickup_time_end')->nullable();
            $table->time('cargo_export_time')->nullable();
            $table->bigInteger('delivery_service_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('delivery_service_id')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_pickup_times');
    }
};
