<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->string('delivery_cost')->nullable(false)->change();
            $table->string('delivery_cost_pay')->nullable(false)->change();
        });
        Schema::table('delivery_order_places', function (Blueprint $table) {
            $table->string('number')->nullable()->change();
            $table->string('barcode')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->integer('delivery_cost')->nullable()->change();
            $table->integer('delivery_cost_pay')->nullable()->change();
        });
        Schema::table('delivery_order_places', function (Blueprint $table) {
            $table->string('number')->nullable(false)->change();
            $table->string('barcode')->nullable(false)->change();
        });
    }
};
