<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_delivery_service_link', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_service')->change();
        });
        Schema::table('city_delivery_service_link', function (Blueprint $table) {
            $table->foreign('delivery_service')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
        });

        Schema::table('delivery_prices', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_service')->change();
        });
        Schema::table('delivery_prices', function (Blueprint $table) {
            $table->foreign('delivery_service')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
        });

        Schema::table('points', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_service')->change();
        });
        Schema::table('points', function (Blueprint $table) {
            $table->foreign('delivery_service')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
        });

        Schema::table('tariffs', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_service')->change();
        });
        Schema::table('tariffs', function (Blueprint $table) {
            $table->foreign('delivery_service')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_delivery_service_link', function (Blueprint $table) {
            $table->dropForeign(['delivery_service']);
        });
        //unsignedTinyInteger не может быть возвращен https://laravel.com/docs/master/migrations#modifying-columns

        Schema::table('delivery_prices', function (Blueprint $table) {
            $table->dropForeign(['delivery_service']);
        });

        Schema::table('points', function (Blueprint $table) {
            $table->dropForeign(['delivery_service']);
        });

        Schema::table('tariffs', function (Blueprint $table) {
            $table->dropForeign(['delivery_service']);
        });
    }
};
