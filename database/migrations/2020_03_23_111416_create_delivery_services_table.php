<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->dateTime('registered_at');

            $table->string('legal_info_company_name')->nullable();
            $table->json('legal_info_company_address')->nullable();
            $table->string('legal_info_inn')->nullable();
            $table->string('legal_info_payment_account')->nullable();
            $table->string('legal_info_bik')->nullable();
            $table->string('legal_info_bank')->nullable();
            $table->string('legal_info_bank_correspondent_account')->nullable();

            $table->string('general_manager_name')->nullable();

            $table->string('contract_number')->nullable();
            $table->date('contract_date')->nullable();

            $table->tinyInteger('vat_rate')->nullable();
            $table->tinyInteger('taxation_type')->nullable();

            $table->unsignedTinyInteger('status')->default(1);

            $table->text('comment')->nullable();
            $table->string('apiship_key')->nullable();

            $table->unsignedInteger('priority')->default(1);
            $table->unsignedInteger('max_shipments_per_day')->nullable();
            $table->time('max_cargo_export_time')->nullable();

            $table->boolean('do_consolidation');
            $table->boolean('do_deconsolidation');
            $table->boolean('do_zero_mile')->default(true);
            $table->boolean('do_express_delivery');
            $table->boolean('do_return')->default(true);
            $table->boolean('do_dangerous_products_delivery')->default(true);

            $table->boolean('add_partial_reject_service')->default(false);
            $table->boolean('add_insurance_service')->default(false);
            $table->boolean('add_fitting_service')->default(false);
            $table->boolean('add_return_service')->default(false);
            $table->boolean('add_open_service')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_services');
    }
};
