<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_order_status_mapping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('delivery_service_id')->unsigned();

            $table->tinyInteger('status', false, true);
            $table->string('external_status');

            $table->timestamps();

            $table->foreign('delivery_service_id')->references('id')->on('delivery_services')->onDelete('cascade');
            $table->unique(['delivery_service_id', 'status', 'external_status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_status_mapping');
    }
};
