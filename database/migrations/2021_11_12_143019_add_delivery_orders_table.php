<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('delivery_id')->unsigned();
            $table->bigInteger('delivery_service_id')->unsigned();

            $table->string('number');
            $table->string('external_id')->nullable();
            $table->tinyInteger('status');
            $table->string('external_status')->nullable();
            $table->integer('height');
            $table->integer('length');
            $table->integer('width');
            $table->integer('weight');
            $table->integer('shipment_method');
            $table->integer('delivery_method');
            $table->integer('tariff_id');
            $table->date('delivery_date')->nullable();
            $table->bigInteger('point_in_id')->unsigned()->nullable();
            $table->bigInteger('point_out_id')->unsigned()->nullable();
            $table->time('shipment_time_start')->nullable();
            $table->time('shipment_time_end')->nullable();
            $table->time('delivery_time_start')->nullable();
            $table->time('delivery_time_end')->nullable();
            $table->string('delivery_time_code')->nullable();
            $table->text('description')->nullable();

            $table->integer('assessed_cost')->nullable();
            $table->integer('delivery_cost')->nullable();
            $table->integer('delivery_cost_vat')->nullable();
            $table->integer('delivery_cost_pay')->nullable();
            $table->integer('cod_cost')->nullable();
            $table->boolean('is_delivery_payed_by_recipient')->default(false);

            $table->boolean('sender_is_seller')->default(true);
            $table->string('sender_inn')->nullable();
            $table->json('sender_address');
            $table->string('sender_company_name')->nullable();
            $table->string('sender_contact_name')->nullable();
            $table->string('sender_email')->nullable();
            $table->string('sender_phone')->nullable();
            $table->string('sender_comment')->nullable();

            $table->json('recipient_address')->nullable();
            $table->string('recipient_company_name')->nullable();
            $table->string('recipient_contact_name')->nullable();
            $table->string('recipient_email')->nullable();
            $table->string('recipient_phone')->nullable();
            $table->string('recipient_comment')->nullable();

            $table->foreign('delivery_service_id')->references('id')->on('delivery_services');
            $table->foreign('tariff_id')->references('id')->on('tariffs');
            $table->foreign('point_in_id')->references('id')->on('points');
            $table->foreign('point_out_id')->references('id')->on('points');

            $table->unique(['delivery_service_id', 'external_id']);
        });

        Schema::create('delivery_order_places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('delivery_order_id');

            $table->string('number');
            $table->string('barcode');
            $table->integer('height');
            $table->integer('length');
            $table->integer('width');
            $table->integer('weight');

            $table->foreign('delivery_order_id')
                ->references('id')
                ->on('delivery_orders')
                ->onDelete('cascade');

            $table->unique(['delivery_order_id', 'number']);
        });

        Schema::create('delivery_order_place_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('delivery_order_place_id');

            $table->string('vendor_code')->nullable();
            $table->string('barcode')->nullable();
            $table->string('name');
            $table->decimal('qty', 18, 4);
            $table->decimal('qty_delivered', 18, 4)->nullable();
            $table->integer('height');
            $table->integer('length');
            $table->integer('width');
            $table->integer('weight');
            $table->integer('assessed_cost');
            $table->decimal('cost', 18, 4);
            $table->integer('cost_vat');
            $table->decimal('price', 18, 4);

            $table->foreign('delivery_order_place_id')
                ->references('id')
                ->on('delivery_order_places')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_place_items');
        Schema::dropIfExists('delivery_order_places');
        Schema::dropIfExists('delivery_orders');
    }
};
