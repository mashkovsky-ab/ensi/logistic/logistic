<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            $table->bigInteger('store_id')->unsigned();

            $table->tinyInteger('status', false, true)->default(1);
            $table->dateTime('status_at')->nullable();
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->boolean('is_problem')->default(false);
            $table->dateTime('is_problem_at')->nullable();
            $table->text('shipping_problem_comment')->nullable();

            $table->bigInteger('delivery_service_id')->unsigned();
            $table->string('cdek_intake_number')->nullable();
            $table->string('xml_id')->nullable();
            $table->text('error_xml_id')->nullable();

            $table->decimal('width', 18, 4);
            $table->decimal('height', 18, 4);
            $table->decimal('length', 18, 4);
            $table->decimal('weight', 18, 4);

            $table->timestamps();

            $table->foreign('delivery_service_id')->references('id')->on('delivery_services')->onDelete('cascade');
        });

        Schema::create('cargo_shipment_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cargo_id')->unsigned();
            $table->bigInteger('shipment_id')->unsigned();

            $table->timestamps();

            $table->unique(['cargo_id', 'shipment_id']);

            $table->foreign('cargo_id')->references('id')->on('cargo')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo_shipment_links');
        Schema::dropIfExists('cargo');
    }
};
