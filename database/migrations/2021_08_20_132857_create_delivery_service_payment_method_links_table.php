<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_service_payment_method_links', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('delivery_service_id');
            $table->unsignedTinyInteger('payment_method');

            $table->timestamps();

            $table->foreign('delivery_service_id')
                ->references('id')
                ->on('delivery_services')
                ->onDelete('cascade');
            $table->unique(['delivery_service_id', 'payment_method']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_service_payment_method_links');
    }
};
