<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_metro_station_links', function (Blueprint $table) {
            $table->foreign('metro_station_id')
                ->references('id')
                ->on('metro_stations')->onDelete('cascade');

            $table->foreign('point_id')
                ->references('id')
                ->on('points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_metro_station_links', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
            $table->dropForeign(['point_id']);
        });
    }
};
