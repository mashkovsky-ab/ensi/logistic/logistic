<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_kpi_ct', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::table('delivery_kpi_ppt', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
