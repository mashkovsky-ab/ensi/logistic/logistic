<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cargo_id');

            $table->string('timeslot_id');
            $table->timestamp('timeslot_from', 6);
            $table->timestamp('timeslot_to', 6);

            $table->string('cdek_intake_number')->nullable();
            $table->string('external_id')->nullable();
            $table->text('error_external_id')->nullable();

            $table->unsignedTinyInteger('status');
            $table->timestamp('date', 6);
            $table->timestamps();

            $table->foreign('cargo_id')->references('id')->on('cargo')->onDelete('cascade');
        });

        Schema::table('cargo', function (Blueprint $table) {
            $table->dropColumn('cdek_intake_number');
            $table->dropColumn('xml_id');
            $table->dropColumn('error_xml_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo_orders');

        Schema::table('cargo', function (Blueprint $table) {
            $table->string('cdek_intake_number')->nullable();
            $table->string('xml_id')->nullable();
            $table->text('error_xml_id')->nullable();
        });
    }
};
