<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table delivery_orders alter column delivery_cost type integer using delivery_cost::integer');
        DB::statement('alter table delivery_orders alter column delivery_cost_pay type integer using delivery_cost_pay::integer');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->string('delivery_cost')->nullable(false)->change();
            $table->string('delivery_cost_pay')->nullable(false)->change();
        });
    }
};
