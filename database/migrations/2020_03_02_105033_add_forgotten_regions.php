<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /** @var array */
    const FEDERAL_DISTRICTS_WITH_REGIONS = [
        [
            'name' => 'Центральный федеральный округ',
            'regions' => [
                [
                    'name' => 'Смоленская область',
                    'guid' => 'e8502180-6d08-431b-83ea-c7038f0df905',
                ],
                [
                    'name' => 'Тульская область',
                    'guid' => 'd028ec4f-f6da-4843-ada6-b68b3e0efa3d',
                ],
            ],
        ],
        [
            'name' => 'Приволжский федеральный округ',
            'regions' => [
                [
                    'name' => 'Кировская область',
                    'guid' => '0b940b96-103f-4248-850c-26b6c7296728',
                ],
            ],
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::FEDERAL_DISTRICTS_WITH_REGIONS as $datum) {
            $federalDistrict = DB::table('federal_districts')->where('name', $datum['name'])->first();

            if ($federalDistrict && isset($datum['regions'])) {
                foreach ($datum['regions'] as $regionDatum) {
                    DB::table('regions')->insert([
                        'federal_district_id' => $federalDistrict->id,
                        'name' => $regionDatum['name'],
                        'guid' => $regionDatum['guid'],
                        'created_at' => now(),
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::FEDERAL_DISTRICTS_WITH_REGIONS as $datum) {
            if (isset($datum['regions'])) {
                foreach ($datum['regions'] as $regionDatum) {
                    DB::table('regions')->where('guid', $regionDatum['guid'])->delete();
                }
            }
        }
    }
};
