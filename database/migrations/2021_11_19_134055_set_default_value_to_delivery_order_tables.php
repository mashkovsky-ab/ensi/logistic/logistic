<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_order_places', function (Blueprint $table) {
            $table->integer('height')->default(0)->change();
            $table->integer('length')->default(0)->change();
            $table->integer('width')->default(0)->change();
            $table->integer('weight')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_order_places', function (Blueprint $table) {
            $table->integer('height')->default(null)->change();
            $table->integer('length')->default(null)->change();
            $table->integer('width')->default(null)->change();
            $table->integer('weight')->default(null)->change();
        });
    }
};
