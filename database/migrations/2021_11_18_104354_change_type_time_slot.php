<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cargo_orders', function (Blueprint $table) {
            $table->time('timeslot_from')->change();
            $table->time('timeslot_to')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cargo_orders', function (Blueprint $table) {
            $table->timestamp('timeslot_from', 6)->change();
            $table->timestamp('timeslot_to', 6)->change();
        });
    }
};
