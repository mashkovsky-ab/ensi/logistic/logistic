<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        DB::table('delivery_service_documents')->update(['file' => null]);
        Schema::table('delivery_service_documents', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
        });
    }

    public function down()
    {
        DB::table('delivery_service_documents')->update(['file' => null]);
        DB::statement('alter table delivery_service_documents alter column file type jsonb using file::jsonb');
    }
};
