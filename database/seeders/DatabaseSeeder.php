<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DeliveryPricesSeeder::class);
        $this->call(DeliveryKpisSeeder::class);
        $this->call(DeliveryOrderStatusMappingSeeder::class);
    }
}
