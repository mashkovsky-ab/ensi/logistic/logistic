<?php

namespace Database\Seeders;

use App\Domain\DeliveryPrices\Models\DeliveryPrice;
use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\Geos\Models\FederalDistrict;
use Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DeliveryPricesSeeder extends Seeder
{
    /** @var int */
    const FAKER_SEED = 123456;

    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table((new DeliveryPrice())->getTable())->truncate();

        $faker = Faker\Factory::create('ru_RU');
        $faker->seed(self::FAKER_SEED);

        /** @var Collection|FederalDistrict[] $federalDistricts */
        $federalDistricts = FederalDistrict::query()->with('regions')->get();

        $prices = [];
        foreach ($federalDistricts as $federalDistrict) {
            foreach (DeliveryService::all() as $deliveryService) {
                foreach (DeliveryMethod::all() as $deliveryMethod) {
                    //Создаем цену для федерального округа
                    $deliveryPrice = new DeliveryPrice();
                    $deliveryPrice->federal_district_id = $federalDistrict->id;
                    $deliveryPrice->delivery_service = $deliveryService->id;
                    $deliveryPrice->delivery_method = $deliveryMethod->id;
                    $deliveryPrice->price = $deliveryMethod->id == DeliveryMethod::METHOD_PICKUP ?
                        (isset($prices[join('-', [$federalDistrict->id, $deliveryService->id, DeliveryMethod::METHOD_DELIVERY])]) ?
                            (int)($prices[join('-', [$federalDistrict->id, $deliveryService->id, DeliveryMethod::METHOD_DELIVERY])] / 2) :
                            $faker->randomFloat(0, 100, 250)) : $faker->randomFloat(0, 100, 500);
                    $deliveryPrice->save();
                    $prices[join('-', [$federalDistrict->id, $deliveryService->id, $deliveryMethod->id])] =
                        $deliveryPrice->price;

                    //Уточняем цены для регионов
                    foreach ($federalDistrict->regions as $region) {
                        $deliveryPrice = new DeliveryPrice();
                        $deliveryPrice->federal_district_id = $federalDistrict->id;
                        $deliveryPrice->region_id = $region->id;
                        $deliveryPrice->region_guid = $region->guid;
                        $deliveryPrice->delivery_service = $deliveryService->id;
                        $deliveryPrice->delivery_method = $deliveryMethod->id;
                        $deliveryPrice->price = DeliveryMethod::METHOD_PICKUP ?
                            (isset($prices[join('-', [$federalDistrict->id, $deliveryService->id, DeliveryMethod::METHOD_DELIVERY, $deliveryPrice->region_id])]) ?
                                (int)($prices[join('-', [$federalDistrict->id, $deliveryService->id, DeliveryMethod::METHOD_DELIVERY, $deliveryPrice->region_id])] / 2) :
                                $faker->randomFloat(0, 100, 250)) : $faker->randomFloat(0, 100, 500);
                        $deliveryPrice->save();
                        $prices[join('-', [$federalDistrict->id, $deliveryService->id, $deliveryMethod->id, $deliveryPrice->region_id])] =
                            $deliveryPrice->price;
                    }
                }
            }
        }
    }
}
