<?php

namespace Database\Seeders;

use App\Domain\DeliveryKpis\Models\DeliveryKpi;
use Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryKpisSeeder extends Seeder
{
    /** @var int */
    const FAKER_SEED = 123456;

    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table((new DeliveryKpi())->getTable())->truncate();

        $faker = Faker\Factory::create('ru_RU');
        $faker->seed(self::FAKER_SEED);


        $deliveryKpi = new DeliveryKpi();
        $deliveryKpi->rtg = $faker->randomFloat(0, 30, 60);
        $deliveryKpi->ct = $faker->randomFloat(0, 30, 60);
        $deliveryKpi->ppt = $faker->randomFloat(0, 100, 120);
        $deliveryKpi->save();
    }
}
