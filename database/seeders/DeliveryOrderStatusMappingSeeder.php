<?php

namespace Database\Seeders;

use App\Domain\DeliveryOrders\Actions\DeliveryOrderStatusMapping\CreateDeliveryOrderStatusMappingAction;
use App\Domain\DeliveryOrders\Enums\DeliveryOrderStatus;
use App\Domain\DeliveryOrders\Models\DeliveryOrderStatusMapping;
use App\Domain\DeliveryServices\Models\DeliveryService;
use App\Domain\External\B2Cpl\Lists\B2CplDeliveryOrderStatus;
use App\Domain\External\Cdek\Lists\CdekDeliveryOrderStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryOrderStatusMappingSeeder extends Seeder
{
    protected const DATA = [
        //B2CPL
        [
            'delivery_service_id' => DeliveryService::SERVICE_B2CPL,
            'status' => DeliveryOrderStatus::NEW,
            'external_status' => [
                B2CplDeliveryOrderStatus::CREATED,
                B2CplDeliveryOrderStatus::ASSEMBLING,
                B2CplDeliveryOrderStatus::AWAITING_SHIPMENT,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_B2CPL,
            'status' => DeliveryOrderStatus::SHIPPED,
            'external_status' => [
                B2CplDeliveryOrderStatus::ON_WAY,
                B2CplDeliveryOrderStatus::RING_UP,
                B2CplDeliveryOrderStatus::DELIVERING,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_B2CPL,
            'status' => DeliveryOrderStatus::READY_FOR_RECIPIENT,
            'external_status' => [
                B2CplDeliveryOrderStatus::READY_FOR_RECIPIENT,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_B2CPL,
            'status' => DeliveryOrderStatus::DONE,
            'external_status' => [
                B2CplDeliveryOrderStatus::DONE,
                B2CplDeliveryOrderStatus::PARTIALLY_DONE,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_B2CPL,
            'status' => DeliveryOrderStatus::CANCELED,
            'external_status' => [
                B2CplDeliveryOrderStatus::PROBLEM,
                B2CplDeliveryOrderStatus::CANCEL,
                B2CplDeliveryOrderStatus::LOST,
            ],
        ],
        //CDEK
        [
            'delivery_service_id' => DeliveryService::SERVICE_CDEK,
            'status' => DeliveryOrderStatus::NEW,
            'external_status' => [
                CdekDeliveryOrderStatus::CREATED,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_CDEK,
            'status' => DeliveryOrderStatus::SHIPPED,
            'external_status' => [
                CdekDeliveryOrderStatus::ON_POINT_IN,
                CdekDeliveryOrderStatus::READY_FOR_DELIVERY,
                CdekDeliveryOrderStatus::RETURN_FOR_DELIVERY,
                CdekDeliveryOrderStatus::ON_CARRIER_AT_DEPARTURE_CITY,
                CdekDeliveryOrderStatus::DELIVERING_TO_TRANSIT_CITY,
                CdekDeliveryOrderStatus::MET_IN_TRANSIT_CITY,
                CdekDeliveryOrderStatus::TAKE_ON_TRANSIT_STORE,
                CdekDeliveryOrderStatus::RETURNED_TO_TRANSIT_STORE,
                CdekDeliveryOrderStatus::TO_CARRIER_AT_TRANSIT_CITY,
                CdekDeliveryOrderStatus::ON_CARRIER_AT_TRANSIT_CITY,
                CdekDeliveryOrderStatus::DELIVERING_TO_DESTINATION_CITY,
                CdekDeliveryOrderStatus::MET_IN_DESTINATION_CITY,
                CdekDeliveryOrderStatus::ON_POINT_OUT,
                CdekDeliveryOrderStatus::DELIVERING,
                CdekDeliveryOrderStatus::RETURNED_FROM_DELIVERY,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_CDEK,
            'status' => DeliveryOrderStatus::READY_FOR_RECIPIENT,
            'external_status' => [
                CdekDeliveryOrderStatus::READY_FOR_RECIPIENT,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_CDEK,
            'status' => DeliveryOrderStatus::DONE,
            'external_status' => [
                CdekDeliveryOrderStatus::DONE,
            ],
        ],
        [
            'delivery_service_id' => DeliveryService::SERVICE_CDEK,
            'status' => DeliveryOrderStatus::CANCELED,
            'external_status' => [
                CdekDeliveryOrderStatus::CANCEL,
                CdekDeliveryOrderStatus::UNDONE,
            ],
        ],
    ];

    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run(CreateDeliveryOrderStatusMappingAction $action)
    {
        DB::table((new DeliveryOrderStatusMapping())->getTable())->truncate();

        foreach (static::DATA as $datum) {
            foreach ($datum['external_status'] as $externalStatus) {
                $fields = $datum;
                $fields['external_status'] = $externalStatus;
                $action->execute($fields);
            }
        }
    }
}
