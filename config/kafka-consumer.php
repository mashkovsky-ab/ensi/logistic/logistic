<?php

use App\Domain\Kafka\Actions\Listen\DeliveryListenAction;
use App\Domain\Kafka\Actions\Listen\StorePickupTimeListenAction;

return [
    'processors' => [
        [
            'topic' => topic("units.fact.store-pickup-times.1"),
            'consumer' => 'default',
            'type' => 'action',
            'class' => StorePickupTimeListenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic("orders.fact.deliveries.1"),
            'consumer' => 'default',
            'type' => 'action',
            'class' => DeliveryListenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
    ],
];
