<?php

return [
    'apiship' => [
        'token' => env('APISHIP_TOKEN', ''),
    ],
    
    'b2cpl' => [
        'login' => env('B2CPL_LOGIN', ''),
        'key' => env('B2CPL_KEY', ''),
    ],
];
