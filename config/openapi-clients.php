<?php

return [
    'units' => [
        'bu' => [
            'base_uri' => env('BU_SERVICE_HOST') . "/api/v1",
        ]
    ],
    'orders' => [
        'oms' => [
            'base_uri' => env('OMS_SERVICE_HOST') . "/api/v1",
        ]
    ],
];
