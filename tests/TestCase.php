<?php

namespace Tests;

use App\Domain\DeliveryServices\Enums\DeliveryMethod;
use App\Domain\DeliveryServices\Enums\DeliveryServiceEnum;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\Seller;
use Ensi\BuClient\Dto\SellerResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StoreFillablePropertiesAddress;
use Ensi\BuClient\Dto\StoreResponse;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\OrderResponse;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\Shipment;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;
use Ensi\OmsClient\Dto\Timeslot;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Mockery\MockInterface;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function mockOrdersOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class)->allows([
            'getOrder' => new OrderResponse([
                'data' => new Order([
                    'id' => 1,
                    'number' => '1000000',
                    'customer_email' => 'customer@mail.ru',
                    'cost' => 100,
                    'price' => 90,
                    'payment_status' => PaymentStatusEnum::HOLD,
                    'payment_status_at' => now()->toIso8601String(),
                    'payed_at' => now()->toIso8601String(),
                    'payment_method' => PaymentMethodEnum::ONLINE,
                    'receiver_name' => 'Иванов Иван Иванович',
                    'receiver_phone' => '+79031234567',
                    'receiver_email' => 'receiver@mail.ru',
                    'delivery_service' => DeliveryServiceEnum::B2CPL()->value,
                    'delivery_method' => DeliveryMethod::METHOD_PICKUP,
                    'delivery_cost' => 50,
                    'delivery_price' => 40,
                    'delivery_tariff_id' => 1,
                    'delivery_point_id' => 1,
                    'delivery_address' => null,
                    'deliveries' => [new Delivery([
                        'id' => 1,
                        'order_id' => 1,
                        'number' => '1000000-1',
                        'status_at' => now()->toIso8601String(),
                        'cost' => 100,
                        'width' => 10,
                        'height' => 20,
                        'length' => 30,
                        'weight' => 100,
                        'date' => '2021-10-15',
                        'timeslot' => new Timeslot([
                            'id' => 1,
                            'from' => '12:00',
                            'to' => '14:00',
                        ]),
                        'status' => DeliveryStatusEnum::ASSEMBLED,
                        'shipments' => [
                            new Shipment([
                                'id' => 1,
                                'number' => '1000000-1-1',
                                'delivery_id' => 2,
                                'seller_id' => 1,
                                'store_id' => 1,
                                'cost' => 100,
                                'width' => 10,
                                'height' => 20,
                                'length' => 30,
                                'weight' => 100,
                                'status' => ShipmentStatusEnum::ASSEMBLED,
                                'order_items' => [
                                    new OrderItem([
                                        'id' => 4,
                                        'order_id' => 1,
                                        'shipment_id' => 2,
                                        'offer_id' => 1,
                                        'name' => 'Шапка',
                                        'qty' => 1,
                                        'price' => 90,
                                        'price_per_one' => 90,
                                        'cost' => 100,
                                        'cost_per_one' => 100,
                                        'product_weight' => 100,
                                        'product_weight_gross' => 100,
                                        'product_width' => 10,
                                        'product_height' => 20,
                                        'product_length' => 30,
                                        'offer_external_id' => '1-1',
                                        'offer_storage_address' => '1-1',
                                        'product_storage_area' => '1-1',
                                        'product_barcode' => '123456789',
                                    ]),
                                ],
                            ]),
                        ]
                    ])],
                ])
            ])
        ]);
    }

    protected function mockBuSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class)->allows([
            'getSeller' => new SellerResponse([
                'data' => new Seller([
                    'id' => 1,
                    'legal_name' => 'ООО Продавец',
                    'inn' => '1234567891',
                    'kpp' => '1234567891',
                    'legal_address' => 'Москва',
                    'fact_address' => 'Москва',
                    'payment_account' => '11111111111',
                    'bank' => 'СберБанк',
                    'bank_address' => 'Москва',
                    'bank_bik' => '2222222222',
                ])
            ])
        ]);
    }

    protected function mockBuStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class)->allows([
            'getStore' => new StoreResponse([
                'data' => new Store([
                    'id' => 'id',
                    'seller_id' => 1,
                    'active' => 1,
                    'name' => 'Склад',
                    'address' => new StoreFillablePropertiesAddress([
                        'address_string' => 'г. Москва, г. Зеленоград, корпус 322а',
                        'country_code' => 'RU',
                        'post_index' => '124482',
                        'region' => 'Москва',
                        'region_guid' => '0c5b2444-70a0-4932-980c-b4dc0d3f02b5',
                        'area' => '',
                        'area_guid' => '',
                        'city' => 'г. Зеленоград',
                        'city_guid' => 'ec44c0ee-bf24-41c8-9e1c-76136ab05cbf',
                        'street' => 'Яблоневая аллея',
                        'house' => '322а',
                        'block' => '',
                        'flat' => '',
                        'porch' => '',
                        'floor' => '2',
                        'intercom' => '',
                        'comment' => '',
                        'geo_lat' => '55.996732',
                        'geo_lon' => '37.214714'
                    ]),
                    'contacts' => [
                        new StoreContact([
                            'id' => 1,
                            'store_id' => 1,
                            'name' => 'Петров Петр Петрович',
                            'phone' => '+79161234567',
                            'email' => 'store-contact@mail.ru'
                        ]),
                    ],
                ])
            ])
        ]);
    }
}
